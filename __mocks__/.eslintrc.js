module.exports = {
    rules: {
        // We don't care about this rule in mock directory
        'node/no-unpublished-import': 'off',
    },
};
