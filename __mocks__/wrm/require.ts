// eslint-disable-next-line node/no-unpublished-import
import { Deferred } from 'jquery';

export default jest.fn(() => Deferred().resolve().promise());
