import * as React from 'react';
import { useEffect } from 'react';

// Transition
interface TransitionProps {
    children?: null;
}

const FakeTransition: React.FC<TransitionProps> = ({ children }) => children || null;
export const ModalTransition = FakeTransition;

// Modal parts
const FakeModalHeader: React.FC = ({ children }) => <header>{children}</header>;
const FakeModalTitle: React.FC = ({ children }) => <h1>{children}</h1>;
const FakeModalBody: React.FC = ({ children }) => <div>{children}</div>;
const FakeModalFooter: React.FC = ({ children }) => <div>{children}</div>;

export const ModalHeader = FakeModalHeader;
export const ModalTitle = FakeModalTitle;
export const ModalBody = FakeModalBody;
export const ModalFooter = FakeModalFooter;

// Modal
interface ModalDialogProps {
    onClose?: () => void;
}

/* eslint-disable jsx-a11y/no-redundant-roles, jsx-a11y/role-has-required-aria-props */
const ModalDialog: React.FC<ModalDialogProps> = ({ children, onClose }) => {
    useEffect(() => {
        const keyHandler = (event: KeyboardEvent) => {
            if (event.keyCode === 27 && onClose) {
                onClose();
            }
        };

        document.addEventListener('keydown', keyHandler);

        return () => {
            document.removeEventListener('keydown', keyHandler);
        };
    }, [onClose]);

    return <div role="dialog">{children}</div>;
};
/* eslint-enable jsx-a11y/no-redundant-roles, jsx-a11y/role-has-required-aria-props */

export default ModalDialog;
