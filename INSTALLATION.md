# Installing Client-side Extensions in your product

First, you need to install all the required artifacts and packages:

## Required maven artifacts

### `atlassian-plugins-webresource`

Provides ability to push CSS, JS, and other non-HTML assets in to the browser.

-   Version: `>= 4.0.0`
-   Status: `stable and public`
-   Code: [atlassian/atlassian-plugins-webresource](https://bitbucket.org/atlassian/atlassian-plugins-webresource)

Refer to the documentation of the project on how to install the required artifacts.

### `atlassian-plugins-webfragment`

Provides ability to render 3rd-party HTML fragments inside 1st-party product views.

-   Version: `>= 5.0.0`
-   Status: `stable and public`
-   Code: [atlassian/atlassian-plugins-webfragment](https://bitbucket.org/atlassian/atlassian-plugins-webfragment/src/master/)

Refer to the documentation of the project on how to install the required artifacts.

### `atlassian-clientside-extensions-runtime`

Provides the browser runtime required by the Client-side extensions.

-   Version: `>=2.0.0`
-   Status: `stable and public`
-   Code: [atlassian/atlassian-clientside-extensions](https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/)
-   Artifact: [com/atlassian/plugins/atlassian-clientside-extensions-runtime/](https://packages.atlassian.com/webapp/#/artifacts/browse/tree/General/maven-private-local/com/atlassian/plugins/atlassian-clientside-extensions-runtime)

Refer to the `Installation Process` section on how to install the required artifacts.

## Required Node packages

### `@atlassian/clientside-extensions-components`

Provides React components for registering extension-points in JS-rendered views.

-   Version: `>=2.0.0`
-   Status: `stable and public`
-   Code: [atlassian/atlassian-clientside-extensions](https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/)
-   Package: [@atlassian/clientside-extensions-components](https://npmjs.com/package/@atlassian/clientside-extensions-components)

### `@atlassian/clientside-extensions-schema`

Provides tooling to generate validation and react components from schema definitions.

-   Version: `>=2.0.0`
-   Status: `stable and public`
-   Code: [atlassian/atlassian-clientside-extensions](https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/)
-   Package: [@atlassian/clientside-extensions-schema](https://npmjs.com/package/@atlassian/clientside-extensions-schema)

## Installation Process

### 1. Bundle the new platform components

Add all the maven projects listed above to your product’s webapp. Depending on how your product is set up, you will either want to:

-   Add them as a direct dependency of your webapp module, or
-   If your product has a “bundled plugins” maven project, add them as a dependency to that.

```xml
<project>
    <dependencies>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-clientside-extensions-runtime</artifactId>
            <version>${atlassian.clientside.extensions.version}</version>
        </dependency>
    </dependencies>

    <properties>
        <atlassian.clientside.extensions.version>1.1.0</atlassian.clientside.extensions.version>
    </properties>
</project>
```

#### Upgrading to webfragments v5

Follow the [upgrade guides](https://bitbucket.org/atlassian/atlassian-plugins-webfragment/src/master/) available in the Webfragments repo.

Ensure your product provides an implementation of `DynamicWebInterfaceManager`.

Ensure you provide an implementation of `WebItemModuleDescriptor` that can resolve the new `getEntryPoint` method.

#### Upgrading to webresources v4

Follow the [upgrade guides](https://bitbucket.org/atlassian/atlassian-plugins-webresource/) available in the WRM repo.

#### Bundle `atlassian-plugin-point-safety`

This is required to make the REST endpoint of our runtime work.

Example of required artifacts added in the `pom.xml` file:

```xml
<project>
    <dependencies>
        <dependency>
            <groupId>com.atlassian.ozymandias</groupId>
            <artifactId>atlassian-plugin-point-safety</artifactId>
            <version>${atlassian.ozymandias.version}</version>
        </dependency>
    </dependencies>

    <properties>
        <atlassian.ozymandias.version>1.0.0</atlassian.ozymandias.version>
    </properties>
</project>
```

### 2. Bundle the new runtime to the product

You need to ensure the Client-side Extensions registry is loaded synchronously along with the product’s other runtime code. This likely means adding the web-resource key of registry resource to your product’s `superbatch`.

The full web-resource key for the registry is:

```
com.atlassian.plugins.atlassian-clientside-extensions-runtime:runtime
```

Depending on the product the resources included in the `superbatch` might be configured by either:

-   a Java class that implements the `ResourceBatchingConfiguration` interface, or
-   a Java class that extends the `DefaultResourceBatchingConfiguration` class, or
-   a `web-resouce` configuration located in on the product's `atlassian-plugin.xml` files, that is added to `superbatch` by the Java class mentioned above

### 3. Add `@atlassian/clientside-extensions-registry` as a provided dependency

The React `@atlassian/clientside-extensions-components` package will pull in a redundant copy of the runtime.
This can be deduplicated via webpack and [`atlassian-webresource-webpack-plugin`] plugin by adding a `providedDependency` option with the runtime module:

```js
new WRMPlugin({
    providedDependencies: {
        '@atlassian/clientside-extensions-registry': {
            dependency: 'com.atlassian.plugins.atlassian-clientside-extensions-runtime:runtime',
            import: {
                var: "require('@atlassian/clientside-extensions-registry')",
                amd: '@atlassian/clientside-extensions-registry',
            },
        },
    },
});
```

### 4. Add the schema-loader for extension-schema-definitions to your webpack config

As of `v2.0` of the Client-Side Extensions, new extension points are created by simply importing the schema-definitions of an extension point.
This both makes the developer experience simpler and enforces to write proper extension-point schemas. To be able to load these schemas we made a webpack-loader available.
The webpack loader will generate all possible React hooks and React components for the extension-point during build-time, including a validator-function that will ensure registerd extensions comply with the schema of an extension point.

```js
{
    //…
    module: {
        rules: [
            //…
            ,{
                test: /\.extensions.graphql$/,
                loader: '@atlassian/clientside-extensions-schema/loader',
            }
        ],
    }
    //…
}
```

> Notice that `test: /\.extensions.graphql$/,` can be replaced with whatever you want to call your extension-schema files.
> Though it is recommended to keep the `.graphql` extensions to get syntax highlighting and code-completion.

### 5. _(Optional)_ - Add a yarn/npm postinstall step to generate base types for extensions and extension-point definitions

While this step is _optional_ it is highly recommended, for the best possible developer experience.
These two scripts simply generate both a _base_ graphql file containing all types provided by the schema package, as well as a `.d.ts` file
for imported schema files. This is needed to make your editor of choice understand that the imported `.graphql` file will actually export typed methods.

```json
{
    "scripts": {
        "postinstall": "npm run generate:graphql-definitions && npm run generate:module-definitions",
        "generate:graphql-definitions": "generate-graphql-definitions ./.cse-graphql-definitions.graphql",
        "generate:module-definitions": "generate-module-definitions-file ./.cse-graphql-module.d.ts"
    }
}
```

> The binaries `generate-module-definitions-file` and `generate-graphql-definitions` are provided by the `@atlassian/clientside-extensions-schema` package

### 6. Verify the runtime works in the browser

After you install and configure the required modules, you should restart the product. Once the product is restarted, navigate to any page in the browser and open the browser developers console.

To verify if the registry module was loaded, enter and execute the code in the console:

```js
require('@atlassian/clientside-extensions-registry');
```

The console should output the module with an object similar to this one:

```
{locations: {…}, plugins: Array(0), initializedLocations: {…}}
initializedLocations: {}
locations: {}
plugins: []
```

[`atlassian-webresource-webpack-plugin`]: https://www.npmjs.com/package/atlassian-webresource-webpack-plugin
