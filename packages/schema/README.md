# Client-side Extensions

Refer to the [official documentation](https://developer.atlassian.com/server/framework/clientside-extensions/guides/how-to/setup-schema-loader/) to learn how to use the CSE schema-loader
