module.exports = {
    env: {
        jest: true,
    },

    rules: {
        // Enable importing devDependencies within the testing fixtures
        'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    },
};
