import type { Configuration } from 'webpack';

const path = require('path');

const OUTPUT_DIR = path.join(__dirname, '../dist');

const schemaLoader = path.resolve(__dirname, '../../../loader.js');

module.exports = {
    devtool: false,
    mode: 'development',
    entry: {},

    resolve: {
        alias: {
            'wrm/require': path.resolve(__dirname, 'wrm-require-mock.ts'),
            'wrm/context-path': path.resolve(__dirname, 'wrm-context-path-mock.ts'),
        },
    },
    module: {
        rules: [
            {
                test: /\.cse\.graphql$/,
                use: [
                    {
                        loader: schemaLoader,
                    },
                ],
            },
        ],
    },

    output: {
        filename: '[name].js',
        path: OUTPUT_DIR,
        libraryTarget: 'commonjs2',
    },
} as Configuration;
