export {
    useExtensions,
    useExtensionsLoadingState,
    useExtensionsUnsupported,
    useExtensionsAll,
    ExtensionPoint,
    ExtensionPointInfo, // @ts-expect-error - TS doesn't have types for *.graphql file
} from './schema-with-provided-types.cse.graphql';
