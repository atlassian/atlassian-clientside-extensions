export {
    useExtensions,
    useExtensionsLoadingState,
    useExtensionsUnsupported,
    useExtensionsAll,
    ExtensionPoint,
    ExtensionPointInfo, // @ts-expect-error - TS doesn't have types for *.graphql file
} from './simple-schema.cse.graphql';
