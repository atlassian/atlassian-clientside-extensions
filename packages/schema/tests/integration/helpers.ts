/* eslint-disable global-require */
import type { Configuration, Stats } from 'webpack';
import type { LoaderOptions } from '../../lib/loader/types';

const path = require('path');
const webpack = require('webpack');

const runWebpack = (config: Configuration): Promise<Stats> => {
    const compiler = webpack(config);

    return new Promise<Stats>((resolve, reject) => {
        compiler.run((err, stats) => {
            if (err) {
                reject(err);
                return;
            }

            if (stats!.hasErrors()) {
                const { errors } = stats!.toJson();

                reject(new Error(errors?.join('\n')));
                return;
            }

            resolve(stats);
        });
    });
};

// eslint-disable-next-line import/prefer-default-export
export const compileFile = async <T>(entrypointPath: string, loaderOptions?: LoaderOptions): Promise<T> => {
    const config = require('./src/webpack.config');

    // Extend webpack config
    config.entry[entrypointPath] = path.join(__dirname, entrypointPath);

    if (loaderOptions) {
        config.module.rules[0].use[0].options = loaderOptions;
    }

    await runWebpack(config);

    return import(`./dist/${entrypointPath}`);
};
