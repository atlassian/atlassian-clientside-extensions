/**
 * @jest-environment jsdom
 */
// We use window object in this test
import { compileFile } from './helpers';
import type { LoaderOptions } from '../../lib/loader/types';
import type * as SimpleSchemaType from './src/simple-schema';
import type * as SchemaWithProvidedTypesType from './src/schema-with-provided-types';

// eslint-disable-next-line import/order
import path = require('path');

describe('schema-loader', () => {
    beforeAll(() => {
        // @ts-expect-error We need setImmediate so we can compile the code with webpack
        global.setImmediate = (callback) => {
            setTimeout(callback, 1);
        };
    });

    it('should transform the *.cse.graphql file and expose the exported members', async () => {
        expect.assertions(6);

        const module = await compileFile<typeof SimpleSchemaType>('./src/simple-schema.ts');

        expect(module.useExtensions).toEqual(expect.any(Function));
        expect(module.useExtensionsLoadingState).toEqual(expect.any(Function));
        expect(module.useExtensionsUnsupported).toEqual(expect.any(Function));
        expect(module.useExtensionsAll).toEqual(expect.any(Function));
        expect(module.ExtensionPoint).toEqual(expect.any(Function));
        expect(module.ExtensionPointInfo).toEqual(expect.any(Function));
    });

    it('should transform a schema with provided types without errors', async () => {
        expect.assertions(6);

        const loaderOptions = {
            providedTypes: '**/*.type.graphql',
            cwd: path.resolve(__dirname),
        } as LoaderOptions;

        const module = await compileFile<typeof SchemaWithProvidedTypesType>('./src/schema-with-provided-types.ts', loaderOptions);

        expect(module.useExtensions).toEqual(expect.any(Function));
        expect(module.useExtensionsLoadingState).toEqual(expect.any(Function));
        expect(module.useExtensionsUnsupported).toEqual(expect.any(Function));
        expect(module.useExtensionsAll).toEqual(expect.any(Function));
        expect(module.ExtensionPoint).toEqual(expect.any(Function));
        expect(module.ExtensionPointInfo).toEqual(expect.any(Function));
    });
});
