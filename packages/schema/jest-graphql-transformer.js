module.exports = {
    process: () => {
        return `
const React = require('react');

module.exports = {
    __esModule: true,
    useExtensions: jest.fn(() => []),
    useExtensionsAll: jest.fn(() => []),
    useExtensionsUnsupported: jest.fn(() => []),
    useExtensionsLoadingState: jest.fn(() => []),
    ExtensionPoint: () => React.createElement('div', null, 'MockExtensionPoint'),
    ExtensionPointInfo: () => React.createElement('div', null, 'MockExtensionPointInfo'),
};`;
    },
};
