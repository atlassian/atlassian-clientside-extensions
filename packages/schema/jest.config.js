const base = require('../../jest.config.base');
const pack = require('./package.json');

module.exports = {
    ...base,
    displayName: pack.name,
    modulePathIgnorePatterns: ['\\/tests\\/.*\\/dist\\/'],
    moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx', 'node', 'graphql'],
};

if (process.env.WEBPACK_4) {
    module.exports.cacheDirectory = 'node_modules/.cache/jest-cache-webpack-4';
    module.exports.moduleNameMapper = {
        ...module.exports.moduleNameMapper,
        '^webpack((\\/.*)?)$': 'webpack-4$1',
    };
}
