#!/usr/bin/env node
import { glob } from 'glob';
import yargs from 'yargs';
import path from 'path';
import fs from 'fs';
import mkdirp from 'mkdirp';
import { rimraf } from 'rimraf';
import chokidar from 'chokidar';
import snakeCase from 'lodash.snakecase';
import generateCode from '../code-generator/code-generator';
import { loadSchema } from '../utils/schema';

import type { FileWriter, GeneratorOptions } from '../code-generator/code-generator';

const appendExtension = (baseFileName: string, extension: string) => {
    return `${baseFileName}.${extension}`;
};

const getGeneratorTargetDirectory = (filePath: string) => {
    return path.join(path.dirname(filePath), 'generated');
};

const ensureFileName = (name: string) => {
    return snakeCase(name) as string;
};

const bustCacheForFile = (filePath: string) => {
    delete require.cache[require.resolve(filePath)]; // watch mode cache busting
};

const toRelativeIfExists = (filePath: string) => {
    if (!filePath) {
        return null;
    }

    return `./${filePath}`;
};

const writeContentsToFile =
    (targetDirectory: string): FileWriter =>
    async (baseIdentifier: string, typescriptCode: string, javascriptCode: string) => {
        const baseFilename = ensureFileName(baseIdentifier);
        const wrapperFileNameTs = path.join(targetDirectory, appendExtension(baseFilename, 'ts'));
        const wrapperFileNameJs = path.join(targetDirectory, appendExtension(baseFilename, 'js'));
        await mkdirp(path.dirname(wrapperFileNameTs));
        await fs.promises.writeFile(wrapperFileNameTs, typescriptCode, 'utf8');
        await fs.promises.writeFile(wrapperFileNameJs, javascriptCode, 'utf8');

        return toRelativeIfExists(baseFilename);
    };

const generateContentForSchemaFile = async (filePath: string, generatorOptions: GeneratorOptions) => {
    bustCacheForFile(filePath);
    const schemaData = await loadSchema(filePath);
    if (!schemaData) {
        return;
    }
    const targetDirectory = getGeneratorTargetDirectory(filePath);
    try {
        await generateCode(schemaData, generatorOptions, writeContentsToFile(targetDirectory), true);
    } catch (e) {
        console.error(`Something went wrong trying to generate the hooks for your extension point specified at "${filePath}".
If this persists, feel free to raise an issue with us at: https://bitbucket.org/atlassian/atlassian-clientside-extensions/issues

Provided Schema:
${schemaData}
Original Error:
${e}`);
    }
};

const cleanupTargetDir = async (targetDir: string) => {
    await rimraf(targetDir);
};

type Options = yargs.Arguments<{
    glob: string;
    cwd: string;
    watch: boolean;
    providedTypes: string;
    strictMode: boolean;
    validatorOnly: boolean;
}>;

const getProvidedTypesPaths = async (options: Options): Promise<string[]> => {
    let paths: string[] = [];
    const { providedTypes: pattern, cwd } = options;

    if (pattern) {
        paths = await glob(pattern, { cwd, ignore: '**/node_modules/**', dotRelative: true });

        if (!paths.length) {
            console.warn(`Schema code generator: Provided pattern "${pattern}" could not find any files with GraphQL provided types`);
        }
    }

    return paths;
};

const generate = async (options: Options, providedTypesPaths: string[]) => {
    const { glob: pattern, cwd, strictMode, validatorOnly } = options;

    // TODO: Extract all this
    const resultFiles = await glob(pattern, { cwd, ignore: '**/node_modules/**', dotRelative: true });
    const absoluteFilePaths = resultFiles.map((filePath) => path.resolve(path.join(cwd, filePath)));

    // eslint-disable-next-line no-restricted-syntax
    for (const absolutePath of absoluteFilePaths) {
        // we have to run this first to remove all preexisting directories before generating any files
        // eslint-disable-next-line no-await-in-loop
        await cleanupTargetDir(getGeneratorTargetDirectory(absolutePath));
    }

    const generatorOptions: GeneratorOptions = {
        providedTypesPaths,
        strictMode,
        validatorOnly,
    };

    // eslint-disable-next-line no-restricted-syntax
    for (const absolutePath of absoluteFilePaths) {
        bustCacheForFile(absolutePath);
        // eslint-disable-next-line no-await-in-loop
        await generateContentForSchemaFile(absolutePath, generatorOptions);
    }
};

const run = async (options: Options) => {
    // TODO: Refactor this part to be more smart about watching changes in provided types
    let providedTypesPaths = await getProvidedTypesPaths(options);

    await generate(options, providedTypesPaths);

    if (options.watch) {
        console.log('Schema code generator watching for changes....');
        chokidar
            .watch([options.glob, options.providedTypes], {
                cwd: options.cwd,
                ignored: (changedFilePath: string) => {
                    return changedFilePath.includes('node_modules');
                },
            })
            .on('all', async (eventName: string, filePath: string) => {
                // eslint-disable-next-line no-console
                console.log(`Schema code generator detected changes. Rebuilding "${filePath}"...`);
                const absolutePath = path.resolve(path.join(options.cwd, filePath));
                if (eventName !== 'unlink') {
                    bustCacheForFile(absolutePath);

                    // TODO: Refactor this part to be more smart about watching changes in provided types
                    providedTypesPaths = await getProvidedTypesPaths(options);

                    const generatorOptions: GeneratorOptions = {
                        providedTypesPaths,
                        strictMode: options.strictMode,
                        validatorOnly: options.validatorOnly,
                    };

                    await generateContentForSchemaFile(absolutePath, generatorOptions);
                }
            });
    }
};
// eslint-disable-next-line @typescript-eslint/no-unused-expressions
yargs
    .command(
        '$0 [options]',
        'Generate hooks and other related code based on provided schema.',
        (builder) => {
            builder
                .option('glob', {
                    describe: `Glob pattern to find schemas.`,
                    type: 'string',
                    demandOption: true,
                })
                .option('strict-mode', {
                    describe: 'Toggles validation strict mode',
                    type: 'boolean',
                    default: true,
                })
                .option('provided-types', {
                    describe: `Glob pattern to find provided types for schemas.`,
                    type: 'string',
                    default: '',
                })
                /** @since 2.1.0 */
                .option('validator-only', {
                    describe: `Only generate validators for the given schema files.`,
                    type: 'boolean',
                    default: false,
                })
                .option('watch', {
                    describe: `Continuously watch for and generate on changes.`,
                    type: 'boolean',
                    default: false,
                })
                .option('cwd', {
                    describe: `The cwd in which the glob is searching for schemas. Defaults to process.cwd()`,
                    type: 'string',
                    default: process.cwd(),
                });
        },
        run,
    )
    .help().argv; // for --help and -h to work
