#!/usr/bin/env node
import { isAbsolute, join } from 'path';
import yargs from 'yargs';
import fs from 'fs';
import getDefinitions from '../code-generator/type-definitions';

type Options = {
    path?: string;
    moduleGlob: string;
};

const run = async (options: Options) => {
    const definitions = getDefinitions(options.moduleGlob);

    if (!options.path) {
        console.log(definitions);
        process.exit(0);
    }

    // we now know it exists!
    const optionPath = options.path!;

    const filePath = isAbsolute(optionPath) ? optionPath : join(process.cwd(), optionPath);
    try {
        await fs.promises.writeFile(filePath, definitions, 'utf8');
        console.log(`Successfully wrote definitions to
${filePath}`);
        process.exit(0);
    } catch (e) {
        console.error(`Failed to write definitions to ${filePath}.`, e);
        process.exit(1);
    }
};

// eslint-disable-next-line @typescript-eslint/no-unused-expressions
yargs
    .command(
        '$0 [path] [options]',
        'Generate a definition file of available Scalars, Enums and Union types baked into clientside extensions.',
        (builder) => {
            builder
                .positional('path', {
                    describe: `Filepath to store definitions in.`,
                    type: 'string',
                })

                .option('module-glob', {
                    alias: 'mg',
                    describe: 'The name of the module the definitions are generated for. Defaults to "*.cse.graphql"',
                    type: 'string',
                    default: '*.cse.graphql',
                });
        },
        run,
    )
    .help().argv; // for --help and -h to work
