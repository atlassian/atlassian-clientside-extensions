#!/usr/bin/env node
import yargs from 'yargs';
import fs from 'fs';
import path from 'path';
import graphqlDefinitions from '../graphql-definition';

type Options = {
    path?: string;
};
const run = async (options: Options) => {
    const definitions = await graphqlDefinitions();

    if (!options.path) {
        console.log(definitions);
        process.exit(0);
    }

    const filePath = path.isAbsolute(options.path) ? options.path : path.join(process.cwd(), options.path);
    try {
        await fs.promises.writeFile(filePath, definitions, 'utf8');
        console.log(`Successfully wrote definitions to
${filePath}`);
        process.exit(0);
    } catch (e) {
        console.error(`Failed to write definitions to ${filePath}.`, e);
        process.exit(1);
    }
};

// eslint-disable-next-line @typescript-eslint/no-unused-expressions
yargs
    .command(
        '$0 [path]',
        'Generate a definition file of available Scalars, Enums and Union types baked into clientside extensions.',
        (builder) => {
            builder.positional('path', {
                describe: `Filepath to store definitions in.`,
                type: 'string',
            });
        },
        run,
    )
    .help().argv; // for --help and -h to work
