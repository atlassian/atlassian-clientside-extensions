import IconGlyphScalar from './IconGlyph';
import IconType from './Icon';

const providedTypes = [IconGlyphScalar, IconType];

export default providedTypes;
