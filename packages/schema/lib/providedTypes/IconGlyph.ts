export default `
"""
The value must be either the name of the glyph or a functional React component returning an SVG glyph.
"""
union IconGlyph = String | Function
`;
