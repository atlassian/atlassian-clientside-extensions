export default `
type Icon {
    """
    Icon to render
    """
    glyph: IconGlyph!

    """
    Custom CSS class name passed to icon wrapper
    """
    className: String

    """
    String to use as the aria-label for the icon.
    Set to an empty string if you are rendering the icon with visible text to prevent accessibility label duplication.
    """
    label: String

    """
    Primary colour of the icons
    """
    primaryColor: String

    """
    Secondary colour of the two-color icons
    """
    secondaryColor: String
}
`;
