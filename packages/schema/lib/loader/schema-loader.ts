import { getOptions, interpolateName, parseQuery } from 'loader-utils';
import { glob } from 'glob';
import { validate } from 'schema-utils';

import type { Schema } from 'schema-utils/declarations/validate';
import type { GeneratorOptions } from '../code-generator/code-generator';
import generateCode from '../code-generator/code-generator';
import schema from './loader-schema.json';
import type { LoaderOptions, LoaderContext, LoaderCallback, QueryObject } from './types';

const toFilename = (loaderContext: LoaderContext, identifier: string, content: string) =>
    interpolateName(loaderContext, `${identifier}.[contenthash].js`, {
        context: loaderContext.rootContext,
        content,
    });

const fileStore = new Map<string, string>();

const findProvidedTypes = async (pattern: string, cwd?: string): Promise<string[]> => {
    const providedTypes = await glob(pattern, {
        ignore: '**/node_modules/**',
        absolute: true,
        cwd,
        dotRelative: true,
    });

    if (!providedTypes.length) {
        console.warn(`CSE schema-loader: Provided pattern "${pattern}" could not find any files with GraphQL provided types`);
    }

    return providedTypes;
};

// We need a way to serialize and convert Windows OS style path before outputting it as a string. The webpack loader had issues when the "\"
// directory separator character was not escaped and as a result the file path end-up glued together e.g. "C:foobarfile.graphql".
// The solution is either to convert this path "C:\foo\bar\file.graphql" to "C:\\foo\\bar\\file.graphql"
// or to a unix style "C:/foo/bar/file.graphql"
const serializePath = (filePath: string): string => {
    return filePath.replace(/\\/g, '\\\\');
};

// The full name of the webpack GraphQL loader we are referencing in the generated code
const graphqlLoaderPath = '@atlassian/clientside-extensions-schema/loader';

const defaultLoaderOptions: LoaderOptions = {
    providedTypes: '',
    strictMode: true,
};

// get's a "this" context so !MUST! be a "function" and !NOT! an arrow-function
export default async function schemaLoader(this: LoaderContext, content: string) {
    const callback = this.async() as LoaderCallback;

    // Get and validate loader options
    const loaderOptions: Readonly<LoaderOptions> = getOptions(this);

    validate(schema as Schema, loaderOptions, { name: 'CSE schema-loader' });

    const { providedTypes: providedTypesPattern, strictMode }: LoaderOptions = { ...defaultLoaderOptions, ...loaderOptions };

    // Get provided types
    let providedTypesPaths: string[] = [];

    if (providedTypesPattern) {
        providedTypesPaths = await findProvidedTypes(providedTypesPattern, loaderOptions.cwd);

        // Track custom type files
        providedTypesPaths.forEach((file: string) => this.addDependency(file));
    }

    // Run schema compiler
    const loaderContext = this as LoaderContext;

    if (this.resourceQuery) {
        const rqOptions = parseQuery(this.resourceQuery) as QueryObject;

        if (rqOptions.subfile && fileStore.has(rqOptions.subfile)) {
            callback(null, fileStore.get(rqOptions.subfile));
            return;
        }

        callback(new Error(`CSE Schema-loader: Unknown subfile ${rqOptions.subfile} requested.`));
        return;
    }

    try {
        const generatorOptions: Partial<GeneratorOptions> = {
            providedTypesPaths,
            strictMode,
        };

        const [, , generatedJavascript] = await generateCode(
            content,
            generatorOptions,
            async (baseIdentifier: string, typescriptCode: string, javascriptCode: string) => {
                const filename = toFilename(loaderContext, baseIdentifier, javascriptCode);
                fileStore.set(filename, javascriptCode);

                const resourcePath = serializePath(loaderContext.resourcePath);

                return `!!${graphqlLoaderPath}!${resourcePath}?subfile=${filename}`;
            },
            false,
        );
        callback(null, generatedJavascript);
    } catch (err) {
        const error = err as Error;

        callback(error);
    }
}
