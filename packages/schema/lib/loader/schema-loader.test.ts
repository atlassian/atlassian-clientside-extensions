import { mocked } from 'ts-jest/utils';
import generateCode from '../code-generator/code-generator';
import schemaLoader from './schema-loader';
import type { LoaderCallback, LoaderContext, LoaderOptions } from './types';

jest.mock('../code-generator/code-generator', () => ({
    __esModule: true,
    default: jest.fn(),
}));

describe('schema-loader', () => {
    let loaderCallback: LoaderCallback;

    beforeEach(() => {
        loaderCallback = jest.fn() as LoaderCallback;

        mocked(generateCode).mockReturnValue(Promise.resolve());
    });

    it('should call the generator with strict mode turned on by default', async () => {
        // given
        /* @ts-expect-error Stub only the required properties */
        const fakeLoaderContext = {
            async: jest.fn().mockReturnValue(loaderCallback),
        } as LoaderContext;

        // when
        const input = 'fake input';

        await schemaLoader.call(fakeLoaderContext, input);

        // then
        expect(generateCode).toHaveBeenCalledWith(
            input,
            {
                providedTypesPaths: expect.any(Array),
                strictMode: true,
            },
            expect.any(Function),
            expect.any(Boolean),
        );
    });

    it('should call the generator with loose mode', async () => {
        // given
        const loaderOptions: Partial<LoaderOptions> = {
            strictMode: false,
        };

        /* @ts-expect-error Stub only the required properties */
        const fakeLoaderContext = {
            async: jest.fn().mockReturnValue(loaderCallback),
            query: loaderOptions,
        } as LoaderContext;

        // when
        const input = 'fake input';

        await schemaLoader.call(fakeLoaderContext, input);

        // then
        expect(generateCode).toHaveBeenCalledWith(
            input,
            {
                providedTypesPaths: expect.any(Array),
                strictMode: false,
            },
            expect.any(Function),
            expect.any(Boolean),
        );
    });
});
