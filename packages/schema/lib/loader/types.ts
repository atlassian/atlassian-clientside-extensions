import type { OptionObject, getOptions } from 'loader-utils';

export type LoaderOptions = {
    readonly providedTypes?: string;
    readonly strictMode?: boolean;
    readonly cwd?: string;
};

export interface QueryObject extends OptionObject {
    readonly subfile: string;
}

export type LoaderContext = Parameters<typeof getOptions>[0];
export type LoaderCallback = LoaderContext['callback'];
