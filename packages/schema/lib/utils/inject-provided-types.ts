import providedTypes from '../providedTypes';
import injectTypes from './injcect-types';

const injectProvidedTypes = (query: string) => injectTypes(query, providedTypes);

export default injectProvidedTypes;
