import type { Maybe } from 'graphql/jsutils/Maybe';

import SchemaDocument from './SchemaDocument';
import { populateDocument } from './schema-document-populator';
import { getSchemaObject } from './create-schema';

export type SerializedDocument = {
    objectTypes: { [key: string]: SerializedObjectType };
    scalars: { [key: string]: SerializedScalar };
    enumTypes: { [key: string]: SerializedEnumType };
    unionTypes: { [key: string]: SerializedUnionType };
};

export type SerializedObjectTypeDescriptor = {
    description?: Maybe<string>;
    name: string;
    required: boolean;
    type: string;
};

export type SerializedObjectType = {
    name: string;
    description?: Maybe<string>;
    descriptors: SerializedObjectTypeDescriptor[];
};

export type SerializedScalar = {
    name: string;
    description?: Maybe<string>;
};

export type SerializedEnumType = {
    name: string;
    description?: Maybe<string>;
    values: string[];
};

export type SerializedUnionType = {
    name: string;
    description?: Maybe<string>;
    types: string[];
};

export class SerializableDocument extends SchemaDocument {
    getSerializedObjectTypes() {
        const serializedObjectTypes: { [key: string]: SerializedObjectType } = {};

        for (const [key, objectType] of this.getObjectTypes().entries()) {
            const { name, description, descriptors } = objectType;

            serializedObjectTypes[key] = {
                name,
                description,
                // eslint-disable-next-line @typescript-eslint/no-shadow
                descriptors: descriptors.map(({ description, name, required, typeString }) => ({
                    description,
                    name,
                    required,
                    type: typeString,
                })),
            };
        }

        return serializedObjectTypes;
    }

    getSerializedEnumTypes() {
        const enumTypes: { [key: string]: SerializedEnumType } = {};

        for (const enumType of this.getEnumTypes()) {
            const { name, description } = enumType;
            const values = enumType.getValues().map((val) => val.value);

            enumTypes[name] = { name, description, values };
        }

        return enumTypes;
    }

    getSerializedUnionTypes() {
        const unionTypes: { [key: string]: SerializedUnionType } = {};

        for (const unionType of this.getUnionTypes()) {
            const { name, description } = unionType;
            const types = unionType.getTypes().map((type) => type.name);

            unionTypes[name] = { name, description, types };
        }

        return unionTypes;
    }

    getSerializedScalars() {
        const scalars: { [key: string]: SerializedScalar } = {};

        for (const scalar of this.getScalars()) {
            const { name, description } = scalar;

            scalars[name] = { name, description };
        }

        return scalars;
    }

    serialize(): SerializedDocument {
        return {
            objectTypes: this.getSerializedObjectTypes(),
            enumTypes: this.getSerializedEnumTypes(),
            unionTypes: this.getSerializedUnionTypes(),
            scalars: this.getSerializedScalars(),
        };
    }
}

export const serializeSchemaDocument = (extensionPoint: string, schema: string, baseType: string): SerializedDocument | null => {
    try {
        const schemaDocument = new SerializableDocument();
        populateDocument(schemaDocument, getSchemaObject(schema, baseType));

        return schemaDocument.serialize();
    } catch (error) {
        console.error(
            `Client-side Extension: the schema for extension point "${extensionPoint}" does not contain a ${baseType} definition.`,
        );

        return null;
    }
};
