import { getSchemaObject } from './create-schema';
import SchemaDocument from './SchemaDocument';
import { populateDocument } from './schema-document-populator';

describe('Schema Document Populator', () => {
    let schemaDocument: SchemaDocument;

    beforeEach(() => {
        const schema = getSchemaObject(
            `
enum FooEnum {
    foo
    BAR
    baz
}

enum UnusedFooEnum {
    foo
    BAR
    baz
}

type FooType {
    such: String
    very: Number
}

type UnusedFooType {
    such: String
    very: Number
}

union FooUnion = String | Function | FooType | FooEnum
union UnusedFooUnion = String | Function | FooType | FooEnum

type Schema {
    glyph: FooEnum!
    foo: Number
    bar: [FooUnion!]
}
`,
            'Schema',
        );

        schemaDocument = new SchemaDocument();
        populateDocument(schemaDocument, schema);
    });

    describe('SchemaDocument', () => {
        it('should collect all used object types', () => {
            expect(schemaDocument.getObjectTypes().size).toBe(2);
            expect(Array.from(schemaDocument.getObjectTypes().keys())).toEqual(['Schema', 'FooType']);
            expect(Array.from(schemaDocument.getObjectTypes().keys())).not.toContain('UnusedFooType');
        });

        it('should collect all used enum types', () => {
            expect(schemaDocument.getEnumTypes().size).toBe(1);
            const enums = Array.from(schemaDocument.getEnumTypes());
            const enumNames = enums.map((enumType) => enumType.name);
            expect(enumNames).toEqual(['FooEnum']);
            expect(enumNames).not.toContain('UnusedFooEnum');
        });

        it('should contain all used unions', () => {
            expect(schemaDocument.getUnionTypes().size).toBe(1);
            const unions = Array.from(schemaDocument.getUnionTypes());
            const unionNames = unions.map((unionTypes) => unionTypes.name);
            expect(unionNames).toEqual(['FooUnion']);
            expect(unionNames).not.toContain('UnusedFooUnion');
        });

        it('should contain all used scalars', () => {
            expect(schemaDocument.getScalars().size).toBe(3);
            const scalars = Array.from(schemaDocument.getScalars());
            const scalarNames = scalars.map((scalar) => scalar.name);
            expect(scalarNames).toEqual(['Number', 'String', 'Function']);
            expect(scalarNames).not.toContain('Boolean');
        });
    });

    describe('Circular Dependencies', () => {
        it('should short-circuit on circular dependencies and not explode', () => {
            const schema = getSchemaObject(
                `
type OtherStuff {
    bar: Number!
    baz: [String!]
}

type Comment {
    id: Number!
    comments: [Comment]
    otherStuff: OtherStuff
}

type Schema {
    comment: Comment!
}
`,
                'Schema',
            );

            schemaDocument = new SchemaDocument();
            expect(() => populateDocument(schemaDocument, schema)).not.toThrow();
        });

        it('should list circular dependency as regular type', () => {
            const schema = getSchemaObject(
                `
type OtherStuff {
    bar: Number!
    baz: [String!]
}

type Comment {
    id: Number!
    comments: [Comment]
    otherStuff: OtherStuff
}

type Schema {
    comment: Comment!
}
`,
                'Schema',
            );

            schemaDocument = new SchemaDocument();
            populateDocument(schemaDocument, schema);
            expect(schemaDocument.getObjectTypes().has('Comment')).toEqual(true);
        });
    });
});
