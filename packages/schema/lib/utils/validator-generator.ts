import type { GraphQLEnumType, GraphQLObjectType, GraphQLScalarType, GraphQLUnionType } from 'graphql';
import { isEnumType, isListType, isObjectType, isScalarType, isUnionType } from 'graphql';

import scalars from '../scalars';
import { ValidationTypes } from '../scalars/types';

import type SchemaDocument from './SchemaDocument';
import type { AcceptedTypes, FieldDescriptor, NonNullNonListType, ObjectType } from './ast-utils';
import { containsListType, isFirstRequired, isStackEndRequired } from './ast-utils';
import {
    arrayValidator,
    booleanValidator,
    constValueValidator,
    dateValidator,
    enumValidator,
    functionValidator,
    numberValidator,
    objectValidator,
    promiseValidator,
    regexpValidator,
    stringValidator,
} from './validator-checks';

import type { ValidationOptions } from './validator-types';

const createFnName = (hint: string) => {
    return `validate_${hint}`;
};

let counter = 0;
const createIterFnName = () => {
    return `validate_${counter++}`;
};

const stringifyFormatter = () => {
    const name = 'stringifyFormatter';
    const fn = `
const ${name} = (_: unknown, value: unknown): string => {
    if (value === undefined) {
        return 'undefined';
    }

    if (typeof value === 'function') {
        return value.toString();
    }

    return value as string;
};
`;
    return {
        fn,
        name,
    };
};

const toSafeAttribute = (attribute: string) => JSON.stringify(attribute);

const getValidatorForScalar = (type: ValidationTypes) => {
    switch (type) {
        case ValidationTypes.boolean:
            return booleanValidator();
        case ValidationTypes.constant:
            return constValueValidator();
        case ValidationTypes.date:
            return dateValidator();
        case ValidationTypes.function:
            return functionValidator();
        case ValidationTypes.number:
            return numberValidator();
        case ValidationTypes.promise:
            return promiseValidator();
        case ValidationTypes.regexp:
            return regexpValidator();
        case ValidationTypes.string:
            return stringValidator();
        default:
            throw new Error(`Unexpected validation type: "${type}". Validator generation failed.`);
    }
};

const getScalarDefinitionFromName = (name: string) => {
    const scalar = scalars.has(name) && scalars.get(name);

    if (!scalar) {
        throw new Error(`Scalar with name: "${name}" was not registered. Validator generation failed.`);
    }

    return scalar;
};

const getErrorMessageForAttribute = (fnStack: Set<string>, objectTypeName: string, attributeName: string, expectedType: string) => {
    const { name, fn } = stringifyFormatter();
    fnStack.add(fn);

    return (
        `'"${objectTypeName}" failed to validate. Attribute "${attributeName}" expected a value of type ${expectedType},` +
        ` but received: ' + JSON.stringify(subject[${JSON.stringify(attributeName)}], ${name})`
    );
};

const getErrorMessageForValues = (fnStack: Set<string>, objectTypeName: string, attributeName: string, expectedValues: string[]) => {
    const { name, fn } = stringifyFormatter();
    fnStack.add(fn);
    const exceptedValuesString = expectedValues.map((val) => JSON.stringify(val)).join(', ');

    return (
        `'"${objectTypeName}" failed to validate. Attribute "${attributeName}" expected one of the following values: ${exceptedValuesString}.` +
        ` Received value: ' + JSON.stringify(subject[${JSON.stringify(attributeName)}], ${name})`
    );
};

const ifAttributeExistsCheck = (attributeName: string) => {
    const safeAttributeName = toSafeAttribute(attributeName);
    return `${safeAttributeName} in subject && subject[${safeAttributeName}] !== null`;
};

const createRequiredElse = (isRequired: boolean, objectTypeName: string, attributeName: string, type: string) => {
    if (!isRequired) {
        return '';
    }

    return `else {
    errors.push(errorFactory(errorPrefix, '"${objectTypeName}" failed to validate. Attribute "${attributeName}" of type ${type} is missing in "${objectTypeName}".'));
}`;
};

const conditionValidationScalar = (fnStack: Set<string>, scalar: GraphQLScalarType, runtimeVariable: string) => {
    const scalarDefinition = getScalarDefinitionFromName(scalar.name);
    const { fn, name } = getValidatorForScalar(scalarDefinition.validationType);

    fnStack.add(fn);

    // we have to treat constants different - as we compare rather than look at a value
    if (scalarDefinition.validationType === ValidationTypes.constant) {
        const { acceptedValue } = scalarDefinition;
        return `!${name}(${toSafeAttribute(acceptedValue as string)}, ${runtimeVariable})`;
    }

    return `!${name}(${runtimeVariable})`;
};

const getEnumValues = (enumType: GraphQLEnumType) => enumType.getValues().map((value) => value.value);

const conditionValidationEnum = (fnStack: Set<string>, enumType: GraphQLEnumType, runtimeVariable: string) => {
    const { fn, name } = enumValidator();

    fnStack.add(fn);

    const acceptableValues = getEnumValues(enumType);
    return `!${name}(${JSON.stringify(acceptableValues)}, ${runtimeVariable})`;
};

const getErrorPrefixForObjectType = (objectTypeName: string, attributeName: string) =>
    `"${objectTypeName}" failed on attribute "${attributeName}". `;

const callObjectTypeCheck = (fnName: string, errorPrefix: string, runtimeVariable: string, errorsVariables = 'errors') => {
    return `${fnName}(errorPrefix + '${errorPrefix}', ${runtimeVariable}, ${errorsVariables})`;
};

const checkObjectType = (fnStack: Set<string>, objectTypeName: string, attributeName: string, objectTypeObject: GraphQLObjectType) => {
    return callObjectTypeCheck(
        createFnName(objectTypeObject.name),
        getErrorPrefixForObjectType(objectTypeName, attributeName),
        `subject[${toSafeAttribute(attributeName)}]`,
    );
};

const conditionValidationObjectTypeObject = (fnStack: Set<string>, objectTypeObject: GraphQLObjectType, runtimeVariable: string) => {
    const errorsVariable = 'errorsOrWarnings';
    const objectTypeCheck = callObjectTypeCheck(createFnName(objectTypeObject.name), '', runtimeVariable, errorsVariable);

    return `
    (function() {
        const ${errorsVariable} = [];
        const errorPrefix = '';

        ${objectTypeCheck};

        const errors = ${errorsVariable}.filter(errorOrWarning => errorOrWarning.severity === ErrorLevels.ERROR);

        return errors.length !== 0;
    }())`;
};

const conditionValidationUnion = (fnStack: Set<string>, unionType: GraphQLUnionType, runtimeVariable: string): string => {
    // built in typing does not reflect reality
    const typesOfUnion = unionType.getTypes() as unknown as NonNullNonListType[];

    const checks = typesOfUnion
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        .map((type) => getBaseTypeCondition(fnStack, type, runtimeVariable));

    return `${checks.join('&&')}`;
};

const getBaseTypeCondition = (fnStack: Set<string>, type: NonNullNonListType | undefined, runtimeVariable: string) => {
    if (isScalarType(type)) {
        return conditionValidationScalar(fnStack, type, runtimeVariable);
    }

    if (isEnumType(type)) {
        return conditionValidationEnum(fnStack, type, runtimeVariable);
    }

    if (isObjectType(type)) {
        return conditionValidationObjectTypeObject(fnStack, type, runtimeVariable);
    }

    if (isUnionType(type)) {
        return conditionValidationUnion(fnStack, type, runtimeVariable);
    }

    throw new Error(`Unexpected base type: "${type}". No check generation possible. Validator generation failed.`);
};

const ifCaseScalar = (fnStack: Set<string>, objectTypeName: string, attributeName: string, scalar: GraphQLScalarType) => {
    const runtimeVariable = `subject[${toSafeAttribute(attributeName)}]`;
    const condition = conditionValidationScalar(fnStack, scalar, runtimeVariable);

    return `
    if (${condition}) {
        errors.push(errorFactory(errorPrefix, ${getErrorMessageForAttribute(fnStack, objectTypeName, attributeName, scalar.name)}));
    }
`;
};

const ifCaseEnum = (fnStack: Set<string>, objectTypeName: string, attributeName: string, enumType: GraphQLEnumType) => {
    const runtimeVariable = `subject[${toSafeAttribute(attributeName)}]`;
    const condition = conditionValidationEnum(fnStack, enumType, runtimeVariable);

    return `
    if (${condition}) {
        errors.push(errorFactory(errorPrefix, ${getErrorMessageForValues(
            fnStack,
            objectTypeName,
            attributeName,
            getEnumValues(enumType),
        )}));
    }
`;
};

const checkUnionType = (fnStack: Set<string>, objectTypeName: string, attributeName: string, unionType: GraphQLUnionType): string => {
    // typing does not reflect reality
    const typesOfUnion = unionType.getTypes() as unknown as NonNullNonListType[];
    const typeString = typesOfUnion.map((type) => type.name).join(' | ');

    const condition = conditionValidationUnion(fnStack, unionType, `subject[${toSafeAttribute(attributeName)}]`);

    return `
    (function(){
        if (${condition}) {
            errors.push(errorFactory(errorPrefix, ${getErrorMessageForAttribute(fnStack, objectTypeName, attributeName, typeString)}));
        }
    }())
`;
};

const getBaseTypeCheck = (fnStack: Set<string>, objectTypeName: string, attributeName: string, type: NonNullNonListType) => {
    if (isScalarType(type)) {
        return ifCaseScalar(fnStack, objectTypeName, attributeName, type);
    }

    if (isEnumType(type)) {
        return ifCaseEnum(fnStack, objectTypeName, attributeName, type);
    }

    if (isObjectType(type)) {
        return checkObjectType(fnStack, objectTypeName, attributeName, type);
    }

    if (isUnionType(type)) {
        return checkUnionType(fnStack, objectTypeName, attributeName, type);
    }

    throw new Error(`Unexpected base type: "${type}". No check generation possible. Validator generation failed.`);
};

const createIterationFn = (fnStack: Set<string>, isRequired: boolean, validateFn: string) => {
    const iterFnName = createIterFnName();
    const { name, fn } = arrayValidator();
    fnStack.add(fn);
    fnStack.add(`
function ${iterFnName}(subject: any) {
    ${
        !isRequired
            ? `if (subject === null) {
        return true;
    }`
            : ''
    }
    if(!${name}(subject)) {
        return false;
    }


    return subject.every( elem => ${validateFn}(elem));
}
`);
    return iterFnName;
};

const reverseCreateStackValidator = (fnStack: Set<string>, typeStack: AcceptedTypes[], prevValidatorFn?: string): string => {
    if (typeStack.length === 0 && prevValidatorFn) {
        return prevValidatorFn;
    }

    const isRequired = isStackEndRequired(typeStack);
    const type = typeStack.pop();
    if (isRequired) {
        typeStack.pop();
    }
    if (!isListType(type)) {
        const validatorCondition = getBaseTypeCondition(fnStack, type, 'subject');
        const validatorNullCheck = isRequired ? '' : 'subject === null || ';
        const validatorFnName = createIterFnName();
        const validatorFnBody = `function ${validatorFnName}(subject: unknown){return ${validatorNullCheck}!(${validatorCondition})}`;
        fnStack.add(validatorFnBody);

        return reverseCreateStackValidator(fnStack, typeStack, validatorFnName);
    }

    // we have a list
    if (!prevValidatorFn) {
        throw new Error('missing validator fn to call');
    }

    const iteratorFnName = createIterationFn(fnStack, isRequired, prevValidatorFn);

    return reverseCreateStackValidator(fnStack, typeStack, iteratorFnName);
};

const getListCheck = (fnStack: Set<string>, objectTypeName: string, descriptor: FieldDescriptor) => {
    const stackValidatorName = reverseCreateStackValidator(fnStack, descriptor.typeStack);
    return `if (!${stackValidatorName}(subject[${JSON.stringify(descriptor.name)}])) {
        errors.push(errorFactory(errorPrefix, ${getErrorMessageForAttribute(
            fnStack,
            objectTypeName,
            descriptor.name,
            descriptor.typeString,
        )}));
    }
`;
};

const getInnerCheck = (fnStack: Set<string>, objectTypeName: string, descriptor: FieldDescriptor) => {
    const containsList = containsListType(descriptor.typeStack);

    // nothing special to do
    if (!containsList) {
        return getBaseTypeCheck(fnStack, objectTypeName, descriptor.name, descriptor.baseType.reference);
    }

    return getListCheck(fnStack, objectTypeName, descriptor);
};

const getExcessiveKeysCheck = (objectType: ObjectType, options: ValidationOptions) => {
    if (!options.checkExcessiveKeys) {
        return null;
    }

    const errorFactoryFn = options.strictMode === true ? 'errorFactory' : 'warningFactory';

    const keys = objectType.descriptors.map((descriptor) => descriptor.name);
    return `
const expectedKeys = ${JSON.stringify(keys)};
const excessiveKeys = Object.keys(subject).filter(key => !expectedKeys.includes(key))
if (excessiveKeys.length) {
    errors.push(${errorFactoryFn}(errorPrefix, '"${
        objectType.name
    }". Excessive key' + (excessiveKeys.length === 1 ? '':'s') + ' [' + excessiveKeys.join(', ') + '] ' + (excessiveKeys.length === 1 ? 'is':'are') + ' not specified in the schema.'));
}

`;
};

const generateObjectTypeValidator = (fnStack: Set<string>, objectType: ObjectType, options: ValidationOptions) => {
    const { name, fn } = objectValidator();
    fnStack.add(fn);

    const excessiveKeyValidation = getExcessiveKeysCheck(objectType, options);

    const attributeValidation = objectType.descriptors
        .map((descriptor) => {
            // e.g. foo: Number! (then the first element in the stack will be required type)
            const isRequired = isFirstRequired(descriptor.typeStack);

            const outerIfCondition = ifAttributeExistsCheck(descriptor.name);
            const outerElseStatement = createRequiredElse(isRequired, objectType.name, descriptor.name, descriptor.typeString);

            const baseTypeCheck = getInnerCheck(fnStack, objectType.name, descriptor);

            return `
if (${outerIfCondition}) {
    ${baseTypeCheck}
} ${outerElseStatement}
`;
        })
        .join('\n\n');

    const objectTypeValidationFnName = createFnName(objectType.name);
    const objectTypeValidationFunction = `
function ${objectTypeValidationFnName}(errorPrefix: string, subject: any, errors: ValidationErrorsAndWarnings[]) {
    if(!${name}(subject)) {
        errors.push(
            errorFactory(errorPrefix, '"${objectType.name}" failed to validate. Expected an object of type "${objectType.name}".'
        ));
        return;
    }
    ${excessiveKeyValidation}
    ${attributeValidation}
}
`;
    fnStack.add(objectTypeValidationFunction);
};

// eslint-disable-next-line import/prefer-default-export
export const createValidator = (schemaDocument: SchemaDocument, baseTypeName: string, options: ValidationOptions) => {
    const fnStack = new Set<string>();
    // eslint-disable-next-line no-restricted-syntax
    for (const objectType of Array.from(schemaDocument.getObjectTypes().values())) {
        generateObjectTypeValidator(fnStack, objectType, options);
    }

    return `
import { ErrorLevels } from '@atlassian/clientside-extensions-schema';
import type { Validator, ValidationResult, ValidationError, ValidationWarning } from '@atlassian/clientside-extensions-schema';

type ValidationErrorsAndWarnings = ValidationError | ValidationWarning;

const errorFactory = (errorPrefix: string, error: string): ValidationError => ({
    severity: ErrorLevels.ERROR,
    error: errorPrefix + error,
});

const warningFactory = (errorPrefix: string, error: string): ValidationWarning => ({
    severity: ErrorLevels.WARNING,
    warning: errorPrefix + error,
});

${Array.from(fnStack).join('\n\n')}
export const validate: Validator = function validator${baseTypeName}(subject: unknown): ValidationResult {
    const errorsAndWarnings: ValidationErrorsAndWarnings[] = [];

    ${createFnName(baseTypeName)}('', subject, errorsAndWarnings);

    const errors: ValidationError[] = [];
    const warnings: ValidationWarning[] = [];

    for (const errorOrWarning of errorsAndWarnings) {
        if (errorOrWarning.severity === ErrorLevels.ERROR) {
            errors.push(errorOrWarning);
        } else if (errorOrWarning.severity === ErrorLevels.WARNING) {
            warnings.push(errorOrWarning);
        }
    }

    return {
        errors,
        warnings,
    };
}
`;
};
