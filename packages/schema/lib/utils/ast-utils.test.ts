import { getSchemaObject } from './create-schema';
import type { FieldDescriptor } from './ast-utils';
import { getObjectTypeDescription } from './ast-utils';

describe('Schema Document Populator', () => {
    let descriptors: FieldDescriptor[];

    beforeEach(() => {
        const schema = getSchemaObject(
            `
enum FooEnum {
    foo
    BAR
    baz
}

enum UnusedFooEnum {
    foo
    BAR
    baz
}

type FooInput {
    such: String
    very: Number
}

type UnusedFooInput {
    such: String
    very: Number
}

type Circular {
    circles: [Circular]
}

union FooUnion = String | Function | FooInput | FooEnum
union UnusedFooUnion = String | Function | FooInput | FooEnum

type Schema {
    glyph: FooEnum!
    foo: Number
    bar: [FooUnion!]
    circle: Circular
}
`,
            'Schema',
        );

        descriptors = getObjectTypeDescription(schema).descriptors;
    });

    describe('Input Types', () => {
        it('should flag fields as required', () => {
            const requiredDesc = descriptors.find((descriptor) => descriptor.name === 'glyph');
            const optionalDesc = descriptors.find((descriptor) => descriptor.name === 'foo');

            expect(requiredDesc?.required).toBe(true);
            expect(optionalDesc?.required).toBe(false);
        });

        it('should unpack fields nullable an list fields correctly', () => {
            const listDesc = descriptors.find((descriptor) => descriptor.name === 'bar');
            const nullableDesc = descriptors.find((descriptor) => descriptor.name === 'foo');

            expect(listDesc?.baseType.name).toBe('FooUnion');
            expect(nullableDesc?.baseType.name).toBe('Number');
        });

        it('should handle circular dependencies', () => {
            const circleDesc = descriptors.find((descriptor) => descriptor.name === 'circle');

            // nothing really to check here - beyond it not exploding
            expect(circleDesc?.baseType.name).toBe('Circular');
        });
    });
});
