import camelCase from 'lodash.camelcase';

const pascalCase = (str: string) => {
    const camelCased = camelCase(str);
    return camelCased.charAt(0).toUpperCase() + camelCased.slice(1);
};

export default pascalCase;
