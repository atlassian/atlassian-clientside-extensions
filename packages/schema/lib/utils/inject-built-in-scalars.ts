import scalars from '../scalars';

const scalarNames = Array.from(scalars.keys())
    .map((scalarName) => `scalar ${scalarName}`)
    .join('\n');

const injectBuiltInScalars = (query: string) => {
    return `
${scalarNames}

${query}`;
};

export default injectBuiltInScalars;
