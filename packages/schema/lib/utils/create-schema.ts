import type { GraphQLObjectType } from 'graphql';
import type { IResolvers } from '@graphql-tools/utils';
import { makeExecutableSchema } from '@graphql-tools/schema';

import defaultResolvers from './default-resolvers';
import injectProvidedTypes from './inject-provided-types';
import injectBuiltInScalars from './inject-built-in-scalars';

export const createExecutableSchema = (baseSchema: string, resolvers: IResolvers = defaultResolvers) => {
    try {
        const typeDefs = injectBuiltInScalars(injectProvidedTypes(baseSchema));
        return makeExecutableSchema({
            typeDefs,
            resolvers,
            resolverValidationOptions: {
                requireResolversForResolveType: 'ignore',
            },
        });
    } catch (err) {
        const error = err as Error;

        throw new Error(`Failed to parse
schema:
${baseSchema}


message:
${error.toString()}`);
    }
};

export class SchemaDefinitionMissingError extends Error {
    constructor(m: string) {
        super(m);

        // Set the prototype explicitly.
        // see https://github.com/Microsoft/TypeScript-wiki/blob/master/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        Object.setPrototypeOf(this, SchemaDefinitionMissingError.prototype);
    }
}

/**
 * Extracts the GraphQL object type definition of the object type of name <basename> from the GraphQL definition
 *
 * @param schemaString - GraphQL schema string
 * @param baseTypeName - name of the type we want to extract
 * @returns - Object type
 */
export const getSchemaObject = (schemaString: string, baseTypeName: string): GraphQLObjectType => {
    const schema = createExecutableSchema(schemaString);
    const schemaDefinition = schema.getTypeMap()[baseTypeName] as GraphQLObjectType;

    if (schemaDefinition === null || schemaDefinition === undefined) {
        throw new SchemaDefinitionMissingError(`No Schema definition found for ${baseTypeName}!`);
    }

    return schemaDefinition;
};
