// eslint-disable-next-line
// @ts-ignore - TS is not happy that we are trying to use internal NodeJS modules
import { Module } from 'module';
import * as ts from 'typescript';
import type { CompilerOptions } from 'typescript';
import cases from 'jest-in-case';
// eslint-disable-next-line node/no-extraneous-import, import/no-extraneous-dependencies
import 'jest-chain';
// eslint-disable-next-line node/no-extraneous-import, import/no-extraneous-dependencies
import 'jest-extended';

import { getSchemaObject } from './create-schema';
import SchemaDocument from './SchemaDocument';
import { populateDocument } from './schema-document-populator';
import { createValidator } from './validator-generator';

import type { ValidationOptions, Validator } from './validator-types';

const stringFormatter = (_: unknown, value: unknown): string => {
    if (value === null) {
        return 'null';
    }

    if (value === undefined) {
        return 'undefined';
    }

    if (typeof value === 'function') {
        return value.toString();
    }

    return value as string;
};

describe('Validator Generator', () => {
    type SchemaTypeName = 'Schema' | 'ContextSchema';

    const defaultOptions: ValidationOptions = { checkExcessiveKeys: false, strictMode: true };

    const getExecutableValidatorFromSchema = (
        schemaString: string,
        schemaTypeName: SchemaTypeName = 'Schema',
        options: ValidationOptions = defaultOptions,
    ): Validator => {
        const schema = getSchemaObject(schemaString, schemaTypeName);
        const schemaDocument = new SchemaDocument();
        populateDocument(schemaDocument, schema);

        const validatorString = createValidator(schemaDocument, schemaTypeName, options);
        const compilerOptions: CompilerOptions = {
            // Update those values when we change supported version of Node or when Node will get native support for ESM
            module: ts.ModuleKind.CommonJS,
            target: ts.ScriptTarget.ES2019,
        };
        const transpiledModule = ts.transpileModule(validatorString, { compilerOptions });

        const mod = new Module('test');
        const oldRequire = mod.require;

        // Patch the require call so we can self-invoke the schema package from the virtual module
        const newRequire = (id: string) => {
            if (id === '@atlassian/clientside-extensions-schema') {
                // eslint-disable-next-line global-require
                return require('../index');
            }

            return oldRequire(id);
        };

        // the "require" function contains additional properties we need to assign to the new function
        mod.require = Object.assign(newRequire, oldRequire);

        // @ts-expect-error - TS doesn't know about the "_compile" function
        // eslint-disable-next-line no-underscore-dangle
        mod._compile(transpiledModule.outputText, 'no file name');

        return mod.exports.validate;
    };

    describe('Base', () => {
        const validatorFn = getExecutableValidatorFromSchema(`
type Schema {
    foo: Number
}
`);
        it('should abort generation if base type "Schema" is missing', () => {
            expect(() => {
                getExecutableValidatorFromSchema(`
type Schoma {
    foo: Number
}
`);
            }).toThrow();
        });

        it('should create a module exporting a callable validation function', () => {
            expect(validatorFn).not.toThrow();
        });

        it('should return an array of errors and warnings found during the validation', () => {
            expect(validatorFn()).toEqual({
                errors: expect.toBeArray(),
                warnings: expect.toBeArray(),
            });
        });

        it('should report if no value is provided', () => {
            const { errors } = validatorFn();

            expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                error: '"Schema" failed to validate. Expected an object of type "Schema".',
                severity: 'ERROR',
            });
        });

        it('should report an empty arrays if the value passes validation', () => {
            const { errors, warnings } = validatorFn({});

            expect(errors).toBeArray().toBeEmpty();
            expect(warnings).toBeArray().toBeEmpty();
        });
    });

    describe('Scalar validation-type attributes', () => {
        const getError = (expectedType: string, invalidFoo: unknown) => {
            return `"Schema" failed to validate. Attribute "foo" expected a value of type ${expectedType}, but received: ${JSON.stringify(
                invalidFoo,
            )}`;
        };
        cases(
            'should correctly validate all supported validation types',
            (opts) => {
                const validatorFn = getExecutableValidatorFromSchema(`type Schema {foo: ${opts.type}}`);
                /* eslint-disable jest/no-standalone-expect */
                const validResults = validatorFn(opts.validPayload);
                expect(validResults.errors).toBeArray().toBeEmpty();
                expect(validResults.warnings).toBeArray().toBeEmpty();

                const invalidResult = validatorFn(opts.invalidPayload);

                expect(invalidResult.errors)
                    .toBeArray()
                    .toHaveLength(1)
                    .toContainEqual({
                        severity: 'ERROR',
                        error: getError(opts.type, opts.invalidPayload.foo),
                    });
                expect(invalidResult.warnings).toBeArray().toBeEmpty();
                /* eslint-enable jest/no-standalone-expect */
            },
            {
                boolean: {
                    validPayload: { foo: true },
                    invalidPayload: { foo: 'bar' },
                    type: 'Boolean',
                },
                number: {
                    validPayload: { foo: 1 },
                    invalidPayload: { foo: 'bar' },
                    type: 'Number',
                },
                string: {
                    validPayload: { foo: 'bar' },
                    invalidPayload: { foo: 1 },
                    type: 'String',
                },
                date: {
                    validPayload: { foo: new Date() },
                    invalidPayload: { foo: 'bar' },
                    type: 'Date',
                },
                regexp: {
                    validPayload: { foo: /1/ },
                    invalidPayload: { foo: 'bar' },
                    type: 'RegExp',
                },
                promise: {
                    validPayload: { foo: Promise.resolve() },
                    invalidPayload: { foo: 'bar' },
                    type: 'Promise',
                },
                function: {
                    validPayload: { foo() {} },
                    invalidPayload: { foo: 'bar' },
                    type: 'Function',
                },
                constant: {
                    validPayload: { foo: 'async-panel' },
                    invalidPayload: { foo: 'bar' },
                    type: 'AsyncPanelExtension',
                },
            },
        );
    });

    describe('Enum type attributes', () => {
        const validatorFn = getExecutableValidatorFromSchema(`
enum Foo {
    bar
    baz
}

type Schema {
    foo: Foo
}
`);
        test.each(['bar', 'baz'])('should pass validation if specified correctly. Input: %s', (val) => {
            const { errors, warnings } = validatorFn({ foo: val });

            expect(errors).toBeArray().toBeEmpty();
            expect(warnings).toBeArray().toBeEmpty();
        });

        test.each(['foo', 'something'])('should fail validation if specified incorrect value. Input: %s', (val) => {
            const { errors, warnings } = validatorFn({ foo: val });

            expect(errors)
                .toHaveLength(1)
                .toContainEqual({
                    error: `"Schema" failed to validate. Attribute "foo" expected one of the following values: "bar", "baz". Received value: ${JSON.stringify(
                        val,
                        stringFormatter,
                    )}`,
                    severity: 'ERROR',
                });
            expect(warnings).toBeArray().toBeEmpty();
        });

        test.each([1, true, [], () => {}])('should fail validation if specified a non-string value. Input: %j', (val) => {
            const { errors, warnings } = validatorFn({ foo: val });

            expect(errors)
                .toHaveLength(1)
                .toContainEqual({
                    error: `"Schema" failed to validate. Attribute "foo" expected one of the following values: "bar", "baz". Received value: ${JSON.stringify(
                        val,
                        stringFormatter,
                        2,
                    )}`,
                    severity: 'ERROR',
                });
            expect(warnings).toBeArray().toBeEmpty();
        });
    });

    describe('Object type', () => {
        describe('excess attributes', () => {
            const validatorFn = getExecutableValidatorFromSchema(
                `
type Foo {
    baz: String
}

type Schema {
    foo: String!
    bar: Number
    foo2: Foo
}
`,
                'Schema',
                {
                    strictMode: true,
                    checkExcessiveKeys: true,
                },
            );
            cases(
                'should pass validation if specified correctly',
                (val) => {
                    const { errors, warnings } = validatorFn(val);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [{ foo: 'asd' }, { foo: 'asd', bar: 2 }],
            );

            cases(
                'should fail validation if excessive keys are specified',
                (val) => {
                    const { errors, warnings } = validatorFn(val.payload);

                    const expectedErrors = val.expectedErrors.map((error) => ({
                        severity: 'ERROR',
                        error,
                    }));

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toHaveLength(expectedErrors.length).toEqual(expectedErrors);
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: { baz: 1 },
                        expectedErrors: [
                            '"Schema". Excessive key [baz] is not specified in the schema.',
                            '"Schema" failed to validate. Attribute "foo" of type String! is missing in "Schema".',
                        ],
                    },
                    {
                        payload: { foo: 'asd', baz: 1 },
                        expectedErrors: ['"Schema". Excessive key [baz] is not specified in the schema.'],
                    },
                    {
                        payload: { foo: 'asd', baz: 1, bazinga: 'foo' },
                        expectedErrors: ['"Schema". Excessive keys [baz, bazinga] are not specified in the schema.'],
                    },
                    {
                        payload: { foo: 'asd', foo2: { bar: 'asd' } },
                        expectedErrors: ['"Schema" failed on attribute "foo2". "Foo". Excessive key [bar] is not specified in the schema.'],
                    },
                    {
                        payload: { foo: 'asd', foo2: { bar: 'asd', foo: 2 } },
                        expectedErrors: [
                            '"Schema" failed on attribute "foo2". "Foo". Excessive keys [bar, foo] are not specified in the schema.',
                        ],
                    },
                ],
            );
        });

        describe('attributes', () => {
            const validatorFn = getExecutableValidatorFromSchema(`
enum FooEnum {
    bar
    baz
}

type Foo {
    bar: Number
    baz: String
}

type Bar {
    foo: FooEnum!
    baz: Boolean!
}

type Schema {
    foo: Foo!
    bar: Bar
}
`);
            cases(
                'should pass validation if specified correctly',
                (val) => {
                    const { errors, warnings } = validatorFn(val);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    { foo: {} },
                    { foo: {}, bar: { foo: 'bar', baz: false } },
                    { foo: { bar: 1 }, bar: { foo: 'baz', baz: true } },
                    { foo: { baz: '11' } },
                    { foo: { baz: '11', bar: 1 } },
                ],
            );

            cases(
                'should fail validation if values are missing',
                (val) => {
                    const { errors, warnings } = validatorFn(val.payload);
                    const expectedErrors = val.expectedErrors.map((error) => ({
                        severity: 'ERROR',
                        error,
                    }));

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(expectedErrors.length).toEqual(expectedErrors);
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedErrors: ['"Schema" failed to validate. Attribute "foo" of type Foo! is missing in "Schema".'],
                    },
                    {
                        payload: { bar: { foo: 'bar', baz: false } },
                        expectedErrors: ['"Schema" failed to validate. Attribute "foo" of type Foo! is missing in "Schema".'],
                    },
                    {
                        payload: { foo: { bar: 1 }, bar: { baz: true } },
                        expectedErrors: [
                            '"Schema" failed on attribute "bar". "Bar" failed to validate. Attribute "foo" of type FooEnum! is missing in "Bar".',
                        ],
                    },
                    {
                        payload: { foo: { bar: 1 }, bar: { foo: true } },
                        expectedErrors: [
                            '"Schema" failed on attribute "bar". "Bar" failed to validate. Attribute "foo" expected one of the following values: "bar", "baz". Received value: true',

                            '"Schema" failed on attribute "bar". "Bar" failed to validate. Attribute "baz" of type Boolean! is missing in "Bar".',
                        ],
                    },
                    {
                        payload: { foo: { baz: 11 }, bar: {} },
                        expectedErrors: [
                            '"Schema" failed on attribute "foo". "Foo" failed to validate. Attribute "baz" expected a value of type String, but received: 11',

                            '"Schema" failed on attribute "bar". "Bar" failed to validate. Attribute "foo" of type FooEnum! is missing in "Bar".',

                            '"Schema" failed on attribute "bar". "Bar" failed to validate. Attribute "baz" of type Boolean! is missing in "Bar".',
                        ],
                    },
                ],
            );

            cases(
                'should fail validation if specified invalid values',
                (val) => {
                    const { errors, warnings } = validatorFn(val.payload);

                    const expectedErrors = val.expectedErrors.map((error) => ({
                        severity: 'ERROR',
                        error,
                    }));

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(expectedErrors.length).toEqual(expectedErrors);
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: { foo: 1, bar: true },
                        expectedErrors: [
                            '"Schema" failed on attribute "foo". "Foo" failed to validate. Expected an object of type "Foo".',
                            '"Schema" failed on attribute "bar". "Bar" failed to validate. Expected an object of type "Bar".',
                        ],
                    },
                    {
                        payload: { foo: Promise.resolve() },
                        expectedErrors: ['"Schema" failed on attribute "foo". "Foo" failed to validate. Expected an object of type "Foo".'],
                    },
                ],
            );
        });
    });

    describe('Union type attributes', () => {
        const validatorFn = getExecutableValidatorFromSchema(`
enum Foo {
    bar
    baz
}

type FooType {
    foo: String!
    bar: Number
}

union FooUnion = Number | Foo

union BarUnion = Function | FooUnion | FooType

type Schema {
    foo: FooUnion!
    bar: BarUnion
}
`);

        cases(
            'should pass validation if specified correctly',
            (val) => {
                const { errors, warnings } = validatorFn(val.payload);

                /* eslint-disable jest/no-standalone-expect */
                expect(errors).toBeArray().toHaveLength(val.expectedErrors.length).toEqual(val.expectedErrors);
                expect(warnings).toBeArray().toBeEmpty();
                /* eslint-enable jest/no-standalone-expect */
            },
            [
                {
                    payload: { foo: 1 },
                    expectedErrors: [],
                },
                {
                    payload: { foo: 1, bar: 2 },
                    expectedErrors: [],
                },
                {
                    payload: { foo: 'bar' },
                    expectedErrors: [],
                },
                {
                    payload: { foo: 'baz' },
                    expectedErrors: [],
                },
                {
                    payload: { foo: 'baz', bar() {} },
                    expectedErrors: [],
                },
                {
                    payload: { foo: 'baz', bar: 'bar' },
                    expectedErrors: [],
                },
                {
                    payload: { foo: 'baz', bar: { foo: 'bar' } },
                    expectedErrors: [],
                },
            ],
        );

        cases(
            'should fail validation if specified invalid values',
            (val) => {
                const { errors, warnings } = validatorFn(val.payload);

                const expectedErrors = val.expectedErrors.map((error: string) => ({
                    severity: 'ERROR',
                    error,
                }));

                /* eslint-disable jest/no-standalone-expect */
                expect(errors).toBeArray().toHaveLength(expectedErrors.length).toEqual(expectedErrors);
                expect(warnings).toBeArray().toBeEmpty();
                /* eslint-enable jest/no-standalone-expect */
            },
            [
                {
                    payload: {},
                    expectedErrors: ['"Schema" failed to validate. Attribute "foo" of type FooUnion! is missing in "Schema".'],
                },
                {
                    payload: { foo: 'bar', bar: 'foo' },
                    expectedErrors: [
                        '"Schema" failed to validate. Attribute "bar" expected a value of type Function | FooUnion | FooType, but received: "foo"',
                    ],
                },
                {
                    payload: { foo: 'baz', bar: true },
                    expectedErrors: [
                        '"Schema" failed to validate. Attribute "bar" expected a value of type Function | FooUnion | FooType, but received: true',
                    ],
                },
                {
                    payload: { foo: 'foo', bar() {} },
                    expectedErrors: [
                        '"Schema" failed to validate. Attribute "foo" expected a value of type Number | Foo, but received: "foo"',
                    ],
                },
                {
                    payload: { bar: 'bar' },
                    expectedErrors: ['"Schema" failed to validate. Attribute "foo" of type FooUnion! is missing in "Schema".'],
                },
            ],
        );

        describe('strict mode', () => {
            const schema = `
                type Foo {
                    foo: String!
                }

                type Bar {
                    foo: String!
                    # bar is missing
                }

                union FooOrBar = Foo | Bar

                type Schema {
                    fooOrBar: FooOrBar!
                }
            `;

            const input = {
                fooOrBar: {
                    foo: 'foo',
                    bar: 'bar',
                },
            };

            it('should pass the validation in loose mode and with excessive keys enabled', () => {
                const validator = getExecutableValidatorFromSchema(schema, 'Schema', {
                    strictMode: false,
                    checkExcessiveKeys: true,
                });

                const { errors, warnings } = validator(input);

                expect(errors).toBeArray().toHaveLength(0).toEqual([]);
                expect(warnings).toBeArray().toBeEmpty();
            });

            it('should fail the validation in strict mode and with excessive keys enabled', () => {
                const validator = getExecutableValidatorFromSchema(schema, 'Schema', {
                    strictMode: true,
                    checkExcessiveKeys: true,
                });

                const { errors, warnings } = validator(input);

                expect(errors)
                    .toBeArray()
                    .toHaveLength(1)
                    .toEqual([
                        {
                            error: expect.stringContaining(
                                '"Schema" failed to validate. Attribute "fooOrBar" expected a value of type Foo | Bar, but received:',
                            ),
                            severity: 'ERROR',
                        },
                    ]);
                expect(warnings).toBeArray().toBeEmpty();
            });
        });
    });

    describe('Optional attributes', () => {
        const validatorFn = getExecutableValidatorFromSchema(`
type Schema {
    foo: Number
}
`);
        it('should pass validation if not specified', () => {
            const { errors, warnings } = validatorFn({});

            expect(errors).toBeArray().toBeEmpty();
            expect(warnings).toBeArray().toBeEmpty();
        });

        it('should pass validation if specified correctly', () => {
            const { errors, warnings } = validatorFn({ foo: 1 });

            expect(errors).toBeArray().toBeEmpty();
            expect(warnings).toBeArray().toBeEmpty();
        });

        it('should fail validation with an error when provided incorrect value', () => {
            const { errors, warnings } = validatorFn({ foo: 'bar' });

            expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                error: '"Schema" failed to validate. Attribute "foo" expected a value of type Number, but received: "bar"',
                severity: 'ERROR',
            });
            expect(warnings).toBeArray().toBeEmpty();
        });
    });

    describe('Required attributes', () => {
        const validatorFn = getExecutableValidatorFromSchema(`
type Schema {
    foo: Number!
}
`);
        it('should fail validation if not specified', () => {
            const { errors, warnings } = validatorFn({});

            expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                error: '"Schema" failed to validate. Attribute "foo" of type Number! is missing in "Schema".',
                severity: 'ERROR',
            });
            expect(warnings).toBeArray().toBeEmpty();
        });

        it('should pass validation if specified correctly', () => {
            const { errors, warnings } = validatorFn({ foo: 1 });

            expect(errors).toBeArray().toBeEmpty();
            expect(warnings).toBeArray().toBeEmpty();
        });

        it('should fail validation with an error when provided incorrect value', () => {
            const { errors, warnings } = validatorFn({ foo: 'bar' });

            expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                error: '"Schema" failed to validate. Attribute "foo" expected a value of type Number, but received: "bar"',
                severity: 'ERROR',
            });
            expect(warnings).toBeArray().toBeEmpty();
        });
    });

    describe('Lists of attributes', () => {
        describe('Optional list', () => {
            const schema = `type Schema { foo: [Number]}`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct (%j)',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [{}, { foo: [] }, { foo: [1, 2] }],
            );

            cases(
                'should fail validation if input is incorrect (%s)',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: { foo: 'bar' },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [Number], but received: "bar"',
                    },
                    {
                        payload: { foo: [1, true] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [Number], but received: [1,true]',
                    },
                ],
            );
        });

        describe('Required list', () => {
            const schema = `type Schema { foo: [Number]!}`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [{ foo: [] }, { foo: [1, 2] }],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [Number]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: [false, 2] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [Number]!, but received: [false,2]',
                    },
                    {
                        payload: { foo: null },
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [Number]! is missing in "Schema".',
                    },
                ],
            );
        });

        describe('Required list with required values', () => {
            const schema = `type Schema { foo: [Number!]!}`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    // eslint-disable-next-line jest/no-standalone-expect
                    expect(errors).toBeArray().toBeEmpty();
                    // eslint-disable-next-line jest/no-standalone-expect
                    expect(warnings).toBeArray().toBeEmpty();
                },
                [{ foo: [1, 2] }, { foo: [] }],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [Number!]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: [null] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [Number!]!, but received: [null]',
                    },
                    {
                        payload: { foo: null },
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [Number!]! is missing in "Schema".',
                    },
                ],
            );
        });

        describe('Required list with nested nullable list', () => {
            const schema = `type Schema { foo: [[Number]]!}`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },

                [{ foo: [null] }, { foo: [[1, null, 2]] }],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [[Number]]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: null },
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [[Number]]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: [1] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number]]!, but received: [1]',
                    },
                ],
            );
        });

        describe('Required list with nested required list', () => {
            const schema = `type Schema { foo: [[Number]!]!}`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [{ foo: [[]] }, { foo: [[null]] }, { foo: [[1]] }],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [[Number]!]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: null },
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [[Number]!]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: [null] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number]!]!, but received: [null]',
                    },
                    {
                        payload: { foo: [[2], null] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number]!]!, but received: [[2],null]',
                    },
                    {
                        payload: { foo: [[false, 2]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number]!]!, but received: [[false,2]]',
                    },
                ],
            );
        });

        describe('Required list with nested required list with nullable values', () => {
            const schema = `type Schema { foo: [[Number!]]!}`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [{ foo: [] }, { foo: [null] }, { foo: [[], null, []] }, { foo: [[1]] }],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [[Number!]]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: null },
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [[Number!]]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: [[null]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number!]]!, but received: [[null]]',
                    },
                    {
                        payload: { foo: [[false, 2]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number!]]!, but received: [[false,2]]',
                    },
                ],
            );
        });

        describe('Required list with nested required list with required values', () => {
            const schema = `type Schema { foo: [[Number!]!]!}`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [{ foo: [] }, { foo: [[]] }, { foo: [[1, 2, 3]] }],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [[Number!]!]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: null },
                        expectedError: '"Schema" failed to validate. Attribute "foo" of type [[Number!]!]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: [null] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number!]!]!, but received: [null]',
                    },
                    {
                        payload: { foo: [[null]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number!]!]!, but received: [[null]]',
                    },
                    {
                        payload: { foo: [[2, null]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[Number!]!]!, but received: [[2,null]]',
                    },
                ],
            );
        });

        describe('Complex list deep - please do not try this at home', () => {
            const schema = `type Schema { foo: [[[[[[Number!]!]]]!]]!}`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    { foo: [] },
                    { foo: [null] },
                    { foo: [[[]]] },
                    { foo: [[[null]]] },
                    { foo: [[[[]]]] },
                    { foo: [[[[null]]]] },
                    {
                        foo: [[[[[[1, 2]], [[1, 2]], []]], [[null]]], [[]], null],
                    },
                ],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" of type [[[[[[Number!]!]]]!]]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: null },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" of type [[[[[[Number!]!]]]!]]! is missing in "Schema".',
                    },
                    {
                        payload: { foo: 'bar' },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[[[[[Number!]!]]]!]]!, but received: "bar"',
                    },
                    {
                        payload: { foo: [[null]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[[[[[Number!]!]]]!]]!, but received: [[null]]',
                    },
                    {
                        payload: { foo: [[[[[null]]]]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[[[[[Number!]!]]]!]]!, but received: [[[[[null]]]]]',
                    },
                    {
                        payload: { foo: [[[[[['bar']]]]]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [[[[[[Number!]!]]]!]]!, but received: [[[[[["bar"]]]]]]',
                    },
                ],
            );
        });

        describe('Complex list types - please do not try this at home', () => {
            const schema = `
enum Foo {
    bar
    baz
}

type FooType {
    foo: String!
    bar: Number
}

union FooUnion = Number | Foo

union BarUnion = Function | FooUnion | FooType

type Schema {
    foo: [FooUnion]!
    bar: [[BarUnion!]]!
}
`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    { foo: [], bar: [] },
                    { foo: [null], bar: [[1, 'bar', () => {}, { foo: 'bar', bar: 2 }]] },
                    { foo: [null], bar: [['bar', 'baz']] },
                ],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: { foo: [], bar: [[null]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "bar" expected a value of type [[BarUnion!]]!, but received: [[null]]',
                    },
                    {
                        payload: { foo: ['foo'], bar: [null] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "foo" expected a value of type [FooUnion]!, but received: ["foo"]',
                    },
                    {
                        payload: { foo: [null], bar: [['foo']] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "bar" expected a value of type [[BarUnion!]]!, but received: [["foo"]]',
                    },
                    {
                        payload: { foo: [null], bar: [[{ foo: 2 }]] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "bar" expected a value of type [[BarUnion!]]!, but received: [[{"foo":2}]]',
                    },
                    {
                        payload: { foo: [null], bar: [['bar', 'baz', 'foo']] },
                        expectedError:
                            '"Schema" failed to validate. Attribute "bar" expected a value of type [[BarUnion!]]!, but received: [["bar","baz","foo"]]',
                    },
                ],
            );
        });

        describe('Circular dependencies', () => {
            const schema = `
type Comment {
    id: Number!
    comments: [Comment]
}

type Schema {
    comment: Comment!
}
`;
            const validatorFn = getExecutableValidatorFromSchema(schema);

            cases(
                'should pass validation if input is correct',
                (input) => {
                    const { errors, warnings } = validatorFn(input);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toBeEmpty();
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    { comment: { id: 1 } },
                    { comment: { id: 2, comments: [{ id: 2, comments: [] }] } },
                    {
                        comment: {
                            id: 2,
                            comments: [
                                { id: 2, comments: [] },
                                { id: 3, comments: [] },
                            ],
                        },
                    },
                    {
                        comment: {
                            id: 2,
                            comments: [
                                { id: 2, comments: [] },
                                {
                                    id: 3,
                                    comments: [
                                        { id: 4, comments: [] },
                                        { id: 5, comments: [{ id: 6, comments: [] }] },
                                    ],
                                },
                            ],
                        },
                    },
                ],
            );

            cases(
                'should fail validation if input is incorrect',
                (input) => {
                    const { errors, warnings } = validatorFn(input.payload);

                    /* eslint-disable jest/no-standalone-expect */
                    expect(errors).toBeArray().toHaveLength(1).toContainEqual({
                        error: input.expectedError,
                        severity: 'ERROR',
                    });
                    expect(warnings).toBeArray().toBeEmpty();
                    /* eslint-enable jest/no-standalone-expect */
                },
                [
                    {
                        payload: {},
                        expectedError: '"Schema" failed to validate. Attribute "comment" of type Comment! is missing in "Schema".',
                    },
                    {
                        payload: { comment: { id: 'asd' } },
                        expectedError:
                            '"Schema" failed on attribute "comment". "Comment" failed to validate. Attribute "id" expected a value of type Number, but received: "asd"',
                    },
                    {
                        payload: { comment: { id: 1, comments: [{ comment: { id: 2, comments: [] } }] } },
                        expectedError:
                            '"Schema" failed on attribute "comment". "Comment" failed to validate. Attribute "comments" expected a value of type [Comment], but received: [{"comment":{"id":2,"comments":[]}}]',
                    },
                    {
                        payload: {
                            comment: {
                                id: 1,
                                comments: [
                                    { id: 2, comments: [] },
                                    { id: '3', comments: [] },
                                ],
                            },
                        },
                        expectedError:
                            '"Schema" failed on attribute "comment". "Comment" failed to validate. Attribute "comments" expected a value of type [Comment], but received: [{"id":2,"comments":[]},{"id":"3","comments":[]}]',
                    },
                    {
                        payload: {
                            comment: {
                                id: 1,
                                comments: [
                                    { id: 2, comments: [] },
                                    {
                                        id: 3,
                                        comments: [
                                            { id: 4, comments: [] },
                                            { id: '5', comments: [] },
                                        ],
                                    },
                                ],
                            },
                        },
                        expectedError:
                            '"Schema" failed on attribute "comment". "Comment" failed to validate. Attribute "comments" expected a value of type [Comment], but received: [{"id":2,"comments":[]},{"id":3,"comments":[{"id":4,"comments":[]},{"id":"5","comments":[]}]}]',
                    },
                    {
                        payload: {
                            comment: {
                                id: 1,
                                comments: [
                                    { id: 2, comments: [] },
                                    {
                                        id: 3,
                                        comments: [
                                            { id: 4, comments: [] },
                                            { id: 5, comments: [{ id: '6', comments: [] }] },
                                        ],
                                    },
                                ],
                            },
                        },
                        expectedError:
                            '"Schema" failed on attribute "comment". "Comment" failed to validate. Attribute "comments" expected a value of type [Comment], but received: [{"id":2,"comments":[]},{"id":3,"comments":[{"id":4,"comments":[]},{"id":5,"comments":[{"id":"6","comments":[]}]}]}]',
                    },
                ],
            );
        });

        describe('strict mode', () => {
            const schema = `
                type Bar {
                    foo: String!
                    # bar is missing
                }

                type Schema {
                    bars: [Bar]
                }
            `;

            const input = {
                bars: [
                    {
                        foo: 'foo',
                    },
                    {
                        foo: 'foo',
                        bar: 'bar',
                    },
                    {
                        foo: 'foo',
                    },
                ],
            };

            it('should pass the validation in loose mode and with excessive keys enabled', () => {
                const validator = getExecutableValidatorFromSchema(schema, 'Schema', {
                    strictMode: false,
                    checkExcessiveKeys: true,
                });

                const { errors, warnings } = validator(input);

                expect(errors).toBeArray().toHaveLength(0).toEqual([]);
                expect(warnings).toBeArray().toBeEmpty();
            });

            it('should fail the validation in strict mode and with excessive keys enabled', () => {
                const validator = getExecutableValidatorFromSchema(schema, 'Schema', {
                    strictMode: true,
                    checkExcessiveKeys: true,
                });

                const { errors, warnings } = validator(input);

                expect(errors)
                    .toBeArray()
                    .toHaveLength(1)
                    .toEqual([
                        {
                            error: expect.stringContaining(
                                '"Schema" failed to validate. Attribute "bars" expected a value of type [Bar], but received:',
                            ),
                            severity: 'ERROR',
                        },
                    ]);
                expect(warnings).toBeArray().toBeEmpty();
            });
        });
    });

    describe('strict mode', () => {
        describe('with excessive keys turned on', () => {
            const schema = `
                type ContextSchema {
                    foo: String
                    # bar: String # missing key
                }
            `;

            const input = {
                foo: 'foo',
                bar: 'bar',
            };

            it('should generate validator in a loose mode and return a warning', () => {
                const validatorFn = getExecutableValidatorFromSchema(schema, 'ContextSchema', {
                    strictMode: false,
                    checkExcessiveKeys: true,
                });

                const { errors, warnings } = validatorFn(input);

                expect(errors).toBeEmpty();
                expect(warnings).toContainEqual({
                    severity: 'WARNING',
                    warning: '"ContextSchema". Excessive key [bar] is not specified in the schema.',
                });
            });

            it('should generate validator in a strict mode and return an error', () => {
                const validatorFn = getExecutableValidatorFromSchema(schema, 'ContextSchema', {
                    strictMode: true,
                    checkExcessiveKeys: true,
                });

                const { errors, warnings } = validatorFn(input);
                expect(warnings).toBeEmpty();
                expect(errors).toContainEqual({
                    error: '"ContextSchema". Excessive key [bar] is not specified in the schema.',
                    severity: 'ERROR',
                });
            });
        });
    });
});
