import fs from 'fs';

// eslint-disable-next-line import/prefer-default-export
export const loadSchema = async (filePath: string) => {
    try {
        const schema = await fs.promises.readFile(filePath, 'utf8');
        if (!schema) {
            console.error(`File "${filePath}" does appear to contain any information. Skipping…`);
            return null;
        }

        return schema;
    } catch (e) {
        console.error(`Failed to read "${filePath}".`);
        return null;
    }
};
