export const objectValidator = () => {
    const name = 'validateObject';
    const fn = `
function ${name}(subject: any) {
    return typeof subject === "object" && subject !== null && Object.prototype.toString.call(subject) === "[object Object]"
}`;
    return {
        name,
        fn,
    };
};

export const stringValidator = () => {
    const name = 'validateString';
    const fn = `
function ${name}(subject: any) {
    return typeof subject === "string";
}`;
    return {
        name,
        fn,
    };
};

export const regexpValidator = () => {
    const name = 'validateRegexp';
    const fn = `
function ${name}(subject: any) {
    return typeof subject === "object" && Object.prototype.toString.call(subject) === "[object RegExp]";
}`;
    return {
        name,
        fn,
    };
};

export const promiseValidator = () => {
    const name = 'validatePromise';
    const fn = `
function ${name}(subject: any) {
    return typeof subject === "object" && Object.prototype.toString.call(subject) === "[object Promise]";
}`;
    return {
        name,
        fn,
    };
};

export const dateValidator = () => {
    const name = 'validateDate';
    const fn = `
function ${name}(subject: any) {
    return typeof subject === "object" && Object.prototype.toString.call(subject) === "[object Date]";
}`;
    return {
        name,
        fn,
    };
};

export const functionValidator = () => {
    const name = 'validateFunction';
    const fn = `
function ${name}(subject: any) {
    return typeof subject === "function";
}`;
    return {
        name,
        fn,
    };
};

export const booleanValidator = () => {
    const name = 'validateBoolean';
    const fn = `
function ${name}(subject: any) {
    return typeof subject === "boolean";
}`;
    return {
        name,
        fn,
    };
};

export const numberValidator = () => {
    const name = 'validateNumber';
    const fn = `
function ${name}(subject: any) {
    return typeof subject === "number" && !isNaN(subject);
}`;
    return {
        name,
        fn,
    };
};

export const arrayValidator = () => {
    const name = 'validateArray';
    const fn = `
function ${name}(subject: any) {
    return Array.isArray(subject);
}`;
    return {
        name,
        fn,
    };
};

export const constValueValidator = () => {
    const name = 'validateConstant';
    const fn = `
function ${name}(constantValue: any, subject: any) {
    return constantValue === subject;
}`;
    return {
        name,
        fn,
    };
};

export const enumValidator = () => {
    const name = 'validateEnum';
    const fn = `
function ${name}(enumValues: any[], subject: any) {
    return enumValues.includes(subject);
}`;
    return {
        name,
        fn,
    };
};

export const unionValidator = () => {
    const name = 'validateUnion';
    const fn = `
function ${name}(unionValidators: any[], subject: any) {
    return unionValidators.some(validator => validator(subject));
}`;
    return {
        name,
        fn,
    };
};

export const notNullCheck = () => {
    const name = 'nullCheck';
    const fn = `
function ${name}(subject: any) {
    return subject !== null;
}
`;
    return {
        name,
        fn,
    };
};

export const createRequiredElse = (name: string, parentName: string) => {
    return `else {
    throw new Error("validation failed. Expected '${name}' in '${parentName}'");
}`;
};
