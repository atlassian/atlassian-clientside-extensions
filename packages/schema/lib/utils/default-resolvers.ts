import { GraphQLScalarType } from 'graphql';
import type { IResolvers } from '@graphql-tools/utils';
import scalars from '../scalars';
import type { Scalar } from '../scalars/types';

const toGraphQlScalar = (scalar: Scalar) =>
    new GraphQLScalarType({
        name: scalar.name,
        description: scalar.description,
        parseValue: () => {},
        serialize(value: unknown) {
            return String(value); // unimportant
        },
        parseLiteral(ast: unknown) {
            return String(ast); // unimportant
        },
    });

const defaultResolvers: IResolvers = Object.fromEntries(Array.from(scalars).map(([name, scalar]) => [name, toGraphQlScalar(scalar)]));

export default defaultResolvers;
