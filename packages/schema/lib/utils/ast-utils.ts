import type {
    GraphQLField,
    GraphQLFieldMap,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLOutputType,
    GraphQLType,
} from 'graphql';
import { isEnumType, isListType, isNonNullType, isObjectType, isScalarType, isUnionType } from 'graphql';
import matter from 'gray-matter';
import pascalCase from './pascal-case';

export type NonNullNonListType = Exclude<GraphQLOutputType, GraphQLList<GraphQLOutputType> | GraphQLNonNull<GraphQLOutputType>>;
export type AcceptedTypes = GraphQLOutputType;

export const isListOrNonNullType = (typeObject: GraphQLType): typeObject is GraphQLList<GraphQLType> | GraphQLNonNull<GraphQLType> => {
    return isNonNullType(typeObject) || isListType(typeObject);
};

export const getTypeStack = (typeObject: AcceptedTypes): AcceptedTypes[] => {
    const typeStack: AcceptedTypes[] = [];
    let drillDownType = typeObject;
    while (isListOrNonNullType(drillDownType)) {
        typeStack.push(drillDownType);
        drillDownType = drillDownType.ofType;
    }
    typeStack.push(drillDownType);

    return typeStack;
};

export const getBaseType = (typeObject: AcceptedTypes) => {
    return getTypeStack(typeObject).pop() as NonNullNonListType;
};

export const isFirstRequired = (typeStack: AcceptedTypes[]) => {
    return isNonNullType(typeStack[0]);
};

export const isStackEndRequired = (typeStack: AcceptedTypes[]) => {
    if (typeStack.length < 2) {
        return false;
    }
    return isNonNullType(typeStack[typeStack.length - 2]);
};

export const containsListType = (typeStack: AcceptedTypes[]): boolean => {
    return typeStack.some((stackItem) => isListType(stackItem));
};

export enum BaseType {
    scalar,
    union,
    enum,
    object,
}

export const getBaseTypeType = (baseType: NonNullNonListType) => {
    if (isScalarType(baseType)) {
        return BaseType.scalar;
    }

    if (isUnionType(baseType)) {
        return BaseType.union;
    }

    if (isEnumType(baseType)) {
        return BaseType.enum;
    }

    if (isObjectType(baseType)) {
        return BaseType.object;
    }

    throw new Error(`The type: ${baseType} is not supported.`);
};

const getMetaFromDescription = (description: GraphQLField<unknown, unknown>['description']) => {
    const input = description || '';

    const { data, content } = matter(input);
    return { data, content };
};

export interface BaseTypeDescriptor {
    type: BaseType;
    reference: NonNullNonListType;
    name: string;
}

export interface ObjectType {
    name: string;
    type: GraphQLObjectType;
    description: string;
    meta: { [key: string]: unknown };
    descriptors: FieldDescriptor[];
}

export interface FieldDescriptor {
    required: boolean;
    description: string;
    meta: { [key: string]: unknown };
    name: string;
    typeString: string;
    typeStack: AcceptedTypes[];
    type: AcceptedTypes;
    baseType: BaseTypeDescriptor;
}

const getObjectTypeFieldDescriptors = (fields: GraphQLFieldMap<unknown, unknown>) => {
    return Object.entries(fields).map(([name, { type, description }]) => {
        const required = isNonNullType(type);
        const baseTypeObject = getBaseType(type);
        const { data, content } = getMetaFromDescription(description);
        return {
            name,
            required,
            type,
            description: content,
            meta: data,
            typeString: `${type}`,
            typeStack: getTypeStack(type),
            baseType: {
                type: getBaseTypeType(baseTypeObject),
                name: `${baseTypeObject}`,
                reference: baseTypeObject,
            },
        };
    });
};

export const getObjectTypeDescription = (objectType: GraphQLObjectType): ObjectType => {
    const { name, description } = objectType;
    const { data, content } = getMetaFromDescription(description);
    return {
        name: pascalCase(name),
        type: objectType,
        description: content,
        meta: data,
        descriptors: getObjectTypeFieldDescriptors(objectType.getFields()),
    };
};
