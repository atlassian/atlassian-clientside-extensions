import { isEnumType, isInputObjectType, isInterfaceType, isObjectType, isScalarType, isUnionType } from 'graphql';
import type SchemaDocument from './SchemaDocument';
import type { NonNullNonListType } from './ast-utils';
import { getBaseType, getObjectTypeDescription } from './ast-utils';

// eslint-disable-next-line import/prefer-default-export
export const populateDocument = (schemaDocument: SchemaDocument, schemaObject: NonNullNonListType) => {
    if (isObjectType(schemaObject)) {
        // circuit breaker
        if (schemaDocument.getObjectTypes().has(schemaObject.name)) {
            return schemaDocument;
        }

        schemaDocument.addObjectType(schemaObject.name, getObjectTypeDescription(schemaObject));

        Object.values(schemaObject.getFields())
            .map((field) => getBaseType(field.type))
            .forEach((fieldBase) => {
                populateDocument(schemaDocument, fieldBase);
            });

        return schemaDocument;
    }

    if (isScalarType(schemaObject)) {
        schemaDocument.addScalar(schemaObject);
        return schemaDocument;
    }

    if (isEnumType(schemaObject)) {
        schemaDocument.addEnumType(schemaObject);
        return schemaDocument;
    }

    if (isUnionType(schemaObject)) {
        schemaDocument.addUnionType(schemaObject);
        // typings here are broken?!
        (schemaObject.getTypes() as unknown as NonNullNonListType[]).forEach((fieldBase) => {
            populateDocument(schemaDocument, fieldBase);
        });
        return schemaDocument;
    }

    if (isInterfaceType(schemaObject)) {
        throw new Error(`Encountered unsupported declaration: ${schemaObject}. Interface types are currently not supported! Aborting.`);
    }

    if (isInputObjectType(schemaObject)) {
        throw new Error(`Encountered unsupported declaration: ${schemaObject}. Input types are currently not supported! Aborting.`);
    }

    throw new Error(`Encountered unsupported declaration: ${schemaObject}. Failed to populate document. Aborting.`);
};
