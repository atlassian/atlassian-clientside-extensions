export type ValidationOptions = {
    checkExcessiveKeys: boolean;
    strictMode: boolean;
};

export enum ErrorLevels {
    ERROR = 'ERROR',
    WARNING = 'WARNING',
}

export interface ValidationError {
    severity: ErrorLevels.ERROR;
    error: string;
}

export interface ValidationWarning {
    severity: ErrorLevels.WARNING;
    warning: string;
}

export interface ValidationResult {
    warnings: ValidationWarning[];
    errors: ValidationError[];
}

export type Validator = (input?: unknown) => ValidationResult;
