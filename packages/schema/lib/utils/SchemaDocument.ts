import type { GraphQLEnumType, GraphQLScalarType, GraphQLUnionType } from 'graphql';
import type { ObjectType } from './ast-utils';

class SchemaDocument {
    protected objectTypes = new Map<string, ObjectType>();

    protected enumTypes = new Set<GraphQLEnumType>();

    protected unionTypes = new Set<GraphQLUnionType>();

    protected scalars = new Set<GraphQLScalarType>();

    getObjectTypes() {
        return this.objectTypes;
    }

    getEnumTypes() {
        return this.enumTypes;
    }

    getUnionTypes() {
        return this.unionTypes;
    }

    getScalars() {
        return this.scalars;
    }

    addScalar(scalar: GraphQLScalarType) {
        this.scalars.add(scalar);
    }

    addObjectType(name: string, objectType: ObjectType) {
        this.objectTypes.set(name, objectType);
    }

    addEnumType(enumType: GraphQLEnumType) {
        this.enumTypes.add(enumType);
    }

    addUnionType(unionType: GraphQLUnionType) {
        this.unionTypes.add(unionType);
    }

    getObjectType(name: string) {
        return this.objectTypes.get(name);
    }

    hasObjectType(name: string) {
        return this.objectTypes.has(name);
    }
}

export default SchemaDocument;
