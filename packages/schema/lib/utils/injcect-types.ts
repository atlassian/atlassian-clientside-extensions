const injectTypes = (query: string, types: string[]) => {
    return `
${types.length > 0 ? types.join('\n') : ''}

${query}`;
};

export default injectTypes;
