import { glob } from 'glob';
import type { Scalar } from './scalars/types';

const scalarToDefinitionString = (scalar: Scalar) => {
    return `
"""
${scalar.description}
Example: ${typeof scalar.defaultExample === 'string' ? JSON.stringify(scalar.defaultExample) : scalar.defaultExample}
"""
scalar ${scalar.name}
`;
};

const getScalars = async () => {
    const scalarFiles = await glob('./scalars/**/*.js', { cwd: __dirname, dotRelative: true });
    const scalars = (
        await Promise.all(
            scalarFiles.map(async (filePath) => {
                return (await import(filePath)).default as Scalar;
            }),
        )
    ).filter(Boolean);

    return scalars.map(scalarToDefinitionString).join('\n\n');
};

const getProvidedTypes = async () => {
    const providedTypesFiles = await glob('./providedTypes/*.js', { cwd: __dirname, dotRelative: true });
    const providedTypes = await Promise.all(
        providedTypesFiles.map(async (filePath) => {
            return (await import(filePath)).default as string;
        }),
    );

    return providedTypes.join('\n\n');
};

export default async () => {
    const scalarDefinitions = await getScalars();
    const providedTypesDefinitions = await getProvidedTypes();

    return `
###########################################################
# GENERATED: Scalar definitions for clientside extensions #
###########################################################
${scalarDefinitions}

############################################################################
# GENERATED: Other provided types, enums, unions for clientside extensions #
############################################################################
${providedTypesDefinitions}
`;
};
