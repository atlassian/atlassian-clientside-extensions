import getDefinitions from './type-definitions';

describe('generate-module-definitions-file', () => {
    describe('react definitions', () => {
        const definitions = getDefinitions('react');

        it('should generate react definitions', () => {
            expect(definitions).toMatchSnapshot();
        });

        // Typescript tries to be as smart as possible when generating type-definitions.
        // Missing types or other "faults" will be replaced by any/unknown types.
        // While this might be generally good UX from typescript, this is not what we want, as
        // any "any"/"unknown" type indicated that the underlying generated code is faulty.
        // Therefore, we check that no such thing exists.
        it('should not contain "any" or "unknown"', () => {
            expect(definitions).not.toMatch(/\Wany\W/);
            expect(definitions).not.toMatch(/\Wunknown\W/);
        });
    });
});
