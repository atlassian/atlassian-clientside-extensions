import type { CompilerOptions } from 'typescript';
import ts from 'typescript';
import { basename, extname } from 'path';
import generateReactExtensionWrapper from './react';

const removeNestedDeclareKeywords = (definitions: string): string => {
    const declareConstRegExp = /export declare const/g;
    const declareConstReplacer = 'export const';

    const declareTypeRegExp = /declare type/g;
    const declareTypeReplacer = 'type';

    const regExps: [RegExp, string][] = [
        [declareConstRegExp, declareConstReplacer],
        [declareTypeRegExp, declareTypeReplacer],
    ];

    for (const [regExp, replacer] of regExps) {
        definitions = definitions.replace(regExp, replacer);
    }

    return definitions;
};

const wrapInModuleDefinition = (definitions: string, moduleGlob: string) => {
    return `declare module '${moduleGlob}' {
${removeNestedDeclareKeywords(definitions).replace(/^/gm, ' '.repeat(4))}
}
`;
};

// Generate a fake wrapper and make typescript generate typings for it
const generateModuleDescriptor = (code: string) => {
    const baseFilename = `FAKE_NAME${Math.floor(Math.random() * 1000)}`;

    let definition = '';

    const compilerOptions: CompilerOptions = {
        allowJs: true,
        declaration: true,
        emitDeclarationOnly: true,
        strict: true,

        // We don't want to check the declarations files at this point,
        // https://github.com/microsoft/TypeScript/wiki/Performance#skipping-dts-checking
        skipLibCheck: true,

        // We don't want to put all the types definitions by default
        // https://github.com/microsoft/TypeScript/wiki/Performance#controlling-types-inclusion
        types: [],
    };

    const host = ts.createCompilerHost(compilerOptions);
    const oldRead = host.readFile;
    host.readFile = (fileName: string) => {
        if (baseFilename === basename(fileName, extname(fileName))) {
            return code;
        }
        return oldRead(fileName);
    };
    host.writeFile = (fileName: string, contents: string) => {
        if (basename(fileName).startsWith(baseFilename) && fileName.endsWith('.d.ts')) {
            definition = contents;
        }
    };

    // Prepare and emit the d.ts files
    const program = ts.createProgram([baseFilename], compilerOptions, host);
    program.emit();

    return definition;
};

const getReactDefinitions = () => {
    return generateModuleDescriptor(generateReactExtensionWrapper(null, null, null, null));
};

// Generate a fake wrapper and make typescript generate typings for it
export default function getDefinitions(moduleGlob: string) {
    return wrapInModuleDefinition(getReactDefinitions(), moduleGlob);
}
