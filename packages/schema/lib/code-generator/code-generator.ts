import * as ts from 'typescript';
import { populateDocument } from '../utils/schema-document-populator';
import { getSchemaObject, SchemaDefinitionMissingError } from '../utils/create-schema';
import { createValidator } from '../utils/validator-generator';
import { serializeSchemaDocument } from '../utils/schema-document-serializer';
import SchemaDocument from '../utils/SchemaDocument';
import injectTypes from '../utils/injcect-types';
import { loadSchema } from '../utils/schema';
import { isNotNull } from '../utils';
import type { ValidationOptions } from '../utils/validator-types';
import generateReactExtensionWrapper from './react';

const generateJavascript = (source: string) => {
    const result = ts.transpileModule(source, { compilerOptions: { module: ts.ModuleKind.CommonJS } });
    return result.outputText;
};

const schemaToDocument = (schemaString: string, baseTypeName: string) => {
    const schema = getSchemaObject(schemaString, baseTypeName);
    const schemaDocument = new SchemaDocument();
    populateDocument(schemaDocument, schema);
    return schemaDocument;
};

const getExtensionPointName = (schemaString: string): string | undefined => {
    const document = schemaToDocument(schemaString, 'Schema');
    return document.getObjectType('Schema')?.meta?.extensionPoint as string;
};

const createValidatorForSchema = (schemaString: string, baseTypeName: string, options: ValidationOptions) => {
    return createValidator(schemaToDocument(schemaString, baseTypeName), baseTypeName, options);
};

type ValidatorGeneratorOptions = {
    strictMode: boolean;
};

const generateValidators = async (extensionPoint: string, schema: string, fileWriter: FileWriter, options: ValidatorGeneratorOptions) => {
    const attributeValidationOptions: ValidationOptions = {
        ...options,
        checkExcessiveKeys: false, // TODO: This should be enabled in the future
    };
    const attributeValidatorTypescript = createValidatorForSchema(schema, 'Schema', attributeValidationOptions);

    const validatorFile = await fileWriter(
        `${extensionPoint}_validator`,
        attributeValidatorTypescript,
        generateJavascript(attributeValidatorTypescript),
    );

    try {
        const contextValidationOptions: ValidationOptions = {
            ...options,
            checkExcessiveKeys: true,
        };
        const contextValidatorTypescript = createValidatorForSchema(schema, 'ContextSchema', contextValidationOptions);
        const contextValidatorFile = await fileWriter(
            `${extensionPoint}_validator_context`,
            contextValidatorTypescript,
            generateJavascript(contextValidatorTypescript),
        );
        return [validatorFile, contextValidatorFile];
    } catch (e) {
        // we allow for a missing context schema, as not all extension points have a context
        if (e instanceof SchemaDefinitionMissingError) {
            return [validatorFile];
        }

        // else rethrow
        throw e;
    }
};

const getSchemaDocumentContent = (schemaDocument: string, contextSchemaDocument: string) => `
import { SerializedDocument } from '@atlassian/clientside-extensions-schema';

const schemaDocument: SerializedDocument = ${schemaDocument};

const contextSchemaDocument: SerializedDocument = ${contextSchemaDocument}

export {schemaDocument, contextSchemaDocument }
`;

const generateSchemaDocuments = async (extensionPoint: string, schema: string, fileWriter: FileWriter) => {
    const schemaDocument = serializeSchemaDocument(extensionPoint, schema, 'Schema');
    const contextSchemaDocument = serializeSchemaDocument(extensionPoint, schema, 'ContextSchema');

    const schemaDocumentsTypescript = getSchemaDocumentContent(
        JSON.stringify(schemaDocument, null, 4),
        JSON.stringify(contextSchemaDocument, null, 4),
    );
    const schemaDocumentsFile = await fileWriter(
        `${extensionPoint}_schema_documents`,
        schemaDocumentsTypescript,
        generateJavascript(schemaDocumentsTypescript),
    );

    return schemaDocumentsFile as string;
};

export type FileWriter = (baseFilename: string, typescriptCode: string, javascriptCode: string) => Promise<string | null>;

export interface GeneratorOptions {
    providedTypesPaths: string[];
    strictMode: boolean;
    validatorOnly: boolean;
}

const defaultOptions: GeneratorOptions = {
    providedTypesPaths: [],
    strictMode: true,
    validatorOnly: false,
};

const getGeneratedCode = async (
    attributeValidatorLocation: string | null,
    contextValidatorLocation: string | null,
    extensionPoint: string,
    schema: string,
    fileWriter: FileWriter,
) => {
    const schemaDocumentsFile = await generateSchemaDocuments(extensionPoint, schema, fileWriter);
    return generateReactExtensionWrapper(attributeValidatorLocation, contextValidatorLocation, extensionPoint, schemaDocumentsFile);
};

type GeneratedReturnType = [extensionPoint: string, generatedTsCdode: string, generatedJsCode: string];

type ReturnWithoutWrite<WriteBaseT extends boolean> = WriteBaseT extends true ? void : GeneratedReturnType;

async function generateCode<WriteBaseT extends boolean>(
    schema: string,
    options: Partial<GeneratorOptions>,
    fileWriter: FileWriter,
    writeBase: WriteBaseT,
): Promise<ReturnWithoutWrite<WriteBaseT>> {
    const { providedTypesPaths, strictMode, validatorOnly }: GeneratorOptions = { ...defaultOptions, ...options };
    if (!writeBase && validatorOnly) {
        throw new Error('The "validatorOnly" option is not available when "writeBase" is not true!');
    }

    // Extend schema with custom provided types
    const maybeProvidedTypesSchema: (string | null)[] = await Promise.all(providedTypesPaths.map((filePath) => loadSchema(filePath)));
    const providedTypesSchema: string[] = maybeProvidedTypesSchema.filter(isNotNull);
    const baseSchema = injectTypes(schema, providedTypesSchema);

    // Read the extension point
    const extensionPoint = getExtensionPointName(baseSchema);

    if (!extensionPoint) {
        throw new Error(`Could not extract the extension point from your provided schema.
Please ensure you specify the extensionPoint as part of the meta data within the description of your Schema.

Example:

"""
---
extensionPoint: specify-your-extension-point-name-here
---
This is the description of your Schema
"""
type Schema {
  // content of your schema…
}
`);
    }

    const validatorGeneratorOptions: ValidatorGeneratorOptions = {
        strictMode,
    };

    const [validatorFile, contextValidatorFile] = await generateValidators(
        extensionPoint,
        baseSchema,
        fileWriter,
        validatorGeneratorOptions,
    );

    if (validatorOnly) {
        return undefined as ReturnWithoutWrite<WriteBaseT>;
    }

    const generatedCode = await getGeneratedCode(validatorFile, contextValidatorFile, extensionPoint, baseSchema, fileWriter);

    if (writeBase) {
        await fileWriter(extensionPoint, generatedCode, generateJavascript(generatedCode));
        return undefined as ReturnWithoutWrite<WriteBaseT>;
    }

    return [extensionPoint, generatedCode, generateJavascript(generatedCode)] as ReturnWithoutWrite<WriteBaseT>;
}

export default generateCode;
