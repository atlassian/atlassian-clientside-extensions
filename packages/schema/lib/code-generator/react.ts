const generateReactExtensionWrapper = (
    attributeValidatorLocation: string | null,
    contextValidatorLocation: string | null,
    extensionPoint: string | null,
    schemaDocumentsFile: string | null,
) => {
    const extendedSchemaContextOption: string[] = [];
    const validationImport = attributeValidatorLocation
        ? `import { validate as attributeValidator } from '${attributeValidatorLocation}';`
        : '';
    if (attributeValidatorLocation) {
        extendedSchemaContextOption.push('attributeValidator');
    }

    const contextValidationImport = contextValidatorLocation
        ? `import { validate as contextValidator } from '${contextValidatorLocation}';`
        : '';
    if (contextValidatorLocation) {
        extendedSchemaContextOption.push('contextValidator');
    }

    return `
import {
    useExtensionsLoadingState as useExtensionsLoadingStateOrig,
    useExtensions as useExtensionsOrig,
    useExtensionsUnsupported as useExtensionsUnsupportedOrig,
    useExtensionsAll as useExtensionsAllOrig,
    ExtensionPoint as ExtensionPointOrig,
    ExtensionPointInfo as ExtensionPointInfoOrig,
    Options
} from '@atlassian/clientside-extensions-components';
import type { ExtensionPointProps } from '@atlassian/clientside-extensions-components';
import * as React from 'react';
import type { ExtensionAttributes, Context } from '@atlassian/clientside-extensions-registry';
${validationImport}
${contextValidationImport}
import { schemaDocument, contextSchemaDocument } from '${schemaDocumentsFile}';

const extensionPoint: string = ${JSON.stringify(extensionPoint) ?? 'null'};

const options: Options = { ${extendedSchemaContextOption.join(', ')} };

export const useExtensionsLoadingState = <
  ContextT extends Context<{}> | null = null,
  AttributeU extends ExtensionAttributes = ExtensionAttributes
>(
    context: ContextT
) => {
    return useExtensionsLoadingStateOrig<ContextT, AttributeU>(extensionPoint, context, options);
};

export const useExtensions = <
  ContextT extends Context<{}> | null = null,
  AttributeU extends ExtensionAttributes = ExtensionAttributes
>(
    context: ContextT
) => {
    return useExtensionsOrig<ContextT, AttributeU>(extensionPoint, context, options);
};

export const useExtensionsUnsupported = <
  ContextT extends Context<{}> | null = null,
  AttributeU extends ExtensionAttributes = ExtensionAttributes
>(
    context: ContextT
) => {
    return useExtensionsUnsupportedOrig<ContextT, AttributeU>(extensionPoint, context, options);
};

export const useExtensionsAll = <
  ContextT extends Context<{}> | null = null,
  AttributeU extends ExtensionAttributes = ExtensionAttributes
>(
    context: ContextT
) => {
    return useExtensionsAllOrig<ContextT, AttributeU>(extensionPoint, context, options);
};


type ExcludedExtensionPointProps = Omit<ExtensionPointProps, 'options' | 'name'>;

export const ExtensionPoint: React.FC<ExcludedExtensionPointProps> = ({ children, ...newProps }) => {
  // we don't do JSX here because we currently don't have tooling in place to transpile the jsx into typescript/javascript
  return React.createElement(ExtensionPointOrig, { ...newProps, options, name: extensionPoint }, children);
};

const schemaDocuments = { schema: schemaDocument, contextSchema: contextSchemaDocument };

const schemaDocuments = { schema: schemaDocument, contextSchema: contextSchemaDocument };

export const ExtensionPointInfo: React.FC<{}> = () => {
    
    // we don't do JSX here because we currently don't have tooling in place to transpile the jsx into typescript/javascript
    return React.createElement(ExtensionPointInfoOrig, {
        name: extensionPoint,
        schemaDocuments: schemaDocuments,
    });
};
`.trimLeft();
};

export default generateReactExtensionWrapper;
