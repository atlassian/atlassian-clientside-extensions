// eslint-disable-next-line import/prefer-default-export
export function isNotNull<T>(input: null | T): input is T {
    return input != null;
}
