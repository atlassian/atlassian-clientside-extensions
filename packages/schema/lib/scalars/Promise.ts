import type { Scalar } from './types';
import { ValidationTypes } from './types';

const PromiseScalar: Scalar = {
    name: 'Promise',
    description: 'A javascript Promise object.',
    typescriptType: 'Promise',
    validationType: ValidationTypes.promise,
    defaultExample: 'Promise.resolve()',
};

export default PromiseScalar;
