import type { Scalar } from './types';
import { ValidationTypes } from './types';

const FunctionScalar: Scalar = {
    name: 'Function',
    description: 'The `Function` type represents any javascript function.',
    typescriptType: 'Function',
    validationType: ValidationTypes.function,
    defaultExample: () => {},
};

export default FunctionScalar;
