import type { Scalar } from './types';
import { ValidationTypes } from './types';

const NumberScalar: Scalar = {
    name: 'Number',
    description: 'The value should be a number.',
    typescriptType: 'number',
    validationType: ValidationTypes.number,
    defaultExample: 3.14,
};

export default NumberScalar;
