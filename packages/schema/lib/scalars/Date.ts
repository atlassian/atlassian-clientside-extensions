import type { Scalar } from './types';
import { ValidationTypes } from './types';

const DateScalar: Scalar = {
    name: 'Date',
    description: 'A javascript Date object.',
    typescriptType: 'Date',
    validationType: ValidationTypes.date,
    defaultExample: 'new Date()',
};

export default DateScalar;
