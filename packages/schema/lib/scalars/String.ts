import type { Scalar } from './types';
import { ValidationTypes } from './types';

const StringScalar: Scalar = {
    name: 'String',
    description: 'The `String` type represents textual data.',
    validationType: ValidationTypes.string,
    typescriptType: 'string',
    defaultExample: 'foo',
};

export default StringScalar;
