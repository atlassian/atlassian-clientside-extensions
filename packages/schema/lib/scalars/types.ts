// eslint-disable-next-line import/prefer-default-export
export enum ValidationTypes {
    boolean,
    number,
    string,
    date,
    regexp,
    promise,
    function,
    constant,
}

export interface Scalar {
    name: string;
    description: string;
    typescriptType: string;
    validationType: ValidationTypes;
    defaultExample: unknown;
    acceptedValue?: unknown;
}
