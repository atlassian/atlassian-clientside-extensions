import type { Scalar } from './types';
import { ValidationTypes } from './types';

const RegExpScalar: Scalar = {
    name: 'RegExp',
    description: 'A regexp pattern, that can be used for matching against text.',
    typescriptType: 'RegExp',
    validationType: ValidationTypes.regexp,
    defaultExample: /foo.+bar/,
};

export default RegExpScalar;
