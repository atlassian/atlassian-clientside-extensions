import type { Scalar } from './types';
import { ValidationTypes } from './types';

const BooleanScalar: Scalar = {
    name: 'Boolean',
    description: 'The `Boolean` type represents `true` or `false`.',
    typescriptType: 'boolean',
    validationType: ValidationTypes.boolean,
    defaultExample: true,
};

export default BooleanScalar;
