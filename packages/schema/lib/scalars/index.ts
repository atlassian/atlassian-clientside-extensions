import AsyncPanelExtensionScalar from './extensions/AsyncPanel';
import ButtonExtensionScalar from './extensions/Button';
import LinkExtensionScalar from './extensions/Link';
import ModalExtensionScalar from './extensions/Modal';
import ModalOnActionScalar from './extensions/ModalOnAction';
import PanelExtensionScalar from './extensions/Panel';
import PanelOnActionScalar from './extensions/PanelOnAction';
import SectionExtensionScalar from './extensions/Section';

import BooleanScalar from './Boolean';
import DateScalar from './Date';
import FunctionScalar from './Function';
import NumberScalar from './Number';
import PromiseScalar from './Promise';
import RegExpScalar from './RegExp';
import StringScalar from './String';
import type { Scalar } from './types';

const scalarsArr = [
    AsyncPanelExtensionScalar,
    BooleanScalar,
    ButtonExtensionScalar,
    DateScalar,
    FunctionScalar,
    LinkExtensionScalar,
    ModalExtensionScalar,
    ModalOnActionScalar,
    NumberScalar,
    PanelExtensionScalar,
    PanelOnActionScalar,
    PromiseScalar,
    RegExpScalar,
    SectionExtensionScalar,
    StringScalar,
];

const scalars: Map<string, Scalar> = new Map<string, Scalar>(scalarsArr.map((scalar) => [scalar.name, scalar]));

export default scalars;
