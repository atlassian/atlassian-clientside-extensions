import type { Scalar } from '../types';
import { ValidationTypes } from '../types';

const ModalExtensionScalar: Scalar = {
    name: 'ModalExtension',
    description: 'The value must be the constant "modal".',
    validationType: ValidationTypes.constant,
    acceptedValue: 'modal',
    typescriptType: '"modal"',
    defaultExample: 'modal',
};

export default ModalExtensionScalar;
