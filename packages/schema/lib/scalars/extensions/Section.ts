import type { Scalar } from '../types';
import { ValidationTypes } from '../types';

const SectionExtensionScalar: Scalar = {
    name: 'SectionExtension',
    description: 'The value must be the constant "section".',
    validationType: ValidationTypes.constant,
    acceptedValue: 'section',
    typescriptType: '"section"',
    defaultExample: 'section',
};

export default SectionExtensionScalar;
