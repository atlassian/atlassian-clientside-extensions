import type { Scalar } from '../types';
import { ValidationTypes } from '../types';

const AsyncPanelExtensionScalar: Scalar = {
    name: 'AsyncPanelExtension',
    description: 'The value must be the constant "async-panel".',
    validationType: ValidationTypes.constant,
    acceptedValue: 'async-panel',
    typescriptType: '"async-panel"',
    defaultExample: 'async-panel',
};

export default AsyncPanelExtensionScalar;
