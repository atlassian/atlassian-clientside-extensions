import type { Scalar } from '../types';
import { ValidationTypes } from '../types';

const ButtonExtensionScalar: Scalar = {
    name: 'ButtonExtension',
    description: 'The value must be the constant "button".',
    validationType: ValidationTypes.constant,
    acceptedValue: 'button',
    typescriptType: '"button"',
    defaultExample: 'button',
};

export default ButtonExtensionScalar;
