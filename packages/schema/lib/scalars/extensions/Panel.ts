import type { Scalar } from '../types';
import { ValidationTypes } from '../types';

const PanelExtensionScalar: Scalar = {
    name: 'PanelExtension',
    description: 'The value must be the constant "panel".',
    validationType: ValidationTypes.constant,
    acceptedValue: 'panel',
    typescriptType: '"panel"',
    defaultExample: 'panel',
};

export default PanelExtensionScalar;
