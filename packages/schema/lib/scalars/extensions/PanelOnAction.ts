import type { Scalar } from '../types';
import { ValidationTypes } from '../types';

const PanelOnActionScalar: Scalar = {
    name: 'PanelOnAction',
    description:
        'The value should be a callback function expecting to receive the Panel-API. See https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/panel-api/',
    validationType: ValidationTypes.function,
    typescriptType: 'Function', // TODO: type this better
    defaultExample: () => {}, // TODO: example this better
};

export default PanelOnActionScalar;
