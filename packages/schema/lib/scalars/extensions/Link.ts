import type { Scalar } from '../types';
import { ValidationTypes } from '../types';

const LinkExtensionScalar: Scalar = {
    name: 'LinkExtension',
    description: 'The value must be the constant "link".',
    validationType: ValidationTypes.constant,
    acceptedValue: 'link',
    typescriptType: '"link"',
    defaultExample: 'link',
};

export default LinkExtensionScalar;
