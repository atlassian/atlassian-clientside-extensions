import { DeferredSubject } from '@atlassian/clientside-extensions-base';
import type { LocationsSubjectPayload } from './types';

/**
 * LocationsSubject used by the client extension registry to keep track of extension points
 */
export default class LocationsSubject extends DeferredSubject<LocationsSubjectPayload> {
    protected previousLoadingState: boolean = true;

    override notify(payload: LocationsSubjectPayload) {
        this.previousLoadingState = payload.loadingState;

        super.notify(payload);
    }

    getLoadingState() {
        return this.previousLoadingState;
    }
}
