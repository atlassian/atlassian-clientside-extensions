// import type { JQueryPromise } from 'jquery';

// type WrmRequire = (param: string | string[]) => JQueryPromise<unknown>;
type WrmContextPath = () => string;

// declare module 'wrm/require' {
//     const wrmRequire: WrmRequire;
//
//     export default wrmRequire;
// }

declare module 'wrm/require';

declare module 'wrm/context-path' {
    const contextPath: WrmContextPath;

    export default contextPath;
}
