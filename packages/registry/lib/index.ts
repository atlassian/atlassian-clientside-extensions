import ClientExtensionsRegistry from './registry';
import { registerWebItemsElement } from './RegisterWebItemElement';
import { registerWebPanelsElement } from './RegisterWebPanelElements';

export * from './types';

declare global {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    interface Window {
        // Due to historical name changes the "p" still represents the old "plugin" nomenclature
        ____c_p_r: ClientExtensionsRegistry;
    }
}

// Create a singleton instance for the default export and ensure it can only ever exist once by sharing it globally
// eslint-disable-next-line no-multi-assign, no-underscore-dangle
const registry = (window.____c_p_r = window.____c_p_r || new ClientExtensionsRegistry());

registerWebItemsElement(registry);
registerWebPanelsElement(registry);
export default registry;
