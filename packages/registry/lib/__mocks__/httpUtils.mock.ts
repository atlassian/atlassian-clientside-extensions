const getLocationExtensions = jest.fn();
const getLocationResources = jest.fn();

export { getLocationExtensions, getLocationResources };
