import { rest } from 'msw';
import { Deferred } from 'jquery';
import { setupServer } from 'msw/node';
import type { LoggerPayload } from '@atlassian/clientside-extensions-debug';
import { LogLevel, onDebug } from '@atlassian/clientside-extensions-debug';
import { mocked } from 'ts-jest/utils';
// eslint-disable-next-line import/no-unresolved, node/no-missing-import
import wrmRequire from 'wrm/require';
import { getLocationExtensions, getLocationResources } from './httpUtils';
import type { ExtensionDescriptor } from './types';

jest.mock('@atlassian/clientside-extensions-debug');

const server = setupServer();

const loggerPayload: LoggerPayload[] = [];

beforeAll(() => {
    server.listen();

    mocked(onDebug).mockImplementation((cb) => {
        loggerPayload.push(cb(LogLevel));
    });
});

afterEach(() => {
    server.resetHandlers();
    mocked(onDebug).mockClear();
    mocked(wrmRequire).mockClear();

    loggerPayload.length = 0;
});

describe('getLocationExtensions', () => {
    afterAll(() => server.close());

    it('should load web-items for a given location', async () => {
        expect.assertions(2);

        // given
        const myLocation = 'foo.bar';
        const webItems: ExtensionDescriptor[] = [
            {
                key: 'biz.baz',
                location: myLocation,
                attributes: {},
            },
        ];

        server.use(
            rest.get('/rest/client-plugins/1.0/client-plugins/items', (req, res, ctx) => {
                expect(req.url.searchParams.get('location')).toEqual(myLocation);

                return res(
                    ctx.json({
                        webItems,
                    }),
                );
            }),
        );

        // when
        const result = await getLocationExtensions(myLocation);

        // then
        expect(result).toEqual(webItems);
    });

    it('should fallback to empty array when there are no web-items for given location', async () => {
        expect.assertions(2);

        // given
        const myLocation = 'wrong.location';

        server.use(
            rest.get('/rest/client-plugins/1.0/client-plugins/items', (req, res, ctx) => {
                expect(req.url.searchParams.get('location')).toEqual(myLocation);

                return res(
                    ctx.json({
                        webItems: [],
                    }),
                );
            }),
        );

        // when
        const result = await getLocationExtensions(myLocation);

        // then
        expect(result).toHaveLength(0);
    });

    it('should fallback to empty array when server errors', async () => {
        expect.assertions(3);

        // given
        const myLocation = 'wrong.location';

        server.use(
            rest.get('/rest/client-plugins/1.0/client-plugins/items', (req, res, ctx) => {
                expect(req.url.searchParams.get('location')).toEqual(myLocation);

                return res(ctx.status(500));
            }),
        );

        // when
        const result = await getLocationExtensions(myLocation);

        // then
        expect(result).toHaveLength(0);
        expect(loggerPayload).toContainEqual(
            expect.objectContaining({
                level: LogLevel.error,
            }),
        );
    });
});

describe('getLocationResources', () => {
    it('should load web-resources by extension location', async () => {
        // given
        const myLocation = 'foo.bar';

        // when, then
        // We use `toBeUndefined` since we are only interested in knowing that promise wasn't rejected
        await expect(getLocationResources(myLocation)).resolves.toBeUndefined();

        expect(wrmRequire).toHaveBeenCalledWith('wrc!foo.bar');
    });

    it('should fail loading web-resources by extension location', async () => {
        // given
        const myLocation = 'foo.bar';
        mocked(wrmRequire).mockImplementation(() => Deferred().reject().promise());

        // when, then
        // We use `toBeUndefined` since we are only interested in knowing that promise was rejected
        await expect(getLocationResources(myLocation)).rejects.toBeUndefined();

        expect(wrmRequire).toHaveBeenCalledWith('wrc!foo.bar');
    });
});
