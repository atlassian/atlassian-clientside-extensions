import { registerCustomElement } from './WebFragmentElements';

import type Registry from './registry';
import type { ParsedWebItemDescriptor } from './types';

export const customElementName = 'cse-register-web-items-element';

/** @since 2.1.0 */
export const registerWebItemsElement = (registry: Registry) => {
    registerCustomElement<ParsedWebItemDescriptor>(registry, customElementName);
};
