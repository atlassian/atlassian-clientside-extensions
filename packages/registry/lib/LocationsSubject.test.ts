import LocationSubject from './LocationsSubject';
import type { ExtensionDescriptor } from './types';

type LocationSubjectObserver = Parameters<LocationSubject['subscribe']>[0];

jest.useFakeTimers();

describe('LocationSubject', () => {
    let subject: LocationSubject;

    beforeEach(() => {
        subject = new LocationSubject();
    });

    it('should notify each observer when executed with the given descriptors and loading state', () => {
        expect.assertions(4);
        const testDescriptors = [{}, {}, {}] as ExtensionDescriptor[];

        const stillLoadingCase: LocationSubjectObserver = ({ descriptors, loadingState }) => {
            expect(descriptors).toMatchObject([]);
            expect(loadingState).toBe(true);
        };

        const loadingIsFinishedCase: LocationSubjectObserver = ({ descriptors, loadingState }) => {
            expect(descriptors).toBe(testDescriptors);
            expect(loadingState).toBe(false);
        };

        const cases = [stillLoadingCase, loadingIsFinishedCase];

        subject.subscribe((payload) => {
            const result = cases.shift();

            if (result) {
                result(payload);
            }
        });

        // loading
        subject.notify({
            descriptors: [],
            loadingState: true,
        });

        jest.runAllTimers();

        // receive
        subject.notify({
            descriptors: testDescriptors,
            loadingState: false,
        });
        jest.runAllTimers();
    });
});
