export type Context<T> = {
    [K in keyof T]: T[K];
};

export type DataProviderPayload<T = unknown> = T;

// We need to use either "any" or "unknown" type here since we are extending the data type later with each attributes factory
// Read more https://stackoverflow.com/a/41286276
export type MaybeOnAction = unknown;
export type OnAction = (...args: unknown[]) => unknown;

interface BaseExtensionMethods {
    onAction?: MaybeOnAction;
    contextProvider?: () => Context<{}>;
    // Optional because unsupported extensions (web-items) don’t have attributes other than the ones added by our runtime (label, weight).
    type?: string;
}

export interface ExtensionAttributes extends BaseExtensionMethods {
    [key: string]: unknown;
}

export interface InitialAttributes {
    location: string;
    label?: string;
    weight?: number;
}

interface BaseExtensionDescriptor<T> extends InitialAttributes {
    key: string;
    attributes: T & { label?: string; weight?: number };
    params?: {
        [key: string]: unknown;
    };
}

type AttributeUpdatePayload<T = ExtensionAttributes> = Partial<T> | ((previousAttributes: T) => Partial<T>);
export type ExtensionCleanupCallback = () => void;
export interface ExtensionAPI<T = ExtensionAttributes> {
    updateAttributes: (payload: AttributeUpdatePayload<T>) => void;
    onCleanup: (callback: ExtensionCleanupCallback) => void;
}

export type ExtensionAttributesProvider<AttributesT = ExtensionAttributes, ContextT extends Context<{}> | null = Context<{}> | null> = (
    api: ExtensionAPI<AttributesT>,
    context: ContextT,
) => AttributesT;

export interface ExtensionDescriptor<
    AttributesT extends ExtensionAttributes = ExtensionAttributes,
    AttributesProviderT extends ExtensionAttributesProvider<AttributesT> = ExtensionAttributesProvider<AttributesT>,
> extends BaseExtensionDescriptor<AttributesT> {
    attributesProvider?: AttributesProviderT;
}

export type LocationsSubjectPayload = {
    descriptors: ExtensionDescriptor[];
    loadingState: boolean;
};

interface BaseParsedWebFragmentDescriptor {
    completeKey: string;
    id?: string;
    weight: number;
}

export interface ParsedWebItemDescriptor extends BaseParsedWebFragmentDescriptor {
    label: string;
    section: string;
    url?: string;
    title?: string;
    styleClass?: string;
    params?: unknown;
    accessKey?: string;
    tooltip?: string;
}

type SimpleObject = {
    [key: string]: boolean | number | string | SimpleObject | [SimpleObject];
};

export interface ParsedWebPanelDescriptor extends BaseParsedWebFragmentDescriptor {
    html: string;
    meta: SimpleObject;
}

export type ParsedWebFragmentDescriptor = ParsedWebItemDescriptor | ParsedWebPanelDescriptor;

export type ExtensionHandler = (root: HTMLElement, location: string, legacyItems: ParsedWebFragmentDescriptor[]) => void;

/**
 * Serialized GraphQL schema
 */
export type ExtensionSchema = string;

export interface WRMInterface {
    define(moduleKey: string, module: () => unknown): void;
    require(param: string | string[]): Promise<void>;
    contextPath(): string;
    data: {
        claim: <T>(dataProviderKey: string) => DataProviderPayload<T>;
    };
}
