import { registerCustomElement } from './WebFragmentElements';

import type Registry from './registry';
import type { ParsedWebPanelDescriptor } from './types';

export const customElementName = 'cse-register-web-panels-element';

/** @since 2.1.0 */
export const registerWebPanelsElement = (registry: Registry) => {
    registerCustomElement<ParsedWebPanelDescriptor>(registry, customElementName);
};
