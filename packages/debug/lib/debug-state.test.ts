/* eslint-disable no-underscore-dangle, jest/no-standalone-expect */
import cases from 'jest-in-case';
import type { ExposedClientExtensionDebug } from './debug-state';
import { isDebugEnabled, isDiscoveryEnabled, isLoggingEnabled, isValidationEnabled, getLogLevel, setLogLevel } from './debug-state';
import { LogLevel } from './logger/logger';

describe('debug utils', () => {
    beforeEach(() => {
        (window.____c_p_d as unknown) = false;
    });

    cases(
        'should be active if respective flag is set to TRUE',
        (opts) => {
            expect(opts.isUtil()).toBe(false);

            // switch flag to TRUE
            (window.____c_p_d as unknown) = { [opts.key]: true };

            expect(opts.isUtil()).toBe(true);

            // switch ALL false
            (window.____c_p_d as unknown) = false;

            expect(opts.isUtil()).toBe(false);

            // switch ALL true
            (window.____c_p_d as unknown) = true;

            expect(opts.isUtil()).toBe(true);
        },
        {
            isDebugEnabled: { isUtil: isDebugEnabled, key: 'debug' },
            isLoggingEnabled: { isUtil: isLoggingEnabled, key: 'logging' },
            isValidationEnabled: { isUtil: isValidationEnabled, key: 'validation' },
            isDiscoveryEnabled: { isUtil: isDiscoveryEnabled, key: 'discovery' },
        },
    );

    describe('log levels', () => {
        let prevDebugState: ExposedClientExtensionDebug;

        beforeEach(() => {
            prevDebugState = window.____c_p_d;
        });

        afterEach(() => {
            window.____c_p_d = prevDebugState;
        });

        it('should allow to change the default log level using utils', () => {
            expect(getLogLevel()).toBe(LogLevel.info);

            setLogLevel(LogLevel.debug);

            expect(getLogLevel()).toBe(LogLevel.debug);
        });

        it('should allow to change log level using global variable', () => {
            // Change with enum
            (window.____c_p_d.logLevel as LogLevel) = LogLevel.error;
            expect(getLogLevel()).toBe(LogLevel.error);

            // Change with string value
            (window.____c_p_d.logLevel as string) = 'warn';
            expect(getLogLevel()).toBe(LogLevel.warn);
        });

        it('should fallback to a default "INFO" level when using unsupported runtime value', () => {
            (window.____c_p_d.logLevel as string) = 'show-me-all-the-things';

            expect(getLogLevel()).toBe(LogLevel.info);
        });
    });

    describe('compatibility mode', () => {
        it('should fallback to a default "INFO" level when "logLevel" property is missing e.g. CSE < 2.1.0', () => {
            Object.defineProperty(window, '____c_p_d', {
                value: {},
                configurable: true,
                writable: true,
            });

            expect(getLogLevel()).toBe(LogLevel.info);
        });
    });
});
