import { isDebugEnabled, isLoggingEnabled } from './debug-state';

import type { LoggerCallback, LoggerPayload } from './logger/logger';
import { _loggerSubject, LogLevel } from './logger/logger';

// eslint-disable-next-line import/prefer-default-export
export const onDebug = (callback: LoggerCallback) => {
    // Return early if both debug and logging is disabled
    if (!isDebugEnabled() || !isLoggingEnabled()) {
        return;
    }

    let payload: LoggerPayload | undefined;

    try {
        payload = callback(LogLevel);
    } catch (e) {
        // eslint-disable-next-line no-empty
    }

    if (payload) {
        _loggerSubject.notify(payload);
    }
};
