import { mocked } from 'ts-jest/utils';
import type { LoggerPayload } from './logger';
import { LogLevel } from './logger';
import { consoleLogger } from './console';
import { getLogLevel } from '../debug-state';

jest.mock('../debug-state');

describe('console logger', () => {
    const orgConsole = global.console;

    beforeEach(() => {
        global.console = {
            ...orgConsole,
            log: jest.fn(),
            error: jest.fn(),
            debug: jest.fn(),
            info: jest.fn(),
            warn: jest.fn(),
            groupCollapsed: jest.fn(),
        };
    });

    afterEach(() => {
        mocked(getLogLevel).mockClear();

        global.console = orgConsole;
    });

    const getEventPayload = (level: LogLevel): LoggerPayload => ({
        level,
        message: 'Hello!',
    });

    describe('"DEBUG" level', () => {
        it.each([
            // prettier-ignore-start
            [LogLevel.debug],
            // prettier-ignore-end
        ])('should display "DEBUG" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.debug);

            // when
            consoleLogger(event);

            // then
            expect(global.console.debug).toHaveBeenCalledWith(expect.stringContaining('Hello!'));
            expect(global.console.groupCollapsed).toHaveBeenCalled();
        });

        it.each([
            // prettier-ignore-start
            [LogLevel.error],
            [LogLevel.warn],
            [LogLevel.deprecation],
            [LogLevel.info],
            // prettier-ignore-end
        ])('should not display "DEBUG" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.debug);

            // when
            consoleLogger(event);

            // then
            expect(global.console.debug).not.toHaveBeenCalled();
            expect(global.console.groupCollapsed).not.toHaveBeenCalled();
        });
    });

    describe('"ERROR" level', () => {
        it.each([
            // prettier-ignore-start
            [LogLevel.debug],
            [LogLevel.error],
            // prettier-ignore-end
        ])('should display "ERROR" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.error);

            // when
            consoleLogger(event);

            // then
            expect(global.console.error).toHaveBeenCalledWith(expect.stringContaining('Hello!'));

            if (logLevel === LogLevel.debug) {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(global.console.groupCollapsed).toHaveBeenCalled();
            } else {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(global.console.groupCollapsed).not.toHaveBeenCalled();
            }
        });

        it.each([
            // prettier-ignore-start
            [LogLevel.warn],
            [LogLevel.deprecation],
            [LogLevel.info],
            // prettier-ignore-end
        ])('should not display "ERROR" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.error);

            // when
            consoleLogger(event);

            // then
            expect(global.console.error).not.toHaveBeenCalled();
            expect(global.console.groupCollapsed).not.toHaveBeenCalled();
        });
    });

    describe('"WARN" level', () => {
        it.each([
            // prettier-ignore-start
            [LogLevel.debug],
            [LogLevel.error],
            [LogLevel.warn],
            // prettier-ignore-end
        ])('should display "WARN" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.warn);

            // when
            consoleLogger(event);

            // then
            expect(global.console.warn).toHaveBeenCalledWith(expect.stringContaining('Hello!'));

            if (logLevel === LogLevel.debug) {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(global.console.groupCollapsed).toHaveBeenCalled();
            } else {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(global.console.groupCollapsed).not.toHaveBeenCalled();
            }
        });

        it.each([
            // prettier-ignore-start
            [LogLevel.deprecation],
            [LogLevel.info],
            // prettier-ignore-end
        ])('should not display "WARN" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.warn);

            // when
            consoleLogger(event);

            // then
            expect(global.console.warn).not.toHaveBeenCalled();
            expect(global.console.groupCollapsed).not.toHaveBeenCalled();
        });
    });

    describe('"DEPRECATION" level', () => {
        it.each([
            // prettier-ignore-start
            [LogLevel.debug],
            [LogLevel.error],
            [LogLevel.warn],
            // prettier-ignore-end
        ])('should display "DEPRECATION" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.deprecation);

            // when
            consoleLogger(event);

            // then
            expect(global.console.warn).toHaveBeenCalledWith(expect.stringContaining('Hello!'));

            if (logLevel === LogLevel.debug) {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(global.console.groupCollapsed).toHaveBeenCalled();
            } else {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(global.console.groupCollapsed).not.toHaveBeenCalled();
            }
        });

        it.each([
            // prettier-ignore-start
            [LogLevel.info],
            // prettier-ignore-end
        ])('should not display "DEPRECATION" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.deprecation);

            // when
            consoleLogger(event);

            // then
            expect(global.console.warn).not.toHaveBeenCalled();
            expect(global.console.groupCollapsed).not.toHaveBeenCalled();
        });
    });

    describe('"INFO" level', () => {
        it.each([
            // prettier-ignore-start
            [LogLevel.debug],
            [LogLevel.error],
            [LogLevel.warn],
            [LogLevel.deprecation],
            [LogLevel.info],
            // prettier-ignore-end
        ])('should display "INFO" log when log level is set to "%s"', (logLevel: LogLevel) => {
            // given
            mocked(getLogLevel).mockReturnValue(logLevel);
            const event = getEventPayload(LogLevel.info);

            // when
            consoleLogger(event);

            // then
            expect(global.console.info).toHaveBeenCalledWith(expect.stringContaining('Hello!'));

            if (logLevel === LogLevel.debug) {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(global.console.groupCollapsed).toHaveBeenCalled();
            } else {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(global.console.groupCollapsed).not.toHaveBeenCalled();
            }
        });
    });
});
