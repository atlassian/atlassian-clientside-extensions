import type { LoggerPayload } from './logger';
import { LogLevel } from './logger';
import { getLogLevel } from '../debug-state';

// We need to pick the callable keys from the console object
type ConsoleLogsTypes = keyof Pick<typeof console, 'log' | 'warn' | 'error' | 'info' | 'debug'>;

const getLoggingFromLevel = (level: LogLevel): ConsoleLogsTypes => {
    switch (level) {
        case LogLevel.error:
            return 'error';
        case LogLevel.debug:
            return 'debug';
        case LogLevel.info:
            return 'info';
        case LogLevel.warn:
        case LogLevel.deprecation:
            return 'warn';
        default:
            return 'debug';
    }
};

// Naive implementation but we can't use bitwise operators :(
const debugLevels = Object.values(LogLevel);
const errorLevels = debugLevels.filter((level) => level !== LogLevel.debug);
const warnLevels = errorLevels.filter((level) => level !== LogLevel.error);
const deprecationLevels = warnLevels.filter((level) => level !== LogLevel.warn);
const infoLevels = deprecationLevels.filter((level) => level !== LogLevel.deprecation);

const levelsMap: { [key: string]: string[] } = {
    [LogLevel.debug]: debugLevels,
    [LogLevel.error]: errorLevels,
    [LogLevel.warn]: warnLevels,
    [LogLevel.deprecation]: deprecationLevels,
    [LogLevel.info]: infoLevels,
};

function shouldDisplayLog(logLevel: LogLevel, eventLogLevel: LogLevel) {
    if (logLevel === LogLevel.debug) {
        return true;
    }

    const levels = levelsMap[logLevel];

    return Array.isArray(levels) ? levels.includes(eventLogLevel) : false;
}

// eslint-disable-next-line import/prefer-default-export
export const consoleLogger = (payload: LoggerPayload) => {
    const eventLogLevel = payload.level;
    const logLevel = getLogLevel();

    if (!shouldDisplayLog(logLevel, eventLogLevel)) {
        return;
    }

    const consoleKey = getLoggingFromLevel(eventLogLevel);
    const deprecationWarning =
        payload.level === LogLevel.deprecation
            ? `⚠️⚠️⚠️ DEPRECATION WARNING ⚠️⚠️⚠️
`
            : '';

    console[consoleKey](`${deprecationWarning}[Atlassian Client-side Extensions]: ${payload.message}`);

    // Verbose logging
    if (logLevel === LogLevel.debug) {
        console.groupCollapsed('  <click here for more details about above message>');
        console.log({
            components: payload.components,
            meta: payload.meta,
        });
        console.groupEnd();
    }
};
