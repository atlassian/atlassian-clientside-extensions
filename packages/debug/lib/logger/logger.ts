import { ReplaySubject } from '@atlassian/clientside-extensions-base';
import { DebugSubjects, registerDebugSubject } from '../debug-subjects';

export enum LogLevel {
    debug = 'DEBUG', // Logs all the things
    error = 'ERROR',
    warn = 'WARN',
    deprecation = 'DEPRECATION',
    info = 'INFO', // The lowest level
}

export type LoggerCallback = (levels: typeof LogLevel) => LoggerPayload;

export type LoggerPayload = {
    level: LogLevel;
    message: string;
    components?: string | string[];
    meta?: {
        [key: string]: unknown;
    };
};

const loggerSubject = registerDebugSubject(DebugSubjects.Logger, () => new ReplaySubject<LoggerPayload>(20));

// Exported for internal use only
export { loggerSubject as _loggerSubject };
