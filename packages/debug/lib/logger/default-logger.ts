/* eslint-disable no-underscore-dangle */
import type { Observer, Subscription } from '@atlassian/clientside-extensions-base';
import { consoleLogger } from './console';
import { DebugSubjects, observeDebugSubject } from '../debug-subjects';
import type { LoggerPayload } from './logger';
import { _loggerSubject } from './logger';

declare global {
    interface Window {
        // clientside extension default logging deregister
        ____c_p_d_l_d?: Subscription;
    }
}

/**
 * We need to keep and expose the "observeLogger" function for a legacy reason.
 * It should be removed in CSEv3
 *
 * @deprecated
 */
export const observeLogger = (observer: Observer<LoggerPayload>): Subscription => {
    return observeDebugSubject<LoggerPayload>(DebugSubjects.Logger, observer);
};

const registerConsoleLogger = (): Subscription => {
    // Singleton workaround as this module may be executed more than once.
    // We only ever want one global default logger, however.
    if (window.____c_p_d_l_d !== undefined) {
        return window.____c_p_d_l_d;
    }

    window.____c_p_d_l_d = observeLogger(consoleLogger);

    return window.____c_p_d_l_d;
};

const deregisterLogger = (): void => {
    if (window.____c_p_d_l_d === undefined) {
        return;
    }

    window.____c_p_d_l_d.unsubscribe();
};

export const defaultLogger = registerConsoleLogger();

/**
 * Added in CSE 2.1.0
 *
 * @param logger
 * @public
 */
export const setLogger = (logger: Observer<LoggerPayload>): void => {
    if (typeof logger !== 'function') {
        throw new Error(`Client-side Extension: The provided logger needs to be a function`);
    }

    deregisterLogger();
    _loggerSubject.prune();

    observeDebugSubject<LoggerPayload>(DebugSubjects.Logger, logger);
};

const deregisterDefaultLogger = () => defaultLogger.unsubscribe();

// @VisibleForTesting
export { deregisterDefaultLogger as _deregisterDefaultLogger };
