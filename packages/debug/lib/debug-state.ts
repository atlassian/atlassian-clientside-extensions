/* eslint-disable no-underscore-dangle */

import type { Observer } from '@atlassian/clientside-extensions-base';
import { ReplaySubject } from '@atlassian/clientside-extensions-base';
import { DebugSubjects, observeDebugSubject, registerDebugSubject } from './debug-subjects';
import { LogLevel } from './logger/logger';

enum ClientExtensionDebugTypes {
    debug = 'debug',
    logging = 'logging',
    validation = 'validation',
    discovery = 'discovery',
    /** @since 2.1.0 */
    logLevel = 'logLevel',
}

type DebugTypeKeys = Exclude<keyof typeof ClientExtensionDebugTypes, 'logLevel'>;

export type ClientExtensionDebug = {
    [key in DebugTypeKeys]: boolean;
} & {
    [ClientExtensionDebugTypes.logLevel]: LogLevel;
};

export type ExposedClientExtensionDebug = { [key in keyof ClientExtensionDebug]: PropertyDescriptor } & {
    __initialized: PropertyDescriptor;
};

declare global {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    interface Window {
        // The global ____c_p_d is a primitive boolean value that holds additional properties
        // Due to historical name changes the "p" still represents the old "plugin" nomenclature
        ____c_p_d: ExposedClientExtensionDebug;
    }
}

const defineDebugGlobal = (previousGlobal: unknown) => {
    // initialize as boolean
    const debugGlobal = Object.create(null);
    const debugStates: ClientExtensionDebug = {
        debug: false,
        logging: false,
        validation: false,
        discovery: false,
        /** @since 2.1.0 */
        // In dev mode we set default loglevel to INFO
        logLevel: process.env.NODE_ENV === 'production' ? LogLevel.error : LogLevel.info,
    };

    const debugStateSubject = registerDebugSubject(DebugSubjects.State, () => new ReplaySubject(1));

    const properties: ExposedClientExtensionDebug = {
        debug: {
            get() {
                return debugStates.debug;
            },
            set(val: unknown) {
                debugStates.debug = Boolean(val);
                debugStateSubject.notify(debugStates);
            },
        },
        logging: {
            get() {
                return debugStates.logging;
            },
            set(val: unknown) {
                debugStates.logging = Boolean(val);
                debugStateSubject.notify(debugStates);
            },
        },
        validation: {
            get() {
                return debugStates.validation;
            },
            set(val: unknown) {
                debugStates.validation = Boolean(val);
                debugStateSubject.notify(debugStates);
            },
        },
        discovery: {
            get() {
                return debugStates.discovery;
            },
            set(val: unknown) {
                debugStates.discovery = Boolean(val);
                debugStateSubject.notify(debugStates);
            },
        },
        /** @since 2.1.0 */
        logLevel: {
            get() {
                return debugStates.logLevel;
            },
            set(val: unknown) {
                let level = String(val).toUpperCase() as LogLevel;

                // Handle runtime errors
                if (!Object.values(LogLevel).includes(level)) {
                    level = LogLevel.info;
                }

                debugStates.logLevel = level;
                debugStateSubject.notify(debugStates);
            },
        },
        __initialized: { value: true, writable: false },
    };
    Object.defineProperties(debugGlobal, properties);

    Object.defineProperty(window, '____c_p_d', {
        configurable: true, // needed for testing :(
        get() {
            return debugGlobal;
        },
        set(val) {
            // its a boolean
            if (val === true || val === false) {
                debugGlobal.debug = val;
                debugGlobal.logging = val;
                debugGlobal.validation = val;
                debugGlobal.discovery = val;
                return;
            }

            // we can handle booleans - like above
            // or objects (except null) and nothing else.
            if (typeof val !== 'object' || val === null) {
                return;
            }

            const clientExtensionDebugKeys = Object.keys(ClientExtensionDebugTypes) as (keyof ClientExtensionDebug)[];

            clientExtensionDebugKeys.forEach((key) => {
                if (key in val) {
                    debugGlobal[key] = val[key];
                }
            });
        },
    });

    // Now, we can use the previous value e.g. boolean, to set the value for all flags
    window.____c_p_d = previousGlobal as ExposedClientExtensionDebug;
};

// Register global value only once
if (!('____c_p_d' in window) || !window.____c_p_d.__initialized) {
    defineDebugGlobal(window.____c_p_d ?? process.env.NODE_ENV !== 'production');
}

export const observeStateChange = (observer: Observer<ClientExtensionDebug>) =>
    observeDebugSubject<ClientExtensionDebug>(DebugSubjects.State, observer);

export const isDebugEnabled = () => {
    return window.____c_p_d.debug as boolean;
};

export const isLoggingEnabled = () => {
    return window.____c_p_d.logging as boolean;
};

export const isValidationEnabled = () => {
    return window.____c_p_d.validation as boolean;
};

export const isDiscoveryEnabled = () => {
    return window.____c_p_d.discovery as boolean;
};

/** @since 2.1.0 */
export const getLogLevel = (): LogLevel => {
    const logLevel = window.____c_p_d.logLevel as LogLevel;

    return logLevel ?? LogLevel.info;
};

export const setDebugEnabled = (value: boolean) => {
    (window.____c_p_d.debug as boolean) = value;
};

export const setLoggingEnabled = (value: boolean) => {
    (window.____c_p_d.logging as boolean) = value;
};

export const setValidationEnabled = (value: boolean) => {
    (window.____c_p_d.validation as boolean) = value;
};

export const setDiscoveryEnabled = (value: boolean) => {
    (window.____c_p_d.discovery as boolean) = value;
};

/** @since 2.1.0 */
export const setLogLevel = (value: string) => {
    const level = value.toUpperCase() as LogLevel;

    (window.____c_p_d.logLevel as LogLevel) = level;
};
