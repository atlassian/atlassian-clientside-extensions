import { mocked } from 'ts-jest/utils';
import { getLogLevel, isDebugEnabled, isLoggingEnabled } from './debug-state';
import { onDebug } from './debug';
import { LogLevel } from './logger/logger';
import { observeLogger } from './logger/default-logger';

jest.mock('./debug-state');

describe('onDebug util', () => {
    const orgConsole = global.console;

    beforeEach(() => {
        mocked(isDebugEnabled).mockReturnValue(false);
        mocked(isLoggingEnabled).mockReturnValue(false);
        mocked(getLogLevel).mockReturnValue(LogLevel.debug);

        // Silent down console
        global.console = {
            ...orgConsole,
            error: jest.fn(),
            log: jest.fn(),
            groupCollapsed: jest.fn(),
        };
    });

    afterEach(() => {
        mocked(isDebugEnabled).mockClear();
        mocked(isLoggingEnabled).mockClear();
        mocked(getLogLevel).mockClear();

        global.console = orgConsole;
    });

    it('should only execute the given callback when debug and logging is enabled', () => {
        const debugCallbackSpy = jest.fn();

        // disable
        mocked(isDebugEnabled).mockReturnValue(false);
        mocked(isLoggingEnabled).mockReturnValue(false);

        onDebug(debugCallbackSpy);

        expect(debugCallbackSpy).not.toHaveBeenCalled();

        // enable
        mocked(isDebugEnabled).mockReturnValue(true);
        mocked(isLoggingEnabled).mockReturnValue(true);

        onDebug(debugCallbackSpy);

        expect(debugCallbackSpy).toHaveBeenCalled();
    });

    it('should not notify the observer when logging is disabled', () => {
        const payload = {
            message: 'foo',
            level: LogLevel.error,
        };
        const loggerCallback = jest.fn(() => payload);
        const loggerObserver = jest.fn();

        // need to enable debug in order to run the debugCallback
        mocked(isDebugEnabled).mockReturnValue(true);
        // disable
        mocked(isLoggingEnabled).mockReturnValue(false);

        observeLogger(loggerObserver);
        onDebug(loggerCallback);

        expect(loggerCallback).not.toHaveBeenCalled();
        expect(loggerObserver).not.toHaveBeenCalled();

        // enable
        mocked(isLoggingEnabled).mockReturnValue(true);
        onDebug(loggerCallback);

        expect(loggerCallback).toHaveBeenCalledTimes(1);
        expect(loggerObserver).toHaveBeenCalledWith(payload);
    });

    it('should not throw an error when logger callback fails', () => {
        mocked(isDebugEnabled).mockReturnValue(true);
        mocked(isLoggingEnabled).mockReturnValue(true);

        const callOnDebug = () => {
            const loggerCallback = () => {
                throw new Error('ups');
            };

            onDebug(loggerCallback);
        };

        expect(callOnDebug).not.toThrow();
    });
});
