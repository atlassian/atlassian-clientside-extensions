/* eslint-disable no-underscore-dangle */
import type { Observer, Subject } from '@atlassian/clientside-extensions-base';

type ExposedClientExtensionSubjects = {
    __initialized: PropertyDescriptor;
    subjects: PropertyDescriptor;
};

export const enum DebugSubjects {
    Logger = 'logger',
    State = 'state',
}

type DebugSubjectsMap<SubjectT> = {
    [key in DebugSubjects]: SubjectT;
};

declare global {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    interface Window {
        // Due to historical name changes the "p" still represents the old "plugin" nomenclature
        ____c_p_s: ExposedClientExtensionSubjects;
    }
}

const defineSubjectGlobal = () => {
    // initialize as boolean
    const subjectGlobal = Object.create(null);

    const properties: ExposedClientExtensionSubjects = {
        __initialized: { value: true, writable: false },
        subjects: {
            value: subjectGlobal,
            writable: false,
        },
    };

    Object.defineProperties(subjectGlobal, properties);

    window.____c_p_s = subjectGlobal;
};

if (!('____c_p_s' in window) || !window.____c_p_s.__initialized) {
    defineSubjectGlobal();
}

type UnpackPayloadType<SubjectType> = SubjectType extends Subject<infer U> ? U : never;

/**
 * Registers a debug subject as singleton
 *
 * @param key
 * @param subjectFactory
 */
export const registerDebugSubject = <SubjectType extends Subject<PayloadType>, PayloadType = UnpackPayloadType<SubjectType>>(
    key: DebugSubjects,
    subjectFactory: () => SubjectType,
): SubjectType => {
    const debugSubjects = window.____c_p_s.subjects as DebugSubjectsMap<SubjectType>;

    // good old singletons - ensure we do not register the same subject twice.
    // we also can't fail as multiple instances might try to access the same, so instead we return the previously registered one.
    if (!(key in debugSubjects)) {
        debugSubjects[key] = subjectFactory();
    }

    return debugSubjects[key];
};

export const observeDebugSubject = <PayloadType>(key: DebugSubjects, observer: Observer<PayloadType>) => {
    const debugSubjects = window.____c_p_s.subjects as DebugSubjectsMap<Subject<PayloadType>>;

    if (!(key in debugSubjects)) {
        throw new Error(`No subject registered for key: "${key}"`);
    }

    return debugSubjects[key].subscribe(observer);
};
