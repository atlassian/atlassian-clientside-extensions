import { Subject } from '@atlassian/clientside-extensions-base';
import { DebugSubjects, observeDebugSubject, registerDebugSubject } from './debug-subjects';

describe('debug-subject', () => {
    const debugFactory = (): Subject<string> => new Subject<string>();

    describe('registerDebugSubject', () => {
        it('should create an register a subject under a given namespace', () => {
            const subject = registerDebugSubject(DebugSubjects.Logger, debugFactory);
            expect(subject).toBeInstanceOf(Subject);
        });

        it('should not create a new instance for a previously registered key', () => {
            const subject = registerDebugSubject(DebugSubjects.State, debugFactory);
            const sameSubject = registerDebugSubject(DebugSubjects.State, debugFactory);
            expect(subject).toBe(sameSubject);
        });
    });

    describe('observeDebugSubject', () => {
        it('should throw if the subject does not exist yet', () => {
            // @ts-expect-error We are using a wrong subject key here
            const unregisteredSubject = () => observeDebugSubject('Foo', () => {});

            expect(unregisteredSubject).toThrow(`No subject registered for key: "Foo"`);
        });

        it('should subscribe to a previously registered subject', () => {
            const SUBJECT = DebugSubjects.State;
            const TEST_PAYLOAD = 'some payload';

            const observer = jest.fn();
            const subject = registerDebugSubject(SUBJECT, debugFactory);
            observeDebugSubject(SUBJECT, observer);

            subject.notify(TEST_PAYLOAD);
            expect(observer).toBeCalledWith(TEST_PAYLOAD);
        });
    });
});
