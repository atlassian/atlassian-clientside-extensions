export { onDebug } from './debug';
export type { LoggerPayload } from './logger/logger';
export { LogLevel } from './logger/logger';

export {
    /** @deprecated in 2.1.0 */ observeLogger,
    /** @since in 2.1.0 */ setLogger,
    _deregisterDefaultLogger,
} from './logger/default-logger';

export { /** @since in 2.1.0 */ consoleLogger } from './logger/console';

export {
    isDebugEnabled,
    isLoggingEnabled,
    isDiscoveryEnabled,
    isValidationEnabled,
    setDebugEnabled,
    setLoggingEnabled,
    setDiscoveryEnabled,
    setValidationEnabled,
    observeStateChange,
} from './debug-state';
