# Client-side Extensions

## Debug utilities

Set of utils that provide debug capabilities for client-side extensions system.

Refer to the [official documentation](https://developer.atlassian.com/server/framework/clientside-extensions) for more information.
