import type { ClientsideExtensionsOptions } from './types';

// Utility helper that can be used to read property from an object and provide typing
export const getKeyValue = <T, K extends keyof T>(obj: T, key: K) => obj[key];

export const isPageExtension = (options: ClientsideExtensionsOptions): boolean => Boolean(options.pageUrl);

const KEY_REGEX = /\$key/g;
export const replaceKey = (str: string, key: string) => {
    return str.replace(KEY_REGEX, key);
};

export function isNotNull<T>(input: null | T): input is T {
    return input != null;
}

export function isNotNullOrUndefined<T>(input: null | undefined | T): input is T {
    return input !== undefined && input != null;
}

export function debugLog(message: String): void {
    if (process.env.CSE_DEBUG) {
        // eslint-disable-next-line prefer-rest-params,no-console
        console.debug(message);
    }
}
