import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point my.page.menu
 * @label "My page"
 * @page-url /my-page-with-quoted-title
 * @page-title "The title of my page"
 */
PageExtension.factory(() => {});
