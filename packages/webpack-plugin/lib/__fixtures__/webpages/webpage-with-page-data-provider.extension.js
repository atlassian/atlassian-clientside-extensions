import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point webpage-with-page-data-provider
 * @label "My page with server-side data"
 * @page-url /my-page-with-server-side-data
 * @page-data-provider Foo.Bar.moo
 */
PageExtension.factory(() => {});
