import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point my.page.submenu
 * @label "My page"
 * @page-url /my-page-with-title
 * @page-title the.title.of.my.page
 */
PageExtension.factory(() => {});
