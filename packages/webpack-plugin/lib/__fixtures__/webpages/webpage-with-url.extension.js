import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point foo.bar
 * @label "My first extension"
 * @page-url /my-url
 */
PageExtension.factory(() => {});
