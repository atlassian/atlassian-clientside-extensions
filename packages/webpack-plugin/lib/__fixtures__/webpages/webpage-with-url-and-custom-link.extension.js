import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point biz.baz
 * @label "My page"
 * @link /my-custom-link-url
 * @page-url /my-page-url
 */
PageExtension.factory(() => {});
