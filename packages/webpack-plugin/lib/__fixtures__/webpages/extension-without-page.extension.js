import { ButtonExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point not.a.page
 */
ButtonExtension.factory(() => ({ label: ' first button' }));
