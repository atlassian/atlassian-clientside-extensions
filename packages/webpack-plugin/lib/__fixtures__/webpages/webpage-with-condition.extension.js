import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point webpage-with-condition
 * @label "My page with condition"
 * @page-url /my-page-with-condition
 * @condition class com.example.Foo
 */
PageExtension.factory(() => {});
