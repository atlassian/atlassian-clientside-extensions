import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point bar.baz
 * @label "Bar Baz"
 * @page-url /bar-baz
 */
PageExtension.factory(() => {});
