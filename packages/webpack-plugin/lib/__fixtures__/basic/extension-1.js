import { ButtonExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point foo.bar
 */
ButtonExtension.factory(() => ({ label: 'My first extension' }));
