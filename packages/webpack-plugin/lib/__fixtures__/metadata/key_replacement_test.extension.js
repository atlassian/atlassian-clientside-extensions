import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point webpage-with-condition
 * @label "My Extension key is $key"
 * @page-url /my-page-with-condition?q=$key
 * @condition class com.example.Foo
 */
PageExtension.factory(() => {});
