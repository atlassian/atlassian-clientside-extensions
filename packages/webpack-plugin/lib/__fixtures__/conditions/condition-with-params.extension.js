import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point condition.with.params
 * @label "Condition with params"
 * @page-url /condition-with-params
 * @condition class com.acme.MyConditionWithParams
 * @condition params.0.attributes.name permission
 * @condition params.0.value admin
 */
PageExtension.factory(() => {});
