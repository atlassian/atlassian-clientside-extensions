import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point simple.condition
 * @label "Simple Condition"
 * @page-url /simple-condition
 * @condition class com.acme.MyCondition
 */
PageExtension.factory(() => {});
