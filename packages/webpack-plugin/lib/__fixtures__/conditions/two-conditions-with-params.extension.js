import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point two.conditions.with.params
 * @label "Two conditions with params"
 * @page-url /two-conditions-with-params
 * @condition type AND
 * @condition conditions.0.class com.acme.MyFirstCondition
 * @condition conditions.0.params.0.attributes.name MyParam
 * @condition conditions.0.params.0.value My Value
 * @condition conditions.1.class com.acme.MySecondCondition
 * @condition conditions.1.params.0.attributes.name foo
 * @condition conditions.1.params.0.value bar
 * @condition conditions.1.params.1.attributes.name biz
 * @condition conditions.1.params.1.value baz
 */
PageExtension.factory(() => {});
