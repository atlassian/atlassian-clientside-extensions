import { PageExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point admin.panel
 * @label "My admin page"
 * @page-url /my-admin-page-url
 * @page-data-provider Foo.bar
 */
PageExtension.factory(() => {});
