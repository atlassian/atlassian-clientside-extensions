import { PageExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point admin.panel
 * @page-url /my-admin-page-url
 */
PageExtension.factory(() => {});
