import { PageExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point admin.panel
 * @label "My admin page"
 * @page-decorator atl.admin
 */
PageExtension.factory(() => {});
