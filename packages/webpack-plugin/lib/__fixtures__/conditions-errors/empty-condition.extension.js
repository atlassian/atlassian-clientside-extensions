import { ButtonExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point null.condition
 * @condition
 */
export default ButtonExtension.factory(() => {});
