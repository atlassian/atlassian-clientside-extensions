import { ButtonExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point invalid.condition.class
 * @condition class
 */
export default ButtonExtension.factory(() => {});
