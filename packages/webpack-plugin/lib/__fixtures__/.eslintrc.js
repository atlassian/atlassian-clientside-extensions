module.exports = {
    rules: {
        // Enable importing devDependencies within the testing fixtures
        'import/no-extraneous-dependencies': ['error', { devDependencies: true }],

        // We do import files that are not published
        'node/no-unpublished-import': 'off',

        'no-console': 'off',
    },
};
