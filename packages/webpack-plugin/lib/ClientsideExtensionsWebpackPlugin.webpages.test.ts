import type { Stats } from 'webpack';
import path from 'path';
import WrmPlugin from 'atlassian-webresource-webpack-plugin';
import { mocked } from 'ts-jest/utils';
// eslint-disable-next-line import/order
import webpack = require('webpack');
import { getOutputContentAsJson, fileContent } from './tests/test-helper';

import Plugin = require('./ClientsideExtensionsWebpackPlugin');

const { PAGE_DATA_PROVIDER_DATA_KEY } = require('./generator/webpageDescriptor');

beforeAll(() => {
    // Silent-down the webpack compilation errors
    jest.spyOn(console, 'error').mockImplementation();
    jest.spyOn(console, 'warn').mockImplementation();
});

afterAll(() => {
    mocked(console.error).mockRestore();
    mocked(console.warn).mockRestore();
});

const PLUGIN_KEY = 'a.fake.plugin.key';

describe('webpages compilation', () => {
    const inputDir = path.resolve(__dirname, '__fixtures__/webpages/');
    const outputDir = path.resolve(__dirname, '__fixtures__/target/');
    const wrmXmlOutputFilename = 'wrm-generated-webpages.xml';
    const cseXmlOutputFilename = 'cse-generated-webpages.xml';

    const basicWrmPlugin = new WrmPlugin({
        pluginKey: PLUGIN_KEY,
        xmlDescriptors: path.resolve(outputDir, wrmXmlOutputFilename),
    });

    const csePlugin = new Plugin({
        cwd: inputDir,
        pattern: '**/*.extension.js',
        xmlDescriptors: path.resolve(outputDir, cseXmlOutputFilename),
    });

    // base config for all tests
    const baseWebpackConfig = {
        entry: csePlugin.generateEntrypoints(),
        plugins: [basicWrmPlugin, csePlugin],
        output: {
            path: outputDir,
        },
        optimization: {
            minimize: false,
            // Disable split chunks so the web-resource plugin can generate
            // a single resource descriptor per each extension entrypoint module
            splitChunks: false as false,
        },
    };

    const getWrmOutputContentAsJson = () => getOutputContentAsJson(fileContent(path.join(outputDir, wrmXmlOutputFilename)));
    const getCseOutputContentAsJson = () => getOutputContentAsJson(fileContent(path.join(outputDir, cseXmlOutputFilename)));

    beforeAll((done) => {
        const compiler = webpack(baseWebpackConfig);

        compiler.run((err) => {
            if (err) {
                throw err;
            }

            done();
        });
    });

    it('should generate the web-item and a matching web-page descriptor when using @page-url annotation', () => {
        const expectedExtensionPoint = 'foo.bar';
        const expectedWebItemKey = 'webpage-with-url__extension__js';
        const expectedWebPageKey = 'webpage-webpage-with-url__extension__js';
        const expectedFullExtensionKey = `${PLUGIN_KEY}:${expectedWebItemKey}`;
        const expectedWebResourceKey = `${PLUGIN_KEY}:entrypoint-webpage-with-url-extension-js`;

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                        weight: '',
                    },
                    label: expect.any(Array),
                    link: '/plugins/servlet/my-url',
                },
            ]),
        });

        const expectedWebPageDeclarationShape = expect.objectContaining({
            'web-page': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebPageKey,
                        'extension-point': expectedExtensionPoint,
                        'extension-key': expectedFullExtensionKey,
                    },
                    'url-pattern': '/my-url',
                    dependency: expectedWebResourceKey,
                },
            ]),
        });

        const outputJson = getCseOutputContentAsJson();

        expect(outputJson).toContainEqual(expectedWebItemDeclarationShape);
        expect(outputJson).toContainEqual(expectedWebPageDeclarationShape);
    });

    it('should allow to provide a custom link value when using both @page-url and @link annotations', () => {
        const expectedLinkUrl = '/my-custom-link-url';
        const expectedPageUrl = '/my-page-url';

        const expectedExtensionPoint = 'biz.baz';
        const expectedWebItemKey = 'webpage-with-url-and-custom-link__extension__js';
        const expectedWebPageKey = 'webpage-webpage-with-url-and-custom-link__extension__js';
        const expectedFullExtensionKey = `${PLUGIN_KEY}:${expectedWebItemKey}`;
        const expectedWebResourceKey = `${PLUGIN_KEY}:entrypoint-webpage-with-url-and-custom-link-extension-js`;

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: expect.objectContaining({
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                    }),
                    link: expectedLinkUrl,
                    label: expect.anything(),
                },
            ]),
        });

        const expectedWebPageDeclarationShape = expect.objectContaining({
            'web-page': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebPageKey,
                        'extension-point': expectedExtensionPoint,
                        'extension-key': expectedFullExtensionKey,
                    },
                    'url-pattern': expectedPageUrl,
                    dependency: expectedWebResourceKey,
                },
            ]),
        });

        const outputJson = getCseOutputContentAsJson();

        expect(outputJson).toContainEqual(expectedWebItemDeclarationShape);
        expect(outputJson).toContainEqual(expectedWebPageDeclarationShape);
    });

    it('should allow to provide a custom page decorator using @page-decorator annotation', () => {
        const expectedPageUrl = '/my-admin-page-url';
        const expectedPageDecorator = 'atl.admin';

        const expectedExtensionPoint = 'admin.panel';
        const expectedWebItemKey = 'webpage-with-decorator__extension__js';
        const expectedWebPageKey = 'webpage-webpage-with-decorator__extension__js';
        const expectedFullExtensionKey = `${PLUGIN_KEY}:${expectedWebItemKey}`;
        const expectedWebResourceKey = `${PLUGIN_KEY}:entrypoint-webpage-with-decorator-extension-js`;

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: expect.objectContaining({
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                    }),
                    label: expect.anything(),
                    link: expect.any(String),
                },
            ]),
        });

        const expectedWebPageDeclarationShape = expect.objectContaining({
            'web-page': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebPageKey,
                        'extension-point': expectedExtensionPoint,
                        'extension-key': expectedFullExtensionKey,
                        'page-decorator': expectedPageDecorator,
                    },
                    'url-pattern': expectedPageUrl,
                    dependency: expectedWebResourceKey,
                },
            ]),
        });

        const outputJson = getCseOutputContentAsJson();

        expect(outputJson).toContainEqual(expectedWebItemDeclarationShape);
        expect(outputJson).toContainEqual(expectedWebPageDeclarationShape);
    });

    it('should allow to provide a custom page title using @page-title annotation and property value', () => {
        const expectedPageUrl = '/my-page-with-title';
        const expectedPageTitle = 'the.title.of.my.page';

        const expectedExtensionPoint = 'my.page.submenu';
        const expectedWebItemKey = 'webpage-with-title__extension__js';
        const expectedWebPageKey = 'webpage-webpage-with-title__extension__js';
        const expectedFullExtensionKey = `${PLUGIN_KEY}:${expectedWebItemKey}`;
        const expectedWebResourceKey = `${PLUGIN_KEY}:entrypoint-webpage-with-title-extension-js`;

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: expect.objectContaining({
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                    }),
                    label: expect.anything(),
                    link: expect.any(String),
                },
            ]),
        });

        const expectedWebPageDeclarationShape = expect.objectContaining({
            'web-page': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebPageKey,
                        'extension-point': expectedExtensionPoint,
                        'extension-key': expectedFullExtensionKey,
                    },
                    'url-pattern': expectedPageUrl,
                    'page-title': [
                        {
                            attrs: {
                                key: expectedPageTitle,
                            },
                        },
                    ],
                    dependency: expectedWebResourceKey,
                },
            ]),
        });

        const outputJson = getCseOutputContentAsJson();

        expect(outputJson).toContainEqual(expectedWebItemDeclarationShape);
        expect(outputJson).toContainEqual(expectedWebPageDeclarationShape);
    });

    it('should allow to provide a custom page title using @page-title annotation and quoted value', () => {
        const expectedPageUrl = '/my-page-with-quoted-title';
        const expectedPageTitle = 'The title of my page';

        const expectedExtensionPoint = 'my.page.menu';
        const expectedWebItemKey = 'webpage-with-quoted-title__extension__js';
        const expectedWebPageKey = 'webpage-webpage-with-quoted-title__extension__js';
        const expectedFullExtensionKey = `${PLUGIN_KEY}:${expectedWebItemKey}`;
        const expectedWebResourceKey = `${PLUGIN_KEY}:entrypoint-webpage-with-quoted-title-extension-js`;

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: expect.objectContaining({
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                    }),
                    label: expect.anything(),
                    link: expect.any(String),
                },
            ]),
        });

        const expectedWebPageDeclarationShape = expect.objectContaining({
            'web-page': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebPageKey,
                        'extension-point': expectedExtensionPoint,
                        'extension-key': expectedFullExtensionKey,
                    },
                    'url-pattern': expectedPageUrl,
                    'page-title': [
                        {
                            attrs: {
                                key: expectedPageTitle,
                            },
                        },
                    ],
                    dependency: expectedWebResourceKey,
                },
            ]),
        });

        const outputJson = getCseOutputContentAsJson();

        expect(outputJson).toContainEqual(expectedWebItemDeclarationShape);
        expect(outputJson).toContainEqual(expectedWebPageDeclarationShape);
    });

    it('should allow to configure a data provider when using @page-data-provider annotation', () => {
        const expectedPageUrl = '/my-page-with-server-side-data';
        const expectedWebResourceKey = 'entrypoint-webpage-with-page-data-provider-extension-js';
        const expectedFullWebResourceKey = `${PLUGIN_KEY}:entrypoint-webpage-with-page-data-provider-extension-js`;

        const expectedPageDataProviderKey = PAGE_DATA_PROVIDER_DATA_KEY;
        const expectedPageDataProviderClass = 'Foo.Bar.moo';
        const expectedPageDataProviderFullKey = `${expectedFullWebResourceKey}.${expectedPageDataProviderKey}`;

        const expectedExtensionPoint = 'webpage-with-page-data-provider';
        const expectedWebItemKey = 'webpage-with-page-data-provider__extension__js';
        const expectedWebPageKey = 'webpage-webpage-with-page-data-provider__extension__js';
        const expectedFullExtensionKey = `${PLUGIN_KEY}:${expectedWebItemKey}`;

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: expect.objectContaining({
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                    }),
                    label: expect.anything(),
                    link: expect.any(String),
                },
            ]),
        });

        const expectedWebResourceDeclarationShape = expect.objectContaining({
            'web-resource': expect.arrayContaining([
                expect.objectContaining({
                    attrs: expect.objectContaining({
                        key: expectedWebResourceKey,
                    }),
                    data: expect.arrayContaining([
                        {
                            attrs: expect.objectContaining({
                                key: expectedPageDataProviderKey,
                                class: expectedPageDataProviderClass,
                            }),
                        },
                    ]),
                }),
            ]),
        });

        const expectedWebPageDeclarationShape = expect.objectContaining({
            'web-page': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebPageKey,
                        'extension-point': expectedExtensionPoint,
                        'extension-key': expectedFullExtensionKey,
                        'page-data-provider-key': expectedPageDataProviderFullKey,
                    },
                    'url-pattern': expectedPageUrl,
                    dependency: expectedFullWebResourceKey,
                },
            ]),
        });

        const wrmOutputJson = getWrmOutputContentAsJson();
        const cseOutputJson = getCseOutputContentAsJson();

        expect(wrmOutputJson).toContainEqual(expectedWebResourceDeclarationShape);
        expect(cseOutputJson).toContainEqual(expectedWebItemDeclarationShape);
        expect(cseOutputJson).toContainEqual(expectedWebPageDeclarationShape);
    });

    it('should not generate the condition on the web-resource element when using @condition annotation', () => {
        const expectedPageUrl = '/my-page-with-condition';
        const expectedWebResourceKey = 'entrypoint-webpage-with-condition-extension-js';

        const expectedConditionClass = 'com.example.Foo';

        const expectedExtensionPoint = 'webpage-with-condition';
        const expectedWebItemKey = 'webpage-with-condition__extension__js';
        const expectedWebPageKey = 'webpage-webpage-with-condition__extension__js';
        const expectedFullExtensionKey = `${PLUGIN_KEY}:${expectedWebItemKey}`;
        const expectedFullWebResourceKey = `${PLUGIN_KEY}:entrypoint-webpage-with-condition-extension-js`;

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: expect.objectContaining({
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                    }),
                    label: expect.anything(),
                    link: expect.any(String),
                    condition: expect.arrayContaining([
                        {
                            attrs: expect.objectContaining({
                                class: expectedConditionClass,
                            }),
                        },
                    ]),
                },
            ]),
        });

        const expectedWebResourceDeclarationShape = expect.objectContaining({
            'web-resource': expect.arrayContaining([
                // Web-resource without condition should be present
                expect.objectContaining({
                    attrs: expect.objectContaining({
                        key: expectedWebResourceKey,
                    }),
                }),

                // Web-resource with a condition should not be present
                expect.not.objectContaining({
                    attrs: expect.objectContaining({
                        key: expectedWebResourceKey,
                    }),
                    condition: expect.arrayContaining([
                        {
                            attrs: expect.objectContaining({
                                class: expectedConditionClass,
                            }),
                        },
                    ]),
                }),
            ]),
        });

        const expectedWebPageDeclarationShape = expect.objectContaining({
            'web-page': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebPageKey,
                        'extension-point': expectedExtensionPoint,
                        'extension-key': expectedFullExtensionKey,
                    },
                    'url-pattern': expectedPageUrl,
                    dependency: expectedFullWebResourceKey,
                },
            ]),
        });

        const wrmOutputJson = getWrmOutputContentAsJson();
        const cseOutputJson = getCseOutputContentAsJson();

        expect(wrmOutputJson).toContainEqual(expectedWebResourceDeclarationShape);
        expect(cseOutputJson).toContainEqual(expectedWebItemDeclarationShape);
        expect(cseOutputJson).toContainEqual(expectedWebPageDeclarationShape);
    });
});

describe('webpages compilation errors', () => {
    const inputDir = path.resolve(__dirname, '__fixtures__/webpages-errors/');
    const outputDir = path.resolve(__dirname, '__fixtures__/target/');
    const xmlOutputFilename = 'wr-generated-cse-webpages-errors.xml';

    const getWebpackConfig = (csePattern: string) => {
        const basicWrmPlugin = new WrmPlugin({
            pluginKey: PLUGIN_KEY,
            xmlDescriptors: path.resolve(outputDir, 'wrm.xml'),
        });

        const csePlugin = new Plugin({
            cwd: inputDir,
            pattern: csePattern,
            xmlDescriptors: path.resolve(outputDir, xmlOutputFilename),
        });

        const webpackConfig = {
            entry: csePlugin.generateEntrypoints(),
            plugins: [basicWrmPlugin, csePlugin],
            optimization: {
                minimize: false,
            },
            output: {
                path: outputDir,
            },
        };

        return webpackConfig;
    };

    const compile = (config: webpack.Configuration): Promise<Stats> => {
        const compiler = webpack(config);

        return new Promise<Stats>((resolve, reject) => {
            compiler.run((err, stats) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(stats!);
            });
        });
    };

    it('should throw an error when trying to create an extension with @page-url and no @label annotation', async () => {
        const errorFile = '**/webpage-without-label.*';
        const successFile = '**/webpage-with-label.*';

        expect(() => getWebpackConfig(errorFile)).toThrowError();
        expect(() => getWebpackConfig(successFile)).not.toThrowError();

        await expect(compile(getWebpackConfig(successFile))).resolves.toBeTruthy();
    });

    it('should throw an error when trying to create an extension with @page-decorator and no @page-url annotation', async () => {
        const errorFile = '**/webpage-decorator-without-url.*';
        const successFile = '**/webpage-decorator-with-url.*';

        expect(() => getWebpackConfig(errorFile)).toThrowError();
        expect(() => getWebpackConfig(successFile)).not.toThrowError();

        await expect(compile(getWebpackConfig(successFile))).resolves.toBeTruthy();
    });

    it('should throw an error when trying to create an extension with @page-data-provider but without @page-url annotation', async () => {
        const errorFile = '**/webpage-data-provider-without-url.*';
        const successFile = '**/webpage-data-provider-with-url.*';

        expect(() => getWebpackConfig(errorFile)).toThrowError();
        expect(() => getWebpackConfig(successFile)).not.toThrowError();

        await expect(compile(getWebpackConfig(successFile))).resolves.toBeTruthy();
    });
});
