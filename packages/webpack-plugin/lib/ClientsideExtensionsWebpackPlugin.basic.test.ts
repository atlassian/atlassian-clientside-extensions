import { mocked } from 'ts-jest/utils';
import path from 'path';
import type { Stats } from 'webpack';
import WrmPlugin from 'atlassian-webresource-webpack-plugin';
import { fileContent } from './tests/test-helper';

// eslint-disable-next-line import/order
import webpack = require('webpack');
import Plugin = require('./ClientsideExtensionsWebpackPlugin');

const inputDir = path.resolve(__dirname, '__fixtures__/basic/');
const outputDir = path.resolve(__dirname, '__fixtures__/target/');

beforeAll(() => {
    // Silent-down the webpack compilation errors
    jest.spyOn(console, 'error').mockImplementation();
});

afterAll(() => {
    mocked(console.error).mockRestore();
});

describe('basic compilation', () => {
    const basicWrmPlugin = new WrmPlugin({
        pluginKey: 'a.fake.plugin.key',
        xmlDescriptors: path.resolve(outputDir, 'wrm.xml'),
    });

    const basicPlugin = new Plugin({
        cwd: inputDir,
        pattern: '**/extension*.js',
    });

    // base config for all tests
    const baseWebpackConfig = {
        entry: {
            first: path.resolve(inputDir, 'first.js'),
            second: path.resolve(inputDir, 'second.js'),
            ...basicPlugin.generateEntrypoints(),
        },
        optimization: {
            minimize: false,
        },
        plugins: [basicWrmPlugin, basicPlugin],
        output: {
            path: outputDir,
        },
    };

    let stats: Stats;

    const fileExists = (filePath: string) => {
        if (!stats) {
            throw new Error('webpack compilation failed');
        }

        if (!(filePath in stats.compilation.assets)) {
            throw new Error(
                `File "${filePath}" was not crated by running webpack bundle process: \n${Object.keys(stats.compilation.assets).join(
                    ', ',
                )}`,
            );
        }

        return Boolean(stats.compilation.assets[filePath]);
    };

    beforeAll(async () => {
        const compiler = webpack(baseWebpackConfig);

        return new Promise((resolve, reject) => {
            compiler.run((err, webpackStats) => {
                if (err) {
                    reject(err);
                    return;
                }

                stats = webpackStats!;

                resolve(stats);
            });
        });
    });

    it('generates web-items', () => {
        const fileName = 'wr-generated-clientside-extensions.xml';
        expect(fileExists(fileName)).toBe(true);

        const xmlFileContent = fileContent(path.join(outputDir, fileName));
        const foundExtensions = String(xmlFileContent).match(/<web-item\s/g);
        expect(foundExtensions).not.toBe(null);
        expect(foundExtensions?.length).toBe(3);
    });

    it('outputs compiled extension code to an appropriate location', () => {
        expect(fileExists('extension-1-js.js')).toBe(true);
        expect(fileExists('extension-2-js.js')).toBe(true);
        expect(fileExists('nested-extension-42-js.js')).toBe(true);
    });
});
