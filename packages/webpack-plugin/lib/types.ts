import type { Annotations } from './annotations';

export interface ClientsideExtensionsManifest extends Annotations {
    filePath: string;
    id: string;
}

export interface ClientsideExtensionsOptions extends ClientsideExtensionsManifest {
    /** Webpack entrypoint ID */
    entryPoint: string;

    /** A fully qualified namespace e.g. `example.atlassian.plugin.key:module.descriptor.key` */
    fullyQualifiedNamespace: string;

    /** Plugin key e.g. `example.atlassian.plugin.key` */
    pluginKey: string;
}

export interface DataProvider {
    key: string;
    class: string;
}

export interface IWrmPluginOptions {
    assetContentTypes: string;
    conditionMap: object;
    contextMap: Map<string, string[]>;
    dataProvidersMap: Map<string, DataProvider[]>;
    locationPrefix: string;
    noWRM: boolean;
    pluginKey: string;
    providedDependencies: object;
    transformationMap: object;
    verbose: boolean;
    watch: boolean;
    watchPrepare: boolean;
    webresourceKeyMap: object;
    xmlDescriptors: string;
}

// Type helper type that can be used with `Object.entries` function e.g. `Object.entries(foo) as Entries<typeof foo>`
export type Entries<T> = T extends ArrayLike<infer U> ? [string, U][] : { [K in keyof T]: [K, T[K]] }[keyof T][];
