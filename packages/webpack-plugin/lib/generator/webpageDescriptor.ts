import { renderElement } from './xml';
import type { ClientsideExtensionsOptions } from '../types';
import { isNotNullOrUndefined, isPageExtension, replaceKey } from '../utils';

export const PAGE_DATA_PROVIDER_DATA_KEY = 'data-provider';

const generateWebpageDescriptor = (options: ClientsideExtensionsOptions): string | null => {
    if (!isPageExtension(options)) {
        return null;
    }

    // Complete data provider key e.g. [groupId].[artifactId]:[webResourceKey].[dataKey]
    let pageDataProviderKey;

    if (options.pageDataProvider) {
        pageDataProviderKey = `${options.pluginKey}:${options.entryPoint}.${PAGE_DATA_PROVIDER_DATA_KEY}`;
    }

    // Ensure the module descriptor key is unique
    const moduleKey = `webpage-${options.key}`;

    // Get a full web-resource key for the extension entrypoint web-resource
    const webResourceKey = `${options.pluginKey}:${options.entryPoint}`;

    const webpageAttributes = [
        ['key', moduleKey],
        ['extension-point', options.extensionPoint],
        ['extension-key', options.fullyQualifiedNamespace],
        ['page-data-provider-key', pageDataProviderKey],
        ['page-decorator', options.pageDecorator],
    ];

    const attributes = Object.fromEntries(webpageAttributes.filter(([, value]) => value));

    const urlPatternNode = renderElement('url-pattern', null, options.pageUrl);

    let pageTitleNode;
    if (options.pageTitle) {
        pageTitleNode = renderElement('page-title', { key: options.pageTitle });
    }

    const dependencyNode = renderElement('dependency', null, webResourceKey);

    const children = [urlPatternNode, pageTitleNode, dependencyNode]
        .filter(isNotNullOrUndefined)
        .map((value) => replaceKey(value, options.fullyQualifiedNamespace));

    return renderElement('web-page', attributes, children);
};

export default generateWebpageDescriptor;
