type Attributes = {
    [key: string]: string | number;
};
const serializeAttributes = (attributes: Attributes | string | null) => {
    if (!attributes) {
        return '';
    }

    if (typeof attributes === 'string') {
        return attributes;
    }

    return Object.keys(attributes)
        .map((key) => {
            const val = typeof attributes[key] === 'undefined' ? '' : String(attributes[key]);
            return `${key}="${val}"`;
        })
        .join(' ');
};

export const renderElement = (name: string, attributes: Attributes | string | null, children?: string | string[]) => {
    // render self-closing tags based on whether there is no child input.
    if (!children) {
        return `<${name} ${serializeAttributes(attributes)}/>`;
    }

    // convert children array in to a string.
    if (Array.isArray(children)) {
        children = children.join('\n');
    }

    return `<${name} ${serializeAttributes(attributes)}>${children}</${name}>`;
};

type Param = {
    attributes: Attributes;
    value: string | string[];
};
const renderParams = (params: Param[]) => {
    if (!params) {
        return '';
    }
    return params.map((param) => renderElement('param', param.attributes, param.value)).join('');
};

export type Condition = {
    conditions?: Condition[];
    type: string;
    invert: boolean;
    class: string;
    params: Param[];
};
export const renderCondition = (condition?: null | Condition | Condition[]): string => {
    if (!condition) {
        return '';
    }

    // we have actual conditions
    if (Array.isArray(condition)) {
        return condition.map(renderCondition).join('');
    }
    // we have a "conditions"-joiner for multiple sub conditions
    if (condition.type) {
        return renderElement('conditions', { type: condition.type }, renderCondition(condition.conditions));
    }

    return renderElement(
        'condition',
        `class="${condition.class}" ${condition.invert ? 'invert="true"' : ''}`,
        renderParams(condition.params),
    );
};
