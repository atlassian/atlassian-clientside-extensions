import { renderElement, renderCondition } from './xml';
import type { ClientsideExtensionsOptions } from '../types';
import transformConditionToConditionMapObject from './conditions';
import { isNotNullOrUndefined, replaceKey } from '../utils';

/**
 * This generator ensures that web-fragment tags are generated for the extension. It will generate:
 *  - a <web-item> tag required for the PageExtension
 *  - a legacy <web-item> descriptor required by the extensions in CSE version 1.x
 *    The generation of the legacy <web-item> could be removed once all the products migrate to use only CSE 2+
 *    in a major version of all product. Currently, both CSE 1.x and 2.x are used by Bitbucket Server 7.x.
 */
export default function generateWebfragmentDescriptor(options: ClientsideExtensionsOptions): string | null {
    const label = renderElement('label', { key: options.label! });
    const link = options.link && renderElement('link', {}, options.link);

    let conditions;

    if (options.condition) {
        const conditionMap = transformConditionToConditionMapObject(options.condition);

        conditions = renderCondition(conditionMap);
    }

    const children = [label, link, conditions]
        .filter(isNotNullOrUndefined)
        .map((value) => replaceKey(value, options.fullyQualifiedNamespace));

    return renderElement('web-item', { key: options.key, section: options.extensionPoint, weight: options.weight! }, children);
}
