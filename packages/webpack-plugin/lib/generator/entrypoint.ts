import fs from 'fs';
import path from 'path';
import type { ClientsideExtensionsOptions } from '../types';

const extensionEntrypointTemplate = fs.readFileSync(path.join(__dirname, '..', 'runtime', 'extension-entrypoint.js'), 'utf8');
const webPageEntrypointTemplate = fs.readFileSync(path.join(__dirname, '..', 'runtime', 'web-page-entrypoint.js'), 'utf8');

export const isPageExtension = (extensionOptions: ClientsideExtensionsOptions) => {
    return Boolean(extensionOptions.pageUrl);
};

const warnOnAnnotationDeprecation = (annotation: string, attribute: string = annotation): void => {
    console.error(`!!!
The "@${annotation}" annotation is deprecated. Please provide the "${attribute}" in your javascript code instead.
!!!`);
};

export function generateEntrypointCode(extensionOption: ClientsideExtensionsOptions) {
    const template = isPageExtension(extensionOption) ? webPageEntrypointTemplate : extensionEntrypointTemplate;

    const { weight, extensionPoint, label, link } = extensionOption;

    if (label) {
        warnOnAnnotationDeprecation('label');
    }
    if (weight) {
        warnOnAnnotationDeprecation('weight');
    }
    if (link) {
        warnOnAnnotationDeprecation('link', 'url');
    }

    return template
        .replace(/REPLACE_WITH_PATHNAME/g, extensionOption.filePath)
        .replace(/REPLACE_WITH_PLUGIN_ID/g, extensionOption.fullyQualifiedNamespace)
        .replace(
            /REPLACE_WITH_EXTENSION_OPTIONS/g,
            JSON.stringify({
                location: extensionPoint,
                label,
                weight,
                url: link,
            }),
        );
}
