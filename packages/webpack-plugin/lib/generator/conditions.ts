import set from 'lodash/set';
import type { ConditionMap } from '../annotations';
import type { Condition } from './xml';

export default function transformConditionToConditionMapObject(condition: ConditionMap): Condition {
    const conditionMap = {} as Condition;

    try {
        for (const [keyPath, value] of Object.entries(condition)) {
            set(conditionMap, keyPath, value);
        }
    } catch (e) {
        throw new Error(`Client-side Extension: The syntax of "@condition" annotation is invalid`);
    }

    return conditionMap;
}
