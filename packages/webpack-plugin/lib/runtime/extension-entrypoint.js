import registry from '@atlassian/clientside-extensions-registry';
// eslint-disable-next-line import/no-unresolved
import plugin from 'REPLACE_WITH_PATHNAME';

// we are not in watch mode
// eslint-disable-next-line no-undef
registry.registerExtension('REPLACE_WITH_PLUGIN_ID', plugin, REPLACE_WITH_EXTENSION_OPTIONS);

// we are in watch mode
if (module.hot) {
    module.hot.accept('REPLACE_WITH_PATHNAME', () => {
        // eslint-disable-next-line global-require,import/no-unresolved,no-undef
        registry.registerExtension('REPLACE_WITH_PLUGIN_ID', require('REPLACE_WITH_PATHNAME').default, REPLACE_WITH_EXTENSION_OPTIONS);
    });
}
