module.exports = {
    rules: {
        // We use a tokens that will be replaced during the webpack compilation time
        'node/no-missing-import': 'off',
        'node/no-missing-require': 'off',
    },
};
