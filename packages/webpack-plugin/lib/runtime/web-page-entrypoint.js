// eslint-disable-next-line node/no-missing-import,import/no-unresolved -- This is a special import directive for webpack plugin
import 'wr-dependency!com.atlassian.plugins.atlassian-clientside-extensions-page-bootstrapper:page-bootstrapper';

// eslint-disable-next-line node/no-missing-import,import/no-unresolved
import plugin from 'REPLACE_WITH_PATHNAME';

// We need to trick webpack to skip resolving the runtime AMD module.
// That's why we can't use directed `require(...)` global function or import syntax at this point.
// eslint-disable-next-line node/no-missing-require
window.require(['@atlassian/clientside-extensions-page-bootstrapper'], (pageBootstrapper) => {
    // we are not in watch mode
    pageBootstrapper.registerPage('REPLACE_WITH_PLUGIN_ID', plugin);

    // we are in watch mode
    if (module.hot) {
        module.hot.accept('REPLACE_WITH_PATHNAME', () => {
            // eslint-disable-next-line node/no-missing-require
            pageBootstrapper.registerPage('REPLACE_WITH_PLUGIN_ID', require('REPLACE_WITH_PATHNAME').default);
        });
    }
});
