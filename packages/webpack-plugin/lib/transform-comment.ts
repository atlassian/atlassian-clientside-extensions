import type { Block } from 'comment-parser';
import { parse as parseComment } from 'comment-parser';
import camelCase from 'lodash/camelCase';
import type { AnnotationKey, MultiValueAnnotationKey, RawAnnotations, SingleValueAnnotationKey } from './annotations';
import { debugLog } from './utils';

const CLIENTSIDE_EXTENSION_ANNOTATION = 'clientside-extension';
const MULTI_VALUE_ANNOTATIONS = ['condition'];

const getClientSideExtensionAnnotations = (comments: Block[]) =>
    comments.find((comment) => comment.tags.some(({ tag }) => tag === CLIENTSIDE_EXTENSION_ANNOTATION));

/**
 * Transforms a raw comment with annotations into annotations map
 */
const transformComment = (fileContent: string) => {
    const comments = parseComment(fileContent);

    debugLog(`Parsing raw comments: ${JSON.stringify(comments)}`);

    const extensionAnnotationsComment = getClientSideExtensionAnnotations(comments);

    if (!extensionAnnotationsComment) {
        return null;
    }

    const annotations = {} as RawAnnotations;

    for (const { tag, name, description } of extensionAnnotationsComment.tags) {
        const annotationKey = camelCase(tag) as AnnotationKey;
        const annotationValue = [name, description].join(' ').trim();

        // Merge annotations of the same type
        if (MULTI_VALUE_ANNOTATIONS.includes(annotationKey)) {
            const prevValue = annotations[annotationKey];
            const wrappedPrevValue = Array.isArray(prevValue) ? (prevValue as string[]) : [];

            annotations[annotationKey as MultiValueAnnotationKey] = [...wrappedPrevValue, annotationValue];

            // eslint-disable-next-line no-continue
            continue;
        }

        annotations[annotationKey as SingleValueAnnotationKey] = annotationValue;
    }

    return annotations;
};

export default transformComment;
