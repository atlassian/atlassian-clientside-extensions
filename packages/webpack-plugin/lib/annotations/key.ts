import path from 'path';
import type { AnnotationKey, AnnotationParserFunction } from './types';

const ANNOTATION_KEY: AnnotationKey = 'key';

const keyParser: AnnotationParserFunction<string> = (rawValue, rawAnnotations, options) => {
    let key: string;

    if (rawValue) {
        key = rawValue;
    } else {
        const { cwd, filePath } = options;
        key = path.relative(cwd, filePath);
    }

    // Sanitize the key to keep only the alphanumeric characters, dashes, and underscores.
    // Keep the $ char since "$key" value is a reserved keyword.
    const regExp = /[^a-z0-9$_-]+/gi;

    return {
        [ANNOTATION_KEY]: key.replace(regExp, '__'),
    };
};

export default keyParser;
