import * as parsers from './parsers';
import type { AnnotationParsers } from './types';

export * from './types';

export default parsers as AnnotationParsers;
