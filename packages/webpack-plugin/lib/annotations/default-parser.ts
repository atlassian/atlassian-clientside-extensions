import type { AnnotationKey, AnnotationParserFunction } from './types';

const getDefaultParser = <T>(annotationKey: AnnotationKey) => {
    const defaultParser: AnnotationParserFunction<T> = (rawValue) => {
        if (!rawValue) {
            return null;
        }

        return {
            [annotationKey]: rawValue,
        };
    };

    return defaultParser;
};

export default getDefaultParser;
