import type { AnnotationKey, AnnotationParserFunction } from './types';

const ANNOTATION_KEY: AnnotationKey = 'extensionPoint';

const extensionPointParser: AnnotationParserFunction<string> = (value, rawAnnotations, options) => {
    const { filePath } = options;

    if (!value) {
        throw new Error(`Client-side Extension: The @${ANNOTATION_KEY} in file "${filePath}" requires a non-empty value.`);
    }

    return {
        [ANNOTATION_KEY]: value,
    };
};

export default extensionPointParser;
