import type { AnnotationKey, AnnotationParserFunction } from './types';
import { ANNOTATION_KEY as PAGE_URL_ANNOTATION_KEY } from './page-url';

const ANNOTATION_KEY: AnnotationKey = 'pageDataProvider';

const pageDataProviderParser: AnnotationParserFunction<string> = (rawValue, rawAnnotations, options) => {
    if (!rawValue) {
        return null;
    }

    const { filePath } = options;

    if (!(PAGE_URL_ANNOTATION_KEY in rawAnnotations)) {
        throw new Error(
            `Client-side Extension: The @${ANNOTATION_KEY} annotation in file "${filePath}" requires the usage of @${PAGE_URL_ANNOTATION_KEY} annotation. Please add the missing @${PAGE_URL_ANNOTATION_KEY} annotation to your extension.`,
        );
    }

    const pageDataProvider = rawValue;

    return {
        [ANNOTATION_KEY]: pageDataProvider,
    };
};

export default pageDataProviderParser;
