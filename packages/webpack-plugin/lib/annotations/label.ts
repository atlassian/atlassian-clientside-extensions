import getDefaultParser from './default-parser';
import type { AnnotationKey } from './types';

export const ANNOTATION_KEY: AnnotationKey = 'label';

export default getDefaultParser<string>(ANNOTATION_KEY);
