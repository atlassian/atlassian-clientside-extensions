export type ConditionMap = { [key: string]: string };

export interface RawAnnotations {
    extensionPoint: string;
    key?: string;
    condition?: string[];
    label?: string;
    link?: string;
    pageDataProvider?: string;
    pageDecorator?: string;
    pageTitle?: string;
    pageUrl?: string;
    weight?: string;
}

export type AnnotationKey = keyof RawAnnotations;

export type MultiValueAnnotationKey = Extract<AnnotationKey, 'condition'>;

export type SingleValueAnnotationKey = Exclude<AnnotationKey, MultiValueAnnotationKey>;

export interface AnnotationParserOptions {
    filePath: string;
    cwd: string;
}

export type AnnotationParserFunction<T = string | string[]> = (
    rawValue: T | undefined,
    rawAnnotations: RawAnnotations,
    options: AnnotationParserOptions,
) => Partial<Annotations> | null;

export type AnnotationParsers = {
    [key in AnnotationKey]: AnnotationParserFunction;
};

/** Parsed annotations */
export interface Annotations {
    /** Module descriptor key e.g. `module.descriptor.key` */
    key: string;
    extensionPoint: string;
    condition?: ConditionMap;
    label?: string;
    link?: string;
    pageDataProvider?: string;
    pageDecorator?: string;
    pageTitle?: string;
    pageUrl?: string;
    weight?: number;
}
