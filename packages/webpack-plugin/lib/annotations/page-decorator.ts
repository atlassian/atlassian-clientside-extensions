import type { AnnotationKey, AnnotationParserFunction } from './types';
import { ANNOTATION_KEY as PAGE_URL_ANNOTATION_KEY } from './page-url';

const ANNOTATION_KEY: AnnotationKey = 'pageDecorator';

const standardPageDecorators = ['atl.admin', 'atl.general', 'atl.popup', 'atl.userprofile'];
const standardPageDecoratorsHelpUrl = 'https://developer.atlassian.com/server/framework/atlassian-sdk/using-standard-page-decorators';

const pageDecoratorParser: AnnotationParserFunction<string> = (rawValue, rawAnnotations, options) => {
    if (!rawValue) {
        return null;
    }

    const { filePath } = options;

    if (!(PAGE_URL_ANNOTATION_KEY in rawAnnotations)) {
        throw new Error(
            `Client-side Extension: The @${ANNOTATION_KEY} annotation in file "${filePath}" requires the usage of @${PAGE_URL_ANNOTATION_KEY} annotation. Please add the missing @${PAGE_URL_ANNOTATION_KEY} annotation to your extension.`,
        );
    }

    const pageDecorator = rawValue;

    if (!standardPageDecorators.includes(pageDecorator as string)) {
        console.warn(
            `The provided value of page decorator "${pageDecorator}" in file "${filePath}" is not a standard decorator. Please ensure you want to use a custom page decorator.\n\nPlease check the documentation page: ${standardPageDecoratorsHelpUrl}`,
        );
    }

    return {
        [ANNOTATION_KEY]: pageDecorator,
    };
};

export default pageDecoratorParser;
