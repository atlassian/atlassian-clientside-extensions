import type { AnnotationKey, AnnotationParserFunction, ConditionMap } from './types';

const ANNOTATION_KEY: AnnotationKey = 'condition';

const parseConditionComment = (conditionComment: string, filePath: string) => {
    if (!conditionComment) {
        throw new Error(
            `Client-side Extensions: Empty condition specified in file ${filePath}. Please ensure to specify a valid condition class or remove the @${ANNOTATION_KEY} annotation`,
        );
    }

    const found = conditionComment.match(/([^\s]+)\s(.+)/);

    if (found === null) {
        throw new Error(
            `Client-side Extensions: Invalid condition specified in file ${filePath}. Please verify the @${ANNOTATION_KEY} annotation`,
        );
    }

    const [, path, value] = found;
    return { path, value };
};

const conditionParser: AnnotationParserFunction<string[]> = (rawValue, rawAnnotations, options) => {
    if (!rawValue) {
        return null;
    }

    const { filePath } = options;

    const conditionMap = {} as ConditionMap;

    for (const conditionComment of rawValue) {
        const { path, value } = parseConditionComment(conditionComment, filePath);

        conditionMap[path] = value;
    }

    return {
        [ANNOTATION_KEY]: conditionMap,
    };
};

export default conditionParser;
