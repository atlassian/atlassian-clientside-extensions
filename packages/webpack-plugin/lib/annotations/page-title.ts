import type { AnnotationKey, AnnotationParserFunction } from './types';
import { ANNOTATION_KEY as PAGE_URL_ANNOTATION_KEY } from './page-url';

const ANNOTATION_KEY: AnnotationKey = 'pageTitle';

const pageTitleParser: AnnotationParserFunction<string> = (rawValue, rawAnnotations, options) => {
    if (!rawValue) {
        return null;
    }

    if (!(PAGE_URL_ANNOTATION_KEY in rawAnnotations)) {
        const { filePath } = options;

        throw new Error(
            `Client-side Extension: The @${ANNOTATION_KEY} annotation in file "${filePath}" requires the usage of @${PAGE_URL_ANNOTATION_KEY} annotation. Please add the missing @${PAGE_URL_ANNOTATION_KEY} annotation to your extension.`,
        );
    }

    const pageDecorator = rawValue;

    return {
        [ANNOTATION_KEY]: pageDecorator,
    };
};

export default pageTitleParser;
