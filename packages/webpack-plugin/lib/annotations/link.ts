import getDefaultParser from './default-parser';
import type { AnnotationKey } from './types';

export const ANNOTATION_KEY: Extract<AnnotationKey, 'link'> = 'link';

export default getDefaultParser<string>(ANNOTATION_KEY);
