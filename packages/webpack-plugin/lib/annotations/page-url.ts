import type { AnnotationKey, AnnotationParserFunction, Annotations } from './types';
import { ANNOTATION_KEY as LINK_ANNOTATION_KEY } from './link';
import { ANNOTATION_KEY as LABEL_ANNOTATION_KEY } from './label';

export const ANNOTATION_KEY: AnnotationKey = 'pageUrl';

const LEFT_SLASH_REGEX = /^\//;

const generatePageLink = (pageUrl: string) => {
    return `/plugins/servlet/${pageUrl.replace(LEFT_SLASH_REGEX, '')}`;
};

const pageUrlParser: AnnotationParserFunction<string> = (rawValue, rawAnnotations, options) => {
    if (!rawValue) {
        return null;
    }

    const annotations = {
        [ANNOTATION_KEY]: rawValue,
    } as Partial<Annotations>;
    const { filePath } = options;

    if (!(LABEL_ANNOTATION_KEY in rawAnnotations)) {
        throw new Error(
            `Client-side Extension: The @${ANNOTATION_KEY} annotation in file "${filePath}" requires the usage of @${LABEL_ANNOTATION_KEY} annotation. Please add the missing @${LABEL_ANNOTATION_KEY} annotation to your extension.`,
        );
    }

    if (LINK_ANNOTATION_KEY in rawAnnotations) {
        console.warn(
            `Client-side Extension: The provided @${LINK_ANNOTATION_KEY} annotation in file "${filePath}" will overwrite the default page URL. Please ensure you want to have a custom URL link.`,
        );

        return annotations;
    }

    const linkValue = generatePageLink(rawValue);

    annotations[LINK_ANNOTATION_KEY] = linkValue;

    return annotations;
};

export default pageUrlParser;
