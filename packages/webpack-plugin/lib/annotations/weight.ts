import type { AnnotationKey, AnnotationParserFunction } from './types';

const ANNOTATION_KEY: AnnotationKey = 'weight';

const weightParser: AnnotationParserFunction<string> = (rawValue) => {
    if (!rawValue) {
        return null;
    }

    const parsedWeight = parseInt(rawValue, 10);

    if (!Number.isNaN(parsedWeight)) {
        return {
            [ANNOTATION_KEY]: parsedWeight,
        };
    }

    return null;
};

export default weightParser;
