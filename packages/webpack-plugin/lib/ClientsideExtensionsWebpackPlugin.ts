import fs from 'fs';
import path from 'path';
import type { Compilation, Compiler, WebpackPluginInstance } from 'webpack';
import { glob } from 'glob';
import kebabCase from 'lodash/kebabCase';
import { pd as PrettyData } from 'pretty-data';
import VirtualModulePlugin from 'webpack-virtual-modules';
import transformComment from './transform-comment';
import type { AnnotationKey, AnnotationParserFunction, AnnotationParserOptions, Annotations, RawAnnotations } from './annotations';
import annotationsParsers from './annotations';

import generateWebfragmentDescriptor from './generator/webfragmentDescriptor';
import generateWebpageDescriptor, { PAGE_DATA_PROVIDER_DATA_KEY } from './generator/webpageDescriptor';
import { generateEntrypointCode, isPageExtension } from './generator/entrypoint';

import type { ClientsideExtensionsManifest, ClientsideExtensionsOptions, DataProvider, IWrmPluginOptions } from './types';
import { debugLog, getKeyValue, isNotNull } from './utils';

import transformConditionToConditionMapObject from './generator/conditions';

// Infer the type "Source" which is not exposed by Webpack
type Asset = Compilation['assets'] extends Record<string, infer S> ? S : never;

interface IClientsideExtensionsPluginOptions {
    pattern?: string;
    cwd?: string;
    xmlDescriptors?: string;
    ignore?: string[];
}

class ClientsideExtensionsWebpackPlugin {
    private readonly pattern: string;

    private readonly cwd: string;

    private readonly outputFilename?: string;

    private manifests: ClientsideExtensionsManifest[];

    constructor(options: IClientsideExtensionsPluginOptions = {}) {
        if (!options.pattern) {
            throw new Error('No pattern was specified to match clientside extensions');
        }

        this.outputFilename = options.xmlDescriptors;
        this.pattern = options.pattern;
        this.cwd = options.cwd || process.cwd();
        const ignore = options.ignore || ['**/node_modules/**', 'build/**', 'target/**'];

        debugLog(`cwd: ${this.cwd}`);

        const globFoundFiles = ClientsideExtensionsWebpackPlugin.findFilesByPattern(this.pattern, this.cwd, ignore);

        debugLog(`Glob found (with the pattern: ${this.pattern}) these files: ${globFoundFiles}`);

        this.manifests = globFoundFiles
            .map((file) => ClientsideExtensionsWebpackPlugin.parseManifestFromComment(file, this.cwd))
            .filter(isNotNull);

        debugLog(`Found the following manifests ${JSON.stringify(this.manifests)}`);
    }

    public generateEntrypoints() {
        return this.manifests.reduce((agg, manifest) => {
            const id = ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(manifest.key);
            agg[id] = ClientsideExtensionsWebpackPlugin.getFilenameForEntry(id);

            return agg;
        }, {} as { [key: string]: string });
    }

    apply(compiler: Compiler) {
        const wrmOptions = ClientsideExtensionsWebpackPlugin.getWrmWebpackPluginOptions(compiler);

        const extensionsOptions = this.manifests.map((manifest) => {
            return ClientsideExtensionsWebpackPlugin.manifestToPluginOptions(wrmOptions.pluginKey, manifest);
        });

        // side effect
        ClientsideExtensionsWebpackPlugin.addContextToEntrypoints(extensionsOptions, wrmOptions);

        // side effect
        ClientsideExtensionsWebpackPlugin.addDataProvidersToEntrypoints(extensionsOptions, wrmOptions);

        // side effect
        ClientsideExtensionsWebpackPlugin.addConditionToEntrypoint(extensionsOptions, wrmOptions);

        const virtualModules = extensionsOptions.reduce((agg, extensionOption) => {
            const fileName = ClientsideExtensionsWebpackPlugin.getFilenameForEntry(extensionOption.id);
            agg[fileName] = generateEntrypointCode(extensionOption);
            return agg;
        }, {} as Record<string, string>);
        new VirtualModulePlugin(virtualModules).apply(compiler);

        compiler.hooks.compilation.tap('Tap compilation for XML generation', (compilation) => {
            const webfragmentDescriptors = extensionsOptions.map((extensionOption) => generateWebfragmentDescriptor(extensionOption));
            const webpageDescriptors = extensionsOptions.map((extensionOption) => generateWebpageDescriptor(extensionOption));

            const descriptors = [...webfragmentDescriptors, ...webpageDescriptors].join('');
            const xmlDescriptors = PrettyData.xml(`<clientside-extension>${descriptors}</clientside-extension>`);

            const xmlDescriptorDirname = path.dirname(
                path.relative(compiler.options.output.path || '', this.outputFilename || wrmOptions.xmlDescriptors),
            );
            const xmlDescriptorFilename = path.basename(this.outputFilename || 'wr-generated-clientside-extensions.xml');
            const webpanelXmlDescriptorFilename = path.join(xmlDescriptorDirname, xmlDescriptorFilename);

            compilation.hooks.additionalAssets.tap('Generate clientside-extension XML', () => {
                compilation.assets[webpanelXmlDescriptorFilename] = {
                    source: () => Buffer.from(xmlDescriptors),
                    size: () => Buffer.byteLength(xmlDescriptors),
                } as Asset;
            });
        });
    }

    private static addContextToEntrypoints(extensionOptions: ClientsideExtensionsOptions[], wrmOptions: IWrmPluginOptions) {
        extensionOptions
            .filter((extensionOption) => !isPageExtension(extensionOption))
            .forEach((extensionOption) => {
                const { contextMap } = wrmOptions;
                const entrypointId = ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(extensionOption.key);
                const context = extensionOption.extensionPoint;

                contextMap.set(entrypointId, [context]);
            });
    }

    private static addDataProvidersToEntrypoints(extensionOptions: ClientsideExtensionsOptions[], wrmOptions: IWrmPluginOptions) {
        extensionOptions
            .filter((extensionOption) => Boolean(extensionOption.pageDataProvider))
            .forEach((extensionOption) => {
                const { dataProvidersMap } = wrmOptions;
                const entrypointId = ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(extensionOption.key);
                const dataProvider: DataProvider = {
                    key: PAGE_DATA_PROVIDER_DATA_KEY,
                    class: extensionOption.pageDataProvider!,
                };

                dataProvidersMap.set(entrypointId, [dataProvider]);
            });
    }

    private static addConditionToEntrypoint(extensionOptions: ClientsideExtensionsOptions[], wrmOptions: IWrmPluginOptions) {
        // eslint-disable-next-line @typescript-eslint/no-shadow
        const hasCondition = (extensionOptions: ClientsideExtensionsOptions) => Boolean(extensionOptions.condition);
        // eslint-disable-next-line @typescript-eslint/no-shadow
        const isNotPageExtension = (extensionOptions: ClientsideExtensionsOptions) => !isPageExtension(extensionOptions);

        extensionOptions
            .filter(hasCondition)
            .filter(isNotPageExtension)
            .forEach((extensionOption) => {
                const entrypointId = ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(extensionOption.key);
                const conditionMap = transformConditionToConditionMapObject(extensionOption.condition!);

                const conditionMapPluginOption = wrmOptions.conditionMap as Map<string, object>;

                conditionMapPluginOption.set(entrypointId, conditionMap);
            });
    }

    private static transformAnnotationsIntoManifest(
        rawAnnotations: RawAnnotations,
        filePath: string,
        cwd: string,
    ): ClientsideExtensionsManifest {
        const parserOptions: AnnotationParserOptions = {
            cwd,
            filePath,
        };

        const annotations = {} as Annotations;
        const entries = Object.entries(annotationsParsers) as [[AnnotationKey, AnnotationParserFunction]];

        for (const [annotationKey, annotationParser] of entries) {
            const rawAnnotation = getKeyValue(rawAnnotations, annotationKey);
            const newAnnotations = annotationParser(rawAnnotation, rawAnnotations, parserOptions);

            if (newAnnotations !== null) {
                Object.assign(annotations, newAnnotations);
            }
        }

        const manifest: ClientsideExtensionsManifest = {
            id: ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(annotations.key),
            filePath,
            ...annotations,
        };

        debugLog(`Generated manifest: ${JSON.stringify(manifest)}`);

        return manifest;
    }

    private static parseManifestFromComment(file: string, cwd: string): ClientsideExtensionsManifest | null {
        const fileContent = fs.readFileSync(file, 'utf8');
        const annotations = transformComment(fileContent);

        if (!annotations) {
            console.warn(`Client-side Extension: The file: "${file}" does not contain meta comments. Skipping XML-descriptor generation.`);
            return null;
        }

        return ClientsideExtensionsWebpackPlugin.transformAnnotationsIntoManifest(annotations, file, cwd);
    }

    private static findFilesByPattern(pattern: string, cwd: string, ignore?: string[]) {
        return glob.sync(pattern, { absolute: true, cwd, ignore, dotRelative: true });
    }

    private static getWrmWebpackPluginOptions(compiler: Compiler): IWrmPluginOptions {
        const { plugins } = compiler.options;
        if (!plugins) {
            throw new Error(`Something went mighty wrong. Can't find any plugins in webpack`);
        }
        const wrmPlugin = plugins.find((p) => p.constructor.name === 'WrmPlugin');

        if (!wrmPlugin) {
            throw new Error(
                `Can't find the WrmPlugin. Make sure you are using this plugin only in conjunction with the atlassian-webresource-webpack-plugin`,
            );
        }

        return (wrmPlugin as WebpackPluginInstance & { options: IWrmPluginOptions }).options;
    }

    private static getFilenameForEntry(manifestId: string) {
        return `./generated-clientside-extension/${manifestId}.js`;
    }

    private static manifestToPluginOptions(pluginKey: string, manifest: ClientsideExtensionsManifest): ClientsideExtensionsOptions {
        const { id, key } = manifest;
        const resourceKey = `entrypoint-${id}`;

        return {
            ...manifest,
            pluginKey,
            entryPoint: resourceKey,
            fullyQualifiedNamespace: `${pluginKey}:${key}`,
        };
    }

    private static getIdFromManifestKey(key: string) {
        return kebabCase(key);
    }
}

export = ClientsideExtensionsWebpackPlugin;
