import type { Stats } from 'webpack';
import { promisify } from 'util';
import path from 'path';
import WrmPlugin from 'atlassian-webresource-webpack-plugin';
import { mocked } from 'ts-jest/utils';
// eslint-disable-next-line import/order
import webpack = require('webpack');
import { fileContent, getOutputContentAsJson } from './tests/test-helper';

import Plugin = require('./ClientsideExtensionsWebpackPlugin');

const PLUGIN_KEY = 'a.fake.plugin.key';

beforeAll(() => {
    // Silent-down the webpack compilation errors
    jest.spyOn(console, 'error').mockImplementation();
});

afterAll(() => {
    mocked(console.error).mockRestore();
});

describe('condition compilation', () => {
    const inputDir = path.resolve(__dirname, '__fixtures__/conditions/');
    const outputDir = path.resolve(__dirname, '__fixtures__/target/');
    const xmlCseOutputFilename = 'wr-generated-cse-conditions.xml';
    const xmlWebresourcesOutputFilename = 'wr-generated-webresources-conditions.xml';

    const basicWrmPlugin = new WrmPlugin({
        pluginKey: PLUGIN_KEY,
        xmlDescriptors: path.resolve(outputDir, xmlWebresourcesOutputFilename),
    });

    const csePlugin = new Plugin({
        cwd: inputDir,
        pattern: '**/*.extension.js',
        xmlDescriptors: path.resolve(outputDir, xmlCseOutputFilename),
    });

    // base config for all tests
    const baseWebpackConfig = {
        entry: csePlugin.generateEntrypoints(),
        optimization: {
            minimize: false,
        },
        plugins: [basicWrmPlugin, csePlugin],
        output: {
            path: outputDir,
        },
    };

    beforeAll(async () => {
        const compiler = webpack(baseWebpackConfig);
        const run = promisify(compiler.run.bind(compiler));
        await run();
    });

    it('should generate the simple condition with class', () => {
        const expectedExtensionPoint = 'simple.condition';
        const expectedWebItemKey = 'simple-condition__extension__js';
        const expectedCondition = {
            attrs: { class: 'com.acme.MyCondition' },
        };

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                        weight: expect.anything(),
                    },
                    label: expect.anything(),
                    condition: [expectedCondition],
                    link: expect.anything(),
                },
            ]),
        });

        const outputJson = getOutputContentAsJson(fileContent(path.join(outputDir, xmlCseOutputFilename)));
        expect(outputJson).toContainEqual(expectedWebItemDeclarationShape);
    });

    it('should generate the condition with class params', () => {
        const expectedExtensionPoint = 'condition.with.params';
        const expectedWebItemKey = 'condition-with-params__extension__js';
        const expectedParam = {
            attrs: {
                name: 'permission',
            },
            '#text': 'admin',
        };
        const expectedCondition = {
            attrs: { class: 'com.acme.MyConditionWithParams' },
            param: [expectedParam],
        };

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                        weight: expect.anything(),
                    },
                    label: expect.anything(),
                    condition: [expectedCondition],
                    link: expect.anything(),
                },
            ]),
        });

        const outputJson = getOutputContentAsJson(fileContent(path.join(outputDir, xmlCseOutputFilename)));
        expect(outputJson).toContainEqual(expectedWebItemDeclarationShape);
    });

    it('should generate conjunction of two conditions with params', () => {
        const expectedExtensionPoint = 'two.conditions.with.params';
        const expectedWebItemKey = 'two-conditions-with-params__extension__js';

        // First condition
        const expectedFistConditionParam = {
            attrs: {
                name: 'MyParam',
            },
            '#text': 'My Value',
        };

        const expectedFirstCondition = {
            attrs: { class: 'com.acme.MyFirstCondition' },
            param: [expectedFistConditionParam],
        };

        // Second condition
        const expectedSecondConditionParamFoo = {
            attrs: {
                name: 'foo',
            },
            '#text': 'bar',
        };

        const expectedSecondConditionParamBiz = {
            attrs: {
                name: 'biz',
            },
            '#text': 'baz',
        };

        const expectedSecondCondition = {
            attrs: { class: 'com.acme.MySecondCondition' },
            param: [expectedSecondConditionParamFoo, expectedSecondConditionParamBiz],
        };

        // Condition
        const expectedConditions = {
            attrs: {
                type: 'AND',
            },

            condition: [expectedFirstCondition, expectedSecondCondition],
        };

        const expectedWebItemDeclarationShape = expect.objectContaining({
            'web-item': expect.arrayContaining([
                {
                    attrs: {
                        key: expectedWebItemKey,
                        section: expectedExtensionPoint,
                        weight: expect.anything(),
                    },
                    label: expect.anything(),
                    conditions: [expectedConditions],
                    link: expect.anything(),
                },
            ]),
        });

        const outputJson = getOutputContentAsJson(fileContent(path.join(outputDir, xmlCseOutputFilename)));
        expect(outputJson).toContainEqual(expectedWebItemDeclarationShape);
    });
});

describe('condition compilation errors', () => {
    const inputDir = path.resolve(__dirname, '__fixtures__/conditions-errors/');
    const outputDir = path.resolve(__dirname, '__fixtures__/target/');
    const xmlOutputFilename = 'wr-generated-cse-condition-errors.xml';

    const getWebpackConfig = (csePattern: string) => {
        const basicWrmPlugin = new WrmPlugin({
            pluginKey: PLUGIN_KEY,
            xmlDescriptors: path.resolve(outputDir, 'wrm.xml'),
        });

        const csePlugin = new Plugin({
            cwd: inputDir,
            pattern: csePattern,
            xmlDescriptors: path.resolve(outputDir, xmlOutputFilename),
        });

        const webpackConfig = {
            entry: csePlugin.generateEntrypoints(),
            plugins: [basicWrmPlugin, csePlugin],
            optimization: {
                minimize: false,
            },
            output: {
                path: outputDir,
            },
        };

        return webpackConfig;
    };

    const compile = (config: webpack.Configuration): Promise<Stats> => {
        const compiler = webpack(config);

        return new Promise<Stats>((resolve, reject) => {
            compiler.run((err, stats) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(stats!);
            });
        });
    };

    it('should throw an error when specifying an empty @condition annotation', async () => {
        const errorFile = '**/empty-condition.*';

        expect(() => compile(getWebpackConfig(errorFile))).toThrowError();
    });

    it('should throw an error when specifying an invalid condition class', async () => {
        const errorFile = '**/invalid-condition-class.*';

        expect(() => compile(getWebpackConfig(errorFile))).toThrowError();
    });
});
