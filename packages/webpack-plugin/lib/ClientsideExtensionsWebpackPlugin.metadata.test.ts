import { resolve, join } from 'path';
import WrmPlugin from 'atlassian-webresource-webpack-plugin';
import { mocked } from 'ts-jest/utils';
import { fileContent } from './tests/test-helper';

// eslint-disable-next-line import/order
import webpack = require('webpack');
import Plugin = require('./ClientsideExtensionsWebpackPlugin');

const inputDir = resolve(__dirname, '__fixtures__/metadata/');
const outputDir = resolve(__dirname, '__fixtures__/target/');

beforeAll(() => {
    // Silent-down the webpack compilation errors
    jest.spyOn(console, 'error').mockImplementation();
});

afterAll(() => {
    mocked(console.error).mockRestore();
});

describe('comment metadata to XML attributes/elements', () => {
    const fileName = 'metadata-test-fragments.xml';

    const basicWrmPlugin = new WrmPlugin({
        pluginKey: 'a.fake.plugin.key',
        xmlDescriptors: resolve(outputDir, 'metadata-test.xml'),
    });

    const basicPlugin = new Plugin({
        cwd: inputDir,
        pattern: '**/*.extension.js',
        xmlDescriptors: resolve(outputDir, fileName),
    });

    // base config for all tests
    const baseWebpackConfig = {
        entry: {
            ...basicPlugin.generateEntrypoints(),
        },
        optimization: {
            minimize: false,
        },
        plugins: [basicWrmPlugin, basicPlugin],
        output: {
            path: outputDir,
        },
    };

    beforeAll((done) => {
        const compiler = webpack(baseWebpackConfig);

        compiler.run((err) => {
            if (err) {
                throw err;
            }

            done();
        });
    });

    it('should replace $key with the key of the extension when specified', () => {
        const webItemDeclaration = /<web-item key="key_replacement_test__extension__js" section="webpage-with-condition" weight="">/;
        const labelDeclaration = /<label key="My Extension key is a.fake.plugin.key:key_replacement_test__extension__js"\/>/;

        const linkDeclaration =
            /<link >\/plugins\/servlet\/my-page-with-condition\?q=a.fake.plugin.key:key_replacement_test__extension__js<\/link>/;

        const urlDeclaration =
            /<url-pattern >\/my-page-with-condition\?q=a.fake.plugin.key:key_replacement_test__extension__js<\/url-pattern>/;

        const xmlFileContent = fileContent(join(outputDir, fileName));

        expect(xmlFileContent).toMatch(webItemDeclaration);
        expect(xmlFileContent).toMatch(labelDeclaration);
        expect(xmlFileContent).toMatch(linkDeclaration);
        expect(xmlFileContent).toMatch(urlDeclaration);
    });
});
