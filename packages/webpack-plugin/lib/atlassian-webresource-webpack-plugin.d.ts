class IWrmPlugin {
    public constructor(options: object);

    apply(compiler: unknown): void;
}

declare module 'atlassian-webresource-webpack-plugin' {
    export default IWrmPlugin;
}
