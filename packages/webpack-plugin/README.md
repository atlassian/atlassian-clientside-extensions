# Client-side Extensions

## Webpack plugin

A webpack plugin to facilitate the consumption of client-side extensions using webpack. It will read comment annotations to create the
necessary web-fragments XML declarations for plugin devs.

Refer to the [official webpack plugin documentation](https://developer.atlassian.com/server/framework/clientside-extensions/reference/webpack-plugin/webpack-plugin/)
for more information.
