const base = require('../../jest.config.base');
const pack = require('./package.json');

module.exports = {
    ...base,
    displayName: pack.name,
    watchPathIgnorePatterns: ['<rootDir>/lib/__fixtures__/target'],
};

if (process.env.WEBPACK_4) {
    module.exports.cacheDirectory = 'node_modules/.cache/jest-cache-webpack-4';
    module.exports.moduleNameMapper = {
        ...module.exports.moduleNameMapper,
        '^webpack((\\/.*)?)$': 'webpack-4$1',
        '^atlassian-webresource-webpack-plugin((\\/.*)?)$': 'atlassian-webresource-webpack-plugin-4$1',
    };
}
