import type { Observer, Subscription } from './Subject';
import Subject from './Subject';

export type Tick = null | number | NodeJS.Timer;

/**
 * Subject for a single location and its observers
 *
 * Observers are callbacks provided by the different instances of a location that want to be notified
 * when a list of descriptors changed and is available to be rendered.
 */
export default class DeferredSubject<T> extends Subject<T> {
    private notifyTimer: Tick = null;

    private lastPayload!: T;

    constructor(private timeout: number = 0) {
        super();
    }

    /**
     * @Override
     * Notify all observers with a list of descriptors. Send the last value sent if no value is provided.
     */
    public override notify(payload: T) {
        this.clearTimer();
        this.lastPayload = payload;

        // The resources for a specific location will be loaded all at once.
        // We defer updating the observers until the last such update made it in
        // so we dont call observers overly often, which could result in a waste of expensive calculations.
        this.notifyTimer = setTimeout(() => {
            this.flush();
        }, this.timeout);
    }

    protected clearTimer() {
        clearTimeout(this.notifyTimer as number);
        this.notifyTimer = null;
    }

    protected flush() {
        this.clearTimer();
        this.observers.forEach((observer) => observer(this.lastPayload));
    }

    /**
     * @Override
     * Add an observer to the location subject. The observer will be notified each time there's a new list of
     * descriptors ready to be renderer.
     */
    override subscribe(observer: Observer<T>): Subscription {
        this.observers.push(observer);

        // no notify timer is running - otherwise just wait for the cycle to update the location observer
        const noNotifyTimer = this.notifyTimer === null;
        if (this.lastPayload !== undefined && noNotifyTimer) {
            observer(this.lastPayload);
        }

        return {
            unsubscribe: () => this.unsubscribe(observer),
        };
    }
}
