import type { ExtensionAPI } from '@atlassian/clientside-extensions-registry';
import * as ButtonExtension from './ButtonExtension';
import type { ButtonAttributesProvider } from './ButtonExtension';

interface TestContext {
    value?: number;
}

describe('ButtonExtension', () => {
    // @ts-expect-error We set the invalid type
    const testAttributesProvider: ButtonAttributesProvider = (api, context: TestContext) => {
        api.onCleanup(jest.fn());
        api.updateAttributes({});

        return {
            type: 'not-valid',
            label: context.value === 42 ? 'The meaning of life' : 'A label',
            onAction: jest.fn(),
        };
    };

    const testAPI: ExtensionAPI<typeof testAttributesProvider> = {
        onCleanup: jest.fn(),
        updateAttributes: jest.fn(),
    };

    it('should generate the extension descriptor with the correct type', () => {
        const extensionProvider = ButtonExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, {});

        // expect all atributes to be merged, and type to be overwritten with the correct value.
        expect(extension).toEqual({
            type: 'button',
            label: 'A label',
            onAction: expect.any(Function),
        });
    });

    it('should provide a extensionAPI object to the extension provider', () => {
        expect(testAPI.onCleanup).toHaveBeenCalledTimes(0);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(0);

        const extensionProvider = ButtonExtension.factory(testAttributesProvider);
        extensionProvider(testAPI, {});

        expect(testAPI.onCleanup).toHaveBeenCalledTimes(1);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(1);
    });

    it('should share any provided context with the attributes provider', () => {
        const extensionProvider = ButtonExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, { value: 42 });

        // expect label to change because of context
        expect(extension).toEqual({
            type: 'button',
            label: 'The meaning of life',
            onAction: expect.any(Function),
        });
    });
});
