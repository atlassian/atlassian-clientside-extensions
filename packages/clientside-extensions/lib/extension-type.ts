// eslint-disable-next-line import/prefer-default-export
export enum ExtensionType {
    'asyncPanel' = 'async-panel',
    button = 'button',
    link = 'link',
    modal = 'modal',
    panel = 'panel',
    page = 'page',
    section = 'section',
}
