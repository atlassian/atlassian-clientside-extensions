import type { ExtensionAttributes, ExtensionAttributesProvider, OnAction } from '@atlassian/clientside-extensions-registry';

import { ExtensionType } from './extension-type';

export interface LinkAttributes extends ExtensionAttributes {
    onAction?: OnAction;
    url: string;
    label: string;
    type?: ExtensionType.link;
}

export type LinkAttributesProvider<ContextT> = ExtensionAttributesProvider<LinkAttributes, ContextT>;

// eslint-disable-next-line import/prefer-default-export
export const factory = <ContextT>(provider: LinkAttributesProvider<ContextT>): LinkAttributesProvider<ContextT> => {
    return (...args) => {
        return {
            ...provider(...args),
            type: ExtensionType.link,
        };
    };
};
