import { onDebug } from '@atlassian/clientside-extensions-debug';

// We are fine with any type here
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Callable = (...args: any[]) => any;

export interface LifecycleCallback {
    (element: HTMLElement): void;
}

export interface MountableExtension {
    onMount(mountCallback: LifecycleCallback): MountableExtension;

    onUnmount(unmountCallback: LifecycleCallback): MountableExtension;
}

export function safeguard<T extends Callable>(callbackDebugName: string, callback: T): (...args: Parameters<T>) => ReturnType<T> {
    return (...args: Parameters<T>) => {
        let result;
        try {
            result = callback(...args);
        } catch (e) {
            onDebug(({ error }) => ({
                level: error,
                message: `Failed to execute ${callbackDebugName} callback.
    Error: ${e}`,
            }));
        }
        return result;
    };
}
