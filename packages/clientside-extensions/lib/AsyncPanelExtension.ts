import type { ExtensionAttributes, ExtensionAttributesProvider, Context } from '@atlassian/clientside-extensions-registry';
import type { PanelRenderExtension } from './PanelExtension';
import { Api } from './PanelExtension';
import { ExtensionType } from './extension-type';

export interface AsyncPanelRenderExtension extends PanelRenderExtension {}

export interface AsyncPanelAttributes extends ExtensionAttributes {
    onAction: AsyncPanelRenderExtension;
    type?: ExtensionType.asyncPanel;
}

export type AsyncPanelAttributesProvider<ContextT> = ExtensionAttributesProvider<AsyncPanelAttributes, ContextT>;

export interface AsyncPanelAttributesWithContext extends AsyncPanelAttributes {
    contextProvider: () => Context<{}>;
}

export type AsyncPanelAttributesProviderWithContext<ContextT> = ExtensionAttributesProvider<AsyncPanelAttributesWithContext, ContextT>;

export const factory = <ContextT>(provider: AsyncPanelAttributesProvider<ContextT>): AsyncPanelAttributesProviderWithContext<ContextT> => {
    return (extensionApi, context): AsyncPanelAttributesWithContext => {
        return {
            ...provider(extensionApi, context),
            // We are passing the context provider function reference into the <AsyncPanelHandler.AsyncPanelRenderer> component
            contextProvider() {
                return context;
            },
            type: ExtensionType.asyncPanel,
        };
    };
};

export { Api };
