// eslint-disable-next-line node/no-extraneous-import
import type { Dispatch, SetStateAction } from 'react';
import type { ExtensionAPI } from '@atlassian/clientside-extensions-registry';
import { onDebug } from '@atlassian/clientside-extensions-debug';
import * as ModalExtension from './ModalExtension';

jest.mock('@atlassian/clientside-extensions-debug');

interface TestContext {
    value?: number;
}

describe('ModalExtension', () => {
    // @ts-expect-error - invalid values for type. Factory should overwrite it.
    const testAttributesProvider: ModalExtension.ModalAttributesProvider = (api, context: TestContext) => {
        api.onCleanup(jest.fn());
        api.updateAttributes({});

        return {
            type: 'not-valid',
            label: context.value === 42 ? 'The meaning of life' : 'A label',
            onAction: jest.fn(),
        };
    };

    const testAPI: ExtensionAPI<typeof testAttributesProvider> = {
        onCleanup: jest.fn(),
        updateAttributes: jest.fn(),
    };

    it('should generate the extension descriptor with the correct type', () => {
        const extensionProvider = ModalExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, {});

        // expect all attributes to be merged, and type to be overwritten with the correct value.
        expect(extension).toEqual({
            type: 'modal',
            label: 'A label',
            onAction: expect.any(Function),
        });
    });

    it('should provide a extensionAPI object to the extension provider', () => {
        expect(testAPI.onCleanup).toHaveBeenCalledTimes(0);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(0);

        const extensionProvider = ModalExtension.factory(testAttributesProvider);
        extensionProvider(testAPI, {});

        expect(testAPI.onCleanup).toHaveBeenCalledTimes(1);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(1);
    });

    it('should share any provided context with the attributes provider', () => {
        const extensionProvider = ModalExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, { value: 42 });

        // expect label to change because of context
        expect(extension).toEqual({
            type: 'modal',
            label: 'The meaning of life',
            onAction: expect.any(Function),
        });
    });

    it('should export all the supported appreanace for modals', () => {
        expect(ModalExtension.Appearance).toBeTruthy();
    });

    it('should export all the supported sizes for modals', () => {
        expect(ModalExtension.Width).toBeTruthy();
    });

    describe('API', () => {
        let closeFn;
        let api: ModalExtension.Api;
        const el: HTMLElement = document.createElement('div');
        const setStateFn: Dispatch<SetStateAction<ModalExtension.ModalAction[]>> = () => {};

        beforeEach(() => {
            closeFn = jest.fn();
            api = new ModalExtension.Api(closeFn, setStateFn);
        });

        it('#onMount callback used in getRenderCallback', () => {
            const cb = jest.fn();
            api.onMount(cb);
            api.getRenderCallback()(el);
            expect(cb).toHaveBeenCalledTimes(1);
        });

        it('#onMount does nothing if not set', () => {
            expect(() => {
                api.getRenderCallback()(el);
            }).not.toThrow();
        });

        it('#onMount does nothing if callback is not function', () => {
            // @ts-expect-error checking that non-TS consumers don't shoot themselves (or others) in the foot
            api.onMount(null);
            expect(() => {
                api.getRenderCallback()(el);
            }).not.toThrow();
        });

        it('#onMount fails gracefully if callback throws error', () => {
            api.onMount(() => {
                throw Error('whoops');
            });
            expect(() => {
                api.getRenderCallback()(el);
            }).not.toThrow();
        });

        it('#onUnmount callback used in getCleanupCallback', () => {
            const cb = jest.fn();
            api.onUnmount(cb);
            api.getCleanupCallback()(el);
            expect(cb).toHaveBeenCalledTimes(1);
        });

        it('#onUnmount does nothing if not set', () => {
            expect(() => {
                api.getCleanupCallback()(el);
            }).not.toThrow();
        });

        it('#onUnmount does nothing if callback is not function', () => {
            // @ts-expect-error checking that non-TS consumers don't shoot themselves (or others) in the foot
            api.onUnmount(null);
            expect(() => {
                api.getCleanupCallback()(el);
            }).not.toThrow();
            expect(onDebug).toHaveBeenCalledTimes(1);
        });

        it('#onUnmount fails gracefully if callback throws error', () => {
            api.onUnmount(() => {
                throw Error('whoops');
            });
            expect(() => {
                api.getCleanupCallback()(el);
            }).not.toThrow();
            expect(onDebug).toHaveBeenCalledTimes(1);
        });
    });
});
