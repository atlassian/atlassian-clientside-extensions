import type { ExtensionAPI } from '@atlassian/clientside-extensions-registry';
import { onDebug } from '@atlassian/clientside-extensions-debug';
import * as AsyncPanelExtension from './AsyncPanelExtension';

jest.mock('@atlassian/clientside-extensions-debug');

interface TestContext {
    value?: number;
}

describe('AsyncPanelExtension', () => {
    // @ts-expect-error - invalid values for contextProvider and type. Factory should overwrite them.
    const testAttributesProvider: AsyncPanelExtension.AsyncPanelAttributesProvider = (api, context: TestContext) => {
        api.onCleanup(jest.fn());
        api.updateAttributes({});

        return {
            type: 'not-valid',
            contextProvider: 'not-valid-context-provider',
            label: context.value === 42 ? 'The meaning of life' : 'A label',
            onAction: jest.fn(),
        };
    };

    const testAPI: ExtensionAPI<typeof testAttributesProvider> = {
        onCleanup: jest.fn(),
        updateAttributes: jest.fn(),
    };

    it('should generate the extension descriptor with the correct type and contextProvider', () => {
        const extensionProvider = AsyncPanelExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, {});

        // expect all attributes to be merged, type and contextProvider to be overwritten with the correct values.
        expect(extension).toEqual({
            type: 'async-panel',
            label: 'A label',
            onAction: expect.any(Function),
            contextProvider: expect.any(Function),
        });
    });

    it('should provide a extensionAPI object to the extension provider', () => {
        expect(testAPI.onCleanup).toHaveBeenCalledTimes(0);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(0);

        const extensionProvider = AsyncPanelExtension.factory(testAttributesProvider);
        extensionProvider(testAPI, {});

        expect(testAPI.onCleanup).toHaveBeenCalledTimes(1);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(1);
    });

    it('should share any provided context with the attributes provider', () => {
        const extensionProvider = AsyncPanelExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, { value: 42 });

        // expect label to change because of context
        expect(extension).toEqual({
            type: 'async-panel',
            label: 'The meaning of life',
            onAction: expect.any(Function),
            contextProvider: expect.any(Function),
        });
    });

    it('should add a contextProvider method to retreive the shared context', () => {
        const extensionProvider = AsyncPanelExtension.factory(testAttributesProvider);
        const testContext: TestContext = { value: 42 };
        const extension = extensionProvider(testAPI, testContext);

        expect(extension.contextProvider()).toBe(testContext);
    });

    describe('API', () => {
        let api: AsyncPanelExtension.Api;
        const el: HTMLElement = document.createElement('div');

        beforeEach(() => {
            api = new AsyncPanelExtension.Api();
        });

        it('#onMount callback used in getRenderCallback', () => {
            const cb = jest.fn();
            api.onMount(cb);
            api.getRenderCallback()(el);
            expect(cb).toHaveBeenCalledTimes(1);
        });

        it('#onMount does nothing if not set', () => {
            expect(() => {
                api.getRenderCallback()(el);
            }).not.toThrow();
        });

        it('#onMount does nothing if callback is not function', () => {
            // @ts-expect-error checking that non-TS consumers don't shoot themselves (or others) in the foot
            api.onMount(null);
            expect(() => {
                api.getRenderCallback()(el);
            }).not.toThrow();
        });

        it('#onMount fails gracefully if callback throws error', () => {
            api.onMount(() => {
                throw Error('whoops');
            });
            expect(() => {
                api.getRenderCallback()(el);
            }).not.toThrow();
        });

        it('#onUnmount callback used in getCleanupCallback', () => {
            const cb = jest.fn();
            api.onUnmount(cb);
            api.getCleanupCallback()(el);
            expect(cb).toHaveBeenCalledTimes(1);
        });

        it('#onUnmount does nothing if not set', () => {
            expect(() => {
                api.getCleanupCallback()(el);
            }).not.toThrow();
        });

        it('#onUnmount does nothing if callback is not function', () => {
            // @ts-expect-error checking that non-TS consumers don't shoot themselves (or others) in the foot
            api.onUnmount(null);
            expect(() => {
                api.getCleanupCallback()(el);
            }).not.toThrow();
            expect(onDebug).toHaveBeenCalledTimes(1);
        });

        it('#onUnmount fails gracefully if callback throws error', () => {
            api.onUnmount(() => {
                throw Error('whoops');
            });
            expect(() => {
                api.getCleanupCallback()(el);
            }).not.toThrow();
            expect(onDebug).toHaveBeenCalledTimes(1);
        });
    });
});
