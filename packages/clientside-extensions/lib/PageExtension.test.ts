/**
 * @jest-environment jsdom
 */

import * as PageExtension from './PageExtension';

describe('PageExtension', () => {
    it('should generate the extension shape', () => {
        // given
        const fakeRenderCallback = jest.fn();

        // when
        const extensionProvider = PageExtension.factory(fakeRenderCallback);
        const extension = extensionProvider();

        // then
        expect(extension).toEqual({
            type: 'page',
            onAction: expect.any(Function),
        });
    });

    it('should call the render callback', () => {
        // given
        const fakeRenderCallback = jest.fn();
        const extensionProvider = PageExtension.factory(fakeRenderCallback);
        const extension = extensionProvider();

        const fakeNode = document.createElement('div');
        const fakeDataProvider = { foo: 'bar' };

        // when
        extension.onAction(fakeNode, fakeDataProvider);

        // then
        expect(fakeRenderCallback).toHaveBeenCalledWith(fakeNode, fakeDataProvider);
    });
});
