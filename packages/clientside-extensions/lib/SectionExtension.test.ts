import type { ExtensionAPI } from '@atlassian/clientside-extensions-registry';
import * as SectionExtension from './SectionExtension';
import type { SectionAttributesProvider } from './SectionExtension';

interface TestContext {
    value?: number;
}

describe('SectionExtension', () => {
    // @ts-expect-error we set the invalid type
    const testAttributesProvider: SectionAttributesProvider = (api, context: TestContext) => {
        api.onCleanup(jest.fn());
        api.updateAttributes({});

        return {
            type: 'not-valid',
            label: context.value === 42 ? 'The meaning of life' : 'A label',
        };
    };
    const testAPI: ExtensionAPI<typeof testAttributesProvider> = {
        onCleanup: jest.fn(),
        updateAttributes: jest.fn(),
    };

    it('should generate the extension descriptor with the correct type', () => {
        const extensionProvider = SectionExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, {});

        // expect all attributes to be merged, and type to be overwritten with the correct value.
        expect(extension).toEqual({
            type: 'section',
            label: 'A label',
        });
    });

    it('should provide a extensionAPI object to the extension provider', () => {
        expect(testAPI.onCleanup).toHaveBeenCalledTimes(0);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(0);

        const extensionProvider = SectionExtension.factory(testAttributesProvider);
        extensionProvider(testAPI, {});

        expect(testAPI.onCleanup).toHaveBeenCalledTimes(1);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(1);
    });

    it('should share any provided context with the attributes provider', () => {
        const extensionProvider = SectionExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, { value: 42 });

        // expect label to change because of context
        expect(extension).toEqual({
            type: 'section',
            label: 'The meaning of life',
        });
    });
});
