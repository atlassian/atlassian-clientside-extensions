import type { ExtensionAttributes, ExtensionAttributesProvider, OnAction } from '@atlassian/clientside-extensions-registry';

import { ExtensionType } from './extension-type';

export interface ButtonAttributes extends ExtensionAttributes {
    onAction?: OnAction;
    label: string;
    type?: ExtensionType.button;
}

export type ButtonAttributesProvider<ContextT> = ExtensionAttributesProvider<ButtonAttributes, ContextT>;

export const factory = <ContextT>(provider: ButtonAttributesProvider<ContextT>): ButtonAttributesProvider<ContextT> => {
    return (...args) => {
        return {
            ...provider(...args),
            type: ExtensionType.button,
        };
    };
};
