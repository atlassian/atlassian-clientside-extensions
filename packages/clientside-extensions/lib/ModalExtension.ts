import type { ExtensionAttributes, ExtensionAttributesProvider } from '@atlassian/clientside-extensions-registry';
import { onDebug } from '@atlassian/clientside-extensions-debug';
import type {
    Dispatch as ReactDispatch,
    KeyboardEvent as ReactKeyboardEvent,
    MouseEvent as ReactMouseEvent,
    SetStateAction as ReactSetStateAction,
    // eslint-disable-next-line node/no-extraneous-import
} from 'react';

import { ExtensionType } from './extension-type';
import type { LifecycleCallback, MountableExtension } from './AbstractExtension';
import { safeguard } from './AbstractExtension';

// A type defined by the @atlaskit/modal-dialog
type KeyboardOrMouseEvent = ReactMouseEvent<HTMLElement> | ReactKeyboardEvent<HTMLElement> | KeyboardEvent;

interface ModalCloseCallback {
    (e: KeyboardOrMouseEvent): boolean | void;
}

export type ModalAction = {
    text: string;
    onClick: () => void;
    isDisabled?: boolean;
    isLoading?: boolean;
    testId?: string;
};

export type ModalRenderExtension = (api: Api) => void;

export interface ModalAttributes extends ExtensionAttributes {
    onAction: ModalRenderExtension;
    label: string;
    type?: ExtensionType.modal;
}

export type ModalAttributesProvider<ContextT> = ExtensionAttributesProvider<ModalAttributes, ContextT>;

export const factory = <ContextT>(provider: ModalAttributesProvider<ContextT>): ModalAttributesProvider<ContextT> => {
    return (...args) => {
        const attributes = provider(...args);
        return {
            ...attributes,
            type: ExtensionType.modal,
        };
    };
};

const noop = () => {};

export enum Appearance {
    'danger' = 'danger',
    'warning' = 'warning',
}

export enum Width {
    'small' = 'small',
    'medium' = 'medium',
    'large' = 'large',
    'x-large' = 'x-large',
}

export class Api implements MountableExtension {
    private title = '';

    private width!: Width;

    private appearance!: Appearance;

    private renderCallback: LifecycleCallback = noop;

    private cleanupCallback: LifecycleCallback = noop;

    private readonly closeModalFn: () => void = noop;

    private onCloseCallback: ModalCloseCallback = () => true;

    constructor(closeModalFn: () => void, setActions: ReactDispatch<ReactSetStateAction<ModalAction[]>>) {
        this.closeModalFn = safeguard('onClose', closeModalFn);

        this.setActions = (actions) => {
            setActions(Api.filterActionProps(actions));

            return this;
        };
    }

    public setTitle = (title: string) => {
        this.title = title;

        return this;
    };

    public setWidth = (width: Width) => {
        // Runtime checking for 'non-ts' usage
        if (!(width in Width)) {
            onDebug(({ error }) => ({
                level: error,
                message: `Invalid value specified as "width" of modal. Please make sure to use one of the supported values.`,
                meta: {
                    value: width,
                    supported: Object.keys(Width).concat(),
                },
            }));
            return this;
        }

        this.width = width;
        return this;
    };

    public setAppearance = (appearance: Appearance) => {
        // Runtime checking for 'non-ts' usage
        if (!(appearance in Appearance)) {
            onDebug(({ error }) => ({
                level: error,
                message: `Invalid value specified as "appearance" of modal. Please make sure to use one of the supported values.`,
                meta: {
                    value: appearance,
                    supported: Object.keys(Appearance).concat(),
                },
            }));
            return this;
        }

        this.appearance = appearance;
        return this;
    };

    public onMount = (callback: LifecycleCallback) => {
        this.renderCallback = safeguard('onMount', callback);

        return this;
    };

    public onUnmount = (callback: LifecycleCallback) => {
        this.cleanupCallback = safeguard('onUnmount', callback);

        return this;
    };

    public closeModal = () => {
        this.closeModalFn();
    };

    public getTitle() {
        return this.title;
    }

    public getWidth() {
        return this.width;
    }

    public getAppearance() {
        return this.appearance;
    }

    public getRenderCallback() {
        return this.renderCallback;
    }

    public getCleanupCallback() {
        return this.cleanupCallback;
    }

    public setActions: (actions: ModalAction[]) => void;

    public onClose(callback: ModalCloseCallback) {
        this.onCloseCallback = safeguard('onClose', callback);

        return this;
    }

    public getOnCloseCallback() {
        return this.onCloseCallback;
    }

    static filterActionProps(actions: unknown[]): ModalAction[] {
        if (!Array.isArray(actions)) {
            return [];
        }

        return actions.map(({ text, onClick, isDisabled, isLoading, testId }) => {
            return { text, onClick, isDisabled, isLoading, testId };
        });
    }
}
