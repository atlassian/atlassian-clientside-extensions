import type { DataProviderPayload, ExtensionAttributes } from '@atlassian/clientside-extensions-registry';

import { ExtensionType } from './extension-type';

type RenderCallback<DataProviderT> = (node: HTMLElement, data: DataProviderPayload<DataProviderT>) => void;

export interface PageAttributes<DataProviderT> extends ExtensionAttributes {
    onAction: RenderCallback<DataProviderT>;
    type: ExtensionType.page;
}

export type PageAttributesProvider<DataProviderT = unknown> = () => PageAttributes<DataProviderT>;

export const factory = <DataProviderT>(renderCallback: RenderCallback<DataProviderT>): PageAttributesProvider<DataProviderT> => {
    return () => ({
        type: ExtensionType.page,
        onAction: (node, data) => {
            renderCallback(node, data);
        },
    });
};
