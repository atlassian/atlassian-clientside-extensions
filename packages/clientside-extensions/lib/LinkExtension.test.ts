import type { ExtensionAPI } from '@atlassian/clientside-extensions-registry';
import * as LinkExtension from './LinkExtension';
import type { LinkAttributesProvider } from './LinkExtension';

interface TestContext {
    value?: number;
}

describe('LinkExtension', () => {
    // @ts-expect-error we set the invalid type
    const testAttributesProvider: LinkAttributesProvider = (api, context: TestContext) => {
        api.onCleanup(jest.fn());
        api.updateAttributes({});

        return {
            type: 'not-valid',
            label: context.value === 42 ? 'The meaning of life' : 'A label',
            link: 'https://go.atlassian.com/clientside-extensions',
        };
    };

    const testAPI: ExtensionAPI<typeof testAttributesProvider> = {
        onCleanup: jest.fn(),
        updateAttributes: jest.fn(),
    };

    it('should generate the extension descriptor with the correct type', () => {
        const extensionProvider = LinkExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, {});

        // expect all attributes to be merged, and type to be overwritten with the correct value.
        expect(extension).toEqual({
            type: 'link',
            label: 'A label',
            link: 'https://go.atlassian.com/clientside-extensions',
        });
    });

    it('should provide a extensionAPI object to the extension provider', () => {
        expect(testAPI.onCleanup).toHaveBeenCalledTimes(0);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(0);

        const extensionProvider = LinkExtension.factory(testAttributesProvider);
        extensionProvider(testAPI, {});

        expect(testAPI.onCleanup).toHaveBeenCalledTimes(1);
        expect(testAPI.updateAttributes).toHaveBeenCalledTimes(1);
    });

    it('should share any provided context with the attributes provider', () => {
        const extensionProvider = LinkExtension.factory(testAttributesProvider);
        const extension = extensionProvider(testAPI, { value: 42 });

        // expect label to change because of context
        expect(extension).toEqual({
            type: 'link',
            label: 'The meaning of life',
            link: 'https://go.atlassian.com/clientside-extensions',
        });
    });
});
