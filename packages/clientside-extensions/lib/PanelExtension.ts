import type { Context, ExtensionAttributes, ExtensionAttributesProvider } from '@atlassian/clientside-extensions-registry';

import { ExtensionType } from './extension-type';
import type { LifecycleCallback, MountableExtension } from './AbstractExtension';
import { safeguard } from './AbstractExtension';

export type PanelRenderExtension = (api: Api, context?: Context<{}>, data?: unknown) => void;

export interface PanelAttributes extends ExtensionAttributes {
    onAction: PanelRenderExtension;
    type?: ExtensionType.panel;
}

export type PanelAttributesProvider<ContextT> = ExtensionAttributesProvider<PanelAttributes, ContextT>;

export const factory = <ContextT>(provider: PanelAttributesProvider<ContextT>): PanelAttributesProvider<ContextT> => {
    return (...args) => {
        return {
            ...provider(...args),
            type: ExtensionType.panel,
        };
    };
};

const noop = () => {};

export class Api implements MountableExtension {
    private renderCallback: LifecycleCallback = noop;

    private cleanupCallback: LifecycleCallback = noop;

    public onMount(callback: LifecycleCallback) {
        this.renderCallback = safeguard('onMount', callback);

        return this;
    }

    public onUnmount(callback: LifecycleCallback) {
        this.cleanupCallback = safeguard('onUnmount', callback);

        return this;
    }

    public getRenderCallback() {
        return this.renderCallback;
    }

    public getCleanupCallback() {
        return this.cleanupCallback;
    }
}
