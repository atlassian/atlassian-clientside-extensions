import type { ExtensionAttributes, ExtensionAttributesProvider } from '@atlassian/clientside-extensions-registry';

import { ExtensionType } from './extension-type';

export interface SectionAttributes extends ExtensionAttributes {
    type?: ExtensionType.section;
}

export type SectionAttributesProvider<ContextT> = ExtensionAttributesProvider<SectionAttributes, ContextT>;

export const factory = <ContextT>(provider: SectionAttributesProvider<ContextT>): SectionAttributesProvider<ContextT> => {
    return (...args) => {
        return {
            ...provider(...args),
            type: ExtensionType.section,
        };
    };
};
