import { useEffect, useState } from 'react';
import { isDebugEnabled, setDebugEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';
import type { DebugToggleHook } from './types';

const useDebug = (): DebugToggleHook => {
    const [debugState, setDebugState] = useState<boolean>(isDebugEnabled());

    useEffect(() => {
        const subscription = observeStateChange((newState) => {
            setDebugState(newState.debug);
        });
        return () => subscription.unsubscribe();
    }, []);

    return [debugState, setDebugEnabled];
};

export default useDebug;
