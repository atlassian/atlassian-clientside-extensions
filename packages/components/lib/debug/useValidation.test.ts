import { mocked } from 'ts-jest/utils';
import { act, renderHook } from '@testing-library/react-hooks';
import { isValidationEnabled, LogLevel, observeStateChange } from '@atlassian/clientside-extensions-debug';
import useValidation from './useValidation';

jest.mock('@atlassian/clientside-extensions-debug');

describe('useValidation hook', () => {
    beforeEach(() => {
        mocked(isValidationEnabled).mockClear();
        mocked(observeStateChange).mockClear();
    });

    it('should return the current state of the validation debug setting', () => {
        const INITIAL_VALUE = false;
        mocked(isValidationEnabled).mockReturnValueOnce(INITIAL_VALUE);
        mocked(observeStateChange).mockReturnValueOnce({ unsubscribe() {} });

        const { result } = renderHook(() => useValidation());
        const [validationValue] = result.current;

        expect(validationValue).toBe(INITIAL_VALUE);
    });

    it('should react to changes to the validation debug setting', () => {
        const INITIAL_VALUE = true;
        const CHANGED_BOOLEAN_VALUE = false;

        let updateCallback: Parameters<typeof observeStateChange>[0];
        mocked(isValidationEnabled).mockReturnValueOnce(INITIAL_VALUE);
        mocked(observeStateChange).mockImplementation((expectedCallback) => {
            updateCallback = expectedCallback;
            return {
                unsubscribe() {},
            };
        });

        const { result } = renderHook(() => useValidation());
        let [validationValue] = result.current;

        expect(validationValue).toBe(INITIAL_VALUE);

        act(() => {
            updateCallback({
                discovery: CHANGED_BOOLEAN_VALUE,
                debug: CHANGED_BOOLEAN_VALUE,
                logging: CHANGED_BOOLEAN_VALUE,
                validation: CHANGED_BOOLEAN_VALUE,
                logLevel: LogLevel.debug,
            });
        });

        [validationValue] = result.current;

        expect(validationValue).toBe(CHANGED_BOOLEAN_VALUE);
    });
});
