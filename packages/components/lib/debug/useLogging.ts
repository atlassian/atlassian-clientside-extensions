import { useEffect, useState } from 'react';
import { isLoggingEnabled, setLoggingEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';
import type { DebugToggleHook } from './types';

const useLogging = (): DebugToggleHook => {
    const [loggingState, setLoggingState] = useState<boolean>(isLoggingEnabled());

    useEffect(() => {
        const subscription = observeStateChange((newState) => {
            setLoggingState(newState.logging);
        });
        return () => subscription.unsubscribe();
    }, []);

    return [loggingState, setLoggingEnabled];
};

export default useLogging;
