import { useEffect, useState } from 'react';
import { isDiscoveryEnabled, setDiscoveryEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';
import type { DebugToggleHook } from './types';

const useDiscovery = (): DebugToggleHook => {
    const [discoveryState, setDiscoveryState] = useState<boolean>(isDiscoveryEnabled());

    useEffect(() => {
        const subscription = observeStateChange((newState) => {
            setDiscoveryState(newState.discovery);
        });
        return () => subscription.unsubscribe();
    }, []);

    return [discoveryState, setDiscoveryEnabled];
};

export default useDiscovery;
