export type DebugToggleHook = [boolean, (value: boolean) => void];
