import { useEffect, useState } from 'react';
import { isValidationEnabled, setValidationEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';
import type { DebugToggleHook } from './types';

const useValidation = (): DebugToggleHook => {
    const [validationState, setValidationState] = useState<boolean>(isValidationEnabled());

    useEffect(() => {
        const subscription = observeStateChange((newState) => {
            setValidationState(newState.validation);
        });
        return () => subscription.unsubscribe();
    }, []);

    return [validationState, setValidationEnabled];
};

export default useValidation;
