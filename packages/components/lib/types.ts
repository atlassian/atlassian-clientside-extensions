import type { ReactNode } from 'react';
import type { ExtensionAttributes, ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
// We only import types here
// eslint-disable-next-line node/no-unpublished-import
import type { Validator } from '@atlassian/clientside-extensions-schema';

export type ExtensionState<T extends ExtensionAttributes> = [ExtensionDescriptor<T>[], ExtensionDescriptor<T>[], boolean];
export type ExtensionPointCallback<T> = (supportedDescriptors: T[], unsupportedDescriptors: T[], loading: boolean) => ReactNode;
export type ExtensionPointState<T extends ExtensionAttributes = ExtensionAttributes> = {
    descriptors: ExtensionDescriptor<T>[];
    loadingState: boolean;
};
export type ExtensionPointUpdate<T extends ExtensionAttributes = ExtensionAttributes> = {
    state: ExtensionPointState<T>;
    update: ExtensionDescriptor<T>[];
};

export interface Options {
    attributeValidator: Validator;
    contextValidator?: Validator;
}

export function isNotNullOrUndefined<T>(input: null | undefined | T): input is T {
    return input !== undefined && input != null;
}
