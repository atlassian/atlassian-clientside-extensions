import ReactDOM from 'react-dom';
import type { ComponentType } from 'react';
import React from 'react';
import type { MountableExtension } from '@atlassian/clientside-extensions';

function renderElementAsReact<PropsT>(renderApi: MountableExtension, RenderElement: ComponentType<PropsT>, additionalProps?: PropsT) {
    renderApi
        .onMount((element) => {
            // eslint-disable-next-line react/jsx-props-no-spreading
            ReactDOM.render(<RenderElement {...(additionalProps as PropsT)} />, element);
        })
        .onUnmount((element) => {
            ReactDOM.unmountComponentAtNode(element);
        });
}

export default renderElementAsReact;
