import styled from '@emotion/styled';
import { keyframes } from '@emotion/core';
import * as colors from '@atlaskit/theme/colors';
import { themed } from '@atlaskit/theme/components';
import { gridSize } from '@atlaskit/theme/constants';

export const BlinkerContainer = styled.span`
    position: relative;
`;

const blinkAnimation = keyframes`
    from {
        transform: scale(1);
    }

    to {
        transform: scale(1.3);
    }
`;

export const Blinker = styled.button`
    height: 22px;
    width: 22px;
    background: rgba(7, 71, 166, 0.5);
    border-radius: 50%;
    animation: ${blinkAnimation} 1s ease-in-out infinite;
    animation-direction: alternate;
    cursor: pointer;
    border: none;

    &:after {
        display: block;
        background: rgb(7, 71, 166, 0.8);
        position: absolute;
        height: 12px;
        width: 12px;
        content: '';
        left: 50%;
        top: 50%;
        margin: -6px 0 0 -6px;
        border-radius: 50%;
    }
`;

// AK doesn't provide a plain Table styled component.
// Using AK theme to be compatible with their dark mode.
// Styles copied from: https://bitbucket.org/atlassian/atlassian-frontend/src/master/packages/design-system/dynamic-table/src/styled/
const rowTheme = {
    focusOutline: themed({ light: colors.B100, dark: colors.B100 }),
    borderColor: themed({ light: colors.N40, dark: colors.DN50 }),
    highlightedBackground: themed({ light: colors.N20, dark: colors.DN50 }),
    hoverBackground: themed({ light: colors.N10, dark: colors.DN40 }),
    hoverHighlightedBackground: themed({ light: colors.N30, dark: colors.DN60 }),
};

const headTheme = {
    focusOutline: themed({ light: colors.B100, dark: colors.B100 }),
    borderColor: themed({ light: colors.N40, dark: colors.DN50 }),
    textColor: themed({ light: colors.N300, dark: colors.DN300 }),
};

export const Table = styled.table`
    border-collapse: collapse;
    width: 100%;
`;

export const HeadRow = styled.tr`
    border-bottom: 2px solid ${headTheme.borderColor};
`;

export const HeadCell = styled.th`
    padding: ${gridSize() / 2}px ${gridSize}px;
    &:first-of-type {
        padding-left: 0;
    }
    &:last-of-type {
        padding-right: 0;
    }
    border: none;
    color: ${headTheme.textColor};
    box-sizing: border-box;
    font-size: 12px;
    font-weight: bold;
    position: relative;
    text-align: left;
    vertical-align: top;
    &:focus {
        outline: solid 2px ${headTheme.focusOutline};
    }
`;

const outlineWidth = '2px';

export const TableRow = styled.tr`
    border-bottom: 2px solid ${rowTheme.borderColor};
    &:hover {
        background-color: ${rowTheme.hoverBackground};
    }
    &:focus {
        outline: ${outlineWidth} solid ${rowTheme.focusOutline};
        outline-offset: -${outlineWidth};
    }
`;

export const TableCell = styled.td`
    border: none;
    padding: ${gridSize() / 1.2}px ${gridSize}px;
    text-align: left;

    &:first-of-type {
        padding-left: 0;
    }
    &:last-of-type {
        padding-right: 0;
    }
`;
