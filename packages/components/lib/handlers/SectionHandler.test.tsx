import type { FunctionComponent } from 'react';
import * as React from 'react';
import { render } from '@testing-library/react';

import { SectionHandler } from './SectionHandler';

const TestComponent: FunctionComponent = ({ children }) => {
    return <SectionHandler>{children}</SectionHandler>;
};

describe('SectionHandler', () => {
    it('should render the children passed to it', async () => {
        const { findByText } = render(
            <TestComponent>
                <div>some children</div>
            </TestComponent>,
        );

        expect(await findByText('some children')).toBeTruthy();
    });
});
