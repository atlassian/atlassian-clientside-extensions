import React from 'react';
import type { FunctionComponent } from 'react';

// eslint-disable-next-line import/prefer-default-export
export const SectionHandler: FunctionComponent = ({ children }) => {
    return <>{children}</>;
};
