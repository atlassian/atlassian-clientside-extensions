import type { ButtonProps } from '@atlaskit/button/standard-button';

export interface OnClickHandler {
    onAction: ButtonProps['onClick'];
    appearance?: ButtonProps['appearance'];
    className?: ButtonProps['className'];
    disabled?: ButtonProps['isDisabled'];
    hidden?: ButtonProps['hidden'];
    iconBefore?: ButtonProps['iconBefore'];
    iconAfter?: ButtonProps['iconAfter'];
    spacing?: ButtonProps['spacing'];
}
