import React from 'react';
import Button from '@atlaskit/button/standard-button';
import type { FunctionComponent } from 'react';
import type { OnClickHandler } from './types';

export interface LinkHandlerProps extends Partial<OnClickHandler> {
    href: string;
}

export const LinkHandler: FunctionComponent<LinkHandlerProps> = ({
    appearance = 'link',
    children,
    disabled,
    hidden,
    href,
    iconAfter,
    iconBefore,
    onAction,
    spacing,
}) => {
    if (hidden) {
        return null;
    }

    return (
        <Button
            appearance={appearance}
            href={href}
            iconAfter={iconAfter}
            iconBefore={iconBefore}
            isDisabled={disabled}
            onClick={onAction}
            spacing={spacing}
        >
            {children}
        </Button>
    );
};
