import React from 'react';
import Button from '@atlaskit/button/standard-button';
import type { FunctionComponent } from 'react';
import type { OnClickHandler } from './types';

export interface ButtonHandlerProps extends OnClickHandler {}

export const ButtonHandler: FunctionComponent<ButtonHandlerProps> = ({
    appearance,
    children,
    disabled,
    hidden,
    iconAfter,
    iconBefore,
    onAction,
    spacing,
}) => {
    if (hidden) {
        return null;
    }

    return (
        <Button
            type="button"
            appearance={appearance}
            iconAfter={iconAfter}
            iconBefore={iconBefore}
            isDisabled={disabled}
            onClick={onAction}
            spacing={spacing}
        >
            {children}
        </Button>
    );
};
