const { DefinePlugin } = require('webpack');
const path = require('path');
const WRMPlugin = require('atlassian-webresource-webpack-plugin');

const OUTPUT_PATH = path.join(__dirname, 'target/classes');

const mode = process.env.NODE_ENV;

// Define the metadata so we can use it later
const PLUGIN_KEY = 'com.atlassian.plugins.atlassian-clientside-extensions-runtime';
const RUNTIME_ENTRY_ID = 'runtime';
const ATLASSIAN_DEV_MODE_DATA_PROVIDER_KEY = 'atlassianDevMode';

// The full name of Java class defined by the AtlassianDevModeDataProvider
const ATLASSIAN_DEV_MODE_DATA_PROVIDER_CLASS = 'com.atlassian.plugin.clientsideextensions.AtlassianDevModeDataProvider';

// The full data provider key that we can use with "WRM.data.claim()" is build from:
// "<<full-plugin-key>>:<<web-resource-key>>.<<data-provider-key>>"
const ATLASSIAN_DEV_MODE_DATA_PROVIDER_WEB_RESOURCE_DATA_KEY = `${PLUGIN_KEY}:${RUNTIME_ENTRY_ID}.${ATLASSIAN_DEV_MODE_DATA_PROVIDER_KEY}`;

module.exports = {
    mode,
    entry: {
        [RUNTIME_ENTRY_ID]: path.resolve(__dirname, 'src/main/frontend/runtime.ts'),
    },
    output: {
        path: OUTPUT_PATH,
        libraryTarget: 'amd',
        library: '@atlassian/clientside-extensions-registry',
        libraryExport: 'default',
    },
    resolve: {
        extensions: ['.ts', '.js', '.json'],
    },
    module: {
        rules: [
            {
                test: /\.ts?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
        ],
    },
    optimization: {
        minimize: true,
    },
    plugins: [
        new DefinePlugin({
            // The stringify is required since webpack will inline any string value into code without escaping it
            ATLASSIAN_DEV_MODE_DATA_PROVIDER_KEY: JSON.stringify(ATLASSIAN_DEV_MODE_DATA_PROVIDER_WEB_RESOURCE_DATA_KEY),
        }),

        new WRMPlugin({
            pluginKey: PLUGIN_KEY,
            xmlDescriptors: path.join(OUTPUT_PATH, 'META-INF', 'plugin-descriptors', 'wr-cse-runtime-bundles.xml'),
            addEntrypointNameAsContext: false,
            webresourceKeyMap: {
                [RUNTIME_ENTRY_ID]: 'runtime',
            },
            dataProvidersMap: {
                [RUNTIME_ENTRY_ID]: [
                    {
                        key: ATLASSIAN_DEV_MODE_DATA_PROVIDER_KEY,
                        class: ATLASSIAN_DEV_MODE_DATA_PROVIDER_CLASS,
                    },
                ],
            },
        }),
    ],
};
