package com.atlassian.rest.plugins;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.sal.api.ApplicationProperties;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClientsideExtensionsResourceTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private ClientsideExtensionsResource resource;

    @Mock
    private DynamicWebInterfaceManager dynamicWebInterfaceManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Test
    public void testGetWebItemsWhenLocationExistsWithAttributeShouldReturnWebItemWithGivenAttribute() {
        // Arrange
        WebItem webItem = mock(WebItem.class);
        when(webItem.getWeight()).thenReturn(100);
        when(dynamicWebInterfaceManager.getDisplayableWebItems(anyString(), anyMap()))
                .thenReturn(singletonList(webItem));
        // Act
        Response response = resource.getWebItems("mylocation", emptyList());
        ClientsideExtensionsAssetsDto bean = (ClientsideExtensionsAssetsDto) response.getEntity();
        // Assert
        assertThat(bean.getWebItems().get(0).getWeight(), equalTo(100));
    }

    @Test
    public void testGetWebItemsWithExistingWebItemShouldHaveAWebItemInstance() {
        // Arrange
        WebItem webItem = mock(WebItem.class);
        when(dynamicWebInterfaceManager.getDisplayableWebItems(anyString(), anyMap()))
                .thenReturn(singletonList(webItem));
        // Act
        Response response = resource.getWebItems("mylocation", emptyList());
        ClientsideExtensionsAssetsDto bean = (ClientsideExtensionsAssetsDto) response.getEntity();
        // Assert
        assertThat(bean.getWebItems().get(0), instanceOf(WebItemDto.class));
    }

    @Test
    public void testGetWebItemsWhenNoLocationExistsShouldReturnOk() {
        // Arrange
        resource = new ClientsideExtensionsResource(applicationProperties, dynamicWebInterfaceManager);
        // Act
        Response response = resource.getWebItems("nonExistentLocation", emptyList());
        // Assert
        assertThat(response.getStatus(), is(HttpServletResponse.SC_OK));
    }

    @Test
    public void testGetWebItemsWhenNoLocationExistsShouldReturnEmptyList() {
        // Arrange
        resource = new ClientsideExtensionsResource(applicationProperties, dynamicWebInterfaceManager);
        // Act
        Response response = resource.getWebItems("nonExistentLocation", emptyList());
        ClientsideExtensionsAssetsDto bean = (ClientsideExtensionsAssetsDto) response.getEntity();
        // Assert
        assertThat(bean.getWebItems().size(), is(0));
    }

    @Test
    public void testGetWebItemsOnlyOfMatchingKeyIfSpecified() {
        // Arrange
        WebItem webItemShouldMatch = mock(WebItem.class);
        when(webItemShouldMatch.getCompleteKey()).thenReturn("should-match");

        WebItem webItemShouldNotMatch = mock(WebItem.class);
        when(webItemShouldNotMatch.getCompleteKey()).thenReturn("should-not-match");

        when(dynamicWebInterfaceManager.getDisplayableWebItems(anyString(), anyMap()))
                .thenReturn(asList(webItemShouldMatch, webItemShouldNotMatch));

        // Act
        Response response = resource.getWebItems("mylocation", singletonList("should-match"));
        ClientsideExtensionsAssetsDto bean = (ClientsideExtensionsAssetsDto) response.getEntity();

        // Assert
        assertThat(bean.getWebItems().size(), equalTo(1));
        assertThat(bean.getWebItems().get(0).getKey(), equalTo("should-match"));
    }

    @Test
    public void testGetWebItemsKeyMatchAcceptsMultipleKeys() {
        // Arrange
        WebItem webItemShouldMatch = mock(WebItem.class);
        when(webItemShouldMatch.getCompleteKey()).thenReturn("should-match");

        WebItem webItemShouldAlsoMatch = mock(WebItem.class);
        when(webItemShouldAlsoMatch.getCompleteKey()).thenReturn("should-also-match");

        when(dynamicWebInterfaceManager.getDisplayableWebItems(anyString(), anyMap()))
                .thenReturn(asList(webItemShouldMatch, webItemShouldAlsoMatch));

        // Act
        Response response = resource.getWebItems("mylocation", asList("should-match", "should-also-match"));
        ClientsideExtensionsAssetsDto bean = (ClientsideExtensionsAssetsDto) response.getEntity();

        // Assert
        assertThat(bean.getWebItems().size(), equalTo(2));
        assertThat(bean.getWebItems().get(0).getKey(), equalTo("should-match"));
        assertThat(bean.getWebItems().get(1).getKey(), equalTo("should-also-match"));
    }
}
