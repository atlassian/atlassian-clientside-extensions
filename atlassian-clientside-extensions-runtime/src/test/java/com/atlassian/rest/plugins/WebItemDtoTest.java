package com.atlassian.rest.plugins;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.plugin.web.api.WebItem;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.when;

public class WebItemDtoTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private WebItemDto webItemDto;

    @Mock
    private WebItem webItem;

    @Before
    public void setUp() {
        when(webItem.getCompleteKey()).thenReturn("com.atlassian.test.client-plugins");
        when(webItem.getSection()).thenReturn("reff.test-web-items-location");
        when(webItem.getAccessKey()).thenReturn("test-accesskey");
        when(webItem.getStyleClass()).thenReturn("test-style-class");
        when(webItem.getId()).thenReturn("test-web-item");
        when(webItem.getWeight()).thenReturn(100);
        when(webItem.getUrl()).thenReturn("/mc/bee/url/1");
        when(webItem.getLabel()).thenReturn("A web item dedicated to MC Barry Bee");
        when(webItem.getTitle()).thenReturn("MC Barry Bee");
    }

    @Test
    public void testWebItemBeanConstructor() {
        // Assert
        assertThat(webItemDto, instanceOf(WebItemDto.class));
    }

    @Test
    public void testWebItemBeanHasSetFields() {
        // Arrange
        webItemDto = new WebItemDto(webItem, "");
        // Assert
        assertThat(webItemDto.getKey(), is("com.atlassian.test.client-plugins"));
        assertThat(webItemDto.getLocation(), is("reff.test-web-items-location"));
        assertThat(webItemDto.getAccessKey(), is("test-accesskey"));
        assertThat(webItemDto.getStyleClass(), is("test-style-class"));
        assertThat(webItemDto.getLinkId(), is("test-web-item"));
        assertThat(webItemDto.getWeight(), is(100));
    }

    @Test
    public void testWebItemBeanHasAttributes() {
        // Arrange
        webItemDto = new WebItemDto(webItem, "");
        // Assert
        assertThat(webItemDto.getAttributes().get("url"), is("/mc/bee/url/1"));
        assertThat(webItemDto.getAttributes().get("label"), is("A web item dedicated to MC Barry Bee"));
        assertThat(webItemDto.getAttributes().get("title"), is("MC Barry Bee"));
    }

    @Test
    public void testWebItemBeanWithAllowedParamsInAttributes() {
        // Arrange
        Map<String, String> params = new HashMap<>();
        params.put("tooltip", "disabled");
        params.put("mcParams", "mcParamface");
        when(webItem.getParams()).thenReturn(params);
        // Act
        webItemDto = new WebItemDto(webItem, "");
        // Assert
        assertThat(webItemDto.getAttributes().get("mcParams"), is(nullValue()));
        assertThat(webItemDto.getAttributes().get("tooltip"), is("disabled"));
    }

    @Test
    public void testWebItemsBeanWithParams() {
        // Arrange
        Map<String, String> params = new HashMap<>();
        params.put("tooltip", "disabled");
        params.put("mcParams", "mcParamface");
        when(webItem.getParams()).thenReturn(params);
        // Act
        webItemDto = new WebItemDto(webItem, "");
        // Assert
        assertThat(webItemDto.getParams().size(), is(2));
        assertThat(webItemDto.getParams().get("mcParams"), is("mcParamface"));
        assertThat(webItemDto.getParams().get("tooltip"), is("disabled"));
    }

    @Test
    public void testWebItemsBeanAddsContextPath() {
        final String contextPath = "context-path";
        final String expectedUrl = contextPath + webItem.getUrl();

        webItemDto = new WebItemDto(webItem, contextPath);

        // URL property
        assertThat(webItemDto.getUrl(), is(contextPath + webItem.getUrl()));
        // URL attribute property
        assertThat(webItemDto.getAttributes().get("url"), is(expectedUrl));
    }
}
