package com.atlassian.rest.plugins;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugin.web.api.WebItem;

import static java.util.Collections.emptyList;

@XmlRootElement(name = "assets")
public class ClientsideExtensionsAssetsDto {
    @XmlElement
    private List<WebItemDto> webItems;

    public ClientsideExtensionsAssetsDto() {
        this(emptyList(), "");
    }

    public ClientsideExtensionsAssetsDto(List<WebItem> webItems, @Nonnull String contextPath) {
        this.webItems = webItems.stream()
                .map(webItem -> new WebItemDto(webItem, contextPath))
                .sorted(Comparator.comparing(WebItemDto::getWeight))
                .collect(Collectors.toList());
    }

    public List<WebItemDto> getWebItems() {
        return webItems;
    }

    public void setWebItems(List<WebItemDto> webItems) {
        this.webItems = webItems;
    }
}
