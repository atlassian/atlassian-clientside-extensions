package com.atlassian.rest.plugins;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Path("client-plugins")
@UnrestrictedAccess
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ClientsideExtensionsResource {
    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @ComponentImport
    private final DynamicWebInterfaceManager manager;

    @Inject
    public ClientsideExtensionsResource(
            ApplicationProperties applicationProperties, DynamicWebInterfaceManager manager) {
        this.applicationProperties = applicationProperties;
        this.manager = manager;
    }

    @GET
    @Path("/items")
    public Response getWebItems(
            @QueryParam("location") String location, @Nonnull @QueryParam("key") List<String> keys) {
        final String contextPath = applicationProperties.getBaseUrl(UrlMode.RELATIVE);

        List<WebItem> webItemsBySection = StreamSupport.stream(
                        getWebItemsBySection(manager, location).spliterator(),
                        false) // not worth parallel iterating for what's probably only a few items
                .collect(Collectors.toList());

        if (keys.isEmpty()) {
            return toResponse(webItemsBySection, contextPath);
        }

        return toResponse(getWebItemsByKey(webItemsBySection, keys), contextPath);
    }

    private static Map<String, Object> getContext() {
        return new HashMap<>();
    }

    private static List<WebItem> getWebItemsByKey(List<WebItem> items, List<String> keys) {
        return items.stream()
                .filter(webItem -> {
                    final String webItemKey = webItem.getCompleteKey();
                    return keys.stream().anyMatch(key -> key.equals(webItemKey));
                })
                .collect(toList());
    }

    private static Iterable<WebItem> getWebItemsBySection(DynamicWebInterfaceManager manager, String section) {
        return SafePluginPointAccess.call(() -> manager.getDisplayableWebItems(section, getContext()))
                .getOrElse(emptyList());
    }

    private static Response toResponse(List<WebItem> items, @Nonnull String contextPath) {
        return Response.ok(new ClientsideExtensionsAssetsDto(items, contextPath))
                .build();
    }
}
