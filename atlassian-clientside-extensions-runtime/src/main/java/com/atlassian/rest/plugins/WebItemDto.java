package com.atlassian.rest.plugins;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugin.web.api.WebItem;

import static java.util.Objects.isNull;

@XmlRootElement(name = "webitem")
public class WebItemDto {
    private static String[] DEFAULT_ALLOWED_PARAMS = new String[] {"glyph", "tooltip"};

    @XmlElement
    private String key;

    @XmlElement
    private String location;

    @XmlElement
    private String url;

    @XmlElement
    private String accessKey;

    @XmlElement
    private String label;

    @XmlElement
    private String title;

    @XmlElement
    private String styleClass;

    @XmlElement
    private String linkId;

    @XmlElement
    private Map<String, String> params;

    @XmlElement
    private int weight;

    @XmlElement
    private Map<String, String> attributes;

    public WebItemDto() {}

    public WebItemDto(WebItem item, @Nonnull String contextPath) {
        if (null != item) {
            key = item.getCompleteKey();
            location = item.getSection();
            accessKey = item.getAccessKey();
            styleClass = item.getStyleClass();
            linkId = item.getId();
            params = item.getParams();
            weight = item.getWeight();
            title = item.getTitle();
            url = addContextPathToUrlIfNeeded(item.getUrl(), contextPath);

            // Dynamic attributes and params
            attributes = new HashMap<>();
            attributes.put("url", url);
            attributes.put("label", item.getLabel());
            attributes.put("title", title);
            attributes.values().removeIf(Objects::isNull);
            attributes.putAll(extendByParams(item.getParams()));
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    /**
     * Filter params to DEFAULT_ALLOWED_PARAMS only
     */
    private static Map<String, String> extendByParams(Map<String, String> params) {
        if (params == null) {
            return new HashMap<>();
        }

        return params.entrySet().stream()
                .filter(map -> Arrays.asList(DEFAULT_ALLOWED_PARAMS).contains(map.getKey()) && map.getValue() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Nullable
    private static String addContextPathToUrlIfNeeded(@Nullable String url, @Nonnull String contextPath) {
        if (!isNull(url) && url.startsWith("/")) {
            return contextPath + url;
        }

        return url;
    }
}
