package com.atlassian.plugin.clientsideextensions;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableBoolean;
import com.atlassian.plugin.internal.util.PluginUtils;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

public class AtlassianDevModeDataProvider implements WebResourceDataProvider {
    @Override
    public Jsonable get() {
        return new JsonableBoolean(PluginUtils.isAtlassianDevMode());
    }
}
