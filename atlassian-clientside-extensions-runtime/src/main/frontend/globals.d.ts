/**
 * The value of <code>ATLASSIAN_DEV_MODE_DATA_PROVIDER_KEY</code> is defined in webpack configuration file
 */
declare const ATLASSIAN_DEV_MODE_DATA_PROVIDER_KEY = string;
