// eslint-disable-next-line import/no-unresolved, node/no-missing-import
import { claim as wrmDataClaim } from 'wrm/data';
import registry from '@atlassian/clientside-extensions-registry';

const isAtlassianDevModeEnabled = Boolean(wrmDataClaim<boolean>(ATLASSIAN_DEV_MODE_DATA_PROVIDER_KEY)) ?? false;
registry.toggleDevMode(isAtlassianDevModeEnabled);

export default registry;
