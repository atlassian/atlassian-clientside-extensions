declare module 'wrm/data' {
    // eslint-disable-next-line import/prefer-default-export
    export function claim<T = unknown>(dataKey: string): T;
}
