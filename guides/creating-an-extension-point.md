# Creating an extension point

> This Guide is for CSE version v2.0 and above. Please read here for [the old v1.x guide for writing extension points](./creating-an-extension-point-v1.md).

This is a guide for Product developers, or extension point owners.

First, make sure to install and provide Client-side Extensions in your Product as described in the [Installation Guide](../INSTALLATION.md).

## Creating an extension-schema

To create an extension point you need to specify its shape. This is done through a schema using `graphql`-style syntax.

### What is an extension-schema

You might rightfully ask: why do you need a schema to create a new extension?
We chose this way to define an extension-point because it allows us to statically analyze extension-points in a standardized way.
This helps to:

-   Clearly define the shape of an extension, and what the product expects from an extension developer
-   Allows generating documentation and in-page discovery without requiring you to do any work. This "single source of truth" also guarantees that both documentation and discovery are always up-to-date.
-   Allows generating validators that guarantee that all extensions that you as the extension-point provider receive through our api have the expected shape.
-   Potential to further innovate and provide better developer experience, such as generating typescript-typing or in-page code-playgrounds.

### A Simple example

The following schema describes a simple `button`-extension

```graphql
"""
---
extensionPoint: example.extension.point.name
---
"""
type Schema {
    type: ButtonExtension!

    """
    Text to render on the button
    """
    label: String!

    """
    Callback will be run when user clicks the button
    """
    onAction: Function!

    weight: Number
}
```

## About the available `graphql` syntax

Your schema definition must contain a `type Schema` definition that is used as the base for the extension point.

### Available syntax

The following parts of the `graphql` syntax are available to describe your extension points:

-   `scalar` - used for simple values or primitives
-   `type` - used to describe more complex key/value shapes, similar to an `Object` in javascript.
-   `enum` - enumeration of predefined values
-   `union` - used as an `or` to allow e.g. a key within an input to have multiple possible correct values. A union can consist of `type`, `enum`, `scalars`, or another `union`.

A `!` behind a type makes it _**required**_.

Surrounding a type with square brackets (`[]`) means an array of that type is expected.

### Provided scalars and types

The following types can be used in any schema and are provided by the schema package.

#### Scalars

<table>
    <thead>
        <tr class="header">
            <td>Name</td>
            <td>Description</td>
            <td>Example</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Boolean</td>
            <td>Represents <code>true</code> or <code>false</code>.</td>
            <td><code>false</code></td>
        </tr>
        <tr>
            <td>Date</td>
            <td>A javascript Date object.</td>
            <td><code>new Date()</code></td>
        </tr>
        <tr>
            <td>Function</td>
            <td>Represents any javascript function.</td>
            <td><code>function () { }</code></td>
        </tr>
        <tr>
            <td>Number</td>
            <td>Represents a numeric value. Can be an integer or floating point.</td>
            <td><code>3.14</code></td>
        </tr>
        <tr>
            <td>ModalOnAction</td>
            <td>The value should be a callback function expecting to receive <a href="https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/modal-api/">the <code>Modal</code> extension API</a></td>
            <td><code>function () { }</code></td>
        </tr>
        <tr>
            <td>PanelOnAction</td>
            <td>The value should be a callback function expecting to receive <a href="https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/panel-api/">the <code>Panel</code> extension API</a></td>
            <td><code>function () { }</code></td>
        </tr>
        <tr>
            <td>Promise</td>
            <td>A javascript Promise object.</td>
            <td><code>Promise.resolve()</code></td>
        </tr>
        <tr>
            <td>RegExp</td>
            <td>A regexp pattern, that can be used for matching against text.</td>
            <td><code>/foo.+bar/</code></td>
        </tr>
        <tr>
            <td>String</td>
            <td>The <code>String</code> type represents textual data.</td>
            <td><code>"foo"</code></td>
        </tr>
        <tr>
            <td>AsyncPanelExtension</td>
            <td>The value must be the constant "async-panel".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td>ButtonExtension</td>
            <td>The value must be the constant "button".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td>LinkExtension</td>
            <td>The value must be the constant "link".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td>ModalExtension</td>
            <td>The value must be the constant "modal".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td>PanelExtension</td>
            <td>The value must be the constant "panel".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td>SectionExtension</td>
            <td>The value must be the constant "section".</td>
            <td><code>N/A</code></td>
        </tr>
    </tbody>
</table>

#### Other predefined types

```graphql
type Icon {
    """
    Icon to render
    """
    glyph: IconGlyph!

    """
    Custom CSS class name passed to icon wrapper
    """
    className: String

    """
    String to use as the aria-label for the icon.
    Set to an empty string if you are rendering the icon with visible text to prevent accessibility label duplication.
    """
    label: String

    """
    Primary colour of the icons
    """
    primaryColor: String

    """
    Secondary colour of the two-color icons
    """
    secondaryColor: String
}

"""
The value must be either the name of the glyph or a functional React component returning an SVG glyph.
"""
union IconGlyph = String | Function
```

#### Create your own scalars and types

// TODO

## `useExtensions`

In order to provide a new extension point, import the `useExtensions` by importing your `graphql`-schema file.

```graphql
"""
---
extensionPoint: example.extension.point.name
---
"""
type Schema {
    type: ButtonExtension!

    """
    Text to render on the button
    """
    label: String!

    """
    Callback will be run when user clicks the button
    """
    onAction: Function!

    weight: Number
}
```

```jsx
import React from 'react';
import { useExtensions } from './my-schema.extension.graphql';

export default function MyComponent() {
    // The first parameter passed to the hook is the context value. We will talk about it later.
    const extensions = useExtensions(null);

    return (
        </>
            {extensions.map(extensionDescriptor => {
                // 3. Read the attributes from the descriptor and render the button
                const { key, attributes } = extensionDescriptor;

                return <button key={key} onClick={attributes.onAction}>{attributes.label}</button>
                // ...render them as you need
            })}
        </>
    );
}
```

What you receive is a list of descriptors that then you can render as you want. The shape of such descriptors is:

```ts
interface ExtensionDescriptor {
    // Unique extension key
    key: string;

    // Location, the name of the extension point
    location: string;

    // A number that can be used to decide about the order of extensions
    weight: number;

    // A map of all the attributes provided by the extension author
    attributes: {
        type: string;
        label: string;
        onAction?: () => void;
        [key: string]: unknown;
    };
}
```

You probably are most interested in the `attributes` property, which will be populated with the attributes provided by the extensions.

Descriptors are delivered already sorted by `weight` (from highest to lowest), but you can always sort them differently before rendering.

### `useExtensionsLoadingState`

You can use this hook in order to indicate your users that extensions, and their attributes are loading.

```jsx
import React from 'react';
import { useExtensions, useExtensionsLoadingState } from './my-schema.extension.graphql';

export default function MyComponent() {
    const extensions = useExtensions(null);
    const loading = useExtensionsLoadingState(null);

    return (
        <>
            {loading ? 'loading...' : extensions.map(ext => (
                // ...render them as you need
            ))}
        </>
    )
}
```

### `useExtensionsUnsupported`

There might be cases where you want to support legacy `WebItems` that are created using only the XML definitions inside `atlassian-plugin.xml` plugin descriptor file.

You can make use of `useExtensionsUnsupported` hook to gather all those extensions, and then show them as you want.

```jsx
import React from 'react';
import { useExtensionsUnsupported } from './my-schema.extension.graphql';

export default function MyComponent() {
    const extensions = useExtensionsUnsupported(null);

    return (
        <>
            {extensions.map((ext) => (
                <a href="ext.url">{ext.label}</a>
            ))}
        </>
    );
}
```

### `useExtensionsAll`

You can use `useExtensionsAll` in case you need all the results from `useExtensions`, `useExtensionsLoadingState` and `useExtensionsLoadingState` and prefer to get the results with a single hook.

```jsx
import React from 'react';
import { useExtensionsAll } from './my-schema.extension.graphql';

export default function MyComponent() {
    const [extensions, legacyExtensions, loading] = useExtensionsAll(null);

    const allExtensions = [...extensions, ...legacyExtensions].sort((a, b) => a.weight - b.weight);

    return (
        <>
            {loading ? 'loading...' : allExtensions.map(ext => (
                // ...render them as you need
            ))}
        </>
    );
}
```

## Context

You can share some context data with the extensions. Think about the context as the payload of the extension. This is especially useful
when you want to provide extension authors with additional data that developers can use to build the extension.
To provide a context, amend your extension schema to contain a definition of your schema.
This is both for documentation purpose as well as ensuring that the provided context matches the schema provided with the extension-point.

```graphql
"""
---
extensionPoint: example.extension.point.name
---
"""
type Schema {
    type: ButtonExtension!

    """
    Text to render on the button
    """
    label: String!

    """
    Callback will be run when user clicks the button
    """
    onAction: Function!

    weight: Number
}

type ContextSchema {
    issueKey: String
}
```

```jsx
import React from 'react';
import { useExtensions } from './my-schema.extension.graphql';

export default function IssueView({ issueKey }) {
    const context = {
        issueKey,
    };
    const [extensions, isLoading] = useExtensions(context);

    return (
        <>
            {extensions.map(descriptor => (
                // ...render them as you need
            ))}
        </>
    )
}
```

### `ExtensionPointInfo`

You can add this component to share with extension developers the schema of your locations and highlight the locations in
the current screen.

The information will only be available if the product has enabled their display in development mode.

```jsx
import React from 'react';
import { useExtensions, ExtensionPointInfo } from './my-schema.extension.graphql';

import schema from './schema.json';
import contextSchema from './context-schema.json';

export default function MyComponent({ context }) {
    const [extensions, isLoading] = useExtensions(context);

    return (
        <>
            <h4>
                Product Location
                <ExtensionPointInfo />
            </h4>
            {extensions.map(ext => (
                // ...render them as you need
            ))}
        </>
    )
}
```

Render the `ExtensionPointInfo` in a place that's visible even in edge cases, like when the location is inside a Dropdown
(place it in the dropdown trigger), or part of a Grid (place it in the header).

## Default extension types and handlers

### `Link`

Links are a static set of attributes with at least a `url` and `label` property. You can then decide how to handle
the case of rendering links as you see fit.

```jsx
import React from 'react';
import { LinkHandler } from '@atlassian/clientside-extensions-components';
import { useExtensions, ExtensionPointInfo } from './my-schema.extension.graphql';

function LinkExtension({ ext }) {
    const { onAction, label, url, ...attributes } = ext.attributes;

    return (
        <LinkHandler href={url} onAction={onAction} {...attributes}>
            {label}
        </LinkHandler>
    );
}

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'link':
            return <LinkExtension extension={ext} />;

        default:
            return null;
    }
}

export default function MyComponent({ context }) {
    const [extensions, isLoading] = useExtensions(context);

    return (
        <>
            <h4>
                Product Location
                <ExtensionPointInfo />
            </h4>
            <ul>
                {extensions.map((ext) => (
                    <li key={ext.key}>
                        <ExtensionRenderer extension={ext} />
                    </li>
                ))}
            </ul>
        </>
    );
}
```

### `Button`

Buttons provide a way for extension developers to execute an action when their button is clicked.
They declare such action by adding an `onAction` method to their attributes, so you should bind that method to your
button click handler when rendering.

```jsx
import React from 'react';
import { ButtonHandler } from '@atlassian/clientside-extensions-components';
import { useExtensions, ExtensionPointInfo } from './my-schema.extension.graphql';

function ButtonExtension({ ext }) {
    const { label, onAction, ...attributes } = ext.attributes;

    return (
        <ButtonHandler {...attributes} onClick={onAction}>
            {label}
        </ButtonHandler>
    );
}

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'button':
            return <ButtonExtension extension={ext} />;

        default:
            return null;
    }
}

export default function MyComponent({ context }) {
    const [extensions, isLoading] = useExtensions(context);

    return (
        <>
            <h4>
                Product Location
                <ExtensionPointInfo />
            </h4>
            <ul>
                {extensions.map((ext) => (
                    <li key={ext.key}>
                        <MyExtensionRenderer extension={ext} />
                    </li>
                ))}
            </ul>
        </>
    );
}
```

### `Panel`

Panels provide a container for extension developers to render custom HTML content (they are the equivalent of legacy web-panels).
In this case, the `onAction` method should receive an API with a `onMount` and `onUnmount` methods, and Products should handle
calling this methods when needed with the appropiate arguments.

Since this can be a bit difficult to mantain, we provide a default renderer that you can use.

```jsx
import React from 'react';
import { PanelHandler } from '@atlassian/clientside-extensions-components';
import { useExtensions, ExtensionPointInfo } from './my-schema.extension.graphql';
import { Button } from '@atlaskit/button';

const PanelExtension = ({ ext }) => {
    const { onAction } = ext.attributes;

    return <PanelHandler render={onAction} />;
};

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'panel':
            return <PanelExtension extension={ext} />;

        default:
            return null;
    }
}

export default function MyComponent({ context }) {
    const [extensions, isLoading] = useExtensions(context);

    return (
        <>
            <h4>
                Product Location
                <ExtensionPointInfo />
            </h4>
            <ul>
                {extensions.map((ext) => (
                    <li key={ext.key}>
                        <MyExtensionRenderer extension={ext} />
                    </li>
                ))}
            </ul>
        </>
    );
}
```

### `Modal`

Modals provide a button that opens a modal, and then a container in the body of that modal to render custom content.
It also provides a set of APIs to interact with the modal.

Since this case is very complicated to handle, we recommend using the provided handler.

```jsx
import React from 'react';
import { ModalWithActionHandler } from '@atlassian/clientside-extensions-components';
import { useExtensions, ExtensionPointInfo } from './my-schema.extension.graphql';
import { Button } from '@atlaskit/button';

import schema from './schema.json';
import contextSchema from './context-schema.json';

const ModalExtension = ({ ext }) => {
    const { onAction, label, ...attributes } = ext.attributes;
    return (
        <ModalWithActionHandler render={onAction} {...attributes}>
            {label}
        </ModalWithActionHandler>
    );
};

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'modal':
            return <ModalExtension extension={ext} />;

        default:
            return null;
    }
}

export default function MyComponent({ context }) {
    const [extensions, isLoading] = useExtensions(context);

    return (
        <>
            <h4>
                Product Location
                <ExtensionPointInfo />
            </h4>
            <ul>
                {extensions.map((ext) => (
                    <li key={ext.key}>
                        <MyExtensionRenderer extension={ext} />
                    </li>
                ))}
            </ul>
        </>
    );
}
```

In case you want to provide your own modal, take a look at the default renderer for a guide on how to do so.

## Alternative to Hooks

A component to define a Location is also provided in case you prefer it over hooks:

```jsx
import React from 'react';
import { ExtensionPoint } from './my-schema.extension.graphql';

const LinkExtension = ({ ext }) => {
    /*...*/
};

const ButtonExtension = ({ ext }) => {
    /*...*/
};

const PanelExtension = ({ ext }) => {
    /*...*/
};

const ModalExtension = ({ ext }) => {
    /*...*/
};

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'link':
            return <LinkExtension extension={ext} />;

        case 'button':
            return <ButtonExtension extension={ext} />;

        case 'panel':
            return <PanelExtension extension={ext} />;

        case 'modal':
            return <ModalExtension extension={ext} />;

        default:
            return null;
    }
}

export default function MyComponent({ context }) {
    return (
        <>
            <h4>
                Product Location
                <ExtensionPointInfo />
            </h4>
            <ul>
                <ExtensionPoint context={context}>
                    {(extensions) =>
                        extensions.map((ext) => (
                            <li key={ext.key}>
                                <MyExtensionRenderer extension={ext} />
                            </li>
                        ))
                    }
                </ExtensionPoint>
            </ul>
        </>
    );
}
```

It uses hooks internally, so you will need to use a version of React which supports hooks.
