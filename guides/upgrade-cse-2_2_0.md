# Upgrading to CSE 2.2

Client-Side Extensions 2.2 uses the [Custom Elements browser API](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements)
to implement a number of behaviours for extensions.

All modern browsers support the Custom Elements API.

## Developer test environment

If you have JavaScript tests for CSE code, you will have to ensure that your testing environment has all the required runtime APIs.
If you're testing outside a browser, we recommend using [JSDOM version 16.2 or higher](https://github.com/jsdom/jsdom/releases/tag/16.2.0)
which includes support for Custom Elements.

### Jest

If you are using the [Jest](https://jestjs.io/) testing framework, you need to upgrade to at least [version 26](https://github.com/facebook/jest/releases/tag/v26.0.0),
which bundles and uses a compatible JSDOM version.

Note that Jest version 27 does not use JSDOM as its default environment, so you will need to explicitly configure
your test environment to use it.

If you can't upgrade Jest, you can still update the local JSDOM version:

1. Install a Jest JSDOM environment dependency version 26 or newer:

    ```sh
    npm install jest-environment-jsdom@26.0.0 --save-dev
    ```

2. Configure Jest:

    ```js
    // jest.config.js
    module.exports = {
        // ...
        // remove me after upgrading to Jest > 26
        testEnvironment: require.resolve('jest-environment-jsdom'),
        // ...
    };
    ```
