# Releasing

All packages in the monorepo are released under the same version to a public [NPM registry](https://registry.npmjs.org).

The `runtime` and `page-bootstrapper` packages are released as a public artifacts to [Atlassian's registry](https://packages.atlassian.com)

> ## WARNING! Before running a release read the next paragraphs!

## Release steps

To perform a release, go to [CSE Pipelines](https://bitbucket.org/atlassian/atlassian-clientside-extensions/addon/pipelines/home)
and click the **Run pipeline** button.

You'll be able to select between two types of releases: `custom: snapshot-release` or `custom: stable-release`.

### Snapshot

A snapshot release is meant to be for testing a work in progress. It will create a version with the hash of the last
commit on the branch, and tag it as `snapshot` in NPM registry.

> It **will not** push any changes made to package versions to GIT after finishing.

### Stable

Use a stable release when bumping the version of the packages for a new major/minor/patch. It's only meant to be use for
work that's been tested and planned to be released.

As a rule of thumb, release a new snapshot release before releasing a stable release.

This kind of pipelines can only be executed on a `release/` branch, so make sure you have created one beforehand.
When running the pipeline, it will request for you to add a version number `RELEASE_VERSION`, and a distribution tag `DIST_TAG` (defaults to `latest`).

The release script **WILL** push version changes to the `release/` branch after the release finishes.
Make sure to create a Pull Request to upmerge the changes to any newer release branches and a `master` branch.

> When releasing a new stable version for the **1.x line** from `release/1.x` branch, please use the `1.x` value for the `DIST_TAG` option.

> When releasing a new stable version for the **2.x line** from `release/2.x` branch, you can keep `DIST_TAG` option empty of use `latest` value.

### Tagged

You can also use the stable release to perform a tagged release, meant to be use for `next` or `beta` releases for early
adopters to test our work.

You should specify the tag you want to use to distribute your version (e.g: `next`).

It **WILL** push version changes to the `release/` branch, but you don't need to merge the changes to `master` until you perform a `latest` release.

### Release scripts

> ### WARNING! It's recommended **to avoid** running the script locally

You can also use the release scripts locally if you need to. Run the command to learn how to use them:

```bash
yarn release
```

Before releasing a new version locally you will need to grant temporary publishing permissions to [Atlassian' registry][pac]:

```bash
atlas packages permission grant
```

## Performing a release with Pipelines

To perform a new release with Pipeline you should:

1. Add changes to the `CHANGELOG.md` file

    Make sure to add changes to the [CHANGELOG.md](./CHANGELOG.md) file. If you're working a new feature or bug fix that's not going to be released right after, add it to the top **[Unreleased]** section.

2. Update the `CHANGELOG.md` file and set correct version and a release date.
3. Go to [Pipelines](https://bitbucket.org/atlassian/atlassian-clientside-extensions/addon/pipelines/home) and click the **Run pipeline** button.
4. Inside the **Run pipeline** modal, select a release branch, e.g. `release/2.3.x` and then select `custom: stable-release` Pipeline.
5. Provide a version number into `RELEASE_VERSION` field, and a distribution tag `DIST_TAG` (defaults to `latest`).
6. Now, press the **Run** button and wait for the release to be completed.

### Post-release

#### Documentation

We're releasing the documentation to [DAC][dac] through a special docs package released to [PAC](https://packages.atlassian.com/ui/packages/npm:%2F%2F@atlassian%2Fdocs-dac-clientside-extensions) from a [NPMJS](https://www.npmjs.com/package/@atlassian/clientside-extensions-docs) package (publishing directly was not possible in the past from public repository pipelines. Until [DCA11Y-1272](https://hello.jira.atlassian.cloud/browse/DCA11Y-1272) is completed, and we adjust the process we run an additional pipeline in the DAC repo - this has the benefit of allowing us to control the DAC publication a bit more).

The NPM package released from the DAC pipeline will be published to DAC within a minute.

Please note that although we have versioning of `cse-docs` on NPMJS, DAC only has a single version - please ensure to not publish to DAC from old release branches to avoid moving the documentation back in time.

The DAC pipeline takes the [latest](https://www.npmjs.com/package/@atlassian/clientside-extensions-docs/v/latest) NPMJS `cse-docs` package version available and pushes it to [PAC](https://packages.atlassian.com/ui/packages/npm:%2F%2F@atlassian%2Fdocs-dac-clientside-extensions) after making [slight modifications to it](https://bitbucket.org/atlassian/dac/src/daec415fcb601d26b7ed1298195fd533923040aa/scripts/publish-clientside-extensions-docs.sh#lines-12) - because of this we can differentiate two docs publishing scenarios:

1. After a normal stable release
    - `cse-docs` are published as `latest` to NPMJS as part of each stable release and so, we only need to run the DAC pipeline when we want to publish that version to DAC
2. After making docs-only changes
    - since we don't want to publish a new version of all the packages after making documentation-only changes we need to push an additional `cse-docs@VERSION-docs-X` update-release to NPMJS so that it's available to users (possible and potentially desirable for older branches also) and to the DAC publishing pipeline if it's tagged as `latest`

(OPTIONAL) To release a docs update-release to NPMJS after making documentation-only changes:

1. Go to the CSE repository and check the version of the [`@atlassian/clientside-extensions-docs` NPM package](./documentation/package.json#lines-3) on the release branch.
2. Write down the current version e.g. `2.3.0-docs-3` or `2.2.0` and increase the suffix version with adding `-docs-X` suffix e.g.
    - `2.3.0-docs-3` => `2.3.0-docs-4`
    - `2.2.0` => `2.2.0-docs-1`
3. Go to [Bamboo Pipelines](https://ecosystem-bamboo.internal.atlassian.com/browse/CSE) and find the last completed run for your branch (the docs update commit should have executed the full pipeline)
4. Trigger the manual release stage and set required variables:
    1. Select the `RELEASE_TYPE` to be `DOCS-UPDATE`
    2. Provide the version number from step 2. as `RELEASE_VERSION`
5. Press the **Run** button and wait for the documentation release to NPMJS to be completed.

To copy the currently `latest` `cse-docs` package to PAC and eventually publish it to DAC (should only do this for latest/master):

1. Ensure the version you want to publish to DAC is tagged as `latest` on [NPMJS](https://www.npmjs.com/package/@atlassian/clientside-extensions-docs/v/latest)
2. Go to [Pipelines](https://bitbucket.org/atlassian/dac/pipelines) in the DAC repository and click the **Run pipeline** button.
3. Select `master` branch and `custom: publish-clientside-extensions-docs` pipeline.
4. Run the pipeline.

#### Upmerging

In most cases you should manually upmerge the `release/X.Y.Z` branch to a newer `release` branches, and finally back to `master` branch.

#### Announcing the release

Announce the new version in our Slack channel and `#server-platform-announcements` and post an internal blog post.

#### Updating the version on `master` branch

Once you release a new minor or a major version you might need to update the version on the `master` branch.

Refer to **Preparing a new minor or a major release from the `master` branch** paragraph for more information on how to do that.

## Preparing a new minor or a major release from the `master` branch

Before releasing a new minor or a major version from the `master` you should update the artifact versions on the `master` branch.
You can do that by using **Maven** and **yarn** commands.

Let's say we want to release a new minor version `3.4.0`. Open the terminal, and in the root of the project call those commands:

```shell
mvn versions:set --batch-mode -DgenerateBackupPoms=false -DnewVersion=3.4.0-SNAPSHOT
yarn lerna version --no-git-tag-version --force-publish --no-push --yes --exact 3.4.0-SNAPSHOT
```

Next, verify the changes and commit those to you branch with commit message, .e.g:

```
chore(release): Prepare a new development version 3.4.0-SNAPSHOT
```

You should push your branch and create a pull request with the changes .

[pac]: https://packages.atlassian.com "Atlassian's registry"
[dac]: https://developer.atlassian.com/server/framework/clientside-extensions/ 'Atlassian Developer'
