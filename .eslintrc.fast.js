// We use this set of rules to run ESLint on staged files only.
// The additional rules are disabled to improve the linting performance.

module.exports = {
    extends: './.eslintrc.js',
    parserOptions: {
        project: null,
    },
    rules: {
        '@typescript-eslint/dot-notation': 'off',
        '@typescript-eslint/no-throw-literal': 'off',
        '@typescript-eslint/no-implied-eval': 'off',
        '@typescript-eslint/return-await': 'off',
    },
};
