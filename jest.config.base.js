const { resolve } = require('path');

module.exports = {
    verbose: true,
    testTimeout: 60000,
    preset: 'ts-jest', // https://kulshekhar.github.io/ts-jest/user/config/
    modulePaths: ['<rootDir>/lib'],
    testPathIgnorePatterns: ['/node_modules/', '/dist/', '/target/', '/__fixtures__/'],
    clearMocks: true,
    moduleNameMapper: {
        '^wrm/require$': resolve(__dirname, '__mocks__/wrm/require.ts'),
        '^wrm/context-path$': resolve(__dirname, '__mocks__/wrm/contextPath.ts'),
        '^@atlaskit/modal-dialog$': resolve(__dirname, '__mocks__/@atlaskit/modal-dialog.tsx'),
    },
    setupFilesAfterEnv: [resolve(__dirname, 'tests/setupJest.ts')],
    setupFiles: ['whatwg-fetch'],
    watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
    globals: {
        // https://huafu.github.io/ts-jest/user/config/isolatedModules
        // We need this flag to avoid problems with Jest using the whole memory for heap when running with `--runInBand` flag on Bamboo
        'ts-jest': {
            isolatedModules: true,
        },
    },
};
