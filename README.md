# Atlassian Client-side Extensions

A set of APIs that allow a plugin author to augment and extend an Atlassian Server product's runtime using client-side JavaScript code.

You can read more about Client-Side Extensions on the official [documentation page](https://developer.atlassian.com/server/framework/clientside-extensions/).

## Creating an extension (Plugin authors)

Refer to the [guide](https://developer.atlassian.com/server/framework/clientside-extensions/guides/introduction/) to learn how to create extensions.

## Providing a new extension point (Product developers)

Refer to the [guide](https://developer.atlassian.com/server/framework/clientside-extensions/guides/extension-points/) to learn how to create a new extension point.

## Installation (Plugin authors)

To start using Client-side extensions in your plugin, please follow the [Getting stared](https://developer.atlassian.com/server/framework/clientside-extensions/guides/introduction/) guide.

## Installation (Product developers)

If you want to provide Client-side Extensions capabilities in your product, please follow the [Installation Guide](./INSTALLATION.md).

## Repository

This repository demonstrates how both product developers and extension authors would make use of CSE APIs.

-   `demos/demo-product` - Shows how a product developer could create a simple "app" with extension points for plugin developers to fill.
-   `demos/demo-extensions-basic` - Shows how a plugin author would create extensions for the demo product.
-   `demos/demo-extensions-complex` - Shows use of more advanced extension features, as well as illustrating some edge-cases.

Open the [Refapp Demo](./environments/refapp/) to see these various demos in action within an Atlassian product & plugin environment.

## Releases

For a full list of features released in each version, please read our [Change Log](./CHANGELOG.md).

## Contributing

We encourage you to contribute to Client-side Extensions! Please check out the [Contributing](./CONTRIBUTING.md) section to know how to get started.

## Help and Support

If you have found a bug, or you have any other issue related to Client-Side Extensions you can [report the issue in the Client-side Extensions project][cse-project].

Click on the "Create" issue button and provide the reproduction steps. Please include as much information as you can, including:

-   Application name and version
-   A version of CSE modules you are using
-   Browser vendor name and version

### Developer Community

You can also ask a question on [Developer Community Portal][cdac] under the **Atlassian Developer Tools** category.

[cse-project]: https://ecosystem.atlassian.net/browse/CSE 'CSE Project'
[cdac]: https://community.developer.atlassian.com/ 'Developer Community Portal'

## Code Quality

This repository enforces the Palantir code style using the Spotless Maven plugin. Any violations of the code style will result in a build failure.

### Guidelines:

-   **Formatting Code:** Before raising a pull request, run `mvn spotless:apply` to format your code.
-   **Configuring IntelliJ:** Follow [this detailed guide](https://hello.atlassian.net/wiki/spaces/AB/pages/4249202122/Spotless+configuration+in+IntelliJ+IDE) to integrate Spotless into your IntelliJ IDE.
