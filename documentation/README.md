# Client-side Extensions Docs

This repository contains the developer documentation for client-side extensions. Documentation is written in Markdown and published to the Atlassian npm repository.

These docs will be published at:

[https://developer.atlassian.com/server/framework/clientside-extensions](https://developer.atlassian.com/server/framework/clientside-extensions])

## Preview

1. Install [Atlas CLI](https://developer.atlassian.com/platform/atlas-cli/users/install/) and DAC plugin `atlas plugin install -n dac`.
2. Run `yarn start` to start the server with the content locally at : [http://localhost:8080/server/framework/clientside-extensions](http://localhost:8080/server/framework/clientside-extensions)

## Validation

`yarn validate`

Will check that all the links work correctly, and all metadata in the MD files are correct.

## Spellcheck

`yarn spellcheck`

You can add your own words to the dictionary by editing the `.spelling` file of this package.
