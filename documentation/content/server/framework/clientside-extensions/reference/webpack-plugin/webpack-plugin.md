---
title: Webpack Plugin - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: webpack
date: '2024-09-24'
---

# Webpack plugin

Client-side Extensions webpack plugin depends on [WRM webpack plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin).

Refer to the [setup guide](/server/framework/clientside-extensions/guides/how-to/setup-webpack-plugin/) to check how to install and configure the webpack plugin.

## Configuration

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>pattern*</td>
            <td>Glob pattern to specify which files should be considered as entry-points.</td>
        </tr>
        <tr>
            <td>ignore</td>
            <td>Define patterns of files to ignore when searching for clientside-extension annotations.</td>
        </tr>
        <tr>
            <td>cwd</td>
            <td>Configure a working directory to look for entry-points.</td>
        </tr>
        <tr>
            <td>xmlDescriptors</td>
            <td>Define the output filename that will contain the XML plugin module descriptors.</td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

## generateEntrypoints()

Generates all the entry-point definitions for the webpack `entry` configuration.

## Usage

```js
const path = require('path');
const ClientsideExtensionsWebpackPlugin = require('@atlassian/clientside-extensions-webpack-plugin');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');

const wrmPlugin = new WrmPlugin(/*...*/);

const clientsideExtensions = new ClientsideExtensionsWebpackPlugin({
    pattern: '**/clientside-extensions/**/*.tsx',
    cwd: path.join(__dirname, './src/main/frontend'),
});

module.exports = {
    entry: {
        ...clientsideExtensions.generateEntrypoints(),
    },
    plugins: [wrmPlugin, clientsideExtensions],
    /*...*/
};
```

## Self troubleshooting tool

We provide a tool that helps troubleshoot the configuration of webpack and Atlassian P2 projects.

To check if your project was configured correctly, open a terminal and navigate to the root directory of your project. Next, run the troubleshooting command:

```shell
npx @atlassian/wrm-troubleshooting
```

The tool will ask you a few questions and guide you if it finds issues with your projects' configuration.

You can read more about the troubleshooting tool on the [NPM page for the `@atlassian/wrm-troubleshooting` package][https://www.npmjs.com/package/@atlassian/wrm-troubleshooting].

### Enable verbose (debug) output

It may be hard to understand what files were scanned and what the result of that was, by setting the environment variable `CSE_DEBUG=true`,
some additional information will be printed out while running.
