---
title: Extension Factories - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: api
date: '2024-09-24'
---

# Extension Factories

Extension factories are a set of provided helper utilities to create [extensions](/server/framework/clientside-extensions/reference/glossary/).

They will set the type of the extensions automatically, and provide type definitions for your [attribute provider](/server/framework/clientside-extensions/reference/glossary/) if you're using TypeScript.

## Signature

```ts
type factory = (api: ExtensionAPI, context: Context<{}>) => ExtensionAttributes;
```

## Parameters

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>extensionAPI</td>
            <td><code>ExtensionAPI</code></td>
            <td><a href="/server/framework/clientside-extensions/reference/api/extension-api">Extension API</a> that provides methods to render and clean up the extension.</td>
        </tr>
        <tr>
            <td>context</td>
            <td><code>object</code></td>
            <td>An object with information that products share with extensions to share important information about the screen where they are rendered.</td>
        </tr>
    </tbody>
</table>

## Custom types and attributes

There is a set of default attributes that all products support, but products can also add custom attributes or extension types to extend their UI in custom ways.

{{% tip %}}
Always remember to check the documentation of each product's extension point and supported attributes.

Read more information about [Revealing extension points on the page](/server/framework/clientside-extensions/guides/introduction/discovering-extension-points/#revealing-extension-points-on-the-page).
{{% /tip %}}
