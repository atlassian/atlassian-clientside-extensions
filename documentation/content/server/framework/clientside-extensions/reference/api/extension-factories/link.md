---
title: Link - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: api
date: '2024-09-24'
---

# Link

A link extension allows to render a link on the screen and navigate to another page when clicked by a user.

## Supported attributes

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>label</code> <strong>*</strong></td>
            <td><code>string</code></td>
            <td>Text to be used as the label of the link.</td>
        </tr>
        <tr>
            <td><code>weight</code></td>
            <td><code>number</code></td>
            <td>
                <p>Determines the order in which this extension appears respect to others in the same location.</p>
                <p>Extensions are displayed top to bottom or left to right in order of ascending weight.</p>
            </td>
        </tr>
        <tr>
            <td><code>url</code> <strong>*</strong></td>
            <td><code>string</code></td>
            <td>Defines a URL that's going to be used for the link.</td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

{{% tip %}}
Always remember to check the documentation of each product's extension point and supported attributes.

Read more information about [Revealing extension points on the page](/server/framework/clientside-extensions/guides/introduction/discovering-extension-points/#revealing-extension-points-on-the-page).
{{% /tip %}}

## Supported annotations

<table>
    <colgroup>
        <col width="30%" />
        <col width="10%" />
        <col width="60%" />
    </colgroup>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>@clientside-extension</code> <strong>*</strong></td>
            <td><code>-</code></td>
            <td>Indicates that the next function is an extension factory to be consumed by the webpack plugin.</td>
        </tr>
        <tr>
            <td><code>@extension-point</code> <strong>*</strong></td>
            <td><code>string</code></td>
            <td>Defines the location where the extension will be rendered.</td>
        </tr>
        <tr>
            <td><code>@condition</code></td>
            <td><code>string | Condition, UrlReadingCondition</code></td>
            <td>
                <p>Defines one or multiple conditions that must be satisfied for the extension to be displayed.</p>
                <p>The conditions are evaluated on the server, and created with Java.</p>
                <p>If one of the conditions is not met, the code of the extension won't be loaded in the client.</p>
                <p>For more information about the conditions please refer to the <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/web-section-plugin-module/#condition-and-conditions-elements">examples of Web items documentation</a>.</p>
            </td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

## Usage

```ts
import { LinkExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default LinkExtension.factory((extensionAPI, context) => ({
    label: 'Go to DAC',
    url: 'https://developer.atlassian.com/',
}));
```

### With context definition

```ts
import { LinkExtension } from '@atlassian/clientside-extensions';

interface ExampleContext {
    title: string;
    url: string;
}

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default LinkExtension.factory<ExampleContext>((extensionAPI, context) => ({
    label: `Go to ${context.title}`,
    url: context.url,
}));
```
