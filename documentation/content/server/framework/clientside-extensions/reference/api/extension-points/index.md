---
title: Extension Points - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: api
date: '2024-09-24'
---

# Extension Points

{{% warning %}}
This is a guide for CSE version v2.x and upwards.

For the v1.x version, visit the [extension point v1 deprecated](/server/framework/clientside-extensions/reference/api/extension-points-v1-deprecated/) reference guide.
{{% /warning %}}

Extension points allow to consume extensions registered for a given extension point name.

An extension point consist primarily of a schema to define what attributes are supported,
and using the provided react hooks and components to get the extensions, share context with their
[factories](/server/framework/clientside-extensions/reference/api/extension-factories/),
and validate the resulting attributes against the schema.

## Schemas

A schema defines the attributes and types expected from an extension to work correctly on a given extension point.
They are created using a `.graphql` file and graphql-style syntax.

At minimum, a schema definition needs to have the name of the extension point and a type named `Schema`.

`schema.cse.graphql`

```graphql
"""
---
extensionPoint: example.extension-point
---
"""
type Schema {
    """
    Extension type
    """
    type: LinkExtension!

    """
    Label of the link
    """
    label: string!
}
```

{{% tip %}}
Refer to the [schema reference](/server/framework/clientside-extensions/reference/api/extension-points/schemas) to learn more about schemas.
{{% /tip %}}

## schema-loader

In order to use schemas, you'll need to install the `@atlassian/clientside-extensions-schema` webpack loader.

From there, you will be able to import the hooks and components directly from the schema as follow:

`schema.cse.graphql`

```graphql
"""
---
extensionPoint: example.extension-point
---
"""
type Schema {
    # schema definition
}
```

_extension-point.tsx_

```jsx
import { useExtensions } from './schema.cse.graphql';

// Use the hooks and components resulting from importing the schema as you need
```

{{% tip %}}
Follow the [How to setup CSE schema-loader](/server/framework/clientside-extensions/guides/how-to/setup-schema-loader)
to learn how to install and configure the loader.
{{% /tip %}}

## Hooks and Components

CSE provides a set of react hooks and components to consume the extensions registered for a given extension point. These
hooks and components will receive an optional context, and validate the resulting attributes against the provided schema.

You can make use of them by importing the schema directly, and the loader will make them available after building the code:

`schema.cse.graphql`

```graphql
"""
---
extensionPoint: example.extension-point
---
"""
type Schema {
    """
    Extension type
    """
    type: LinkExtension!

    """
    Label of the link
    """
    label: string!

    """
    URL of the link
    """
    url: string!
}
```

_extension-point.tsx_

```jsx
import React from "react";
import { useExtensions } from "./schema.cse.graphql";

const ExampleExtensionPoint = ({ context }) => {
    const extensions = useExtensions(context);

    return (
        <>
            {extensions.map(({attributes}) => <a href={attributes.url}>{attributes.label}</a>}
        </>
    )
}

export default ExampleExtensionPoint;
```

{{% tip %}}
Refer to the [hooks and components reference](/server/framework/clientside-extensions/reference/api/extension-points/hooks-and-components) for more details.
{{% /tip %}}

## Extension Handlers

Client-side Extensions comes with a set of default extension types, each requiring [their own API](/server/framework/clientside-extensions/reference/api/extension-api/) to work correctly.

Some of them are very simple, but others require a large API that can be difficult to provide on your own, so
CSE provides default handlers that help you render extensions with Atlaskit components.

{{% tip %}}
Refer to the [extension handlers reference](/server/framework/clientside-extensions/reference/api/extension-points/extension-handlers) to learn how to use them.
{{% /tip %}}
