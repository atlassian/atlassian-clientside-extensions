---
title: Schemas - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: api
date: '2024-09-24'
---

# Schemas

A schema defines the attributes and types expected from an extension to work correctly on a given extension point.
They are created using a `.graphql` file and [graphql-style syntax](https://spec.graphql.org/June2018/).

In order to use the schemas, it's necessary to install and configure the `@atlassian/clientside-extensions-schema`
webpack loader.

Follow the [How to setup CSE schema-loader](/server/framework/clientside-extensions/guides/how-to/setup-schema-loader) guide to do so.

## Schema definition

At minimum, a schema definition needs to have the name of the extension point and a type named `Schema`:

```graphql
"""
---
extensionPoint: example.extension-point
---
"""
type Schema {
    # schema definition
}
```

## Context schema

Schemas can also used to define the shape of a valid context object to be shared with extensions. They are
useful in order to prevent breaking changes caused by unexpected changes in the context provided:

```graphql
"""
---
extensionPoint: example.extension-point
---
"""
type Schema {
    # schema definition
}

type ContextSchema {
    # schema definition
}
```

## Available `graphql` syntax

The following parts of the `graphql` syntax are available to define schemas:

<table>
    <thead>
        <tr class="header">
            <th>Syntax</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>scalar</code></td>
            <td>Used for simple values or primitives.</td>
        </tr>
        <tr>
            <td><code>type</code></td>
            <td>Used to describe more complex key/value shapes, similar to an `Object` in javascript.</td>
        </tr>
        <tr>
            <td><code>enum</code></td>
            <td>Enumeration of predefined values.</td>
        </tr>
        <tr>
            <td><code>union</code></td>
            <td>
                Used as an `or` to allow e.g. a key within an input to have multiple possible correct values.
                A union can consist of `type`, `enum`, `scalars`, or another `union`.
            </td>
        </tr>
        <tr>
            <td><code>!</code></td>
            <td>
                A `!` behind a type makes it <b><i>required</i></b>.
            </td>
        </tr>
        <tr>
            <td><code>[]</code></td>
            <td>
                Surrounding a type with square brackets (`[]`) means an array of that type is expected.
            </td>
        </tr>
    </tbody>
</table>

## Provided scalars

The following scalars can be used in any schema and are provided by the schema package:

<table>
    <thead>
        <tr class="header">
            <td>Name</td>
            <td>Description</td>
            <td>Example</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>Boolean</code></td>
            <td>Represents <code>true</code> or <code>false</code>.</td>
            <td><code>false</code></td>
        </tr>
        <tr>
            <td><code>Date</code></td>
            <td>A javascript Date object.</td>
            <td><code>new Date()</code></td>
        </tr>
        <tr>
            <td><code>Function</code></td>
            <td>Represents any javascript function.</td>
            <td><code>function () { }</code></td>
        </tr>
        <tr>
            <td><code>Number</code></td>
            <td>Represents a numeric value. Can be an integer or floating point.</td>
            <td><code>3.14</code></td>
        </tr>
        <tr>
            <td><code>ModalOnAction</code></td>
            <td>
                The value should be a callback function expecting to receive
                <a href="server/framework/clientside-extensions/reference/api/extension-api/modal-api/">the <code>Modal</code> extension API</a>
            </td>
            <td><code>function () { }</code></td>
        </tr>
        <tr>
            <td><code>PanelOnAction</code></td>
            <td>
                The value should be a callback function expecting to receive
                <a href="/server/framework/clientside-extensions/reference/api/panel-api/">the <code>Panel</code> extension API</a>
            </td>
            <td><code>function () { }</code></td>
        </tr>
        <tr>
            <td><code>Promise</code></td>
            <td>A javascript Promise object.</td>
            <td><code>Promise.resolve()</code></td>
        </tr>
        <tr>
            <td><code>RegExp</code></td>
            <td>A regexp pattern, that can be used for matching against text.</td>
            <td><code>/foo.+bar/</code></td>
        </tr>
        <tr>
            <td><code>String</code></td>
            <td>The <code>String</code> type represents textual data.</td>
            <td><code>"foo"</code></td>
        </tr>
        <tr>
            <td><code>AsyncPanelExtension</code></td>
            <td>The value must be the constant "async-panel".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td><code>ButtonExtension</code></td>
            <td>The value must be the constant "button".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td><code>LinkExtension</code></td>
            <td>The value must be the constant "link".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td><code>ModalExtension</code></td>
            <td>The value must be the constant "modal".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td><code>PanelExtension</code></td>
            <td>The value must be the constant "panel".</td>
            <td><code>N/A</code></td>
        </tr>
        <tr>
            <td><code>SectionExtension</code></td>
            <td>The value must be the constant "section".</td>
            <td><code>N/A</code></td>
        </tr>
    </tbody>
</table>

## Provided types

The following predefined types are provided by the schema package:

### Icon

An Icon object with a glyph that allows to customize its colors and CSS class.

```graphql
type Icon {
    """
    Icon to render
    """
    glyph: IconGlyph!

    """
    Custom CSS class name passed to icon wrapper
    """
    className: String

    """
    String to use as the aria-label for the icon.
    Set to an empty string if you are rendering the icon with visible text to prevent accessibility label duplication.
    """
    label: String

    """
    Primary colour of the icons
    """
    primaryColor: String

    """
    Secondary colour of the two-color icons
    """
    secondaryColor: String
}
```

### IconGlyph

The value must be either the name of the glyph or a functional React component returning an SVG glyph.

```graphql
union IconGlyph = String | Function
```

## Usage notes

### 1. Schema definition with an icon

```graphql
"""
---
extensionPoint: example.extension-point
---
"""
type Schema {
    type: ButtonExtension!

    """
    Text to render on the button
    """
    label: String

    """
    Callback will be run when user clicks the button
    """
    onAction: Function

    """
    An icon to show on the button if provided
    """
    icon: Icon
}
```

### 2. Schema definition that supports modal extensions

```graphql
"""
---
extensionPoint: example.extension-point
---
"""
type Schema {
    type: ModalExtension!

    """
    Text to render on the button
    """
    label: String

    """
    The value should be a callback function expecting to receive the modal extension API
    """
    onAction: ModalOnAction
}
```

### 3. Context schema definition with custom type

```graphql
"""
---
extensionPoint: example.extension-point
---
"""
type Schema {
    type: LinkExtension!

    label: String
}

"""
It's also possible to define custom types in graphql syntax
"""
type User {
    name: String

    age: Number

    lastLogin: Number

    isAdmin: Boolean
}

type ContextSchema {
    """
    Using custom type to define a property of context
    """
    user: User

    unreadMessages: Number
}
```
