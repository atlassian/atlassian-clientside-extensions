---
title: Get Help - Client-side Extensions
platform: server
product: clientside-extensions
category: help
subcategory: get-help
date: '2024-09-24'
---

# Get help

## Client-Side Extensions

If you need help or want to give any feedback about CSE, please refer to our [Issue Tracker](https://ecosystem.atlassian.net/browse/CSE).

Click on the "Create" issue button and provide the reproduction steps. Please include as much information as you can, including:

-   Application name and version
-   A version of CSE modules you are using
-   Browser vendor name and version

## Support

### If you are a Marketplace Developer

If you are a Marketplace Developer, you can [contact support team and request help](https://developer.atlassian.com/platform/marketplace/#marketplace-support).

### If you are not a Marketplace Developer

If you are not a Marketplace Developer but have a Server or DC product license go to [https://support.atlassian.com](https://support.atlassian.com).

At the bottom of the page, there should be a green “Contact Support” button you can use

## Additional links

-   Check the [Debugging and troubleshooting an extension guide](/server/framework/clientside-extensions/guides/how-to/debugging-and-troubleshooting-an-extension)
