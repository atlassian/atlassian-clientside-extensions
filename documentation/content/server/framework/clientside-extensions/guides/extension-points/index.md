---
title: Providing extension points - Client-side Extensions
platform: server
product: clientside-extensions
category: devguide
subcategory: extension-points
date: '2024-09-24'
---

# Getting started

{{% warning %}}
The APIs for creating extension points are based on the React framework and Atlaskit, meaning you can
only use them if you're developing with React.

A framework agnostic solution is being designed and will be available on a future release.
{{% /warning %}}

For this tutorial, you will be creating an extension point on a page in Bitbucket Server 7.x.

The Bitbucket Server team provides a template to create a Bitbucket Plugin with all the tools already configured for
you, including Client-side Extensions (CSE).

You can grab a copy by cloning
the [Bitbucket CSE template](https://bitbucket.org/atlassianlabs/bitbucket-client-side-extensions-template):

```
git clone git@bitbucket.org:atlassianlabs/bitbucket-client-side-extensions-template.git
```

{{% note %}}
If you want to create extension points on your own project, follow the guide on
[how to setup CSE schema-loader](/server/framework/clientside-extensions/guides/how-to/setup-schema-loader/) before you
get started.
{{% /note %}}

## Installing the requirements

You will need to install:

-   Node 12.19+
-   Maven 3.6.3
-   Java JDK 1.8
-   [Atlassian SDK 8](https://developer.atlassian.com/server/framework/atlassian-sdk/downloads/)

## Starting Bitbucket

To install all the dependencies for the first time, run:

```
atlas-package -DskipTests
```

To start Bitbucket from your project, run:

```
atlas-run -DskipTests
```

Once the Bitbucket starts, you should see this message in the terminal:

```
bitbucket started successfully in XYZs at http://localhost:7990/bitbucket
```

Now, you can open the browser and navigate to http://localhost:7990/bitbucket

### Default credentials

The local instance of Bitbucket requires you to log in using those credentials:

-   username: `admin`
-   password: `admin`

## Developing

The template comes with watchmode and hot reload configured for your front-end code. You can run the CSE watch server by
executing:

```
npm start
```

## Working directory

You will be working on a blank page created
with [CSE page extensions](/server/framework/clientside-extensions/reference/api/extension-factories/page/), and will
create an extension point named `extension.points.tutorial`.

{{% note %}}
It is not necessary to
use [CSE page extensions](/server/framework/clientside-extensions/reference/api/extension-factories/page/) to create an
extension point. This guide uses them to provide a clean environment for working with extension point APIs.
{{% /note %}}

All the exercises are going to be developed inside this directory:

-   `src/main/my-app/extensions/extension-points-tutorial/`

There you will find:

-   `./extension-points-page.jsx`: the blank page where you are going to create your extension point and render the
    extensions
-   `./extensions/`: a set of extensions that you're going fetch and render throughout the guides.

Once you install and start Bitbucket Server from the template, you can head to the tutorial page and should see the
title `extension.points.tutorial`

-   http://localhost:7990/bitbucket/plugins/servlet/extension-points

## You're ready!

You're now ready
to [create your first extension point](/server/framework/clientside-extensions/guides/extension-points/creating-an-extension-point/)!
