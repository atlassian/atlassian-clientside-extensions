---
title: null
platform: server
product: clientside-extensions
category: devguide
subcategory: how-to
date: '2024-09-24'
---

# Debugging and troubleshooting an extension

Debugging an extension point can help you find out why your extension is not working as it should. This page describes how to debug a Client-Side Extension, including:

-   How to identify if the extension was loaded on the page
-   Why the extension didn't show up on the page

This guide assumes you are an extension developer (plugin developer), and you [created an extension](/server/framework/clientside-extensions/guides/introduction/creating-an-extension/) that doesn't work for some unknown reason.

This guide requires basic knowledge about the Atlassian plugin system and some experience with debugging HTML applications by using the browser DevTools. To troubleshoot an extension, you should know:

-   How to turn on [browser DevTools](https://developer.chrome.com/docs/devtools/) and how to use the console panel.
-   How to install browser extensions.
-   How to use [`altas` CLI commands](/server/framework/atlassian-sdk/atlas-cli/).

You should also know:

-   Your Atlassian **plugin key**, that can be found in the `atlassian-plugin.xml` file.

## How to check whether the extension loaded on the page

Before you start troubleshooting the extension code, first, you should first ensure that the Atlassian plugin system loaded your plugin and that your extension was loaded on the page.

### Check if your plugin was loaded

1. First, you need to start the application, open the web browser, and navigate to the administration panel of your application.
1. If you are using a local instance of Bitbucket or [Bitbucket CSE Template](https://bitbucket.org/atlassianlabs/bitbucket-client-side-extensions-template) project to develop the plugin, then use `admin` as the user and `admin` as the password to log in into the administration panel.
1. Next, find and navigate to the **Manage Apps** page.
1. On the list of all installed plugins, locate your plugin using **App key** (**plugin key**), and check whether all the plugin's modules are enabled:

    ![Manage Apps](/server/framework/clientside-extensions/guides/how-to/debugging-and-troubleshooting-an-extension/plugin-troubleshooting.png)

1. If your plugin is on the list and all the modules are enabled, you can proceed to the next step.

{{% tip %}}

If you haven't found the plugin on the list, you should try checking and inspecting the application log files. Depending on the application you are running the logs might be located in a different locations:

-   [Logging and profiling Jira](https://confluence.atlassian.com/adminjiraserver/logging-and-profiling-938847671.html)
-   [Working with Confluence Logs](https://confluence.atlassian.com/doc/working-with-confluence-logs-108364721.html)
-   [Configure Bitbucket Server Logging](https://confluence.atlassian.com/bitbucketserverkb/configure-bitbucket-server-logging-779171678.html)

{{% /tip %}}

### Check if the extension was loaded

Now that you know your plugin was loaded, you can check if the extension was loaded on the page.

1.  Install the **[Atlassian Browser DevTools Chrome extension](https://chrome.google.com/webstore/detail/atlassian-server-devtools/ifdlelpkchpppgmhmidenfmlhdafbnke)**. Open a web browser, turn on the DevTools and select the **ABD** tab.
2.  Navigate to the page where you should see the extension point.
3.  If the extension point is located in a different tab, e.g. Diff tab, select this tab on the page.
4.  Now, in the DevTools window, select the **ABD** tab and choose the **WRM Inspector** option.
5.  The **WRM inspector** allows you to see a list of all the web-resources loaded on the page. To filter the list and find a web-resource with your extension code:

    -   Click on the **All context** option and select the name of the extension point
    -   In the **Filter** field, type in part of your plugin name

    ![Filtering and finding web-resource with ABD](/server/framework/clientside-extensions/guides/how-to/debugging-and-troubleshooting-an-extension/abd-finding-web-resource.jpg)

6.  If you can find your web-resource on the list, it means that your extension was configured correctly, and it can be loaded on the page.

{{% warning %}}

If you can't find the **web-resource** with your extension, please check if the configuration of the webpack CSE plugin is correct. For more information refer to the guides:

-   [Setting up the CSE webpack plugin](/server/framework/clientside-extensions/guides/how-to/setup-webpack-plugin)
-   [Creating an Extension](/server/framework/clientside-extensions/guides/introduction/creating-an-extension)

{{% /warning %}}

## What to do when the extension did load but is not rendered on the page?

CSE includes a debug mode that is disabled by default. You can enable the debug mode programmatically using the APIs from the `@atlassian/clientside-extensions-debug` package.

To enable the runtime debug mode in your extension, follow these steps.

1. Install the `@atlassian/clientside-extensions-debug` package:

    ```
    yarn add -D @atlassian/clientside-extensions-debug

    # or

    npm install --save-dev @atlassian/clientside-extensions-debug
    ```

2. Next, import the debug module in your code and use debug functions:

    ```js
    // my-extension.js
    import { ModalExtension } from '@atlassian/clientside-extensions';
    // 1. Add package
    import { setDebugEnabled, setLoggingEnabled } from '@atlassian/clientside-extensions-debug';

    /**
     * @clientside-extension
     * @extension-point bitbucket.ui.pullrequest.overview.summary
     */
    export default ModalExtension.factory((extensionAPI, context) => {
        // 2. Turn on the debug mode and enable logging
        setDebugEnabled(true);
        setLoggingEnabled(true);

        return {
            label: 'Click to open a modal',
            onAction: (modalAPI) => {
                modalAPI.setTitle('Look, a Modal!');
                modalAPI.onMount((container) => {
                    container.innerHTML = '<p>Hello World!</p>';
                });
            },
        };
    });
    ```

3. If you are using webpack with watch mode, you should see the page reload after saving the changes.
   If not, use the [`atlas-package -DskipTests`](/server/framework/atlassian-sdk/atlas-package) command to rebuild your plugin and once it's done, manually refresh the page.
4. Open the browser's developer tools and select the **Console** tab. In the same browser window, navigate to the page that should render your extension.
5. If your extension is causing any problems you should see relevant information in the console tab, .e.g.

    ![Example of validation error](/server/framework/clientside-extensions/guides/how-to/debugging-and-troubleshooting-an-extension/validation-error.png)

6. Taking a look at the example image with the error:

    ```
    Schema validation issue with extension "<<extension name>>":
    "Schema" failed to validate. Attribute "`iconBefore`" of type `IconType`! is missing in "Schema".

    Meta information:

    - location: `bitbucket.ui.pullrequest.overview.summary`
    - extension: <<extension name>>
    ```

    The example validation error is showing information on what attributes are missing for the `bitbucket.ui.pullrequest.overview.summary` extension point. In this case, we need to provide the `iconBefore` attribute that is required.

{{% tip %}}

The above error is caused by missing required attributes.

To catch mistakes like this sooner in development, you can turn on the [discovery mode](/server/framework/clientside-extensions/guides/introduction/discovering-extension-points/#revealing-extension-points-on-the-page) which will reveal location of extension points, and the list of required attributes for a given extension point.

{{% /tip %}}

{{% warning %}}

Remember to include the `*-debug` package and turning the debug mode only in the **development** environment.

The extension code shouldn't include the, e.g. `setDebugEnabled(true)` calls when you create a **production** version of the plugin.

{{% /warning %}}

## Further help

If you followed the guide but are still not sure why the extension doesn't work, you can [report the issue in the Client-side Extensions project](https://ecosystem.atlassian.net/browse/CSE).

Click on the "Create" issue button and provide the reproduction steps. Please include as much information as you can, including:

-   Application name and version
-   A version of CSE modules you are using
-   Browser vendor name and version
