---
title: About Client-side Extensions
platform: server
product: clientside-extensions
category: devguide
subcategory: index
date: '2024-09-24'
---

# About Client-side Extensions

Client-side Extensions (CSE) is a set of APIs that allow a plugin author to augment and extend an Atlassian product's runtime using
client-side JavaScript code.

{{% note %}}
Client-side Extensions is a recent addition to Server Platform, so the availability of new extension points is still limited to the following products:

-   Bitbucket Server 7.0 - Pull Request experience

We're working hard with other products to provide CSE capabilities in the near future.
{{% /note %}}

## Creating extensions

If you're new to CSE, we recommend going through our [introduction guides](/server/framework/clientside-extensions/guides/introduction/).

You will be creating a set of extensions for the new Bitbucket PR experience, while learning about all the
capabilities that CSE brings to front-end developers creating Atlassian Plugins.

### Setup

In case you have an existing project, you can follow our guides on how to [setup CSE webpack plugin](/server/framework/clientside-extensions/guides/how-to/setup-webpack-plugin/)
and browse our [API Reference](/server/framework/clientside-extensions/reference/api/extension-factories) for a quick
look of all you can do with CSE.

### Not using webpack

If you're not using webpack, you can still make use of CSE in any product that supports it.
For examples on how to do so, explore [our demos](https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/environments/refapp/src/main/resources/).

## Creating extension points

Client-side Extensions (CSE) also provides a set of components and APIs that you can use to create your own extension points,
and make your UI extendable by others using the same APIs that they use to extend Atlassian products.

Refer to the [creating extension points](/server/framework/clientside-extensions/guides/extension-points/) guides to learn how to use them in your project.

### Setup

If you want to create extension points on your own project, follow the guide on
[how to setup CSE schema-loader](/server/framework/clientside-extensions/guides/how-to/setup-schema-loader/) before you get started.
