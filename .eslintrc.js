const path = require('path');

const supportedExtensions = ['.ts', '.tsx', '.js', '.jsx'];

module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            modules: true,
            jsx: true,
        },

        tsconfigRootDir: path.resolve(__dirname),
        project: './tsconfig.lint.json',
    },
    plugins: ['@typescript-eslint', 'jest', 'jsx-a11y', 'react', 'react-hooks', 'import', 'sonarjs'],
    extends: [
        'eslint:recommended',
        'airbnb-typescript',
        'plugin:jest/recommended',
        'plugin:jsx-a11y/recommended',
        'plugin:react/recommended',
        'plugin:react-hooks/recommended',
        'plugin:import/recommended',
        'plugin:import/react',
        'plugin:import/warnings',
        'plugin:import/errors',
        'plugin:import/typescript',
        'plugin:node/recommended',
        'plugin:sonarjs/recommended',

        // The prettier preset needs to be a last one. We don't use prettier plugin with eslint but we need to use the
        // preset so we can turn off all the colliding eslint rules.
        // Check https://github.com/prettier/eslint-config-prettier#eslint-config-prettier
        'prettier',
    ],

    settings: {
        'import/parsers': {
            '@typescript-eslint/parser': supportedExtensions,
        },

        'import/resolver': {
            typescript: {
                project: [
                    path.resolve(__dirname, './atlassian-clientside-*/*/tsconfig.json'),
                    path.resolve(__dirname, './packages/*/tsconfig.json'),
                    path.resolve(__dirname, './demos/*/tsconfig.json'),
                    path.resolve(__dirname, './tsconfig.lint.json'),
                    path.resolve(__dirname, './scripts/tsconfig.json'),
                ],

                alwaysTryTypes: true,
            },

            node: {
                extensions: [...supportedExtensions],
            },
        },

        node: {
            tryExtensions: [...supportedExtensions, '.json'],
        },

        react: {
            version: 'detect',
        },
    },

    env: {
        browser: true,
        node: true,
        es6: true,
        'jest/globals': true,
    },

    overrides: [
        // Cypress integration tests
        {
            files: ['**/integration/**/*.spec.ts'],
            extends: ['plugin:cypress/recommended'],
            rules: {
                'jest/expect-expect': 'off',
                'jest/valid-expect': 'off',
                'jest/valid-expect-in-promise': 'off',
                // Having duplicated strings and callbacks are very common in tests (selectors, expected output, etc...)
                'sonarjs/no-duplicate-string': 'off',
                'sonarjs/no-identical-functions': 'off',
            },
        },

        // Tests and mocks
        {
            files: ['**/*.test.js', '**/*.test.jsx', '**/*.test.ts', '**/*.test.tsx', '**/__mocks__/**'],
            rules: {
                'node/no-unpublished-import': 'off',
                // Enable importing devDependencies within the testing fixtures
                'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
                // Having duplicated strings and callbacks are very common in tests (selectors, expected output, etc...)
                'sonarjs/no-duplicate-string': 'off',
                'sonarjs/no-identical-functions': 'off',

                // We don't care about default props in testing components
                'react/require-default-props': 'off',
            },
        },

        // executable in schema
        {
            files: ['packages/schema/lib/bin/*.ts'],
            rules: {
                'node/shebang': 'off',
                'node/no-unpublished-import': 'off',
                'node/no-extraneous-import': 'off',
                'no-console': 'off',
            },
        },

        // executable in scripts
        {
            files: ['scripts/*-cli.ts'],
            rules: {
                'node/shebang': 'off',
            },
        },
    ],

    rules: {
        // We disable those import rules since TS provide the same checks
        // https://github.com/typescript-eslint/typescript-eslint/blob/master/docs/getting-started/linting/FAQ.md#eslint-plugin-import
        'import/named': 'off',
        'import/namespace': 'off',
        'import/default': 'off',
        'import/no-named-as-default-member': 'off',

        // This rules are super slow
        'no-implied-eval': 'off',
        '@typescript-eslint/no-implied-eval': 'off',

        // There is no enforcement on the arrow body that matches our need
        'arrow-body-style': 'off',
        // Though preferred - we want to be able to use normal functions sometimes to use the function
        // name for clarification of the purpose of the function. We therefore need to disable the setting here.
        'prefer-arrow-callback': 'off',
        'no-useless-constructor': 'off',
        '@typescript-eslint/no-useless-constructor': 'error',
        '@typescript-eslint/no-explicit-any': 'error',
        '@typescript-eslint/consistent-type-imports': [
            'error',
            {
                prefer: 'type-imports',
                disallowTypeAnnotations: true,
            },
        ],

        '@typescript-eslint/ban-ts-comment': [
            'error',
            {
                'ts-expect-error': 'allow-with-description',
                'ts-ignore': true,
            },
        ],

        'no-dupe-class-members': 'warn',
        'no-shadow': 'off',
        'class-methods-use-this': 'off',
        'no-console': [
            'warn',
            {
                allow: ['warn', 'error'],
            },
        ],
        'no-param-reassign': 'off',
        'react/prop-types': 'off',
        'no-plusplus': 'off',

        // Disable the rule to silent down the complains about using for...of syntax
        // https://github.com/airbnb/javascript/issues/1271#issuecomment-548688952
        'no-restricted-syntax': 'off',

        // Node ESLint
        'node/no-unsupported-features/es-syntax': [
            'error',
            {
                ignores: [
                    // Modules are supported by TypeScript
                    'modules',

                    // Dynamic imports are supported by TypeScript and Webpack
                    'dynamicImport',
                ],
            },
        ],

        // There are to many issues with devDependencies
        'node/no-unpublished-require': 'off',

        // TypeScript support
        'node/no-missing-import': [
            'error',
            {
                // https://github.com/mysticatea/eslint-plugin-node/issues/236#issue-655234174
                tryExtensions: [...supportedExtensions, '.d.ts'],
            },
        ],

        // We do like exit
        'no-process-exit': 'off',
        'node/no-process-exit': 'off',

        // Sonar
        'sonarjs/prefer-immediate-return': 'off', // We use temporary variables before returning in some places when debugging code with async/await
        'sonarjs/no-nested-template-literals': 'warn', // We don't mind nested template literals

        // Wrapping a test with a promise makes it only more complex
        'jest/no-done-callback': 'off',
    },
    globals: {
        location: true,
    },
};
