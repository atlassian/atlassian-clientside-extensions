# Contributing to Client-side Extensions

## Repository structure

This repository is a "monorepo".
It houses multiple artifacts for making the Client-side Extensions APIs work.
It contains both Maven and Node packages.
Each have their own workflow for building and releasing independently.

## Prerequisites

You will need the following tools in your development environment:

-   Maven 3.6.3
-   Java 8+

If you are working on the front-end code and not managing your workflow via Maven,
you will need additional tools in your development environment:

-   Node 12.19.0+ (you can use `nvm`)
-   Yarn 1.7+

## Getting started

In the root folder, run `mvn clean install`. This will build all the projects and packages in the monorepo.

The following commands are available to you:

-   `mvn clean install` - Cleans, Installs and Builds the Demo, Runtime and Packages using Maven.
-   `yarn start:refapp` - Runs the Refapp demo project.
-   `yarn watch` - Provides watchmode and hot reload to Packages/Demo while running the Refapp.
-   `yarn lint` - Runs all code rules and reports violations.
-   `yarn lint --fix` - Runs all code rules and fixes any violations that can be fixed programmatically.
-   `yarn test` - Runs tests for all packages
-   `yarn test:watch` - Runs tests for all packages in watch mode

## Code rules

All commits will be checked for conformance to the code rules.

### Frontend code

All frontend code in this repository conforms to the [Server Frontend Platform team's code rules (pending)](https://hello.atlassian.net/wiki/spaces/TEC/pages/540067361/Code+formatting).

-   Code style is enforced through Prettier.
-   Code quality is enforced through ESLint.

### Backend code

All backend code must conform to [The Server Backend Platform team's code rules (pending)](https://hello.atlassian.net/wiki/spaces/AB/pages/331784335/Forming+the+team).

## Test configuration

### base

We group al common Jest configuration in `jest.config.base.js`. If you need to modify the configuration of ALL packages
(including root config), please apply you changes in this file

### root config

We maintain a root Jest configuration that make use of jest projects to run all package tests in a single run
and generate a single `coverage` report. Try to avoid adding something here.

### packages config

Each package extends `base` configuration, and are free to add anything extra if they need to (ex: envs). Be wise
on editing these configs, and limit it to only modify per package use case. Otherwise, modify the `base` if the configuration
can be shared across packages.

## Testing local version in a Product

If you need to test your local changes in a product, you'll need to build the artifacts/packages you're editing from their sources, and add them to the product as follow:

### atlassian-clientside-extensions-runtime

1. Clone this repository from `master`
2. Compile Locally

    ```
    mvn clean install
    ```

3. Update your product to consume your local version of the runtime plugin. Or, you can also upload the `.jar` of your local version to the product via “Manage Apps”.

### @atlassian/clientside-extensions-\* packages

1.  Clone this repository from `master`
2.  Build Locally

    ```
    yarn build
    ```

3.  Link your local version of the library using either npm or yarn

    ```
    cd packages/* && yarn link
    ```

4.  Link your local version of the lib into your project using npm or yarn

    ```
    yarn link @atlassian/clientside-extensions-*
    ```

    This will be as if you were installing the package from npm, but you’re just creating a link in your project to go look for that package locally instead.

### Running integration tests locally with Cypress

1.  Clone this repository from `master`
2.  Install the dependencies

    ```sh
    yarn
    ```

3.  Start the `refapp`:

    ```sh
    yarn start:refapp
    ```

4.  Wait for `refapp` to start until you see the message:

    ```
    refapp started successfully in 32s at http://localhost:5990/refapp
    ```

5.  In a new terminal window start cypress tests:

    ```sh
    yarn test:refapp
    ```

    This will run Cypress test runner in a headless mode and run tests.

### Starting Cypress test runner

If you want to manually run integration tests with Cypress, instead of running `yarn test:refapp` in the previous step run:
`yarn test:refapp:watch`

This will open a Cypress test runner app where you can manually run each test. [Read more about the Cypress test runner](https://docs.cypress.io/guides/core-concepts/test-runner.html)

## Debugging refapp

### Connecting JVM debugger

You can run the **refapp** in debug mode by using `yarn debug:refapp` command. Once the refapp is started, you can connect the [Remote JVM Debugger](https://www.jetbrains.com/help/idea/tutorial-remote-debug.html#95db161b) from IntelliJ IDEA.

### Logs

While the **refapp** is running you can watch the product logs by using the `tail` command:

```shell
tail -f ./environments/refapp/target/refapp.log
```

The logs are generated when refapp is running in both debug and non-debug modes.

## Documentation

Documentation is written using [DAC writing toolkit](https://developer.atlassian.com/platform/writing-toolkit).

Make sure to got through the [Getting Started](https://developer.atlassian.com/platform/writing-toolkit/getting-started/) guide to
gain access to the right package and docker images to work with the writing toolkit.

Refer to [documentation](/documentation) package for details about all the commands you can use while developing with the writing toolkit.

### Keep documentation up to date

If you're adding a new API, or changing an existing one, make sure to add those changes to the [documentation](./documentation).
After releasing a new version of the packages, also release a new version of the documentation if needed.

We're releasing the documentation to [DAC][dac], and all we need to do is release a new `@atlassian/clientside-extensions-docs` package to NPM, and it will be picked up the next day.

## Pipelines

Each push to a branch triggers a Bitbucket Pipeline build to check if your code builds successfully.
Our builds rely the Server Frontend Platform (SFP) [image](https://bitbucket.org/atlassian/sfp/src/master/).

## Releasing

Refer to [releasing](./RELEASING.md) for release and bug fixing process.

[dac]: https://developer.atlassian.com/server/framework/clientside-extensions/ 'Atlassian Developer'
