import { ModalExtension } from '@atlassian/clientside-extensions';
import { renderElementAsReact } from '@atlassian/clientside-extensions-components';
import React, { useState } from 'react';

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location
 */
export default ModalExtension.factory((api) => {
    let callCount = 0;
    return {
        label: `Open Modal: ${callCount}`,
        onAction(modalApi) {
            modalApi.setTitle('An awesome Modal').setWidth(ModalExtension.Width.large).setAppearance(ModalExtension.Appearance.danger);

            const ReactComponent = () => {
                const [requestedUpdate, setRequestedUpdate] = useState(false);
                const [primaryDisabled, setPrimaryDisabled] = useState(false);

                modalApi.onClose(() => {
                    return window.confirm('Are you sure you want to close me? ☹️');
                });

                modalApi.setActions([
                    {
                        text: 'close',
                        onClick() {
                            modalApi.closeModal();
                        },
                        isDisabled: primaryDisabled,
                    },
                    {
                        text: 'request update',
                        onClick() {
                            callCount++;
                            setRequestedUpdate(true);
                            api.updateAttributes({
                                label: `Open Modal: ${callCount}`,
                            });
                        },
                    },
                    {
                        text: 'toggle primary button',
                        onClick() {
                            setPrimaryDisabled(!primaryDisabled);
                        },
                    },
                ]);

                return (
                    <div data-testid="modal-with-action-callback">
                        <h3>Just a Modal</h3>
                        <p>Think twice before closing me!</p>
                        {requestedUpdate ? <strong>requested update.</strong> : null}
                    </div>
                );
            };
            renderElementAsReact(modalApi, ReactComponent);
        },
    };
});
