import { ButtonExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location
 */
export default ButtonExtension.factory((api) => {
    const slowMathFunction = (baseNumber: number) => {
        const time: number = performance.now();
        const calculationArray = [];
        for (let i = baseNumber ** 7; i >= 0; i -= 1) {
            calculationArray.push(Math.atan(i) * Math.tan(i));
        }
        return [performance.now() - time, calculationArray.length];
    };

    const getLabel = () => {
        const [duration, calculations] = slowMathFunction(Math.floor(Math.random() * 10));
        return `Button extension. Click me to do some expensive math. Last result did ${calculations} and took ${duration.toFixed(2)}ms`;
    };

    return {
        label: getLabel(),
        onAction() {
            api.updateAttributes({
                label: getLabel(),
            });
        },
    };
});
