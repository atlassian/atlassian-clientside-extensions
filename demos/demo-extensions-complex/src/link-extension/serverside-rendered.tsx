import { LinkExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 * @label "A link rendered in the server"
 * @link "www.atlassian.com?q=$key"
 */
// @ts-expect-error We want to test the server-side rendered extension
export default LinkExtension.factory(() => {
    return {};
});
