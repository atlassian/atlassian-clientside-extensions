import { LinkExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location
 */
let count = 0;
export default LinkExtension.factory((api) => {
    count += 1;
    const localCount = count;
    let localRenderCount = 0;

    // this is a litmus test to make sure this will never get initialized more than once
    const getLabel = () =>
        `If this number is ever not 1: "${localCount}" something is broken (except if you just hot-reloaded a module).
(Render count: "${localRenderCount}". This should go up)`;
    const intervalToken = setInterval(() => {
        localRenderCount += 1;
        api.updateAttributes({ label: getLabel() });
    }, 5000);

    api.onCleanup(() => {
        clearInterval(intervalToken);
    });

    return {
        label: getLabel(),
        url: '#',
    };
});
