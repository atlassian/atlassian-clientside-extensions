import { PanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location.circular
 */
export default PanelExtension.factory(() => {
    return {
        label: `JS Panel`,
        comment: {
            id: Math.random(),
        },
        weight: 50,
        onAction(panelApi) {
            const content = document.createElement('div');

            panelApi
                .onMount((element) => {
                    content.innerHTML = `
                        <span>Simple Comment Panel</span>
                    `;

                    element.appendChild(content);
                })
                .onUnmount((element) => {
                    element.innerHTML = '';
                });
        },
    };
});
