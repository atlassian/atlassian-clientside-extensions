import { PanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location
 */
let callCount = 0;
export default PanelExtension.factory(() => {
    return {
        label: `JS Panel`,
        onAction(panelApi) {
            const content = document.createElement('div');

            panelApi
                .onMount((element) => {
                    callCount += 1;
                    content.innerHTML = `
                        <h4>Panel with Plain JS</h4>
                        <p data-testid="call-count">The current call count is ${callCount}</p>
                    `;

                    element.appendChild(content);
                })
                .onUnmount((element) => {
                    element.innerHTML = '';
                });
        },
    };
});
