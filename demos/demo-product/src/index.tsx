// eslint-disable-next-line import/no-unresolved, import/no-webpack-loader-syntax, node/no-missing-import
import 'wr-dependency!com.atlassian.auiplugin:aui-sidebar';
import type { FC } from 'react';
import React, { useLayoutEffect, useMemo } from 'react';
import { render } from 'react-dom';
import type { ExtensionDescriptor, Context } from '@atlassian/clientside-extensions-registry';
import { AsyncPanelHandler, SectionHandler } from '@atlassian/clientside-extensions-components';
import type { AsyncPanelRenderExtension } from '@atlassian/clientside-extensions-components';
import type { NavLinkProps } from 'react-router-dom';
import { HashRouter as Router, NavLink, Redirect, Route, Switch, withRouter } from 'react-router-dom';
import kebabCase from 'lodash.kebabcase';
import BasicPage from './BasicPage';
import { useExtensions } from './index.cse.graphql';

const getPathFromLabel = (label: string) => {
    return `/${kebabCase(label)}`;
};

interface NavSectionDescriptor {
    section?: ExtensionDescriptor;
    items: ExtensionDescriptor[];
}

interface NavSectionDescriptorCollection {
    [key: string]: NavSectionDescriptor;
}

const NavLinkWithRouter = withRouter((props) => {
    const { location } = props;
    const { to, activeClassName, className, children } = props as unknown as NavLinkProps;
    const isActive = location.pathname === to;
    return (
        <li className={isActive ? activeClassName : undefined}>
            <NavLink to={to} className={className}>
                {children}
            </NavLink>
        </li>
    );
}) as unknown as FC<NavLinkProps>;

const AuiSidebar: FC = ({ children }) => {
    useLayoutEffect(() => {
        // @ts-expect-error Fix AJS type
        AJS.sidebar('.aui-sidebar');
    }, []);

    return (
        <div className="aui-sidebar">
            <div className="aui-sidebar-wrapper">
                <div className="aui-sidebar-body">
                    <nav className="aui-navgroup aui-navgroup-vertical">
                        <div className="aui-navgroup-inner">{children}</div>
                    </nav>
                </div>

                <div className="aui-sidebar-footer">
                    <button
                        type="button"
                        className="aui-button aui-button-subtle aui-sidebar-toggle aui-sidebar-footer-tipsy"
                        title="Collapse sidebar ( [ )"
                    >
                        <span className="aui-icon aui-icon-small aui-iconfont-chevron-double-left" />
                    </button>
                </div>
            </div>
        </div>
    );
};

interface AuiSidebarGroupProps {
    heading: string;
}
const AuiSidebarGroup: FC<AuiSidebarGroupProps> = ({ heading, children }) => {
    return (
        <div className="aui-sidebar-group">
            <div className="aui-nav-heading" title={heading}>
                <strong>{heading}</strong>
            </div>

            <ul className="aui-nav" title={heading}>
                {children}
            </ul>
        </div>
    );
};

const MyApp = () => {
    const descriptors = useExtensions(null);
    const navigationItems = useMemo(() => descriptors.filter((descriptor) => descriptor.attributes.type === 'async-panel'), [descriptors]);
    const navigationSections = useMemo(() => descriptors.filter((descriptor) => descriptor.attributes.type === 'section'), [descriptors]);

    const extendedNavigationSections = useMemo(
        () =>
            Object.entries(
                navigationItems.reduce(
                    (agg, descriptor) => {
                        const section = descriptor.attributes.section as string;

                        if (!agg[section]) {
                            const navSection = navigationSections.find((navDescriptor) => navDescriptor.attributes.section === section);
                            if (!navSection) {
                                // no such nav section available - add to "ungrouped"
                                agg.ungrouped.items.push(descriptor);
                                return agg;
                            }

                            agg[section] = {
                                section: navSection,
                                items: [descriptor],
                            };
                        } else {
                            agg[section].items.push(descriptor);
                        }

                        return agg;
                    },
                    { ungrouped: { items: [] } } as NavSectionDescriptorCollection,
                ),
            ).sort(([, valA], [, valB]) => {
                const weightA = (valA.section?.attributes?.weight as number) ?? Infinity;
                const weightB = (valB.section?.attributes?.weight as number) ?? Infinity;
                return weightA - weightB;
            }),
        [navigationItems, navigationSections],
    );

    return (
        <Router>
            <AuiSidebar>
                {extendedNavigationSections.map(([sectionId, navigationSection]) => {
                    const isUngrouped = sectionId === 'ungrouped';
                    const sectionLabel = isUngrouped ? 'Other' : (navigationSection.section?.attributes.label as string);
                    if (isUngrouped && navigationSection.items.length === 0) {
                        return null;
                    }
                    return (
                        <AuiSidebarGroup key={sectionLabel} heading={sectionLabel}>
                            {navigationSection.items.map((descriptor) => {
                                const { key } = descriptor;
                                const { label } = descriptor.attributes;
                                return (
                                    <NavLinkWithRouter
                                        key={key}
                                        to={getPathFromLabel(label as string)}
                                        className="aui-nav-item"
                                        activeClassName="aui-nav-selected"
                                    >
                                        <SectionHandler>{label}</SectionHandler>
                                    </NavLinkWithRouter>
                                );
                            })}
                        </AuiSidebarGroup>
                    );
                })}
            </AuiSidebar>
            <BasicPage>
                <Switch>
                    {navigationItems.map((descriptor) => {
                        const { key } = descriptor;
                        const { label, contextProvider, onAction } = descriptor.attributes;

                        return (
                            <Route key={key} path={getPathFromLabel(label as string)}>
                                <AsyncPanelHandler
                                    pluginKey={key}
                                    location="demo-product.extend-navigation"
                                    contextProvider={contextProvider as () => Context<{}>}
                                    renderProvider={onAction as AsyncPanelRenderExtension}
                                    fallback={<div>Loading Page....</div>}
                                />
                            </Route>
                        );
                    })}
                </Switch>
                <Route exact path="/">
                    {/* this one is a bit dodgy */}
                    <Redirect to={getPathFromLabel('1. Basic demo')} />
                </Route>
            </BasicPage>
        </Router>
    );
};

/** Boot our demos! */
const container = document.querySelector('#content');
render(<MyApp />, container);
