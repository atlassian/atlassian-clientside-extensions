import registry from '@atlassian/clientside-extensions-registry';
import React, { useMemo } from 'react';
import { render } from 'react-dom';

import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import type { FunctionComponent } from 'react';

import { useExtensions } from './web-items-to-cse-example.cse.graphql';

type AppType = {
    legacyItems: ExtensionDescriptor[];
};

const App: FunctionComponent<AppType> = ({ legacyItems }) => {
    const legacyKeys = useMemo(() => legacyItems.map((item) => item.key), [legacyItems]);

    const supportedDescriptors = useExtensions(null);

    // Whatever is passed through as legacy items is "the source of truth".
    // Descriptors of the hook with the same key can be filtered out.
    const cleanedDescriptors = supportedDescriptors.filter((item) => !legacyKeys.includes(item.key));

    const weightedItems = [...cleanedDescriptors, ...legacyItems].sort((a, b) => a.attributes.weight! - b.attributes.weight!);

    return (
        <>
            {weightedItems.map((desc) => {
                return (
                    <li key={desc.key}>
                        <a href={desc.attributes.url as string} onClick={desc.attributes.onAction as React.MouseEventHandler}>
                            {desc.attributes.label}
                        </a>
                    </li>
                );
            })}
        </>
    );
};

// We actually dont care about the location here as we use graphql predefined location through our graphql schema.
registry.registerHandler('web-item-to-cse.dropdown', (root, location, legacyWebItems) => {
    const standardizedWebItems = legacyWebItems.map(
        (webItem) =>
            ({
                location,
                key: (webItem as { completeKey: string }).completeKey,
                attributes: {
                    ...webItem,
                },
            } as ExtensionDescriptor),
    );

    render(<App legacyItems={standardizedWebItems} />, root);
});
