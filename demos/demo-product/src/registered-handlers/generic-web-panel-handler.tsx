import registry from '@atlassian/clientside-extensions-registry';
import React, { useMemo } from 'react';
import { render } from 'react-dom';
import { PanelHandler, useWebPanelRenderer } from '@atlassian/clientside-extensions-components';

import type { ExtensionDescriptor, ParsedWebPanelDescriptor } from '@atlassian/clientside-extensions-registry';
import type { FunctionComponent } from 'react';
import type { PanelExtension } from '@atlassian/clientside-extensions';

import { useExtensions } from './web-panel-to-cse-example.cse.graphql';

type AppType = {
    webPanels: ParsedWebPanelDescriptor[];
};

function isWebPanel(descriptor: ExtensionDescriptor | ParsedWebPanelDescriptor): descriptor is ParsedWebPanelDescriptor {
    return (descriptor as ExtensionDescriptor).attributes === undefined;
}

function getWeight(descriptor: ExtensionDescriptor | ParsedWebPanelDescriptor): number {
    if (isWebPanel(descriptor)) {
        return descriptor.weight || 0;
    }

    return descriptor.attributes.weight || 0;
}

const WebPanel = ({ html }: { html: string }) => {
    const ref = useWebPanelRenderer(html);
    return <div ref={ref} />;
};

const App: FunctionComponent<AppType> = ({ webPanels }) => {
    const webpanelKeys = useMemo(() => webPanels.map((item) => item.completeKey), [webPanels]);
    const supportedDescriptors = useExtensions(null);

    // Whatever is passed through as legacy items is "the source of truth".
    // Descriptors of the hook with the same key can be filtered out.
    const cleanedDescriptors = supportedDescriptors.filter((item) => !webpanelKeys.includes(item.key));

    const weightedItems = [...cleanedDescriptors, ...webPanels].sort((a, b) => getWeight(a) - getWeight(b));

    return (
        <>
            {weightedItems.map((desc) => {
                if (isWebPanel(desc)) {
                    return <WebPanel key={desc.completeKey} html={desc.html} />;
                }
                return <PanelHandler key={desc.key} render={desc.attributes.onAction as PanelExtension.PanelRenderExtension} />;
            })}
        </>
    );
};

// We actually dont care about the location here as we use graphql predefined location through our graphql schema.
registry.registerHandler('web-panel-to-cse.generic', (root, location, legacyPanels) => {
    render(<App webPanels={legacyPanels as ParsedWebPanelDescriptor[]} />, root);
});
