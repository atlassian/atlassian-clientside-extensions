import registry from '@atlassian/clientside-extensions-registry';
import React, { useMemo } from 'react';
import { render } from 'react-dom';
import { LinkHandler } from '@atlassian/clientside-extensions-components';

import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import type { FunctionComponent } from 'react';

import { useExtensions } from './web-items-to-cse-example.cse.graphql';

type AppType = {
    legacyItems: ExtensionDescriptor[];
};

const App: FunctionComponent<AppType> = ({ legacyItems }) => {
    const legacyKeys = useMemo(() => legacyItems.map((item) => item.key), [legacyItems]);

    const supportedDescriptors = useExtensions(null);

    // Whatever is passed through as legacy items is "the source of truth".
    // Descriptors of the hook with the same key can be filtered out.
    const cleanedDescriptors = supportedDescriptors.filter((item) => !legacyKeys.includes(item.key));

    const weightedItems = [...cleanedDescriptors, ...legacyItems].sort((a, b) => a.attributes.weight! - b.attributes.weight!);

    return (
        <>
            {weightedItems.map((desc) => {
                return (
                    <LinkHandler
                        key={desc.key}
                        href={desc.attributes.url as string}
                        onAction={desc.attributes.onAction as React.MouseEventHandler}
                    >
                        {desc.attributes.label}
                    </LinkHandler>
                );
            })}
        </>
    );
};

// We actually dont care about the location here as we use graphql predefined location through our graphql schema.
registry.registerHandler('web-item-to-cse.generic', (root, location, legacyItems) => {
    const standardizedWebItems = legacyItems.map(
        (item) =>
            ({
                location,
                key: (item as { completeKey: string }).completeKey,
                attributes: {
                    ...item,
                },
            } as ExtensionDescriptor),
    );

    render(<App legacyItems={standardizedWebItems} />, root);
});
