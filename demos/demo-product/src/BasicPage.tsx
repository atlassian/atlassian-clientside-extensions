import React from 'react';

const BasicPage: React.FC = ({ children }) => {
    return (
        <>
            <header className="aui-page-header">
                <div className="aui-page-header-inner">
                    <div className="aui-page-header-main">
                        <h1>Client-side Extensions</h1>
                    </div>
                </div>
            </header>

            <div className="aui-page-panel">
                <div className="aui-page-panel-inner">
                    <div className="aui-page-panel-content">{children}</div>
                </div>
            </div>
        </>
    );
};

export default BasicPage;
