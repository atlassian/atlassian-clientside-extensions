import { SectionExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point demo-product.extend-navigation
 */
export default SectionExtension.factory(() => {
    return {
        label: 'Debugging',
        section: 'debugging.section',
        weight: 2,
    };
});
