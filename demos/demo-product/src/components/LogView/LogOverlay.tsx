import React, { memo, useState } from 'react';
import styled from '@emotion/styled';
import { N200A, N700A, N10, red, yellow } from '@atlaskit/theme/colors';
import { LogLevel } from '@atlassian/clientside-extensions-debug';
import ArrowDownCircleIcon from '@atlaskit/icon/glyph/arrow-down-circle';
import ArrowRightCircleIcon from '@atlaskit/icon/glyph/arrow-right-circle';
import CrossCircleIcon from '@atlaskit/icon/glyph/cross-circle';
import type { LogPayload } from './observer';
import { useLogger } from './observer';

type PaneProps = { direction: string };
const Pane = styled.div`
    position: fixed;
    bottom: 0;
    ${(props: PaneProps) => (props.direction === 'row' ? 'right: 0;' : 'left: 0;')};
    height: ${(props: PaneProps) => (props.direction === 'row' ? '100vh' : '50vh')};
    width: ${(props: PaneProps) => (props.direction === 'row' ? '30vw' : '100vw')};
    z-index: 9999999999999;

    display: grid;
    grid-auto-columns: 1fr;
    grid-auto-rows: 1fr;
    grid-template-rows: minmax(0, 1fr);
    grid-auto-flow: ${(props: PaneProps) => props.direction};
    grid-gap: 1rem;
    padding: 1rem;
    box-sizing: border-box;

    background: ${N200A};
`;

const LogPane = styled.div`
    display: flex;
    flex-direction: column;
    overflow: hidden;
`;

const LogContent = styled.div`
    flex: 1;
    background: ${N700A};
    color: ${(props) => props.color};
    font-size: 10px;
    overflow: auto;
    display: flex;
    flex-direction: column-reverse;
`;

const LogTitle = styled.h3`
    color: ${N10};
`;

const LogEntryWrapper = styled.div`
    display: grid;
    grid-auto-columns: min-content 1fr;
    grid-auto-flow: column;
    grid-gap: 10px;

    padding: 5px;
    margin: 5px;
    box-sizing: border-box;
    overflow-anchor: none;

    background: rgba(250, 251, 252, 0.05);
`;

const LogEntryHeader = styled.h4`
    color: inherit;
    font-size: 12px;
    padding: 0 2px 2px;
`;

const LogTable = styled.table`
    border-collapse: collapse;
    width: 100%;
    table-layout: fixed;
    overflow-wrap: break-word;
    font-size: 7px;
    margin-top: 10px;

    tr {
        border-bottom: 1px solid;
    }
    tbody tr:last-child {
        border-bottom: none;
    }
    td,
    th {
        border-right: 1px solid;
        padding: 5px;
    }
    td:last-child,
    th:last-child {
        border-right: none;
    }
    td:first-of-type {
        font-style: italic;
    }
`;

const LogEntryTime = styled.div`
    white-space: nowrap;
    font-size: 7px;
`;

const LogScrollAnchor = styled.div`
    overflow-anchor: auto;
    min-height: 1px;
    flex: 1;
`;

const PositionIcon = styled.div`
    cursor: pointer;
    position: absolute;
    top: 10px;
    right: 10px;
`;

const CloseIcon = styled.div`
    cursor: pointer;
    position: absolute;
    top: 10px;
    right: 35px;
`;

const LogMessages: React.FunctionComponent<{ payloads: LogPayload[] }> = ({ payloads }) => {
    return (
        <>
            <LogScrollAnchor />
            {payloads.reverse().map((payload) => {
                return (
                    <LogEntryWrapper key={payload.uniqueKey}>
                        <LogEntryTime>[{payload.time}]</LogEntryTime>
                        <div>
                            {payload.component ? <LogEntryHeader>{payload.component}</LogEntryHeader> : null}
                            <p>{payload.message}</p>
                            <LogTable>
                                <thead>
                                    <tr>
                                        <th>Key</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {Object.entries(payload.meta).map(([key, val]) => {
                                        return (
                                            <tr key={key}>
                                                <td>{key}</td>
                                                <td>{val}</td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </LogTable>
                        </div>
                    </LogEntryWrapper>
                );
            })}
        </>
    );
};

const LogMessagesMemoed = memo(LogMessages);

const LogOverlay: React.FunctionComponent<{ onClose: () => void }> = ({ onClose }) => {
    const [direction, changeDirection] = useState<string>('column');
    const errorLogs = useLogger([LogLevel.error]);
    const warnLogs = useLogger([LogLevel.warn]);
    const otherLogs = useLogger([LogLevel.debug, LogLevel.info]);

    return (
        <>
            <Pane direction={direction}>
                <PositionIcon
                    onClick={() => {
                        changeDirection((lastDirection) => (lastDirection === 'row' ? 'column' : 'row'));
                    }}
                >
                    {direction === 'row' ? (
                        <ArrowDownCircleIcon label="Move Log Overlay down" />
                    ) : (
                        <ArrowRightCircleIcon label="Move Log Overlay right" />
                    )}
                </PositionIcon>
                <CloseIcon onClick={onClose}>
                    <CrossCircleIcon label="Close Panel" />
                </CloseIcon>
                <LogPane>
                    <LogTitle>Errors</LogTitle>
                    <LogContent color={red()}>
                        <LogMessagesMemoed payloads={errorLogs} />
                    </LogContent>
                </LogPane>
                <LogPane>
                    <LogTitle>Warnings</LogTitle>
                    <LogContent color={yellow()}>
                        <LogMessages payloads={warnLogs} />
                    </LogContent>
                </LogPane>
                <LogPane>
                    <LogTitle>Others</LogTitle>
                    <LogContent color={N10}>
                        <LogMessages payloads={otherLogs} />
                    </LogContent>
                </LogPane>
            </Pane>
        </>
    );
};

export default LogOverlay;
