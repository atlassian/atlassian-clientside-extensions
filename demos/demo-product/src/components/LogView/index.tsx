import React, { useState } from 'react';
import LogOverlay from './LogOverlay';

const LogView: React.FunctionComponent = () => {
    const [showLog, changeShowLog] = useState<boolean>(false);

    return (
        <>
            <button type="button" onClick={() => changeShowLog((prev) => !prev)}>
                {showLog ? 'Hide Log Overlay' : 'Show Log Overlay'}
            </button>
            {showLog ? <LogOverlay onClose={() => changeShowLog(false)} /> : null}
        </>
    );
};

export default LogView;
