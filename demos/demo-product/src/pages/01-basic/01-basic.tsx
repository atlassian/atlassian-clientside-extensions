import React from 'react';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { renderElementAsReact } from '@atlassian/clientside-extensions-components';
import { useExtensions } from './basic.cse.graphql';

function asButtons(extension: ExtensionDescriptor) {
    return (
        <button key={extension.key} type="button">
            {extension.attributes.label || 'Default button text'}
        </button>
    );
}

const BasicDemo = () => {
    const extensionDescriptors = useExtensions(null);

    return (
        <>
            <h1>Basic demo</h1>
            <p>We create a simple extension point, then render everything registered in it as a button.</p>

            {extensionDescriptors.map(asButtons)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, BasicDemo);
};
