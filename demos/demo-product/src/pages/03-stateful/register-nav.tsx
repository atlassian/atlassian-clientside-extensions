import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: '3. Stateful',
        section: 'basic.section',
        onAction() {
            return import(/* webpackChunkName: "demo-stateful" */ './03-stateful');
        },
    };
});
