import React, { useEffect, useState } from 'react';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { renderElementAsReact } from '@atlassian/clientside-extensions-components';
import type { StatefulExtensionContext } from './types';
import { useExtensions } from './stateful.cse.graphql';

const asButtons = (extension: ExtensionDescriptor) => {
    const { label, onAction } = extension.attributes;
    return (
        <button key={extension.key} type="button" onClick={onAction as () => {}}>
            {label}
        </button>
    );
};

const baseContext: StatefulExtensionContext = {
    foo: 'bar',
    randomNumber: 7,
};

const StatefulDemo = () => {
    const [extensionContext, setExtensionContext] = useState(baseContext);
    const extensionDescriptors = useExtensions(extensionContext);

    useEffect(() => {
        const interval = setInterval(() => {
            const randomNumber = Math.round(Math.random() * 1000);
            setExtensionContext((prevExtensionData) => ({ ...prevExtensionData, randomNumber }));
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    return (
        <>
            <h1>Stateful extensions</h1>
            <p>Extensions can react to context provided via the plugin point. They can also maintain their own state.</p>

            {extensionDescriptors.map(asButtons)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, StatefulDemo);
};
