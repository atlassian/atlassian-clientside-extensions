import type { FC } from 'react';
import React, { useMemo } from 'react';

import { AsyncPanelHandler, renderElementAsReact } from '@atlassian/clientside-extensions-components';
import type { AsyncPanelRenderExtension } from '@atlassian/clientside-extensions-components';
import Tabs, { Tab, TabList, TabPanel } from '@atlaskit/tabs';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { useExtensions } from './async-tabs.cse.graphql';

const greetings = ['Why, hello there!', 'Sure is nice to see ya!', `You're always welcome here!`, 'Howdy, partner!', `Hey hey hey!`, 'Yo.'];

const AsyncTabsDemo: FC = () => {
    const context = useMemo(
        () => ({
            greetings,
        }),
        [],
    );

    const descriptors = useExtensions(context);

    // Tabs
    const tabsLabels = useMemo(
        () =>
            descriptors.map((extension) => {
                return <Tab key={extension.key}>{extension.attributes.label}</Tab>;
            }),
        [descriptors],
    );

    const tabPanels = useMemo(
        () =>
            descriptors.map((extension) => {
                const { onAction, contextProvider } = extension.attributes;

                return (
                    <TabPanel key={extension.key}>
                        <AsyncPanelHandler
                            location="demo.06-tabs"
                            pluginKey={extension.key}
                            renderProvider={onAction as AsyncPanelRenderExtension}
                            contextProvider={contextProvider}
                            fallback={<div>IM LOADING</div>}
                        />
                    </TabPanel>
                );
            }),
        [descriptors],
    );

    return (
        <section>
            <Tabs id="async-tabs-demo">
                <TabList>{tabsLabels}</TabList>
                {tabPanels}
            </Tabs>
        </section>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, AsyncTabsDemo);
};
