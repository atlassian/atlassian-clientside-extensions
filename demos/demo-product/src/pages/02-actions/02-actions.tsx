import React from 'react';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { renderElementAsReact } from '@atlassian/clientside-extensions-components';
import { useExtensions } from './actions.cse.graphql';

function asButtons(extension: ExtensionDescriptor) {
    const { label, onAction } = extension.attributes; // baz
    return (
        <button key={extension.key} type="button" onClick={onAction as () => {}}>
            {label}
        </button>
    );
}

const ActionsDemo = () => {
    const extensionDescriptors = useExtensions(null);

    return (
        <>
            <h1>Basic demo with actions</h1>
            <p>Extensions can provide whatever you let them in the schema, such as function callbacks.</p>

            {extensionDescriptors.map(asButtons)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, ActionsDemo);
};
