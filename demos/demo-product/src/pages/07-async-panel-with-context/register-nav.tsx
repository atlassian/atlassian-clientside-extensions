import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: '7. Async Panels with context',
        section: 'complex.examples.demo',
        onAction() {
            return import(/* webpackChunkName: "demo-async-tabs" */ './07-async-panel-with-context');
        },
    };
});
