import React, { useEffect, useMemo, useState } from 'react';
import type { FunctionComponent } from 'react';

import { AsyncPanelHandler, renderElementAsReact } from '@atlassian/clientside-extensions-components';
import type { AsyncPanelRenderExtension } from '@atlassian/clientside-extensions-components';
import Tabs, { Tab, TabList, TabPanel } from '@atlaskit/tabs';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { ExtensionPointInfo, useExtensions } from './async-tabs-with-context.cse.graphql';

const AsyncTabsDemo: FunctionComponent = () => {
    // Generate context
    const [counter, setCounter] = useState(0);

    useEffect(() => {
        const timeout = setInterval(() => {
            setCounter((currentCount) => currentCount + 1);
        }, 1000);

        return () => clearInterval(timeout);
    }, []);

    const context = useMemo(
        () => ({
            myCounter: counter,
            foo: {
                bar: 'baz',
            },
        }),
        [counter],
    );

    const descriptors = useExtensions(context);

    // Tabs
    const tabsLabels = useMemo(
        () =>
            descriptors.map((extension) => {
                return <Tab key={extension.key}>{extension.attributes.label}</Tab>;
            }),
        [descriptors],
    );

    const tabPanels = useMemo(
        () =>
            descriptors.map((extension) => {
                const { onAction, contextProvider } = extension.attributes;

                return (
                    <TabPanel key={extension.key}>
                        <AsyncPanelHandler
                            location="demo.07-async-panel-with-context"
                            pluginKey={extension.key}
                            renderProvider={onAction as AsyncPanelRenderExtension}
                            contextProvider={contextProvider}
                            fallback={<div>IM LOADING</div>}
                        />
                    </TabPanel>
                );
            }),
        [descriptors],
    );

    return (
        <section>
            <Tabs id="async-panel-with-context-demo">
                <TabList>{tabsLabels}</TabList>
                {tabPanels}
            </Tabs>
            <ExtensionPointInfo />
        </section>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, AsyncTabsDemo);
};
