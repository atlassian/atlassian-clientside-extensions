import React from 'react';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import { PanelHandler, renderElementAsReact } from '@atlassian/clientside-extensions-components';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import type { ExtensionPointContextDemo04 } from './types';
import { useExtensions } from './panels.cse.graphql';

const asPanels = (ext: ExtensionDescriptor) => {
    const { label, onAction } = ext.attributes;
    return (
        <React.Fragment key={ext.key}>
            <h3>{label}</h3>
            <PanelHandler render={onAction as PanelExtension.PanelRenderExtension} />
        </React.Fragment>
    );
};

const extensionContext: ExtensionPointContextDemo04 = {
    title: `Dragon's rest`,
};

const PanelsDemo = () => {
    const descriptors = useExtensions(extensionContext);

    return (
        <>
            <h1>Panel plugins</h1>
            <p>
                Product developers can allow plugin vendors to provide their own rendering logic via a <dfn>panel</dfn> plugin type.
            </p>

            {descriptors.map(asPanels)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, PanelsDemo);
};
