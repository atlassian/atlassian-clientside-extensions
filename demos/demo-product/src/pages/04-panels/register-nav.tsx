import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: '4. Panels',
        section: 'basic.section',
        onAction() {
            return import(/* webpackChunkName: "demo-panels" */ './04-panels');
        },
    };
});
