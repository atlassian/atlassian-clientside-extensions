// ensure discover buttons in forms dont submit the form
import React, { useCallback, useState } from 'react';
import type { FormEventHandler } from 'react';

import * as formExtension from './complex.form.cse.graphql';

const FormWithExtension = () => {
    const [submitState, setSubmitState] = useState('Form has NOT been submitted');
    const submitHandler = useCallback<FormEventHandler>((e) => {
        e.preventDefault();
        setSubmitState('Form has been submitted');
    }, []);
    return (
        <form className="form-with-extension" onSubmit={submitHandler}>
            <h2>Just a form</h2>
            <p className="submit-status">{submitState}</p>
            <p>This is a contrived example to verify discovery-points do not submit forms.</p>
            <button type="submit" className="submitter">
                I submit forms!
            </button>
            <div className="discover-bubble">
                <formExtension.ExtensionPointInfo />
            </div>
        </form>
    );
};

export default FormWithExtension;
