// ensure schemas with circular type references are correctly rendered

import React from 'react';
import { PanelHandler } from '@atlassian/clientside-extensions-components';
import type { PanelExtension } from '@atlassian/clientside-extensions';

import type { FunctionComponent } from 'react';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';

import * as circularExtension from './complex.circular.cse.graphql';

type Comment = {
    id: number;
    comments: Comment[];
};
const CommentRenderer: FunctionComponent<{ comment: Comment }> = ({ comment }) => {
    return (
        <>
            <div>id: {comment.id}</div>
            {comment.comments && (
                <div>
                    comments:
                    <ul>
                        {comment.comments.map((childComment) => (
                            <li key={childComment.id}>
                                <CommentRenderer comment={childComment} />
                            </li>
                        ))}
                    </ul>
                </div>
            )}
        </>
    );
};

const CircularCommentsRenderer = () => {
    const descriptors = circularExtension.useExtensions({});
    return (
        <>
            <h2>Circular comments</h2>
            <p>This is a contrived example to verify the circular validator works.</p>
            <circularExtension.ExtensionPointInfo />

            {descriptors.map((ext: ExtensionDescriptor) => {
                const { label, onAction, comment } = ext.attributes;
                return (
                    <React.Fragment key={ext.key}>
                        <h3>{label}</h3>
                        <PanelHandler render={onAction as PanelExtension.PanelRenderExtension} />
                        <p>This is rendered by the product</p>
                        <div>
                            <CommentRenderer comment={comment as Comment} />
                        </div>
                    </React.Fragment>
                );
            })}
        </>
    );
};

export default CircularCommentsRenderer;
