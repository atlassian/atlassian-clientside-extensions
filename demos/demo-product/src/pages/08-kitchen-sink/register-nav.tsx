import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: '8. Kitchen sink',
        section: 'complex.examples.demo',
        onAction() {
            return import(/* webpackChunkName: "demo-complex-page" */ './08-kitchen-sink');
        },
    };
});
