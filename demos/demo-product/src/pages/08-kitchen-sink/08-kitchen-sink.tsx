/* eslint-disable react/jsx-props-no-spreading */
import type { FunctionComponent } from 'react';
import React, { useState } from 'react';
import type { ButtonHandlerProps } from '@atlassian/clientside-extensions-components';
import {
    ButtonHandler,
    LinkHandler,
    ModalWithActionHandler,
    PanelHandler,
    renderElementAsReact,
} from '@atlassian/clientside-extensions-components';

import AKButton from '@atlaskit/button';
import type { ModalExtension, PanelExtension } from '@atlassian/clientside-extensions';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import * as complexExtension from './complex.cse.graphql';
import * as legacyItemExtension from './complex.old.cse.graphql';

import DebugSettings from '../../components/DebugSettings';
import LogView from '../../components/LogView';
import CircularCommentsRenderer from './CircularComment';
import FormWithExtensions from './FormWithExtensions';

const headerStyles = {
    marginTop: '15px',
};

const listStyles = {
    listStyle: 'none',
};

const listItemStyles = {
    margin: '12px 0',
    padding: '12px 8px',
};

// eslint-disable-next-line react/require-default-props
type ExtensionProps = { extension: ExtensionDescriptor; collapsible?: boolean };

const CollapsiblePanelExtensionRenderer: FunctionComponent<ExtensionProps> = ({ extension }) => {
    const [isActive, setIsActive] = useState(false);
    const { onAction, label, type: unusedType, ...renderableAttributes } = extension.attributes;

    return (
        <li key={extension.key} style={listItemStyles}>
            <AKButton type="button" {...renderableAttributes} onClick={() => setIsActive(!isActive)}>
                {label}
            </AKButton>

            {isActive && <PanelHandler render={onAction as PanelExtension.PanelRenderExtension} />}
        </li>
    );
};

const ExtensionRenderer = ({ type, extension, collapsible }: { type: string } & ExtensionProps) => {
    const { onAction, label, ...renderableAttributes } = extension.attributes;
    if (type === 'modal') {
        return (
            <ModalWithActionHandler render={onAction as ModalExtension.ModalRenderExtension} {...renderableAttributes}>
                {label}
            </ModalWithActionHandler>
        );
    }

    if (type === 'button') {
        return (
            <ButtonHandler {...renderableAttributes} onAction={onAction as Extract<ButtonHandlerProps, 'onAction'>}>
                {label}
            </ButtonHandler>
        );
    }

    if (type === 'link') {
        return (
            <LinkHandler
                onAction={onAction as Extract<ButtonHandlerProps, 'onAction'>}
                href={renderableAttributes.url as string}
                {...renderableAttributes}
            >
                {label}
            </LinkHandler>
        );
    }

    if (type === 'panel') {
        if (collapsible) {
            return <CollapsiblePanelExtensionRenderer extension={extension} />;
        }

        return <PanelHandler render={onAction as PanelExtension.PanelRenderExtension} />;
    }

    return null;
};

type ExtensionPointWithHookProps = { context: { value: number }; collapsible: boolean };

function ExtensionPointWithHook({ context, collapsible }: ExtensionPointWithHookProps) {
    const [extensionDescriptors, , isLoading] = complexExtension.useExtensionsAll(context);

    return (
        <>
            <h2 style={headerStyles}>
                {isLoading && <div>loading resources...</div>}
                Extension point provided by useExtensions hooks
                <complexExtension.ExtensionPointInfo />
            </h2>

            <ul style={listStyles}>
                {extensionDescriptors.map((extension: ExtensionDescriptor) => (
                    <li key={extension.key} style={listItemStyles}>
                        <ExtensionRenderer
                            key={extension.key}
                            type={extension.attributes.type as string}
                            extension={extension}
                            collapsible={collapsible}
                        />
                    </li>
                ))}
            </ul>
        </>
    );
}

const KitchenSink = () => {
    const [value, setValue] = useState(0);
    const [isExtensionPointHidden, setIsExtensionPointHidden] = useState(false);
    const [isUsingCollapsiblePanels, setUsingCollapsiblePanels] = useState(false);

    return (
        <>
            <section>
                <h2 style={headerStyles} data-testid="current-value">
                    Context value: {value}
                </h2>
                <AKButton type="button" onClick={() => setValue(value - 1)} testId="decrement-current-value">
                    -1
                </AKButton>
                <AKButton type="button" onClick={() => setValue(value + 1)} testId="increment-current-value">
                    +1
                </AKButton>
            </section>

            <section>
                <h2 style={headerStyles}>Show/Hide extension points (testing re-rendering)</h2>
                <AKButton type="button" onClick={() => setIsExtensionPointHidden(!isExtensionPointHidden)} testId="show-hide-location">
                    {isExtensionPointHidden ? 'Show' : 'Hide'}
                </AKButton>
            </section>

            <section>
                <h2 style={headerStyles}>Use collapsible panels</h2>
                <AKButton type="button" onClick={() => setUsingCollapsiblePanels(!isUsingCollapsiblePanels)}>
                    {isUsingCollapsiblePanels ? 'Yes' : 'No'}
                </AKButton>
            </section>

            <section>
                <h2 style={headerStyles}>Show logging overlay</h2>
                <LogView />
            </section>

            <section>
                <h2 style={headerStyles}>Debug utilities:</h2>
                <DebugSettings />
            </section>

            {isExtensionPointHidden ? null : (
                <>
                    <div data-testid="location-fragment">
                        {/* hook implementation */}
                        <ExtensionPointWithHook context={{ value }} collapsible={isUsingCollapsiblePanels} />

                        {/* component implementation */}
                        <h2 style={headerStyles}>
                            Extension point provided by ExtensionPoint component
                            <legacyItemExtension.ExtensionPointInfo />
                        </h2>
                        <legacyItemExtension.ExtensionPoint context={{ value }}>
                            {(extensionDescriptors: ExtensionDescriptor[], unsupportedExtensionDescriptors: ExtensionDescriptor[]) => (
                                <ul style={listStyles}>
                                    {extensionDescriptors.map((extension: ExtensionDescriptor) => (
                                        <li key={extension.key} style={listItemStyles}>
                                            <ExtensionRenderer
                                                key={extension.key}
                                                type={extension.attributes.type as string}
                                                extension={extension}
                                            />
                                        </li>
                                    ))}
                                    {unsupportedExtensionDescriptors.map((extension: ExtensionDescriptor) => (
                                        <ExtensionRenderer key={extension.key} type="link" extension={extension} />
                                    ))}
                                </ul>
                            )}
                        </legacyItemExtension.ExtensionPoint>

                        {/* Test purpose only for circular dependencies */}
                        <CircularCommentsRenderer />
                        {/* Test purpose only for discovery points in forms */}
                        <FormWithExtensions />
                    </div>
                </>
            )}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, KitchenSink);
};
