import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: '9. Custom logger',
        section: 'debugging.section',
        onAction() {
            return import(/* webpackChunkName: "demo-async-tabs" */ './09-debugging');
        },
    };
});
