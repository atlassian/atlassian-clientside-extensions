import React, { useEffect, useState } from 'react';
import Button from '@atlaskit/button';
import { unstable_batchedUpdates as batchedUpdates } from 'react-dom';
import type { FunctionComponent } from 'react';
import { renderElementAsReact } from '@atlassian/clientside-extensions-components';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { setLogger, consoleLogger } from '@atlassian/clientside-extensions-debug';
import type { LoggerPayload } from '@atlassian/clientside-extensions-debug';
import { useExtensions } from './broken-extension.cse.graphql';

interface BrokenContext {
    brokenValue: string;
}

interface DateTimeFormatOptions extends Intl.DateTimeFormatOptions {
    dateStyle?: 'full' | 'long' | 'medium' | 'short';
    timeStyle?: 'full' | 'long' | 'medium' | 'short';
}

const logReplacer = (key: unknown, value: unknown) => {
    if (!key || parseInt(key as string, 10).toString() === key) {
        return value;
    }

    // eslint-disable-next-line default-case
    switch (key as string) {
        case 'time':
            return new Intl.DateTimeFormat('en-gb', { dateStyle: 'short', timeStyle: 'medium' } as DateTimeFormatOptions).format(
                value as number,
            );
            break;
        case 'message':
            return value;
            break;
    }

    return undefined;
};

const CodeBlock: FunctionComponent = ({ children }) => (
    <pre
        style={{
            padding: 8,
            border: '1px solid #ddd',
            borderRadius: 4,
            background: '#fafafa',
            wordBreak: 'break-word',
            whiteSpace: 'break-spaces',
        }}
    >
        {children}
    </pre>
);

const DebugDemo: FunctionComponent = () => {
    const [logs, setLogs] = useState<(LoggerPayload & { time: number })[]>([]);
    const [isUsingCustomLogger, toggleCustomLogger] = useState<boolean>(false);

    const turnOnCustomLogger = () => {
        batchedUpdates(() => {
            setLogger((payload) => {
                setLogs((prevLogs) => [
                    ...prevLogs,
                    {
                        time: Date.now(),
                        ...payload,
                    },
                ]);
            });
            toggleCustomLogger(true);
        });

        console.log('✅ Custom logger was turned on');
    };

    const turnOffCustomLogger = () => {
        batchedUpdates(() => {
            setLogger(consoleLogger);
            setLogs([]);
            toggleCustomLogger(false);
        });

        console.log('🛑 Custom logger was turned off. Reverting to default console logger.');
    };

    const [context, setContext] = useState<BrokenContext | null>(null);

    useEffect(() => {
        const interval = setInterval(() => {
            const newContext: BrokenContext = {
                brokenValue: Date.now().toString(32),
            };
            setContext(newContext);

            console.log('⏱ Context update was triggered');
        }, 1000 * 3);

        return () => {
            clearInterval(interval);
        };
    }, []);

    return (
        <>
            <h2>Custom Logger</h2>
            <p>You can control the custom CSE logger:</p>
            <p>
                <Button type="button" onClick={turnOnCustomLogger}>
                    <span role="img" aria-label="">
                        ✅
                    </span>{' '}
                    Click here to turn on custom logger
                </Button>
            </p>
            <p>
                <Button type="button" onClick={turnOffCustomLogger}>
                    <span role="img" aria-label="">
                        🛑
                    </span>{' '}
                    Click here to turn off custom logger
                </Button>
            </p>

            {isUsingCustomLogger ? (
                <>
                    <h3>
                        <span role="img" aria-label="">
                            📝
                        </span>{' '}
                        Custom Logger
                    </h3>
                    <CodeBlock>{JSON.stringify(logs, logReplacer, '  ')}</CodeBlock>
                </>
            ) : (
                <h3>
                    <span role="img" aria-label="">
                        📄
                    </span>{' '}
                    Using a default console logger
                </h3>
            )}

            <h3>
                <span role="img" aria-label="">
                    🐛
                </span>{' '}
                Broken extension point
            </h3>
            <p>A broken extension point will trigger a debug message every three seconds.</p>
            {useExtensions(context)}
            <CodeBlock>{JSON.stringify(context)}</CodeBlock>
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, DebugDemo);
};
