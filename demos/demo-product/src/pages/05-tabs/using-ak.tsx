import type { FC } from 'react';
import React, { useMemo } from 'react';
import { PanelHandler } from '@atlassian/clientside-extensions-components';
import Tabs, { Tab, TabList, TabPanel } from '@atlaskit/tabs';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { useExtensionsAll } from './tabs.cse.graphql';
import type { ExtensionPointContextDemo05 } from './types';

const AKTabsDemo: FC<{ context: ExtensionPointContextDemo05 }> = ({ context }) => {
    const [descriptors, , isLoading] = useExtensionsAll(context);

    // Initialise tabs once all plugins have loaded.
    if (!isLoading && descriptors.length) {
        // select the first tab by default.
        descriptors[0].attributes.isActive = true;
    }

    // Tabs
    const tabsLabels = useMemo(
        () =>
            descriptors.map((extension) => {
                return <Tab key={extension.key}>{extension.attributes.label}</Tab>;
            }),
        [descriptors],
    );

    const tabPanels = useMemo(
        () =>
            descriptors.map((extension) => {
                const { onAction } = extension.attributes;

                return (
                    <TabPanel key={extension.key}>
                        <PanelHandler render={onAction as PanelExtension.PanelRenderExtension} />
                    </TabPanel>
                );
            }),
        [descriptors],
    );

    return (
        <section className="aui-tabs horizontal-tabs">
            <Tabs id="async-panel-with-context-demo">
                <TabList>{tabsLabels}</TabList>
                {tabPanels}
            </Tabs>
        </section>
    );
};

export default AKTabsDemo;
