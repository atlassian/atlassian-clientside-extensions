import type { FC } from 'react';
import React from 'react';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { PanelHandler } from '@atlassian/clientside-extensions-components';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import { useExtensionsAll } from './tabs.cse.graphql';
// eslint-disable-next-line
import 'wr-dependency!com.atlassian.auiplugin:tabs';
import type { ExtensionPointContextDemo05 } from './types';

const asTabItems = (ext: ExtensionDescriptor) => {
    const id = ext.key;
    const { label, isActive } = ext.attributes;
    return (
        <li className={`menu-item ${isActive ? 'active-tab' : ''}`} key={id}>
            <a href={`#${id}`}>{label}</a>
        </li>
    );
};

const asTabPanes = (ext: ExtensionDescriptor) => {
    const id = ext.key;
    const { label, onAction, isActive } = ext.attributes;
    return (
        <div className={`tabs-pane ${isActive ? 'active-pane' : ''}`} id={id} key={id}>
            <h2>Welcome to {label}</h2>
            <PanelHandler render={onAction as PanelExtension.PanelRenderExtension} />
        </div>
    );
};

const AUITabsDemo: FC<{ context: ExtensionPointContextDemo05 }> = ({ context }) => {
    const [descriptors, , isLoading] = useExtensionsAll(context);

    // Initialise tabs once all plugins have loaded.
    if (!isLoading && descriptors.length) {
        // select the first tab by default.
        descriptors[0].attributes.isActive = true;

        // @ts-expect-error AJS is undefined and we don't have proper types
        // eslint-disable-next-line no-undef
        AJS.tabs.setup();
    }

    return (
        <section className="aui-tabs horizontal-tabs">
            <ul className="tabs-menu">{descriptors.map(asTabItems)}</ul>
            {descriptors.map(asTabPanes)}
        </section>
    );
};

export default AUITabsDemo;
