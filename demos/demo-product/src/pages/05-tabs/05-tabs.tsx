import React, { useState } from 'react';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { renderElementAsReact } from '@atlassian/clientside-extensions-components';
import AuiTabsDemo from './using-aui';
import AkTabsDemo from './using-ak';
import type { ExtensionPointContextDemo05 } from './types';

const pluginData: ExtensionPointContextDemo05 = {
    title: `Dragon's rest`,
};

const impls: Record<string, (data: ExtensionPointContextDemo05) => JSX.Element> = {
    aui: (context) => <AuiTabsDemo context={context} />,
    atlaskit: (context) => <AkTabsDemo context={context} />,
};

const TabsDemo = () => {
    const [impl] = useState('atlaskit');

    return (
        <>
            <h1>Building tabs</h1>
            <p>
                Product developers can combine <dfn>panel</dfn> plugin types with a rich schema to render complex inter-related components,
                such as tabs.
            </p>

            {impls[impl](pluginData)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, TabsDemo);
};
