import type { FC } from 'react';
import React from 'react';
import type { PanelExtension } from '@atlassian/clientside-extensions';
import { renderElementAsReact } from '@atlassian/clientside-extensions-components';

const FooBar: FC = () => {
    return <section>Foooooooooo BAAAAAAAAR</section>;
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, FooBar);
};
