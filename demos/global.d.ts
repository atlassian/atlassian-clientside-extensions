declare namespace Intl {
    interface DateTimeFormatOptions extends Intl.DateTimeFormatOptions {
        dateStyle?: 'full' | 'long' | 'medium' | 'short';
        timeStyle?: 'full' | 'long' | 'medium' | 'short';
    }
}
