import { PanelExtension } from '@atlassian/clientside-extensions';
import type { ExtensionPointContextDemo04 } from '@atlassian/clientside-extensions-demo-product/src/pages/04-panels/types';

let callCount = 0;

/**
 * @clientside-extension
 * @extension-point demo.04-panels
 */
export default PanelExtension.factory<ExtensionPointContextDemo04>((api, context) => {
    const { title } = context;

    return {
        label: 'Plain panel using API callbacks',
        onAction(panelApi) {
            panelApi.onMount((element) => {
                callCount += 1;
                element.innerHTML = `<p>The current call count is ${callCount} at ${title}</p>`;
                element.setAttribute('data-call-count', `${callCount}`);
                console.log('mounted', title, element);
            });
            panelApi.onUnmount((element) => {
                element.innerHTML = '';
                console.log('unmounted', title, element);
            });
        },
    };
});
