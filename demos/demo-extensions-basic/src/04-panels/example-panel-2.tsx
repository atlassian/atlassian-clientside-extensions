import React from 'react';
import { PanelExtension } from '@atlassian/clientside-extensions';
import { renderElementAsReact } from '@atlassian/clientside-extensions-components';

import type { FunctionComponent } from 'react';
import type { ExtensionPointContextDemo04 } from '@atlassian/clientside-extensions-demo-product/src/pages/04-panels/types';

type MyComponentProps = {
    context: ExtensionPointContextDemo04;
};

let callCount = 0;

const MyCustomPanel: FunctionComponent<MyComponentProps> = ({ context }) => {
    const { title } = context;
    callCount += 1;
    return (
        <p>
            The current call count is {callCount} at {title}
        </p>
    );
};

/**
 * @clientside-extension
 * @extension-point demo.04-panels
 */
export default PanelExtension.factory<ExtensionPointContextDemo04>((api, context) => {
    return {
        label: 'Panel using React',
        onAction(panelApi) {
            renderElementAsReact<MyComponentProps>(panelApi, MyCustomPanel, { context });
        },
    };
});
