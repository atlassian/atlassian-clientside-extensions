import { PanelExtension } from '@atlassian/clientside-extensions';
import type { ExtensionPointContextDemo05 } from '@atlassian/clientside-extensions-demo-product/src/pages/05-tabs/types';

/**
 * @clientside-extension
 * @extension-point demo.05-tabs
 */
export default PanelExtension.factory<ExtensionPointContextDemo05>((api, context) => {
    const { title } = context;

    return {
        label: 'Another tab',
        onAction(panelApi) {
            panelApi.onMount((element) => {
                element.innerHTML = `<p>It's not for me to know whether I'm the first in ${title} or not... only that I'm rendered.</p>`;
            });
        },
    };
});
