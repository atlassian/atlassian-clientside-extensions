import { PanelExtension } from '@atlassian/clientside-extensions';
import type { ExtensionPointContextDemo05 } from '@atlassian/clientside-extensions-demo-product/src/pages/05-tabs/types';

const possibleGreetings = [
    'Why, hello there!',
    'Sure is nice to see ya!',
    `You're always welcome here!`,
    'Howdy, partner!',
    `Hey hey hey!`,
    'Yo.',
];

/**
 * @clientside-extension
 * @extension-point demo.05-tabs
 */
export default PanelExtension.factory<ExtensionPointContextDemo05>((api, context) => {
    const { title } = context;

    const anotherGreeting = () => {
        const idx = Math.floor(Math.random() * possibleGreetings.length);
        const greeting = possibleGreetings[idx];
        return `<p><strong>${title}</strong> ${greeting}</p>`;
    };

    return {
        label: 'Be greeted whenever you open this tab (in theory)',
        onAction(panelApi) {
            panelApi.onMount((element) => {
                element.innerHTML += anotherGreeting();
            });
        },
    };
});
