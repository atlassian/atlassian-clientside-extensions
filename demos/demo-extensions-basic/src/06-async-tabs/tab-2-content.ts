import type { PanelExtension } from '@atlassian/clientside-extensions';

export default (panelApi: PanelExtension.Api) => {
    panelApi.onMount((element) => {
        element.innerHTML = `<p>Some content.</p>`;
    });
};
