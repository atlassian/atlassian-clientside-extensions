import type { PanelExtension } from '@atlassian/clientside-extensions';

interface Context {
    greetings: string[];
}

export default (panelApi: PanelExtension.Api, context: Context) => {
    const { greetings } = context;

    const anotherGreeting = () => {
        const idx = Math.floor(Math.random() * greetings.length);
        const greeting = greetings[idx];
        return `<p>${greeting}</p>`;
    };

    panelApi.onMount((element) => {
        element.innerHTML += anotherGreeting();
    });
};
