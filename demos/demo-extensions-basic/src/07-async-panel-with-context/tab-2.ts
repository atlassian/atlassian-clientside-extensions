import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

const panelLoader = () => {
    return import(/* webpackChunkName: "tab-2-content" */ './tab-2-content');
};

/**
 * @clientside-extension
 * @extension-point demo.07-async-panel-with-context
 */
export default AsyncPanelExtension.factory((/* pluginAPI, context */) => {
    return {
        label: 'Async Tab 2 with Context',
        onAction: panelLoader,
        weight: 2,
    };
});
