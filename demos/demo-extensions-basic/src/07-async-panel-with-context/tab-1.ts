import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

const panelLoader = () => {
    return import(/* webpackChunkName: "tab-1-content" */ './tab-1-content');
};

/**
 * @clientside-extension
 * @extension-point demo.07-async-panel-with-context
 */
export default AsyncPanelExtension.factory((/* pluginAPI, context */) => {
    return {
        label: 'Async Tab 1 with Context',
        onAction: panelLoader,
        weight: 1,
    };
});
