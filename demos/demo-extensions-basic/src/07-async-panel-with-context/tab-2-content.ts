import type { PanelExtension } from '@atlassian/clientside-extensions';
import type { Context } from './types';

export default (panelApi: PanelExtension.Api, context: Context) => {
    panelApi.onMount((element) => {
        element.innerHTML = `
            <div>
                <h2>Tab 2 with context</h2>
                <p>
                    <strong>My counter:</strong> <span data-testid="counter">${context.myCounter}</span>
                </p>
            </div>
        `;
    });
};
