import { ButtonExtension } from '@atlassian/clientside-extensions';
import type { StatefulExtensionContext } from '@atlassian/clientside-extensions-demo-product/src/pages/03-stateful/types';

let isLiked = false; // pull this from your database however you like.

/**
 * @clientside-extension
 * @extension-point demo.03-stateful
 */
export default ButtonExtension.factory<StatefulExtensionContext>((api, context) => {
    const getLabel = () => (isLiked ? `You and ${context.randomNumber} other people like this plugin system!` : 'Like this plugin system.');

    return {
        label: getLabel(),
        onAction() {
            isLiked = !isLiked;
            api.updateAttributes({
                label: getLabel(),
            });
        },
    };
});
