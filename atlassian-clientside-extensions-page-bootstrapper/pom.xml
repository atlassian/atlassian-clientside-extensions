<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.plugins</groupId>
        <artifactId>atlassian-clientside-extensions-parent</artifactId>
        <version>3.1.2-SNAPSHOT</version>
    </parent>

    <artifactId>atlassian-clientside-extensions-page-bootstrapper</artifactId>
    <packaging>atlassian-plugin</packaging>

    <name>Atlassian Plugins - Client-side Extensions - Page Bootstrapper</name>
    <description>Provides code for bootstrapping CSE pages.</description>
    <licenses>
        <license>
            <name>BSD License</name>
            <url>https://opensource.org/licenses/BSD-3-Clause</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>

        <!-- jacoco aggregate report -->
        <sonar.coverage.jacoco.xmlReportPaths>${project.basedir}/../${jacoco.report.file}</sonar.coverage.jacoco.xmlReportPaths>
    </properties>

    <dependencies>
        <dependency>
            <groupId>jakarta.servlet</groupId>
            <artifactId>jakarta.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-core</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-servlet</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <scope>provided</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-core</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-beans</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-expression</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.json</groupId>
            <artifactId>atlassian-json-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.annotations</groupId>
            <artifactId>atlassian-annotations</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.dom4j</groupId>
            <artifactId>dom4j</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>amps-maven-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <includedFeModuleManifests>
                        <includedFeModuleManifest>${project.basedir}/package.json</includedFeModuleManifest>
                        <includedFeModuleManifest>${project.basedir}/../yarn.lock</includedFeModuleManifest>
                    </includedFeModuleManifests>
                    <closureJsCompiler>true</closureJsCompiler>
                    <enablePde>false</enablePde>
                    <enableQuickReload>true</enableQuickReload>
                    <extractDependencies>false</extractDependencies>
                    <instructions>
                        <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>
                        <!-- Let Atlassian know about auto-generated XML plugin descriptors created by Webpack WRM plugin -->
                        <Atlassian-Scan-Folders>META-INF/plugin-descriptors</Atlassian-Scan-Folders>
                        <!-- Import the OSGi bridge package because we're using @ModuleType -->
                        <Import-Package>
                            com.atlassian.plugin.osgi.bridge.external,
                            *
                        </Import-Package>
                        <Export-Package>
                            com.atlassian.plugin.clientsideextensions,
                            com.atlassian.plugin.clientsideextensions.moduletype
                        </Export-Package>
                        <Spring-Context>*</Spring-Context>
                    </instructions>
                </configuration>
                <executions>
                    <execution>
                        <id>js-enforcement</id>
                        <goals>
                            <goal>verify-fe-manifest-associations</goal>
                        </goals>
                        <phase>verify</phase>
                        <configuration>
                            <verifyFeManifestAssociationsInputEntrypoint>
                                ${project.build.directory}/${project.build.finalName}.jar
                            </verifyFeManifestAssociationsInputEntrypoint>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>com.github.eirslett</groupId>
                <artifactId>frontend-maven-plugin</artifactId>
                <configuration>
                    <workingDirectory>${project.basedir}</workingDirectory>
                    <installDirectory>${project.parent.build.outputDirectory}</installDirectory>
                </configuration>
                <executions>
                    <execution>
                        <id>yarn-build</id>
                        <goals>
                            <goal>yarn</goal>
                        </goals>
                        <configuration>
                            <arguments>build</arguments>
                        </configuration>
                        <phase>process-resources</phase>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
                <version>${atlassian.spring.scanner.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>atlassian-spring-scanner</goal>
                        </goals>
                        <phase>process-classes</phase>
                    </execution>
                </executions>
                <configuration>
                    <!-- Enable this to get build-time logging of annotations atlassian-spring-scanner-maven-plugin has noticed -->
                    <verbose>true</verbose>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>jacoco-report</id>
            <activation>
                <property>
                    <name>report</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.jacoco</groupId>
                        <artifactId>jacoco-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>agent-for-ut</id>
                                <goals>
                                    <goal>prepare-agent</goal>
                                </goals>
                                <configuration>
                                    <propertyName>jacoco.agent.argLine</propertyName>
                                    <includes>
                                        <include>com/atlassian/plugin/clientsideextensions/**</include>
                                    </includes>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <configuration>
                            <argLine>@{jacoco.agent.argLine}</argLine>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
