package com.atlassian.plugin.clientsideextensions.moduletype;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.EXTENSION_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.EXTENSION_POINT_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_DATA_PROVIDER_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_DECORATOR_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_TITLE_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_TITLE_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.WEB_RESOURCE_KEYS_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.DEPENDENCY_ELEM;
import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.EXTENSION_KEY_ATTR;
import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.EXTENSION_POINT_ATTR;
import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.PAGE_DATA_PROVIDER_KEY_ATTR;
import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.PAGE_DECORATOR_ATTR;
import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.PAGE_TITLE_ELEM;
import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.PAGE_TITLE_KEY_ATTR;
import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.URL_PATTERN_ELEM;

public class WebPageModuleDescriptorTest {

    private static final String EXTENSION_KEY = "theExtensionKey";

    private static final String EXTENSION_POINT = "theExtensionPoint";

    private static final String PAGE_DATA_PROVIDER_KEY = "thePageDataProviderKey";

    private static final String PAGE_DECORATOR = "thePageDecorator";

    private static final String PAGE_TITLE = "thePageTitle";

    private static final String PAGE_TITLE_KEY = "thePageTitleKey";

    private static final String URL_PATTERN_1 = "urlPattern1";

    private static final String URL_PATTERN_2 = "urlPattern2";

    private static final String WEB_RESOURCE_A = "theWebResourceA";

    private static final String WEB_RESOURCE_B = "theWebResourceB";

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Element dependencyElement1;

    @Mock
    private Element dependencyElement2;

    @Mock
    private Element pageTitleElement;

    @Mock
    private Element urlElement1;

    @Mock
    private Element urlElement2;

    @Mock
    private Dom4jDelegatingElement webPageElement;

    @Mock
    private ModuleFactory moduleFactory;

    @Mock
    private Plugin plugin;

    @Mock
    private ServletModuleManager servletModuleManager;

    @InjectMocks
    private WebPageModuleDescriptor moduleDescriptor;

    @Before
    public void setUp() {
        when(plugin.hasAllPermissions()).thenReturn(true);
        when(urlElement1.getTextTrim()).thenReturn(URL_PATTERN_1);
        when(urlElement2.getTextTrim()).thenReturn(URL_PATTERN_2);

        when(dependencyElement1.getTextTrim()).thenReturn(WEB_RESOURCE_A);
        when(dependencyElement2.getTextTrim()).thenReturn(WEB_RESOURCE_B);
    }

    @Test
    public void init_whenXmlIsFullyPopulated_shouldParseRequiredValues() {
        // Set up
        when(webPageElement.attributeValue(EXTENSION_KEY_ATTR)).thenReturn(EXTENSION_KEY);
        when(webPageElement.attributeValue(EXTENSION_POINT_ATTR)).thenReturn(EXTENSION_POINT);
        when(webPageElement.attributeValue(PAGE_DECORATOR_ATTR)).thenReturn(PAGE_DECORATOR);
        when(webPageElement.attributeValue(PAGE_DATA_PROVIDER_KEY_ATTR)).thenReturn(PAGE_DATA_PROVIDER_KEY);
        when(webPageElement.elementTextTrim(PAGE_TITLE_ELEM)).thenReturn(PAGE_TITLE);
        when(webPageElement.element(PAGE_TITLE_ELEM)).thenReturn(pageTitleElement);
        when(webPageElement.attributeValue("key")).thenReturn("webPageKey");
        when(webPageElement.element(URL_PATTERN_ELEM)).thenReturn(urlElement1);
        when(pageTitleElement.attributeValue(PAGE_TITLE_KEY_ATTR)).thenReturn(PAGE_TITLE_KEY);

        // Urls
        final List<Element> urlElements = asList(urlElement1, urlElement2);
        when(webPageElement.elements(URL_PATTERN_ELEM)).thenReturn(urlElements);

        // Dependencies
        final List<Element> dependencyElements = asList(dependencyElement1, dependencyElement2);
        when(webPageElement.elements(DEPENDENCY_ELEM)).thenReturn(dependencyElements);
        when(webPageElement.getDelegate()).thenReturn(mock(org.dom4j.Element.class));

        // Invoke
        moduleDescriptor.init(plugin, webPageElement);

        final List<String> webResources = asList(WEB_RESOURCE_A, WEB_RESOURCE_B);
        String webResourceKeys = String.join(",", webResources);

        // Check
        final Map<String, String> initParams = moduleDescriptor.getInitParams();
        assertThat(initParams, hasEntry(EXTENSION_KEY_PARAM_NAME, EXTENSION_KEY));
        assertThat(initParams, hasEntry(EXTENSION_POINT_PARAM_NAME, EXTENSION_POINT));
        assertThat(initParams, hasEntry(PAGE_DECORATOR_PARAM_NAME, PAGE_DECORATOR));
        assertThat(initParams, hasEntry(PAGE_DATA_PROVIDER_KEY_PARAM_NAME, PAGE_DATA_PROVIDER_KEY));
        assertThat(initParams, hasEntry(PAGE_TITLE_PARAM_NAME, PAGE_TITLE));
        assertThat(initParams, hasEntry(PAGE_TITLE_KEY_PARAM_NAME, PAGE_TITLE_KEY));
        assertThat(initParams, hasEntry(WEB_RESOURCE_KEYS_PARAM_NAME, webResourceKeys));
        assertThat(moduleDescriptor.getPaths(), contains(URL_PATTERN_1, URL_PATTERN_2));
    }
}
