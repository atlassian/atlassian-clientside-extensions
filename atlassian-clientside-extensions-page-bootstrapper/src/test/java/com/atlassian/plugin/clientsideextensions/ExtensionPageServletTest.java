package com.atlassian.plugin.clientsideextensions;

import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;
import static org.junit.rules.ExpectedException.none;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.DEFAULT_PAGE_DECORATOR;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.DEFAULT_PAGE_TITLE;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.EXTENSION_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.EXTENSION_POINT_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.MODEL_KEY_EXTENSION_KEY;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.MODEL_KEY_EXTENSION_POINT;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.MODEL_KEY_PAGE_DATA_PROVIDER_KEY;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.MODEL_KEY_PAGE_DECORATOR;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.MODEL_KEY_PAGE_TITLE;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.MODEL_KEY_PATH;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_DATA_PROVIDER_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_DECORATOR_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_TITLE_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_TITLE_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.RESOURCE_KEY;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.TEMPLATE_KEY;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.WEB_RESOURCE_KEYS_PARAM_NAME;

public class ExtensionPageServletTest {

    private static final String EXTENSION_KEY = "theExtensionPoint";

    private static final String EXTENSION_POINT = "theExtensionPoint";

    private static final String PAGE_DATA_PROVIDER_KEY = "thePageDataProviderKey";

    private static final String PAGE_DECORATOR = "thePageDecorator";

    private static final String PAGE_TITLE = "thePageTitle";

    private static final String PAGE_TITLE_KEY = "thePageTitleKey";

    private static final String PATH_INFO = "thePathInfo";

    private static final String WEB_RESOURCES = "theWebResourceA,theWebResourceB,theWebResourceC";

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final ExpectedException expectedException = none();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private I18nResolver i18nResolver;

    @Mock
    private PrintWriter printWriter;

    @Mock
    private ServletConfig servletConfig;

    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;

    @InjectMocks
    private ExtensionPageServlet extensionPageServlet;

    private void setUpExtensionKey(final String extensionKey) {
        when(servletConfig.getInitParameter(EXTENSION_KEY_PARAM_NAME)).thenReturn(extensionKey);
    }

    private void setUpExtensionPoint(final String extensionPoint) {
        when(servletConfig.getInitParameter(EXTENSION_POINT_PARAM_NAME)).thenReturn(extensionPoint);
    }

    private void setUpPageDataProviderKey(final String pageDataProviderKey) {
        when(servletConfig.getInitParameter(PAGE_DATA_PROVIDER_KEY_PARAM_NAME)).thenReturn(pageDataProviderKey);
    }

    private void setUpPageDecorator(final String pageDecorator) {
        when(servletConfig.getInitParameter(PAGE_DECORATOR_PARAM_NAME)).thenReturn(pageDecorator);
    }

    private void setUpPageTitle(final String pageTitle) {
        when(servletConfig.getInitParameter(PAGE_TITLE_PARAM_NAME)).thenReturn(pageTitle);
    }

    private void setUpPageTitleKey() {
        when(servletConfig.getInitParameter(PAGE_TITLE_KEY_PARAM_NAME)).thenReturn(PAGE_TITLE_KEY);
    }

    private void setUpPathInfo(final String pathInfo) {
        when(request.getPathInfo()).thenReturn(pathInfo);
    }

    private void setUpWebResourceKeys(final String webResourceKeys) {
        when(servletConfig.getInitParameter(WEB_RESOURCE_KEYS_PARAM_NAME)).thenReturn(webResourceKeys);
    }

    @Test
    public void init_whenExtensionKeyMissing_shouldFail() throws Exception {
        assertInvalidExtensionKey(null);
    }

    @Test
    public void init_whenExtensionKeyEmpty_shouldFail() throws Exception {
        assertInvalidExtensionKey("");
    }

    private void assertInvalidExtensionKey(final String extensionKey) throws ServletException {
        // Set up
        expectedException.expect(UnavailableException.class);
        expectedException.expectMessage(is("The required 'extension-key' init-param is missing or undefined"));
        setUpExtensionKey(extensionKey);
        setUpExtensionPoint(EXTENSION_POINT);

        // Invoke
        extensionPageServlet.init(servletConfig);
    }

    @Test
    public void init_whenExtensionPointMissing_shouldFail() throws Exception {
        assertInvalidExtensionPoint(null);
    }

    @Test
    public void init_whenExtensionPointEmpty_shouldFail() throws Exception {
        assertInvalidExtensionPoint("");
    }

    private void assertInvalidExtensionPoint(final String extensionPoint) throws ServletException {
        // Set up
        expectedException.expect(UnavailableException.class);
        expectedException.expectMessage(is("The required 'extension-point' init-param is missing or undefined"));
        setUpExtensionKey(EXTENSION_KEY);
        setUpExtensionPoint(extensionPoint);

        // Invoke
        extensionPageServlet.init(servletConfig);
    }

    @Test
    public void init_whenWebResourceMissing_shouldFail() throws Exception {
        assertInvalidWebResourceKeys(null);
    }

    @Test
    public void init_whenWebResourceEmpty_shouldFail() throws Exception {
        assertInvalidWebResourceKeys("");
    }

    private void assertInvalidWebResourceKeys(final String webResourceKeys) throws ServletException {
        // Set up
        expectedException.expect(UnavailableException.class);
        expectedException.expectMessage(is("The required 'web-resources' init-param is missing or undefined"));
        setUpExtensionKey(EXTENSION_KEY);
        setUpExtensionPoint(EXTENSION_POINT);
        setUpWebResourceKeys(webResourceKeys);

        // Invoke
        extensionPageServlet.init(servletConfig);
    }

    @Test
    public void init_whenExtensionKeyAndPointPresent_shouldSucceed() throws Exception {
        // Set up
        setUpExtensionKey(EXTENSION_KEY);
        setUpExtensionPoint(EXTENSION_POINT);
        setUpWebResourceKeys(WEB_RESOURCES);

        // Invoke
        extensionPageServlet.init(servletConfig);
    }

    @Test
    public void doGet_whenRequestContainsPathInfo_shouldIncludeItInViewModel() throws Exception {
        // Set up
        setUpPathInfo(PATH_INFO);

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, hasEntry(MODEL_KEY_PATH, PATH_INFO));
    }

    @Test
    public void doGet_whenRequestContainsNoPathInfo_shouldNotIncludeItInViewModel() throws Exception {
        // Set up
        setUpPathInfo(null);

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, not(hasKey(MODEL_KEY_PATH)));
    }

    @Test
    public void doGet_whenPageDataProviderSet_shouldIncludeItInViewModel() throws Exception {
        // Set up
        setUpPageDataProviderKey(PAGE_DATA_PROVIDER_KEY);

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, hasEntry(MODEL_KEY_PAGE_DATA_PROVIDER_KEY, PAGE_DATA_PROVIDER_KEY));
    }

    @Test
    public void doGet_whenPageDecoratorCustomised_shouldIncludeItInViewModel() throws Exception {
        // Set up
        setUpPageDecorator(PAGE_DECORATOR);

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, hasEntry(MODEL_KEY_PAGE_DECORATOR, PAGE_DECORATOR));
    }

    @Test
    public void doGet_whenPageDecoratorNotCustomised_shouldIncludeDefaultDecoratorInViewModel() throws Exception {
        // Set up
        setUpPageDecorator(null);

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, hasEntry(MODEL_KEY_PAGE_DECORATOR, DEFAULT_PAGE_DECORATOR));
    }

    @Test
    public void doGet_whenRawPageTitleSet_shouldIncludeItInViewModel() throws Exception {
        // Set up
        setUpPageTitle(PAGE_TITLE);

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, hasEntry(MODEL_KEY_PAGE_TITLE, PAGE_TITLE));
    }

    @Test
    public void doGet_whenPageTitleKeyIsValid_shouldIncludeItsTranslationInViewModel() throws Exception {
        // Set up
        setUpPageTitleKey();
        final String translatedPageTitle = "Translated Page Title";
        setupPageTitleTranslation(translatedPageTitle);

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, hasEntry(MODEL_KEY_PAGE_TITLE, translatedPageTitle));
    }

    @Test
    public void doGet_whenPageTitleKeyIsInvalid_shouldIncludeRawPageTitleInViewModel() throws Exception {
        // Set up
        setUpPageTitle(PAGE_TITLE);
        setUpPageTitleKey();
        setupPageTitleTranslation(PAGE_TITLE_KEY); // means no translation found

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, hasEntry(MODEL_KEY_PAGE_TITLE, PAGE_TITLE));
    }

    private void setupPageTitleTranslation(final String translatedPageTitle) {
        when(i18nResolver.getText(PAGE_TITLE_KEY)).thenReturn(translatedPageTitle);
    }

    @Test
    public void doGet_whenPageTitleNotCustomised_shouldIncludeDefaultPageTitleInViewModel() throws Exception {
        // Set up
        setUpPageTitle(null);

        // Invoke
        final Map<String, Object> templateModel = assertRenderTemplate();
        assertThat(templateModel, hasEntry(MODEL_KEY_PAGE_TITLE, DEFAULT_PAGE_TITLE));
    }

    private Map<String, Object> assertRenderTemplate() throws Exception {
        // Set up
        when(response.getWriter()).thenReturn(printWriter);
        setUpExtensionKey(EXTENSION_KEY);
        setUpExtensionPoint(EXTENSION_POINT);
        setUpWebResourceKeys(WEB_RESOURCES);
        extensionPageServlet.init(servletConfig);

        // Invoke
        extensionPageServlet.doGet(request, response);

        // Check
        verify(response).setContentType(ExtensionPageServlet.CONTENT_TYPE);
        @SuppressWarnings("unchecked")
        final ArgumentCaptor<Map<String, Object>> modelCaptor = ArgumentCaptor.forClass(Map.class);
        verify(soyTemplateRenderer)
                .render(same(printWriter), eq(RESOURCE_KEY), eq(TEMPLATE_KEY), modelCaptor.capture());
        final Map<String, Object> templateModel = modelCaptor.getValue();
        assertThat(templateModel, hasEntry(MODEL_KEY_EXTENSION_KEY, EXTENSION_KEY));
        assertThat(templateModel, hasEntry(MODEL_KEY_EXTENSION_POINT, EXTENSION_POINT));
        return templateModel;
    }
}
