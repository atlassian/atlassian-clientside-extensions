import whenDomReady from 'when-dom-ready';
import { ReplaySubject } from '@atlassian/clientside-extensions-base';
import type { PageExtension } from '@atlassian/clientside-extensions';
import { onDebug } from '@atlassian/clientside-extensions-debug';
import type { WRMInterface, DataProviderPayload } from '@atlassian/clientside-extensions-registry';

declare let WRM: WRMInterface;

type PageExtensionConfig = {
    pageExtensionKey: string;
    pageFactory: PageExtension.PageAttributesProvider;
};

// Create a subject listener so we can notify the listener when the page will get registered.
// Use the replay subject in case the notification was send before we add a subscription listener.
const pageSubject = new ReplaySubject<PageExtensionConfig>(1);

export function registerPage(pageExtensionKey: string, pageFactory: PageExtension.PageAttributesProvider) {
    const pageExtensionConfig: PageExtensionConfig = {
        pageExtensionKey,
        pageFactory,
    };

    pageSubject.notify(pageExtensionConfig);
}

const showErrorMessage = (title: string, body: string) => {
    const messageElement = document.querySelector('#cse-page-error') as HTMLElement | null;

    if (!messageElement) {
        throw new Error(`Can't find error element "#cse-page-error"`);
        return;
    }

    // AJS.message doesn't replace the content of the div, so we clean it first.
    messageElement.innerHTML = '';

    AJS.messages.error(messageElement, {
        title,
        body,
    });
};

export function initPage(extensionKey: string, dataProviderKey: string) {
    const observer = (pageExtensionConfig: PageExtensionConfig) => {
        const { pageExtensionKey, pageFactory } = pageExtensionConfig;

        const rootElement = document.querySelector('#cse-page-root') as HTMLElement | null;

        if (!rootElement) {
            throw new Error(`Can't find root element "#cse-page-root" to mount page into`);
            return;
        }

        try {
            if (pageExtensionKey !== extensionKey) {
                throw new Error(`Expected page extension "${extensionKey}" but received "${pageExtensionKey}"`);
            }

            const attributes = pageFactory();

            if (attributes.type !== 'page') {
                throw new Error(`Unsupported extension type "${attributes.type}". The extension type should equal "page".`);
            }

            // Retrieve server-side data from page data provider
            let dataPayload: DataProviderPayload;
            if (dataProviderKey !== null) {
                dataPayload = WRM.data.claim(dataProviderKey);
            }

            attributes.onAction(rootElement, dataPayload);
        } catch (err) {
            const error = err as Error;
            const message = `Error while rendering page extension: ${pageExtensionKey}`;

            onDebug(({ error: level }) => ({
                level,
                message,
                components: ['Page Bootstrapper'],
                meta: {
                    pageExtensionKey,
                    error,
                },
            }));

            showErrorMessage(message, error.message);
        }
    };

    whenDomReady().then(() => {
        pageSubject.subscribe(observer);
    });
}
