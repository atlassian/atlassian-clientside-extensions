/* eslint-disable vars-on-top */
/* eslint-disable no-var */

// eslint-disable-next-line @typescript-eslint/no-unused-vars
declare var AJS: {
    messages: {
        error: (container, { title: string, body: string }) => void;
    };
};
