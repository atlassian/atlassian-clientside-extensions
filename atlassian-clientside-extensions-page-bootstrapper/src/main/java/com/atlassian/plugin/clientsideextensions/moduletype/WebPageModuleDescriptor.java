package com.atlassian.plugin.clientsideextensions.moduletype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.clientsideextensions.ExtensionPageServlet;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.joining;

import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.EXTENSION_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.EXTENSION_POINT_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_DATA_PROVIDER_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_DECORATOR_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_TITLE_KEY_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.PAGE_TITLE_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.WEB_RESOURCE_KEYS_PARAM_NAME;
import static com.atlassian.plugin.clientsideextensions.ExtensionPageServlet.WEB_RESOURCE_KEYS_SEPARATOR;

/**
 * Each instance of this class represents a {@value #MODULE_TYPE} tag in a plugin descriptor. For example:
 *
 * <pre>
 * &lt;web-page key="some-key-unique-within-your-plugin"
 *           extension-key="my.extension.key.bar"
 *           extension-point="my.extension.location.foo"
 *           page-decorator="atl.admin"
 *           page-data-provider-key="my.plugin:my-web-resource.my-data-provider-key"
 * &gt;
 *     &lt;url-pattern&gt;/my-custom-admin-page&lt;/url-pattern&gt;
 *     &lt;url-pattern&gt;/my-custom-admin-page/*&lt;/url-pattern&gt;
 *     &lt;page-title&gt;My title&lt;/page-title&gt;
 *     &lt;dependency&gt;my.plugin:my-web-resource&lt;/dependency&gt;
 * &lt;/web-page&gt;
 * </pre>
 * <p>
 * We extend {@link ServletModuleDescriptor} because that's the only class that {@link ServletModuleManager#addServletModule}
 * will accept. However because we're not parsing a &lt;{@code servlet}&gt; XML element, we have to override the {@code init} method,
 * which expects that type of element. Because we override {@code init()}, we also override {@code getInitParams()} and {@code getPaths()},
 * which would otherwise return data structures populated by the overridden {@code init} method.
 */
@ParametersAreNonnullByDefault
public class WebPageModuleDescriptor extends ServletModuleDescriptor {

    /**
     * The name of the XML element that P2 plugin developers will add to their
     * {@code atlassian-plugin.xml} in order to add a module of this type to their plugin.
     */
    public static final String MODULE_TYPE = "web-page";

    // XML elements and attributes that we look for within the web-page element

    @VisibleForTesting
    static final String DEPENDENCY_ELEM = "dependency";

    @VisibleForTesting
    static final String EXTENSION_KEY_ATTR = "extension-key";

    @VisibleForTesting
    static final String EXTENSION_POINT_ATTR = "extension-point";

    @VisibleForTesting
    static final String PAGE_DATA_PROVIDER_KEY_ATTR = "page-data-provider-key";

    @VisibleForTesting
    static final String PAGE_DECORATOR_ATTR = "page-decorator";

    @VisibleForTesting
    static final String PAGE_TITLE_ELEM = "page-title";

    @VisibleForTesting
    static final String PAGE_TITLE_KEY_ATTR = "key";

    @VisibleForTesting
    static final String URL_PATTERN_ELEM = "url-pattern";

    private final Map<String, String> initParams;
    private final List<String> urlPatterns;

    public WebPageModuleDescriptor(
            @ComponentImport final ModuleFactory moduleFactory,
            @ComponentImport final ServletModuleManager servletModuleManager) {
        super(moduleFactory, servletModuleManager);
        this.initParams = new HashMap<>();
        this.urlPatterns = new ArrayList<>();
    }

    @Override
    public void init(final Plugin plugin, final Element webPageElement) {
        super.init(plugin, webPageElement);
        moduleClassName = ExtensionPageServlet.class.getName();
        parseServletInitParams(webPageElement);
        parseUrlPatterns(webPageElement);
    }

    private void parseServletInitParams(final Element webPageElement) {
        initParams.put(EXTENSION_KEY_PARAM_NAME, webPageElement.attributeValue(EXTENSION_KEY_ATTR));
        initParams.put(EXTENSION_POINT_PARAM_NAME, webPageElement.attributeValue(EXTENSION_POINT_ATTR));
        initParams.put(PAGE_DATA_PROVIDER_KEY_PARAM_NAME, webPageElement.attributeValue(PAGE_DATA_PROVIDER_KEY_ATTR));
        initParams.put(PAGE_DECORATOR_PARAM_NAME, webPageElement.attributeValue(PAGE_DECORATOR_ATTR));
        initParams.put(PAGE_TITLE_PARAM_NAME, webPageElement.elementTextTrim(PAGE_TITLE_ELEM));
        initParams.put(WEB_RESOURCE_KEYS_PARAM_NAME, getWebResources(webPageElement));

        parsePageTitleKey(webPageElement).ifPresent(key -> initParams.put(PAGE_TITLE_KEY_PARAM_NAME, key));
    }

    private Optional<String> parsePageTitleKey(final Element webPageElement) {
        return Optional.ofNullable(webPageElement.element(PAGE_TITLE_ELEM))
                .map(pageTitleElement -> pageTitleElement.attributeValue(PAGE_TITLE_KEY_ATTR));
    }

    private String getWebResources(final Element webPageElement) {
        final List<?> elements = webPageElement.elements(DEPENDENCY_ELEM);

        return elements.stream()
                .filter(Element.class::isInstance)
                .map(Element.class::cast)
                .map(Element::getTextTrim)
                .collect(joining(WEB_RESOURCE_KEYS_SEPARATOR));
    }

    private void parseUrlPatterns(final Element webPageElement) {
        // When dom4j supports generics, we can do this more cleanly with a stream/map/foreach
        for (final Object object : webPageElement.elements(URL_PATTERN_ELEM)) {
            if (object instanceof Element) {
                final Element element = (Element) object;
                urlPatterns.add(element.getTextTrim());
            }
        }
    }

    @Override
    public Map<String, String> getInitParams() {
        return unmodifiableMap(initParams);
    }

    @Override
    public List<String> getPaths() {
        return unmodifiableList(urlPatterns);
    }
}
