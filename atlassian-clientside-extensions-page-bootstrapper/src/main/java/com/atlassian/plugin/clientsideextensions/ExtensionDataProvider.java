package com.atlassian.plugin.clientsideextensions;

import com.atlassian.webresource.api.data.WebResourceDataProvider;

/**
 * Interface for providing JSON data to the PageExtension JS API.
 * Instances of this class are constructed at plugin enablement time.
 *
 * Refer to the documentation:
 * https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/extension-factories/page/#using-data-provider
 *
 * @since v1.2
 */
public interface ExtensionDataProvider extends WebResourceDataProvider {}
