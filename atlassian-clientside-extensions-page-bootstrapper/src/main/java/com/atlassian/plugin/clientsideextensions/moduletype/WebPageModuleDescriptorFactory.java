package com.atlassian.plugin.clientsideextensions.moduletype;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;

import static com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor.MODULE_TYPE;

/**
 * The {@link com.atlassian.plugin.ModuleDescriptorFactory ModuleDescriptorFactory} for {@link WebPageModuleDescriptor}s.
 */
@Component
@ModuleType({ListableModuleDescriptorFactory.class, ModuleDescriptorFactory.class})
public class WebPageModuleDescriptorFactory extends SingleModuleDescriptorFactory<WebPageModuleDescriptor> {

    /**
     * Constructor.
     *
     * @param hostContainer the host container to use when create descriptor instances
     */
    public WebPageModuleDescriptorFactory(final HostContainer hostContainer) {
        super(hostContainer, MODULE_TYPE, WebPageModuleDescriptor.class);
    }
}
