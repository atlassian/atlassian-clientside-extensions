const path = require('path');
const WRMPlugin = require('atlassian-webresource-webpack-plugin');

const OUTPUT_PATH = path.join(__dirname, 'target/classes');

const mode = process.env.NODE_ENV;

module.exports = {
    mode,
    entry: {
        'page-bootstrapper': path.resolve(__dirname, 'src/main/frontend/page-bootstrapper.ts'),
    },
    output: {
        path: OUTPUT_PATH,
        libraryTarget: 'amd',
        library: '@atlassian/clientside-extensions-page-bootstrapper',
    },
    resolve: {
        extensions: ['.ts', '.js', '.json'],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [
        new WRMPlugin({
            pluginKey: 'com.atlassian.plugins.atlassian-clientside-extensions-page-bootstrapper',
            xmlDescriptors: path.join(OUTPUT_PATH, 'META-INF', 'plugin-descriptors', 'wr-cse-page-bootstrapper-bundles.xml'),
            addEntrypointNameAsContext: false,
            webresourceKeyMap: {
                'page-bootstrapper': 'page-bootstrapper',
            },
            transformationMap: null, // We don't have any transformations
        }),
    ],
    optimization: {
        minimize: true,
    },
};
