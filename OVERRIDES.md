This document explains the overrides applied to the dependencies of our project due to identified vulnerabilities. It provides clarity on why these overrides were necessary and under what conditions they can be removed.

# \*\*/react-router-dom/react-router/path-to-regexp

https://asecurityteam.atlassian.net/browse/VULN-1420167 react-router-dom@5.3.0 > react-router@5.2.1 > path-to-regexp@1.8.0

The path-to-regexp package, used by react-router-dom through its dependency tree, had to be pinned to version 1.9.0 to ensure compatibility with project requirements and to address potential issues in earlier versions.

# \*\*/webpack-dev-server/express/path-to-regexp

The webpack-dev-server uses express, which depends on path-to-regexp. Overriding this to version 1.9.0 resolves compatibility issues that could arise from using an older version.

## \*\*/glob/foreground-child/cross-spawn

https://asecurityteam.atlassian.net/browse/VULN-1449008

eslint -> cross-spawn

execa -> cross-spawn

@atlassian -> clientside-extensions-docs -> @atlassian/doc-scripts -> cross-spawn

The vulnerability was fixed by setting a newer patched version.
