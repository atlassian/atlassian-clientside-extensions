const base = require('./jest.config.base');

module.exports = {
    ...base,
    // specifying the jest configuration of each package will force Jest to NOT run test from Root too,
    // but instead just run tests in packages and use root as the folder to dump the coverage report
    projects: ['<rootDir>/packages/*/jest.config.js', '<rootDir>/scripts/jest.config.cjs'],
};
