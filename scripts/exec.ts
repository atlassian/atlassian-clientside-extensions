/* eslint-disable prefer-promise-reject-errors */
// from: https://blog.cloudboost.io/node-js-writing-shell-scripts-using-modern-javascript-instead-of-bash-774e0859f965

import { spawn } from 'child_process';
import path from 'path';
import { fileURLToPath } from 'url';

// eslint-disable-next-line @typescript-eslint/naming-convention
const __dirname = path.dirname(fileURLToPath(import.meta.url));
const ROOT = path.resolve(__dirname, '..');

export type Response = {
    code: number | null;
    data?: string;
    error?: Object;
};

export type Options = {
    // Set `capture` to TRUE, to capture and return stdout.
    capture?: boolean;

    // Set `echo` to TRUE, to echo command passed.
    echo?: boolean;

    // Working directory
    cwd?: string;
};

/**
 * Executes shell command as it would happen in BASH script
 */
export default (command: string, { capture, echo, cwd }: Options = { capture: false, echo: true, cwd: ROOT }): Promise<Response> => {
    if (echo) {
        console.log(command);
    }

    const childProcess = spawn('bash', ['-c', command], { stdio: capture ? 'pipe' : 'inherit', cwd });

    return new Promise((resolve, reject) => {
        let stdout = '';

        if (capture) {
            childProcess.stdout!.on('data', (data) => {
                stdout += data;
            });
        }

        childProcess.on('error', (error) => {
            reject({ code: 1, error, data: stdout.replace(/[\r\n]/g, '') });
        });

        childProcess.on('close', (code) => {
            if (code !== null && code > 0) {
                reject({ code, error: `Command failed with code ${code}`, data: stdout });
            } else {
                resolve({ code, data: stdout.replace(/[\r\n]/g, '') });
            }
        });
    });
};
