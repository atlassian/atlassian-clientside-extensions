// eslint-disable-next-line import/prefer-default-export
export function getAsyncHandler<Arguments>(handler: (args: Arguments) => void) {
    return async function asyncHandler(options: Arguments) {
        try {
            await handler(options);
        } catch (err) {
            const error = err as Error;

            console.log(error.message);
            process.exit(1);
        }
    };
}
