import type { SemVer } from 'semver';
import semver from 'semver';
import exec from './exec.js';

// https://www.mojohaus.org/versions-maven-plugin/version-rules.html
// <MajorVersion [> . <MinorVersion [> . <IncrementalVersion ] ] [> - <BuildNumber | Qualifier ]>
export const MAVEN_VERSION_FORMAT_REGEXP = /^[0-9]+\.[0-9]+\.[0-9]+(?:-[a-z0-9-]+)?$/;

export const getVersionFromReleaseBranch = async (): Promise<string | null> => {
    const { data } = await exec('git rev-parse --abbrev-ref HEAD', { capture: true });
    const match = data!.trim().match(/^release\/(?<version>.*)$/);

    return match?.groups?.version ?? null;
};

export const validateVersion = async (version: string, snapshot: boolean = false): Promise<void> => {
    if (!semver.valid(version)) {
        throw new Error(
            `The "${version}" version is invalid according to the semver 2.0.0 specification.\nFor more details check: https://semver.org/`,
        );
    }

    // Maven doesn't like more than two dots e.g. "1.2.0-rc.1" would be invalid.
    if (!version.match(MAVEN_VERSION_FORMAT_REGEXP)) {
        throw new Error(
            `The "${version}" version is invalid according to the Maven version number rules.\nFor more details check: https://www.mojohaus.org/versions-maven-plugin/version-rules.html`,
        );
    }

    // Non-snapshot related checks
    if (!snapshot) {
        // Check if the given version was already released
        const tagName = `v${version}`;
        const { data } = await exec(`git tag -l ${tagName}`, { capture: true });

        if (data) {
            throw new Error(`The "${version}" version was already released. Please use a different version.`);
        }

        // Check if the version is matching release branch
        const branchVersion = await getVersionFromReleaseBranch();
        const isOnReleaseBranch = typeof branchVersion == 'string';

        // Convert to semver in case we have an additional suffix like e.g. `-beta-1`
        const semverVersion = semver.coerce(version) as SemVer;

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore -- too stupid to understand we've guarded against null
        if (isOnReleaseBranch && !semver.satisfies(semverVersion, branchVersion)) {
            throw new Error(
                `You can only release a new patch version from the "${branchVersion}" release branch. Your current version "${version}" doesn't match this rule.`,
            );
        }
    }
};

export const getDevelopmentVersion = async (version: string, snapshot?: boolean): Promise<string> => {
    const result = semver.coerce(version);
    const coercedVersion = result?.raw ?? version;

    // revert to current version after done with snapshot release
    if (snapshot) {
        return coercedVersion;
    }

    // If we are releasing a pre-release version like e.g. "beta" we don't want to change the version
    if (coercedVersion !== version) {
        return coercedVersion;
    }

    return `${semver.major(version)}.${semver.minor(version)}.${semver.patch(version) + 1}`;
};

export const getVersion = async (version: string, snapshot?: boolean): Promise<string> => {
    await validateVersion(version, snapshot);

    if (!snapshot) {
        return version;
    }

    // using the commit hash as a suffix for the snapshot version
    const commitHash = await exec('git rev-parse --short --verify HEAD', { capture: true })
        // get the hash from the stdout of the command
        .then((result) => result.data!.replace(/[\r\n]/g, ''));

    const timestampHash = Date.now().toString(36);

    return `${version}-${commitHash}-${timestampHash}`;
};
