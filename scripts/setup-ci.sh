#!/usr/bin/env bash

set -eux

# Setting up credentials ...
curl -s -u $PAC_USER:$PAC_PASSWORD https://packages.atlassian.com/api/npm/atlassian-npm/auth/atlassian > ~/.npmrc
mkdir -p ~/.m2 && cp ./settings.xml ~/.m2/settings.xml

# Setting up node ...
fnm use --install-if-missing

# Setting up java ...
export JAVA_HOME="/java/openjdk17"

# Restore original x option (the rest is good for the downstream pipeline to ensure errors and unbound variables are caught)
set +x

# Setup complete
