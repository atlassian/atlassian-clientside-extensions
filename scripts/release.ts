import inquirer from 'inquirer';

import exec from './exec';
import { getDevelopmentVersion, getVersion } from './version-helpers';
import type { Arguments } from 'yargs';

export type Options = Arguments<{
    version: string;
    snapshot?: boolean;
    distTag?: string;
    dry?: boolean;
    yes?: boolean;
    skipBuild?: boolean;
    skipNpmRelease?: boolean;
    skipMvnRelease?: boolean;
    skipNpmDevelopment?: boolean;
    skipPush?: boolean;
}>;

const getDistTag = ({ snapshot, distTag }: Options) => distTag || (snapshot ? 'snapshot' : 'latest');

const gitCommitAll = (message: string) => exec(`git commit --allow-empty -am  "${message}"`);

const executeIf = async (condition: boolean, message: string, callback: () => Promise<void>) => {
    if (!condition) {
        return;
    }

    console.log(`--- ${message} ---`);
    await callback();
};

const confirm = async (
    options: Options,
    { version, developmentVersion, distTag }: { version: string; developmentVersion: string; distTag: string },
) => {
    const promptOptions = JSON.stringify({ ...options, version, developmentVersion, distTag, _: undefined, $0: undefined }, null, '\t');

    if (options.yes) {
        console.log('--- RELEASING WITH OPTIONS ---');
        console.log(promptOptions);

        return;
    }

    const response = await inquirer.prompt([
        {
            name: 'confirm',
            message: `
You're about to release a new version of CSE with the following options:
${promptOptions}
Do you want to continue? y/n`,
            default: 'y',
        },
    ]);

    if (String(response.confirm).toLowerCase() !== 'y') {
        throw new Error('release canceled.');
    }
};

/**
 * RELEASE SCRIPT
 */
export const release = async (options: Options) => {
    console.log('--- STARTING RELEASE ---');

    const version = await getVersion(options.version, options.snapshot);
    const developmentVersion = await getDevelopmentVersion(options.version, options.snapshot);
    const mvnDevelopmentVersion = `${developmentVersion}-SNAPSHOT`; // Maven development version requires the SNAPSHOT suffix
    const distTag = getDistTag(options);

    await confirm(options, { version, developmentVersion, distTag });

    if (options.dry) {
        console.log('--- DRY RUN FINISHED ---');
        return;
    }

    await executeIf(!options.skipBuild, 'BUILDING', async () => {
        // Skip running yarn installation again since at this point we already installed yarn dependencies
        await exec('yarn build -DskipYarnInstall');
    });

    await executeIf(!options.skipNpmRelease, `RELEASING NPM PACKAGES VERSION ${version}`, async () => {
        // Ensure we publish the package to the public registry
        await exec(`echo "@atlassian:registry=https://registry.npmjs.org/" >> ~/.npmrc`);
        await exec(`yarn release:packages:ci ${version} --dist-tag ${distTag}`);
        await gitCommitAll(`Released NPM Packages. Version set: ${version}`);
    });

    await executeIf(!options.skipMvnRelease, `RELEASING MAVEN ARTIFACTS VERSION ${version}`, async () => {
        // release:prepare also sets the git tag to vX.Y.Z
        await exec(`yarn release:mvn:prepare -Dtag=v${version} -DreleaseVersion=${version} -DdevelopmentVersion=${mvnDevelopmentVersion}`);

        await exec(`yarn release:mvn:perform`);
    });

    await executeIf(!options.skipNpmDevelopment, `SETTING NPM PACKAGES DEVELOPMENT VERSION ${developmentVersion}`, async () => {
        await exec(`yarn version:packages:ci ${mvnDevelopmentVersion}`);
        await gitCommitAll(`Preparing NPM Packages for further development. Version set: ${developmentVersion}`);
    });

    await executeIf(!options.skipPush && !options.snapshot, 'PUSHING CHANGES TO GIT', async () => {
        // Push tags first so we can always merge those manually in case the regular git push fails
        await exec('git push --tags');
        await exec('git push');
    });
};
