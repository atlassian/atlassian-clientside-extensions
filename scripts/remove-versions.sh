#!/usr/bin/env bash

set -eu

versions_to_remove=(
    "3.0.3-alpha-e865ba2-m0w9cdsp"
    "3.0.3-alpha-0e47f2e-m0w9q2tr"
    "3.0.3-alpha-226daed-m0wb3b0y"
    "3.0.3-alpha-925bdb8-m0wbxhjz"
    "3.0.3-alpha-6ee859e-m0wdciw5"
    "3.0.3-alpha-9d473bb-m0wdxaa9"
    "3.0.3-alpha-939968a-m0wjdh9q"
    "3.0.3-alpha-39de5e2-m0wk6hwa"
)

for version_to_remove in "${versions_to_remove[@]}"; do
    ./remove-published-version.sh "$version_to_remove" "Published while troubleshooting a pipeline issue"
done

