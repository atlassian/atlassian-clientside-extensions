const base = require('../jest.config.base');
const pack = require('./package.json');

module.exports = {
    ...base,
    displayName: pack.name,
    moduleNameMapper: {
        // Allow import of ".js" files the same way as ".ts" files in order for ESM to work
        "^(\\./.+|\\../.+).js$": "$1"
    }
};
