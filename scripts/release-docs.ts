#!/usr/bin/env node
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import inquirer from 'inquirer';

import exec from './exec.js';

type Options = yargs.Arguments<{
    version: string;
    yes?: boolean;
    skipPush?: boolean;
}>;

const confirm = async (options: Options) => {
    const promptOptions = JSON.stringify({ ...options, _: undefined, $0: undefined }, null, '\t');

    if (options.yes) {
        console.log('--- RELEASING WITH OPTIONS ---');
        console.log(promptOptions);

        return;
    }

    const response = await inquirer.prompt([
        {
            name: 'confirm',
            message: `
You're about to release a new version of CSE Documentation with the following options:
${promptOptions}
Do you want to continue? y/n`,
            default: 'y',
        },
    ]);

    if (String(response.confirm).toLowerCase() !== 'y') {
        throw new Error('release canceled.');
    }
};

/**
 * RELEASE SCRIPT
 */
const release = async (options: Options) => {
    console.log('--- STARTING DOCS RELEASE ---');

    await confirm(options);

    // Yarn doesn't respect publishConfig options, so we need to write the publish registry on ~/.npmrc instead
    // https://github.com/yarnpkg/yarn/issues/5310
    await exec(`echo "@atlassian:registry=https://registry.npmjs.org/" >> ~/.npmrc`);

    await exec(`yarn publish ./documentation --no-git-tag-version --new-version ${options.version}`);

    await exec(`git commit -am  "Released Documentation Package. Version set: ${options.version}"`);

    await exec(`git tag "docs-v${options.version}"`);

    if (!options.skipPush) {
        console.log('--- PUSHING CHANGES TO GIT ---');

        // Push tags first so we can always merge those manually in case the regular git push fails
        await exec('git push --tags');
        await exec('git push');
    }
};

/**
 * CLI CONFIG
 */
// eslint-disable-next-line @typescript-eslint/no-unused-expressions
yargs(hideBin(process.argv))
    .command(
        '$0 <version> [options]',
        'Release a given version of documentation package.',
        (builder) => {
            builder
                // overriding --version flag: https://github.com/yargs/yargs/issues/1213
                .version(false)
                .positional('version', {
                    describe: 'Semver version to release. E.g: 2.1.3',
                    type: 'string',
                })
                .option('yes', {
                    describe: `Skip confirmation and execute release inmediatly.`,
                    type: 'boolean',
                })
                .option('skipPush', {
                    describe: 'Skip pushing changes to git.',
                    type: 'boolean',
                });
        },
        release,
    )
    .parse();
