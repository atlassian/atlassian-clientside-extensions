const path = require('path');

module.exports = {
    rules: {
        'node/shebang': 'off',
        'node/no-unpublished-import': 'off',
        'node/no-extraneous-import': 'off',

        // Disable node/no-missing-import since the rules doesn't work very well with TypeScript codebase that is trying to import a JS file
        // https://github.com/mysticatea/eslint-plugin-node/issues/248
        // Hopefully, we can remove the *.js file extension in TS 4.6+ and remove the disabled rule in next line.
        'node/no-missing-import': "off",

        'no-console': 'off',
        'import/no-extraneous-dependencies': [
            'error',
            {
                devDependencies: true,
                // We need to point to the root package since the local one is used only to verify TypeScript compilation
                packageDir: path.resolve(__dirname, '..'),
            },
        ],
    },
};
