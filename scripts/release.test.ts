import type { Options } from './release';
import { release } from './release';

jest.mock('./exec');
jest.mock('inquirer', () => ({
    prompt: jest.fn().mockResolvedValue({ confirm: 'y' }),
}));

describe('release', () => {
    let logSpy: jest.SpyInstance;

    beforeEach(() => {
        logSpy = jest.spyOn(console, 'log').mockImplementation();
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should run DRY release', async () => {
        await release({
            version: '0.0.1',
            yes: false,
            dry: true,
            snapshot: false,
            distTag: 'latest',
            skipBuild: false,
            skipNpmRelease: false,
            skipMvnRelease: false,
            skipNpmDevelopment: false,
            skipPush: false,
        } as Options);

        expect(logSpy).toHaveBeenCalledWith('--- STARTING RELEASE ---');
        expect(logSpy).toHaveBeenCalledWith('--- DRY RUN FINISHED ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- BUILDING ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- RELEASING NPM PACKAGES VERSION 0.0.1 ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- RELEASING MAVEN ARTIFACTS VERSION 0.0.1 ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- SETTING NPM PACKAGES DEVELOPMENT VERSION 0.0.2 ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- PUSHING CHANGES TO GIT ---');
    });

    it('should run release', async () => {
        await release({
            version: '0.0.1',
            yes: true,
            dry: false,
            snapshot: false,
            distTag: 'latest',
            skipBuild: false,
            skipNpmRelease: false,
            skipMvnRelease: false,
            skipNpmDevelopment: false,
            skipPush: false,
        } as Options);

        expect(logSpy).toHaveBeenCalledWith('--- STARTING RELEASE ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- DRY RUN FINISHED ---');
        expect(logSpy).toHaveBeenCalledWith('--- BUILDING ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING NPM PACKAGES VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING MAVEN ARTIFACTS VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- SETTING NPM PACKAGES DEVELOPMENT VERSION 0.0.2 ---');
        expect(logSpy).toHaveBeenCalledWith('--- PUSHING CHANGES TO GIT ---');
    });

    it('should run release skipping the build', async () => {
        await release({
            version: '0.0.1',
            yes: true,
            dry: false,
            snapshot: false,
            distTag: 'latest',
            skipBuild: true,
            skipNpmRelease: false,
            skipMvnRelease: false,
            skipNpmDevelopment: false,
            skipPush: false,
        } as Options);

        expect(logSpy).toHaveBeenCalledWith('--- STARTING RELEASE ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- BUILDING ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING NPM PACKAGES VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING MAVEN ARTIFACTS VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- SETTING NPM PACKAGES DEVELOPMENT VERSION 0.0.2 ---');
        expect(logSpy).toHaveBeenCalledWith('--- PUSHING CHANGES TO GIT ---');
    });

    it('should run release skipping the npm release', async () => {
        await release({
            version: '0.0.1',
            yes: true,
            dry: false,
            snapshot: false,
            distTag: 'latest',
            skipBuild: false,
            skipNpmRelease: true,
            skipMvnRelease: false,
            skipNpmDevelopment: false,
            skipPush: false,
        } as Options);

        expect(logSpy).toHaveBeenCalledWith('--- STARTING RELEASE ---');
        expect(logSpy).toHaveBeenCalledWith('--- BUILDING ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- RELEASING NPM PACKAGES VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING MAVEN ARTIFACTS VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- SETTING NPM PACKAGES DEVELOPMENT VERSION 0.0.2 ---');
        expect(logSpy).toHaveBeenCalledWith('--- PUSHING CHANGES TO GIT ---');
    });

    it('should run release skipping the maven release', async () => {
        await release({
            version: '0.0.1',
            yes: true,
            dry: false,
            snapshot: false,
            distTag: 'latest',
            skipBuild: false,
            skipNpmRelease: false,
            skipMvnRelease: true,
            skipNpmDevelopment: false,
            skipPush: false,
        } as Options);

        expect(logSpy).toHaveBeenCalledWith('--- STARTING RELEASE ---');
        expect(logSpy).toHaveBeenCalledWith('--- BUILDING ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING NPM PACKAGES VERSION 0.0.1 ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- RELEASING MAVEN ARTIFACTS VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- SETTING NPM PACKAGES DEVELOPMENT VERSION 0.0.2 ---');
        expect(logSpy).toHaveBeenCalledWith('--- PUSHING CHANGES TO GIT ---');
    });

    it('should run release skipping the npm development', async () => {
        await release({
            version: '0.0.1',
            yes: true,
            dry: false,
            snapshot: false,
            distTag: 'latest',
            skipBuild: false,
            skipNpmRelease: false,
            skipMvnRelease: false,
            skipNpmDevelopment: true,
            skipPush: false,
        } as Options);

        expect(logSpy).toHaveBeenCalledWith('--- STARTING RELEASE ---');
        expect(logSpy).toHaveBeenCalledWith('--- BUILDING ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING NPM PACKAGES VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING MAVEN ARTIFACTS VERSION 0.0.1 ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- SETTING NPM PACKAGES DEVELOPMENT VERSION 0.0.2 ---');
        expect(logSpy).toHaveBeenCalledWith('--- PUSHING CHANGES TO GIT ---');
    });

    it('should run release skipping the push', async () => {
        await release({
            version: '0.0.1',
            yes: true,
            dry: false,
            snapshot: false,
            distTag: 'latest',
            skipBuild: false,
            skipNpmRelease: false,
            skipMvnRelease: false,
            skipNpmDevelopment: false,
            skipPush: true,
        } as Options);

        expect(logSpy).toHaveBeenCalledWith('--- STARTING RELEASE ---');
        expect(logSpy).toHaveBeenCalledWith('--- BUILDING ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING NPM PACKAGES VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- RELEASING MAVEN ARTIFACTS VERSION 0.0.1 ---');
        expect(logSpy).toHaveBeenCalledWith('--- SETTING NPM PACKAGES DEVELOPMENT VERSION 0.0.2 ---');
        expect(logSpy).not.toHaveBeenCalledWith('--- PUSHING CHANGES TO GIT ---');
    });
});
