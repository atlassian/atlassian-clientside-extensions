import type { Options, Response } from '../exec';

export default (command: string, { echo }: Options = { echo: true }): Promise<Response> => {
    if (echo) {
        console.log(command);
    }

    return Promise.resolve({ code: 0, data: '' });
};
