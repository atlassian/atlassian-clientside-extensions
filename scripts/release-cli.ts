#!/usr/bin/env node
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { getAsyncHandler } from './yargs-helper';
import { release } from './release';

export type Options = yargs.Arguments<{
    version: string;
    snapshot?: boolean;
    distTag?: string;
    dry?: boolean;
    yes?: boolean;
    skipBuild?: boolean;
    skipNpmRelease?: boolean;
    skipMvnRelease?: boolean;
    skipNpmDevelopment?: boolean;
    skipPush?: boolean;
}>;

/**
 * CLI CONFIG
 */
// eslint-disable-next-line @typescript-eslint/no-unused-expressions
yargs(hideBin(process.argv))
    .command<Options>(
        '$0 <version> [options]',
        'Release a given version and bumps the project to the next minor version for development.',
        (builder) => {
            builder
                // overriding --version flag: https://github.com/yargs/yargs/issues/1213
                .version(false)
                .positional('version', {
                    describe: 'Semver version to release. E.g: 2.1.3',
                    type: 'string',
                })
                .option('yes', {
                    describe: `Skip confirmation and execute release immediately.`,
                    type: 'boolean',
                })
                .option('dry', {
                    describe: `Calculate the version changes but skip all steps.`,
                    type: 'boolean',
                })
                .option('snapshot', {
                    describe: `Release a snapshot version for canary testing.`,
                    type: 'boolean',
                })
                .option('distTag', {
                    describe: 'Specify a distribution tag for the npm packages.',
                    type: 'string',
                })
                .option('skipBuild', {
                    describe: 'Skip build step.',
                    type: 'boolean',
                })
                .option('skipNpmRelease', {
                    describe: 'Skip releasing npm packages.',
                    type: 'boolean',
                })
                .option('skipMvnRelease', {
                    describe: 'Skip releasing maven artifacts.',
                    type: 'boolean',
                })
                .option('skipNpmDevelopment', {
                    describe: 'Skip bumping npm packages for development.',
                    type: 'boolean',
                })
                .option('skipPush', {
                    describe: 'Skip pushing changes to git.',
                    type: 'boolean',
                });
        },
        getAsyncHandler<Options>(release),
    )
    .parse();
