#!/usr/bin/env bash

set -eu

VERSION=$1
MESSAGE=$2

echo "Deprecating published packages for version $VERSION with message: $MESSAGE ..."
echo "Please stay alert and follow the NPM authorisation prompts as needed."

# We need to remove the packages in the reverse order of their dependencies but for now it won't work either way so this "random" order is fine
package_names_in_removal_order=(
    "@atlassian/clientside-extensions"
    "@atlassian/clientside-extensions-base"
    "@atlassian/clientside-extensions-components"
    "@atlassian/clientside-extensions-debug"
    "@atlassian/clientside-extensions-demo-extensions-basic"
    "@atlassian/clientside-extensions-demo-extensions-complex"
    "@atlassian/clientside-extensions-demo-product"
    "@atlassian/clientside-extensions-demo-refapp"
    "@atlassian/clientside-extensions-docs"
    "@atlassian/clientside-extensions-monorepo"
    "@atlassian/clientside-extensions-page-bootstrapper"
    "@atlassian/clientside-extensions-registry"
    "@atlassian/clientside-extensions-runtime"
    "@atlassian/clientside-extensions-schema"
    "@atlassian/clientside-extensions-scripts"
    "@atlassian/clientside-extensions-webpack-plugin"
)

for package_name in "${package_names_in_removal_order[@]}"; do
    package_identifier="$package_name@$VERSION"
    echo "Processing $package_identifier (https://www.npmjs.com/package/$package_name/v/$VERSION)..."

    (echo "Attempting deprecate ... " && npm deprecate "$package_name@$VERSION" "$MESSAGE") ||
    echo "Deprecate attempt failed!"
done

