#!/usr/bin/env bash

set -eu

# key is provided via pipelines repo variable and needs to be registered with gpg agent
echo "$GPG_SIGNING_KEY" | base64 --decode | gpg --import --batch
# specify public keyserver
echo "keyserver hkp://pgp.mit.edu" > ~/.gnupg/dirmngr.conf
# set pinentry-mode loopback - this is needed to circument some TTY/ioctl device issue
echo "pinentry-mode loopback" > ~/.gnupg/gpg.conf
