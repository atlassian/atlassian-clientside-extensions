/* eslint-disable  node/no-unpublished-import */
// https://github.com/mattphillips/jest-chain#setup
import 'jest-chain';

// https://github.com/jest-community/jest-extended#setup
import * as matchers from 'jest-extended';

expect.extend(matchers);
