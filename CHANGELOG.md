# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.1.0] - 2024-08-28

### Fixed

-   [CSE-164](https://ecosystem.atlassian.net/browse/CSE-164) Upgrade to node v20
    -   Update pipelines to use `sfp-base-agent` image with node v20
-   [VULN-1340404](https://asecurityteam.atlassian.net/browse/VULN-1340404) Upgrade rimraf and glob dependency

## [3.0.2] - 2024-07-23

### Fixed

-   [VULN-1281521](https://asecurityteam.atlassian.net/browse/VULN-1281521) Upgrade chokidar and transitive braces dependency

## [3.0.0] - 2024-04-17

### Fixed

-   [BSP-5256](https://bulldog.internal.atlassian.com/browse/BSP-5256) Fix vulnerability in loader-utils
-   [BSP-5240](https://bulldog.internal.atlassian.com/browse/BSP-5240) Fix vulnerability in commons-fileupload:commons-fileupload
-   [BSP-5209](https://bulldog.internal.atlassian.com/browse/BSP-5209) Fix vulnerability in org.codehaus.jettison:jettison
-   [VULN-1111940](https://asecurityteam.atlassian.net/browse/VULN-1111940) Fix vulnerability in atlaskit/button and atlaskit/modal-dialog

### Changed

-   [BSP-5215](https://bulldog.internal.atlassian.com/browse/BSP-5215) Updated `atlassian-plugins-osgi`, `atlassian-plugins-api`, `atlassian-plugins-core`, `atlassian-plugins-servlet` from `5.7.1` to `7.2.1`
-   Update `@atlaskit/modal-dialog` dependency in `@atlassian/clientside-extensions-components` from version `~12.0.2` to `~12.2.0`.
-   Upgrade from TypeScript 3 to TypeScript 4 and migrate the codebase to use TypeScript 4.
-   [DCPL-925] Adopt new WRM in major version of atlassian-clientside-extensions
-   [DCPL-936] Adopt new Webfragment in major version of atlassian-clientside-extensions
-   [DCPL-968] Adopt new Atlassian Plugins in major version of Atlassian Clientside Extensions
-   [DCPL-1010] Update Master to the next minor `3.1.0` after the branch cut of `3.0.x` for Platform 7 adoption

## [2.4.5] - 2023-09-04

### Security

-   [BSP-5256](https://bulldog.internal.atlassian.com/browse/BSP-5256) Fix vulnerability in loader-utils
-   [BSP-5240](https://bulldog.internal.atlassian.com/browse/BSP-5240) Fix vulnerability in commons-fileupload:commons-fileupload
-   [BSP-5209](https://bulldog.internal.atlassian.com/browse/BSP-5209) Fix vulnerability in org.codehaus.jettison:jettison

## [2.4.4] - 2023-08-07

### Added

-   Debug logging from the clientside-extensions webpack plugin when the environment variable `CSE_DEBUG=true` is set.
-   Update `@atlaskit/button` dependency from version ``~16.1.2` to `~16.8.2`.
-   Update `@atlaskit/modal-dialog` dependency from version `~12.2.0` to `~12.6.4`.
-   Update `@atlaskit/theme` dependency from version `~12.0.0` to `~12.5.5`.

## [2.4.3] - 2022-07-21

### Fixed

-   Prevent excluding files if the working directory is contained within a `target` directory

    Fixes an issue where client side extensions were not found when performing a maven release

## [2.4.2] - 2022-04-21

### Fixed

-   [CSE-149](https://ecosystem.atlassian.net/browse/CSE-149) Fix web-items gotten from web-fragments by adding the context-path to the URL.
-   [BSP-3582] Update dependency `ansi-regex` to 5.0.1 to address security vulnerability.
-   [BSP-3777](https://bulldog.internal.atlassian.com/browse/BSP-3777) Upgraded minimist to 1.2.6
-   [BSP-3799](https://bulldog.internal.atlassian.com/browse/BSP-3799) excluded spring-expression
-   [BSP-3809](https://bulldog.internal.atlassian.com/browse/BSP-3809) Upgraded nth-check to 2.0.1

## [2.4.1] - 2021-10-18

### Removed

-   [SPFE-848](https://ecosystem.atlassian.net/browse/SPFE-848) Remove unused `rehype-add-classes` dependency from the `@atlassian/clientside-extensions-schema` package
-   [SPFE-848](https://ecosystem.atlassian.net/browse/SPFE-848) Move `@atlassian/clientside-extensions-schema` dependency to `devDependency` in the `@atlassian/clientside-extensions-components` package. We only import TS types from `schema` there.

## [2.4.0] - 2021-10-12

### Changed

-   Update `@atlaskit/button` dependency in `@atlassian/clientside-extensions-components` from version `15.1.5` to `~16.1.2`.
-   Update `@atlaskit/modal-dialog` dependency in `@atlassian/clientside-extensions-components` from version `~11.6.0` to `~12.0.2`.
-   Update `@atlaskit/theme` dependency in `@atlassian/clientside-extensions-components` from version `~11.2.5` to `~12.0.0`.

### Removed

-   The `@atlassian/clientside-extensions-components` package does not longer require a `styled-components` as a peer dependency.

    After you update all the CSE NPM packages to the latest version you can check if you still need to use `styled-components` with one of the following commands:

    -   `npm ls styled-components` NPM < 7
    -   `npm why styled-components` NPM 7+
    -   `yarn why styled-components` Yarn

## [2.3.2]

### Security

-   [BSP-5209](https://bulldog.internal.atlassian.com/browse/BSP-5209) Fix vulnerability in org.codehaus.jettison:jettison

## [2.3.0] - 2021-06-23

With release 2.3, we focused on updating the dependencies. We updated most of the direct, internal and build dependencies to stay up to
date. The CSE 2.3 is also fixing any known vulnerability issues detected in both NPM and Java external dependencies.

The JavaScript code of `atlassian-clientside-extensions-runtime` and `atlassian-clientside-extensions-page-bootstrapper` Java P2 plugins is
now bundled using webpack 5 instead of webpack 4.

### Added

-   [SPFE-494](https://ecosystem.atlassian.net/browse/SPFE-494) [NPM] Add support for webpack 5 to the `@atlassian/clientside-extensions-schema` package.

### Changed

-   [SPFE-517](https://ecosystem.atlassian.net/browse/SPFE-517) Update `Refapp` from version `5.2.5` to version `5.3.0`

### Java dependency updates:

-   [SPFE-542](https://ecosystem.atlassian.net/browse/SPFE-542) Update `com.atlassian.platform.platform` dependency from version `5.1.4` to `5.2.1`
-   [SPFE-542](https://ecosystem.atlassian.net/browse/SPFE-542) Update `com.atlassian.plugins.atlassian-plugins-webresource` dependency from version `4.3.3` to `4.3.4`
-   [SPFE-542](https://ecosystem.atlassian.net/browse/SPFE-542) Update `org.codehaus.jackson` dependency from version `1.9.13-atlassian-5` to `1.9.14-atlassian-6`
-   [SPFE-542](https://ecosystem.atlassian.net/browse/SPFE-542) Update internal development and build dependencies

#### NPM dependency updates:

-   [SPFE-542](https://ecosystem.atlassian.net/browse/SPFE-542) Update `@atlaskit/modal-dialog` dependency in `@atlassian/clientside-extensions-components` from version `11.3.0` to `~11.6.0`.
-   [SPFE-542](https://ecosystem.atlassian.net/browse/SPFE-542) Update `@atlaskit/modal-theme` dependency in `@atlassian/clientside-extensions-components` from version `11.1.0` to `~11.2.0`.
-   [SPFE-542](https://ecosystem.atlassian.net/browse/SPFE-542) Update internal development and build dependencies
-   [BSP-3777](https://bulldog.internal.atlassian.com/browse/BSP-3777) Upgraded minimist to 1.2.6
-   [BSP-3809](https://bulldog.internal.atlassian.com/browse/BSP-3809) Upgraded nth-check to 2.0.1

### Fixed

-   [SPFE-542](https://ecosystem.atlassian.net/browse/SPFE-542) Add a missing `styled-components` peer dependency to `@atlassian/clientside-extensions-components` version `~4.4.1`
-   [SPFE-517](https://ecosystem.atlassian.net/browse/SPFE-517) Fix compatibility issues when CSE `@atlassian/clientside-extensions-runtime` and `@atlassian/clientside-extensions-debug` packages are in a different version range.
-   [BSP-3799](https://bulldog.internal.atlassian.com/browse/BSP-3799) excluded spring-expression

## [2.2.2]

### Security

-   [BSP-5209](https://bulldog.internal.atlassian.com/browse/BSP-5209) Fix vulnerability in org.codehaus.jettison:jettison

## [2.2.0] - 2021-06-04

Client-Side Extensions can now render web-items and web-panels using Custom Elements.

[Read the CSE 2.2 upgrade guide](./guides/upgrade-cse-2_2_0.md) for details on migrating test and production code.

### Added

-   [SPFE-336](https://ecosystem.atlassian.net/browse/SPFE-336) Added new API methods to the registry called `registerHandler` and `getHandler` together with two new custom-elements `cse-register-web-items-element` and `cse-register-web-panels-element`.
    The motivation behind these changes is to allow products to migrate existing web-fragment locations to be handled by Clientside-Extension. Basically allowing a migration away from Web-Fragments towards Clientside-Extensions.
    To achieve this, the custom-elements allow products to register existing web-fragments through simple dom-markup with the Clientside-Extension runtime. On the other end of this are handlers that are registered through the registry to handle these web-fragments.
-   [SPFE-336](https://ecosystem.atlassian.net/browse/SPFE-336) Add new option `validator-only` to the `generate-from-schema` binary. If set the generator will only create validators for the matching schema files.

## [2.1.3] - 2023-08-30

### Security

-   [BSP-5256](https://bulldog.internal.atlassian.com/browse/BSP-5256) Fix vulnerability in loader-utils

## [2.1.2] - 2023-08-17

### Security

-   [BSP-5240](https://bulldog.internal.atlassian.com/browse/BSP-5240) Fix vulnerability in commons-fileupload:commons-fileupload
-   [BSP-5209](https://bulldog.internal.atlassian.com/browse/BSP-5209) Fix vulnerability in org.codehaus.jettison:jettison

## [2.1.1] - 2022-04-20

### Fixed

-   [CSE-124](https://ecosystem.atlassian.net/browse/CSE-124) The discover "bubble" for extension points has now a correct "type" set, and will no longer submit forms when clicked.
-   [BSP-3734](https://bulldog.internal.atlassian.com/browse/BSP-3734) Excluding Spring Beans dependency as it has provided scope.
-   [BSP-3777](https://bulldog.internal.atlassian.com/browse/BSP-3777) Upgrade minimist to 1.2.6
-   [BSP-3799](https://bulldog.internal.atlassian.com/browse/BSP-3799) excluded spring-expression
-   [BSP-3809](https://bulldog.internal.atlassian.com/browse/BSP-3809) Upgraded nth-check to 2.0.1

## [2.1.0] - 2021-05-11

No changes, mark the release `2.1.0` as stable. Refer to the previous `rc` releases to read more about the changes in the version.

## [2.1.0-rc-2] - 2021-05-05

### Changed

-   [CSE-117](https://ecosystem.atlassian.net/browse/CSE-117) Remove the non-published features from the CSE registry module.
-   [CSE-101](https://ecosystem.atlassian.net/browse/CSE-101) Update `@atlaskit/modal-dialog` component to version `11.3.0` and fix issue with displaying modals on Safari browser.

## [2.1.0-rc-1] - 2021-04-30

### Added

-   [CSE-100](https://ecosystem.atlassian.net/browse/CSE-100) Add a new `setLogger` function to the `debug` package that allows switching a default logger.
-   [CSE-100](https://ecosystem.atlassian.net/browse/CSE-100) Add a new `getLogLevel` and `setLogLevel` functions to the `debug` package that allows changing default logger level.
-   [CSE-116](https://ecosystem.atlassian.net/browse/CSE-116) User-provided callbacks to `onMount`, `onUnmount`, `onClose` functions will guard against runtime errors and exceptions.

### Fixed

-   [CSE-114](https://ecosystem.atlassian.net/browse/CSE-114) Fix invalid `*.d.ts` files generated by the `schema` CLI tooling.
-   [CSE-114](https://ecosystem.atlassian.net/browse/CSE-114) Improve the compilation time of the schemas `*.d.ts` files. The schema CLI tooling will generate the `*.d.ts` files in 1.5-2 seconds instead 7-8 seconds.
-   [CSE-112](https://ecosystem.atlassian.net/browse/CSE-114) Fix the race conditions when CSE was running in development mode with webpack-dev-server.
    While the application is using webpack-dev-server the CSE runtime wasn't aware that web-resources are loaded from the dev server. That was causing race conditions when the extension was trying to register.
    The side effects of the race condition were causing the unwanted validation errors.

## [2.0.7] - 2021-05-04

### Fixed

-   [CSE-101](https://ecosystem.atlassian.net/browse/CSE-101) While working on the fix, we forgot to update the `@atlaskit/modal-dialog` dependency. This time, it's updated. For real!

## [2.0.6] - 2021-04-15

### Fixed

-   [CSE-111](https://ecosystem.atlassian.net/browse/CSE-111) Fix the compatibility issue between CSE 1 and 2 in the `debug` package.
-   [CSE-101](https://ecosystem.atlassian.net/browse/CSE-101) Update `@atlaskit/modal-dialog` component to version `11.3.0` and fix issue with displaying modals on Safari browser.

## [2.0.5] - 2021-03-18

### Fixed

-   [No Issue] Consume WRM.require via the `wrm/require` module to include a proper dependency.

    This caused issues in the refapp, as WRM.require was being used by the CSE-registry but not included globally causing failures.

## [2.0.4] - 2021-03-12

### Fixed

-   [CSE-110](https://ecosystem.atlassian.net/browse/CSE-110) Fix broken CSE 1.x and CSE 2.x runtime compatibility.

    The CSE 1.x runtime requires a web-item to be generated for the extension to be loaded on the page. We removed the generation of web-items in pull request #195.

## [2.0.3] - 2021-03-04

### Fixed

-   [CSE-107](https://ecosystem.atlassian.net/browse/CSE-107) Generate more unique XML filenames for both `runtime` and `page-bootstrapper` modules.

## [2.0.2] - 2021-02-19

No changes, release previously broken release.

## [2.0.1] - 2021-02-18

No changes, release previously broken release.

## [2.0.0] - 2021-02-18

The Client-Side Extensions 2.0 introduces a new schema validation based on the GraphQL syntax. The previously used JSON schema syntax wasn't good enough to validate the extension points' more complex structures.

The extension developers are not directly affected by this change and can continue using the CSE 1.x. The new schema format and validation tooling are breaking changes for the extension owners only. The existing owners are mostly the Atlassian product developers.

However, if you are a plugin developer, we recommend you upgrade to the latest CSE version so that you can get better support for TypeScript.

You can read more about the GraphQL schemas on the documentation page:

-   [How to use Schemas](https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/extension-points/schemas/)
-   [Setting up the CSE schema-loader](https://developer.atlassian.com/server/framework/clientside-extensions/guides/how-to/setup-schema-loader/)

### Added

-   **FEP-765**: Introduce a new validation based on GraphQL schema. We added a new `@atlassian/clientside-extensions-schema` package that provides:

    -   a webpack loader for `.graphql` files that generate React components and hooks that can be used to display extension points
    -   static generators for built-in GraphQL scalars
    -   static generators for `*.graphl` files that generate validators and React components

-   [CSE-75](https://ecosystem.atlassian.net/browse/CSE-75): Some of the extensions handlers are now more useful, and it is
    encouraged to use them:

    -   The `ButtonHandler` can now be used to render a properly styled Button
    -   The `LinkHandler` can now be used to render a properly styled Link
    -   The `SectionHandler` can now be used to render a section

-   [CSE-75](https://ecosystem.atlassian.net/browse/CSE-75): A new convenience handler for `Modals` was introduced that renders an action handler, either a `button` or a `link`,
    that will take care of the lifecycle of the modal. It is exported as `ModalWithActionHandler`.

-   [CSE-75](https://ecosystem.atlassian.net/browse/CSE-75): A `ExtensionType` enum is exported containing the named identifiers for extensions.
    This can be used to check if a extension descriptor is of a certain type. It can be imported as `import { ExtensionType } from '@atlassian/clientside-extensions-components';`.
    Currently, it supports:

    -   `ExtensionType.asyncPanel`
    -   `ExtensionType.button`
    -   `ExtensionType.link`
    -   `ExtensionType.modal`
    -   `ExtensionType.page`
    -   `ExtensionType.panel`
    -   `ExtensionType.section`

### Removed

-   **FEP-765**: We removed the support for JSON schema used to document and validate extension points APIs. Use the GraphQL based schema instead.

-   [CSE-44](https://ecosystem.atlassian.net/browse/CSE-44) **[BREAKING CHANGE]**: We dropped support for Node 10. The CSE Node packages requires a Node version 12.19 or newer.

-   [CSE-4](https://ecosystem.atlassian.net/browse/CSE-4) **[BREAKING CHANGE]**: We removed the `MountProps` types from the `PanelExtension`
    and `ModalExtension` types. The `MountProps` types used to be an empty interfaces that are no longer needed.

-   [CSE-75](https://ecosystem.atlassian.net/browse/CSE-75) **[BREAKING CHANGE]**: The handlers no longer export the constant `type`.
    This has been moved to an enum called `ExtensionType` which can be accessed from the package directly. See _Added_ section

### Changed

-   [CSE-79](https://ecosystem.atlassian.net/browse/CSE-79) **[BREAKING CHANGE]**: We updated the supported version of JavaScript in generated code. All the Node and NPM packages are now compiled to **ES9 (ES2018)**.

-   [CSE-75](https://ecosystem.atlassian.net/browse/CSE-75) **[BREAKING CHANGE]**: Some of the TypeScript components within the
    `@atlassian/clientside-extensions-components` package were renamed.

    -   `ButtonHandler.ButtonRenderer` => `ButtonHandler`
    -   `LinkHandler.LinkRenderer` => `LinkHandler`
    -   `PanelHandler.PanelRenderer` => `PanelHandler`
    -   `ModalHandler.ModalRenderer` => `ModalHandler`
    -   `AsyncPanelHandler.AsyncPanelRenderer` => `AsyncPanelHandler`
    -   `SectionHandler.SectionRenderer` => `SectionHandler`

-   [CSE-4](https://ecosystem.atlassian.net/browse/CSE-4) **[BREAKING CHANGE]**: The `renderElementAsReact` utility function was moved from
    the `@atlassian/clientside-extensions` to the `@atlassian/clientside-extensions-components` package. Please update your imports.

    Example:

    -   `import { renderElementAsReact } from '@atlassian/clientside-extensions'` becomes
        `import { renderElementAsReact } from '@atlassian/clientside-extensions-components'`

-   [CSE-4](https://ecosystem.atlassian.net/browse/CSE-4) **[BREAKING CHANGE]**: Some of the TypeScript types were moved from
    `@atlassian/clientside-extensions` to the `@atlassian/clientside-extensions-components` package or vice versa. If you are using
    TypeScript in your project please update the types. Example:

    -   `PanelHandler.PanelRenderExtension` => `PanelExtension.PanelRenderExtension`
    -   `ModalHandler.ModalRenderExtension` => `ModalExtension.ModalRenderExtension`
    -   `ModalHandler.ModalAction` => `ModalExtension.ModalAction`

-   [CSE-23](https://ecosystem.atlassian.net/browse/CSE-23): WRM webpack plugin is now a peer dependency of CSE webpack plugin. This will
    prevent CSE from installing its own version of the plugin, causing issues in build time if the consumer uses a non-compatible version.

-   [CSE-8](https://ecosystem.atlassian.net/browse/CSE-8) **[BREAKING CHANGE]**: Renamed the remaining `plugin*` keywords to `extension*` in
    TypeScript types and internal code.

## [1.2.9] - 2021-05-04

### Fixed

-   [CSE-101](https://ecosystem.atlassian.net/browse/CSE-101) While working on the fix, we forgot to update the `@atlaskit/modal-dialog` dependency. This time, it's updated. For real!
-   [BSP-3582] Update dependency `ansi-regex` to 5.0.1 to address security vulnerability.

## [1.2.8] - 2021-04-27

### Fixed

-   Update dependency `ajv` from `6.11.0` to `6.12.6` to address security vulnerability.

## [1.2.7] - 2021-04-15

### Fixed

-   [CSE-101](https://ecosystem.atlassian.net/browse/CSE-101) Update `@atlaskit/modal-dialog` component to version `11.3.0` and fix issue with displaying modals on Safari browser.
-   [CSE-111](https://ecosystem.atlassian.net/browse/CSE-111) Fix the compatibility issue between CSE 1 and 2 in the `debug` package.

## [1.2.6] - 2021-03-04

### Fixed

-   [CSE-107](https://ecosystem.atlassian.net/browse/CSE-107) Generate more unique XML filenames for both `runtime` and `page-bootstrapper` modules.

## [1.2.5] - 2021-01-18

No changes, release previously broken release.

## [1.2.4] - 2021-01-18

### Fixed

-   [CSE-92](https://ecosystem.atlassian.net/browse/CSE-92) Remove the direct dependency on webpack in CSE webpack plugin and move it to peer dependencies.

## [1.2.3] - 2020-10-07

No changes, release previously broken release.

## [1.2.2] - 2020-10-07

### Fixed

-   [Issue 7](https://bitbucket.org/atlassian/atlassian-clientside-extensions/issues/7/the-plugin-does-not-work-when-build-on): fixed generating valid value of `key` attribute for `web-item` descriptor key when running webpack on Windows OS.
-   [CSE-11](https://ecosystem.atlassian.net/browse/CSE-31): Fixed loading all the web-resources defined by the `web-page` module descriptor.

## [1.2.1] - 2020-09-16

### Fixed

-   [CSE-31](https://ecosystem.atlassian.net/browse/CSE-31): Fixed the module resolution for the dependent all CSE Node packages.
    Now the CSE packages will use the same dependant versions of all CSE Node packages.
-   [CSE-39](https://ecosystem.atlassian.net/browse/CSE-39): Fixed the broken builds of Node ESM modules.

## [1.2.0] - 2020-09-08

### Added

-   You can now use Client-Side Extensions to create custom pages in server products. We added a new `PageExtension` extension that allows
    creating custom web pages and will render a navigation link in the product UI.

    Refer to [the guides of creating a new page](https://developer.atlassian.com/server/framework/clientside-extensions/guides/introduction/creating-a-page/)
    and [the guide of installing the `page-bootstrapper` package](https://developer.atlassian.com/server/framework/clientside-extensions/guides/how-to/setup-page-bootstrapper/).

### Fixed

-   [SFP-126](https://ecosystem.atlassian.net/browse/SFP-126) The discovery blue bubble no longer has the dark border around it in Chrome.
-   Provide the named chunk for validation module that is loaded by dynamic import.
-   [SFP-139](https://ecosystem.atlassian.net/browse/SFP-139) CSE webpack plugin will show an appropriate error message when specifying an empty
    or invalid condition annotation.

## [1.1.4] - 2020-09-16

### Fixed

-   [CSE-31](https://ecosystem.atlassian.net/browse/CSE-31): Fixed the module resolution for the dependent CSE node packages.
    Now the CSE packages will use the same dependant versions of all CSE Node packages.

## [1.1.3] - 2020-08-19

### Fixed

-   [SFP-172](https://ecosystem.atlassian.net/browse/SFP-172): fixed factory type definitions. Now extension developers won't need to specify
    the API types for `onAction` method when using Panel/Modal/AsyncPanel factories.
-   [SFP-172](https://ecosystem.atlassian.net/browse/SFP-172): fixed problem with `renderElementAsReact` type definitions when using additional props.

## [1.1.2] - 2020-06-30

-   Re-release broken release.

## [1.1.1] - 2020-06-30

### Fixed

-   [Issue #5](https://bitbucket.org/atlassian/atlassian-clientside-extensions/issues/5): fixed usage of more complex conditions with webpack plugin.
    The usage of `@condition` annotation will now generate the proper XML output for both `web-item` and `web-resource` module descriptors.

## [1.1.0] - 2020-06-11

### Added

-   We added support for `AsyncPanelExtension` extension type and `AsyncPanelRenderer` component helper and promoted modules to the public APIs.
    You can read more in the [official documentation for those modules](https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/async-panel-api/).
-   Added support to use the key of an extension in its metadata by using the `$key` word.

### Fixed

-   [SFP-99](https://ecosystem.atlassian.net/browse/SFP-99) The attributes' provider will no longer crash the extension runtime when returning a non-object value.

### Removed

-   The unused `options` property from the `Modal` and `Panel` render callbacks has been removed.

## [1.0.5] - 2020-09-16

### Fixed

-   [CSE-31](https://ecosystem.atlassian.net/browse/CSE-31): Fixed the module resolution for the dependent CSE node packages.
    Now the CSE packages will use the same dependant versions of all all CSE Node packages.

## [1.0.4] - 2020-05-27

### Fixed

-   We fixed an issue with webpack plugin not generating the `<link>` element on the `web-item` when using `LinkExtension`.

## [1.0.3] - 2020-05-19

### Fixed

-   Fixed an issue with context propagation. Previously, when the context value was updated, then the new value wasn't propagated to the extension provider function.
    This could cause the extension to use the stale context value.

## [1.0.2] - 2020-04-22

### Fixed

-   [Issue 2](https://bitbucket.org/atlassian/atlassian-clientside-extensions/issues/2/v101-file-dist-modalextensiondts-has): fixed `ModalExtension.d.ts` wrong definition for `@atlassian/clientside-extensions` package.

## [1.0.1] - 2020-04-08

### Fixed

-   All methods of the `useExtensions` are now optionally generic and accept the shape of `attributes` the developer expects to receive.
-   `useExtensions*` hooks no longer returns an empty list of descriptors when finished loading, but instead will only return after it finished processing the descriptors.
-   `AsyncPanelHandler` no longer fails to render if there's an error with the renderer function of the async module loaded.

## [1.0.0] - 2020-02-25

First stable release of Client-side Extensions 🎉.

No changes were made from 1.0.0-rc-1.

## [1.0.0-rc-1] - 2020-02-21

These are the last set of breaking changes before our 1.0 release.

### Changed

-   **BREAKING CHANGE** `ExtensionPointInfo` now takes the schemas as a prop instead of getting them from the registry.

### Removed

-   **BREAKING CHANGE** Registry no longers saves a copy of extension schemas. If you're upgrading your registry version, you must also use the new version of ExtensionPointInfo.
-   **BREAKING CHANGE** Removed deprecated Modal options. You should now use the same methods provided in the ModalAPI instead.

## [0.9.0] - 2020-02-18

### Added

-   `setActions` and `onClose` methods has been added to the top level ModalAPI. They will replace the ones provided as "options" in onMount modal method.
-   `setWidth` and `setAppearance` methods have been added to ModalAPI.
-   `setAction` ModalAPI now can an accept a new `testId` property. You can use this property to write unique selectors while working on the e2e tests.

### Fixed

-   Runtime no longer bundles React/AJV.
-   The existing JSON schema fields like `label`, `url`, `title` are not getting overwritten with default values.
-   Attributes are now validated also when calling `updateAttributes`.

### Changed

-   **BREAKING CHANGE**: The `useDebug`, `useValidation`, `useLogging`, `useDiscovery` React hooks were removed from the
    `@atlassian/clientside-extensions-debug` package and moved to the `@atlassian/clientside-extensions-components` package.
-   Runtime no longer validates the type of extensions in production mode.

### Deprecated

-   `setActions` and `onClose` Modal Options API is deprecated in favor for their counterparts in ModalAPI. They are still available
    for you to use, but will be removed on a later version.

## [0.8.1] - 2020-01-29

### Fixed

-   Releasing packages without `dist` folders. There was a problem with our pipelines, so 0.8.0 got released incorrectly.

## [0.8.0] - 2020-01-29

### Added

-   Error boundaries to protect products from crashing when something goes wrong with extensions.

### Changed

-   Changed `registerPlugin` registry API to `registerExtension`. The webpack plugin was updated too, so re-compile your extensions after upgrading.

## [0.7.0] - 2020-01-02

### Changed

-   Rename the Java runtime package `com.atlassian.plugins.client-plugins-runtime` → `com.atlassian.plugins.atlassian-clientside-extensions-runtime`
-   Canonical repository location changed to `https://bitbucket.org/atlassian/atlassian-clientside-extensions`

## [0.6.1] - 2019-12-24

### Fixed

-   Additional renaming tasks focused on public interfaces.

## [0.6.0] - 2019-12-23

This release is focused on changing the name of our APIs and packages and public release.

### Changed

#### Packages

-   All packages changed their name from `@atlassian/client-plugins-*` to `@atlassian/clientside-extensions-*`.
-   Registry AMD module is now called `@atlassian/clientside-extensions-registry`. Use this name when configuring the provided
    dependencies while using WRM webpack plugin.

#### Client-side API

-   `LinkPlugin` → `LinkExtension`
-   `ButtonPlugin` → `ButtonExtension`
-   `PanelPlugin` → `PanelExtension`
-   `ModalPlugin` → `ModalExtension`
-   Method to create a factory is also renamed for all extensions: `.pluginFactory` → `.factory`

#### Client-side components API

-   `usePluginsAll` → `useExtensionsAll`
-   `usePlugins` → `useExtensions`
-   `usePluginsUnsupported` → `useExtensionsUnsupported`
-   `usePluginsLoadingState` → `useExtensionsLoadingState`
-   `PluginLocation` → `ExtensionPoint`
-   `PluginLocationInfo` → `ExtensionPointInfo`
-   `ModalRenderPlugin` → `ModalRenderExtension`
-   `PanelRenderPlugin` → `PanelRenderExtension`

#### Client-side webpack plugin annotations

-   `@client-plugin` → `@clientside-extension`
-   `@location` → `@extension-point`

### Fixed

-   Removed `jest` from registry peer dependencies.
-   Validation now is activated on `validation` instead of `debug` flag is on.

## [0.5.0] - 2019-12-13

### Added

-   Debug flags to activate Debugging, Logging, Validation and Discovery of locations.
-   Hooks to get/set new debug Flags.
-   `__withoutTransition` option for ModalRenderer to get a Modal component without Transition. Usefull when you need to control the container of the Modal.
    (e.j: rendering a modal when a dropdown item is clicked).

### Changed

-   Validation now uses AJV to validate schemas. This mean we fully support JSON Schemas.
-   Modals' `setAction` now takes an array of actions that allows to define the click handler, loading and disabled state.

### Removed

-   `?client-plugins` query param is removed from the plugin system. Products should now provide this functionality with their
    routing solutions using the provided hooks to set the discovery flag.
-   Modal's `onAction`, `onPrimaryAction` and `onSecondaryAction` have been removed in favor of new `setActions` API,

## [0.4.1] - 2019-11-20

### Changed

-   Fix wrong export of `onDebug` utility.

## [0.4.0] - 2019-11-19

### Added

-   Validation for Attribute Schemas.
-   New hooks `usePlugins`, `usePluginsAll`, `usePluginsLoadingState`, `usePluginsUnsupported`.
-   Added `onModalAction` to ModalAPI. Useful when a modal has more than two actions.
-   Webpack plugin now supports setting server `conditions`.
-   Added `onCleanup` cycle to PluginAPI that will be executed when destroying the plugin

### Changed

-   Since Client-side Extensions no longer depends on `entry-point` element for `web-items`, the minimal version of Webfragments artifact
    has been changed to `5.0` instead of `5.1`. Entrypoint element will be deleted in a later Webfragments version.
-   Plugin Factory is now a single function that takes both the `pluginAPI` and `context`, instead of being a closure. Check
    the documentation to familiarise yourself with the new signature.
-   `requestRender` API has been changed to `updateAttributes` API, and now it takes the set of attributes you want to change
    instead of triggering an execution of your attributes provider. Check the documentation to familiarise yourself with the new signature.
-   `usePlugins` has been renamed to `usePluginsAll`. A new hook `usePlugins` now returns only the list of plugins with a type
    that the location supports.

## [0.3.0] - 2019-10-31

### Added

-   Introduced Plugin types.
-   Introduced ClientPlugins Webpack plugin.
-   New Documentation to include Plugin types examples.
-   Chore: Added watchmode to all our packages and demo.
-   Chore: Added bitbucket pipelines to check our build/lint before merging.

### Changed

-   Plugin resources are loaded by Context instead of Resource key.
-   `registry.registerPlugin` now takes Attribute Factories instead of `ClientPlugins` classes. If you need to
    create an equivalent of a WebPanel, use a Panel plugin instead.
-   `PluginPointInfo` component is renamed to `PluginLocationInfo`.
-   The query param to activate the PluginLocationInfo changed from `?plugin-points=true` to `client-plugins-true`.
-   Major refactore of our code base to remove any reference to WebItem/WebPanel. Now everything is a Plugin.

### Removed

-   `<web-entry-point>` custom module was removed. Use a regular `<web-resource>` instead.
-   `registry.registerWebItemCallback` method was removed, and you should now use `registry.registerPlugin` instead.

### Deprecated

-   `<entry-point>` element for WebItemModule is no longer supported, and will be removed in a future version of [Webfragments](https://bitbucket.org/atlassian/atlassian-plugins-webfragment/src)
