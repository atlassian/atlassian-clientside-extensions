package com.atlassian.clientpluginsdemorefapp;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.clientsideextensions.ExtensionDataProvider;

/**
 * An example of a more complex DataProvider class that returns a map of values that we convert to JSON format.
 * We use web-resource data provider to push server-side data to the "PageExtension" JS API by using @page-data-provider annotation.
 *
 * Refer to the documentation:
 * https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/extension-factories/page/#using-data-provider
 */
public class MyComplexDataProvider implements ExtensionDataProvider {
    @Override
    public Jsonable get() {
        return new Jsonable() {
            @Override
            public void write(Writer writer) throws IOException {
                Gson gson = new Gson();
                gson.toJson(sampleData(), Map.class, new PrintWriter(writer));
            }
        };
    }

    public Map<String, Object> sampleData() {
        final Map<String, Object> sampleData = new HashMap<>();
        sampleData.put("name", "John");
        sampleData.put("surname", "Doe");
        sampleData.put("isAdmin", true);

        return sampleData;
    }
}
