package com.atlassian.clientpluginsdemorefapp;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.model.WebLabel;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import static java.util.Objects.requireNonNull;

public class WebItemsToCSEMigrationExample extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html;charset=UTF-8";
    private static final String RESOURCE_KEY =
            "com.atlassian.plugins.atlassian-clientside-extensions-demo:web-items-to-cse-migration-templates";
    private static final String TEMPLATE_KEY = "W2CSEExample.page";
    private static final String WEB_ITEM_LOCATION_A = "web-item.to.cse.location.a";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final DynamicWebInterfaceManager webInterfaceManager;
    private final I18nResolver i18nResolver;

    public WebItemsToCSEMigrationExample(
            final SoyTemplateRenderer soyTemplateRenderer,
            DynamicWebInterfaceManager webInterfaceManager,
            I18nResolver i18nResolver) {
        this.soyTemplateRenderer = requireNonNull(soyTemplateRenderer);
        this.webInterfaceManager = webInterfaceManager;
        this.i18nResolver = i18nResolver;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        render(response, request);
    }

    private String getLabel(WebPanelModuleDescriptor panel, HttpServletRequest request, Map<String, Object> context) {
        try {
            WebLabel webLabel = panel.getWebLabel();
            if (webLabel != null) {
                return webLabel.getDisplayableLabel(request, context);
            }
        } catch (Throwable t) {
            // ignore the exception and use fallbacks below.
        }

        if (panel.getI18nNameKey() != null) {
            return i18nResolver.getText(panel.getI18nNameKey());
        } else {
            return panel.getKey();
        }
    }

    private void render(ServletResponse response, ServletRequest request) throws IOException, ServletException {
        response.setContentType(CONTENT_TYPE);

        final ImmutableMap.Builder<String, Object> context = ImmutableMap.builder();
        context.put("request", request);
        ImmutableMap<String, Object> buildContext = context.build();

        Iterable<WebItem> displayableWebItems =
                webInterfaceManager.getDisplayableWebItems(WEB_ITEM_LOCATION_A, buildContext);

        List<ImmutableMap<String, Object>> collect =
                webInterfaceManager.getDisplayableWebPanelDescriptors("atl.refapp.index", buildContext).stream()
                        .map(webPanelModuleDescriptor -> {
                            WebPanelModuleDescriptor descriptor = (WebPanelModuleDescriptor) webPanelModuleDescriptor;
                            final ImmutableMap.Builder<String, Object> webPanel = ImmutableMap.builder();
                            webPanel.put("html", descriptor.getModule().getHtml(buildContext));
                            webPanel.put("completeKey", webPanelModuleDescriptor.getCompleteKey());
                            webPanel.put("key", webPanelModuleDescriptor.getKey());
                            webPanel.put("label", getLabel(descriptor, (HttpServletRequest) request, buildContext));
                            webPanel.put("weight", webPanelModuleDescriptor.getWeight());
                            return webPanel.build();
                        })
                        .collect(Collectors.toList());

        final ImmutableMap.Builder<String, Object> params = ImmutableMap.builder();
        params.put("webItemsA", ImmutableList.copyOf(displayableWebItems));
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonWebItems = ow.writeValueAsString(ImmutableList.copyOf(displayableWebItems));
        params.put("webItemsJson", jsonWebItems);
        String jsonWebPanels = ow.writeValueAsString(ImmutableList.copyOf(collect));
        params.put("webPanelsJson", jsonWebPanels);

        try {
            soyTemplateRenderer.render(response.getWriter(), RESOURCE_KEY, TEMPLATE_KEY, params.build());
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }
}
