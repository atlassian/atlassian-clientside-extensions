package com.atlassian.clientpluginsdemorefapp;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.plugin.clientsideextensions.ExtensionDataProvider;

/**
 * A simple DataProvider class that returns a string as JSON.
 * We use web-resource data provider to push server-side data to the "PageExtension" JS API by using @page-data-provider annotation.
 *
 * Refer to the documentation:
 * https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/extension-factories/page/#using-data-provider
 */
public class MyDataProvider implements ExtensionDataProvider {
    @Override
    public Jsonable get() {
        return new JsonableString("Server-side hello world!");
    }
}
