package com.atlassian.clientpluginsdemorefapp;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.web.Condition;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

public class RandomCondition implements Condition, UrlReadingCondition {

    private static final Logger log = LoggerFactory.getLogger(RandomCondition.class);

    // Implements both com.atlassian.plugin.web.Condition and
    // com.atlassian.webresource.spi.condition.UrlReadingCondition
    @Override
    public void init(Map<String, String> map) {}

    // Implements com.atlassian.webresource.spi.condition.UrlReadingCondition
    @Override
    public void addToUrl(UrlBuilder urlBuilder) {}

    @Override
    public boolean shouldDisplay(QueryParams params) {
        return shouldDisplay((Map<String, Object>) null);
    }

    // Implements com.atlassian.plugin.web.Condition
    @Override
    public boolean shouldDisplay(Map<String, Object> map) {
        // this is being called multiple times - and should ideally return the same always. Which cant be guaranteed but
        // is pretty close...
        final long currentSeconds = System.currentTimeMillis() / 1000;
        final boolean shouldDisplay = currentSeconds % 2 == 0;
        log.warn("The current second is: {}, 'shouldDisplay' is therefore {}", currentSeconds, shouldDisplay);
        return shouldDisplay;
    }
}
