package com.atlassian.clientpluginsdemorefapp;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.servlet.PluginHttpRequestWrapper;
import com.atlassian.plugin.web.Condition;

public class MagicHiddenPageCondition implements Condition {
    @Override
    public void init(Map<String, String> params) throws PluginParseException {}

    @Override
    public boolean shouldDisplay(Map<String, Object> context) {
        boolean shouldDisplay = false;

        if (context.containsKey("request")) {
            PluginHttpRequestWrapper request = (PluginHttpRequestWrapper) context.get("request");
            String query = request.getQueryString();

            shouldDisplay = query != null && query.equals("__MAGIC_STRING_FOR_DISPLAYING_HIDDEN_PAGE__");
        }

        return shouldDisplay;
    }
}
