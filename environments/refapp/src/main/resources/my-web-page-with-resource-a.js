/* eslint-env amd */
/* eslint-disable import/no-dynamic-require, import/no-amd, node/no-missing-require */
require(['@atlassian/clientside-extensions-page-bootstrapper', 'cse-web-page-demo-module-b', 'cse-web-page-demo-module-c'], function module(
    pageBootstrapper,
    moduleB,
    moduleC,
) {
    function pageFactory() {
        return {
            type: 'page',
            onAction(node) {
                node.innerHTML = `<h2>Page with multiple web-resources</h2>
                <ul>
                    <li>${moduleB}</li>
                    <li>${moduleC}</li>
                </ul>`;
            },
        };
    }

    pageBootstrapper.registerPage(
        'com.atlassian.plugins.atlassian-clientside-extensions-demo:myWebPageWithMultipleWebResources',
        pageFactory,
    );
});
