/* eslint-disable*/
(function () {
    function getModalContent(context) {
        var content = `
            <h4>Cool content</h4>
            <p>This is some cool content generated with JS</p>
        `;

        content += context && context.secondaryClicked ? '<p>secondary clicked' : '';

        return content;
    }

    function modalPluginFactory(pluginAPI, context) {
        return {
            type: 'modal',
            label: 'Modal with JS',
            onAction: function onAction(modalAPI) {
                modalAPI
                    .setTitle('A cool modal with JS')
                    .setWidth('jumbo')
                    .setAppearance('shiny')
                    .onMount(function (container) {
                        // append your content for the modal
                        container.innerHTML = getModalContent();

                        // listen to actions in the modal

                        modalAPI.setActions([
                            {
                                text: 'Primary',
                                onClick: function () {
                                    console.log('modal-plugin.js: onPrimaryAction');
                                    // close the modal :)
                                    modalAPI.closeModal();
                                },
                            },
                            {
                                text: 'Secondary',
                                onClick: function () {
                                    console.log('modal-plugin.js: onSecondaryAction');
                                    container.innerHTML = getModalContent({ secondaryClicked: true });
                                },
                            },
                        ]);

                        modalAPI.onClose(function () {
                            return confirm('Are you sure you want to close this modal?');
                        });
                    });
            },
        };
    }

    require(['@atlassian/clientside-extensions-registry'], function (registry) {
        registry.registerExtension('com.atlassian.plugins.atlassian-clientside-extensions-demo:modal-plugin-js', modalPluginFactory, {
            location: 'reff.old-web-items-location',
            label: 'loading…',
            weight: 200,
        });
    });
})();
