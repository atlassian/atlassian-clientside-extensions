/* eslint-env amd */
/* eslint-disable import/no-dynamic-require, import/no-amd, node/no-missing-require */
require(['@atlassian/clientside-extensions-page-bootstrapper'], function demoWebPageExtension(pageBootstrapper) {
    function pageFactory() {
        return {
            type: 'page',
            onAction(node) {
                node.innerHTML = 'Hi there!';
            },
        };
    }

    pageBootstrapper.registerPage('com.atlassian.plugins.atlassian-clientside-extensions-demo:myExtensionKey', pageFactory);
});
