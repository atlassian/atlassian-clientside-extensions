/* eslint-disable*/
(function () {
    function linkPluginFactory() {
        return {
            type: 'link',
            label: 'Go to Google',
            url: 'https://www.google.com',
        };
    }

    require(['@atlassian/clientside-extensions-registry'], function (registry) {
        registry.registerExtension('com.atlassian.plugins.atlassian-clientside-extensions-demo:link-plugin-js', linkPluginFactory, {
            location: 'reff.old-web-items-location',
            label: 'loading…',
            weight: 300,
        });
    });
})();
