if (module.hot) {
    // eslint-disable-next-line global-require
    const registry = require('@atlassian/clientside-extensions-registry').default;
    window.define('@atlassian/clientside-extensions-registry', [], registry);

    // Force dev mode and set debug log level sine Refapp will provide own version of CSE runtime
    // that might be incompatible with the local CSE version.
    /* eslint-disable no-underscore-dangle */
    window.____c_p_d = true;
    window.____c_p_d.logLevel = 'DEBUG';
    /* eslint-enable no-underscore-dangle */
}
