import * as React from 'react';
import { render } from 'react-dom';
import { PageExtension } from '@atlassian/clientside-extensions';

const HiddenPage: React.FC = () => (
    <>
        <h1>A hidden page</h1>
    </>
);

/**
 * @clientside-extension
 * @extension-point header.links
 * @label "Hidden Page"
 * @page-url /my-hidden-page
 * @page-title "Hidden Page"
 * @condition class com.atlassian.clientpluginsdemorefapp.MagicHiddenPageCondition
 * @weight 6
 */
export default PageExtension.factory((container) => {
    render(<HiddenPage />, container);
});
