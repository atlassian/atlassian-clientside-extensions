import * as React from 'react';
import { render } from 'react-dom';
import { PageExtension } from '@atlassian/clientside-extensions';

interface DataPayload {
    name: string;
    surname: string;
    isAdmin: boolean;
}

interface Props {
    data: DataPayload;
}

const MyAdminApp: React.FC<Props> = ({ data }) => (
    <>
        <h1>Custom React Admin Panel</h1>
        <p>This app is rendered by React</p>
        <h2>Data provided from server</h2>
        <pre>{JSON.stringify(data, null, '\t')}</pre>
    </>
);

/**
 * @clientside-extension
 *
 * @extension-point system.admin/general
 * @label "My Admin Feature"
 * @page-url /my-custom-admin-page
 * @page-decorator atl.admin
 * @page-title my.custom.admin.page
 * @page-data-provider com.atlassian.clientpluginsdemorefapp.MyComplexDataProvider
 */
export default PageExtension.factory<DataPayload>((container, data) => {
    render(<MyAdminApp data={data} />, container);
});
