import * as React from 'react';
import { render } from 'react-dom';
import { PageExtension } from '@atlassian/clientside-extensions';

type DataPayload = string;

interface Props {
    data: DataPayload;
}

const MyApp: React.FC<Props> = ({ data }) => (
    <>
        <h1>Custom React Page</h1>
        <p>This app is rendered by React</p>
        <h2>Data provided from server</h2>
        <p>{data}</p>
    </>
);

/**
 * @clientside-extension
 * @extension-point header.links
 * @label "My Custom Page"
 * @page-url /my-custom-page
 * @page-title "Custom Page"
 * @weight 5
 * @page-data-provider com.atlassian.clientpluginsdemorefapp.MyDataProvider
 */
export default PageExtension.factory((container, data: DataPayload) => {
    render(<MyApp data={data} />, container);
});
