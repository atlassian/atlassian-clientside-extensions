package it.com.atlassian.clientpluginsdemorefapp;

import java.io.IOException;
import java.util.Collection;
import java.util.TreeMap;
import java.util.function.Function;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.atlassian.clientpluginsdemorefapp.rest.BackdoorResource.ModuleDto;
import com.atlassian.plugin.clientsideextensions.moduletype.WebPageModuleDescriptor;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;

/**
 * Integration test of the {@code web-page} module.
 *
 * To run from IDEA, please see {@code atlassian-clientside-extensions-demo-plugin/README.md}.
 */
public class WebPageModuleTest {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final String BOOTSTRAPPER_PLUGIN_KEY =
            "com.atlassian.plugins.atlassian-clientside-extensions-page-bootstrapper";

    private static final String DEMO_PLUGIN_KEY = "com.atlassian.plugins.atlassian-clientside-extensions-demo";

    // AMPS sets this when the tests are run via Maven; this default is for ease of running from IDEA
    private static final String PRODUCT_BASE_URL = System.getProperty("baseurl", "http://localhost:5990/refapp");

    private static final String BACKDOOR_BASE_URL = "/rest/cse-demo/1/backdoor";

    private static class ListOfStringByPluginKey extends TreeMap<String, Collection<String>> {}

    private static <T> T getViaBackdoor(final String relativeUrl, final Class<T> objectType) {
        return get(BACKDOOR_BASE_URL + relativeUrl, responseEntity -> {
            try {
                return OBJECT_MAPPER.readValue(responseEntity.getContent(), objectType);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        });
    }

    /**
     * Checks that the bootstrapper plugin correctly provides the {@code web-page} module type.
     */
    @Test
    public void moduleTypes_whenRetrieved_shouldIncludeWebPageModuleType() {
        final ListOfStringByPluginKey moduleTypesByPlugin =
                getViaBackdoor("/module-types", ListOfStringByPluginKey.class);
        assertThat(
                moduleTypesByPlugin,
                hasEntry(BOOTSTRAPPER_PLUGIN_KEY, singletonList(WebPageModuleDescriptor.MODULE_TYPE)));
    }

    /**
     * Checks that the demo plugin correctly registers the {@code web-page} module defined in its plugin XML.
     */
    @Test
    public void servletModule_whenRetrieved_shouldHaveValuesDefinedInPluginXml() {
        final String moduleCompleteKey = DEMO_PLUGIN_KEY + ":" + "demoWebPage";
        final ModuleDto servletModule = getViaBackdoor("/module/" + moduleCompleteKey, ModuleDto.class);
        assertThat(servletModule.getDescription(), is("Demonstrates how to use the web-page module type."));
        assertThat(servletModule.getKey(), is("demoWebPage"));
        assertThat(servletModule.getName(), is("Demo Web Page"));
        assertThat(servletModule.getPluginKey(), is(DEMO_PLUGIN_KEY));
    }

    /**
     * Checks that our web-page actually exists, i.e. the servlet is correctly mapped and generates the expected content.
     */
    @Test
    public void webPage_whenRetrievedFromFirstUrl_shouldHaveExpectedContent() {
        assertDemoWebPage("/demo-web-page");
    }

    @Test
    public void webPage_whenRetrievedFromSecondUrl_shouldHaveExpectedContent() {
        assertDemoWebPage("/demo-web-page-alt");
    }

    private static void assertDemoWebPage(final String relativeUrl) {
        // Invoke
        final String html = getDemoWebPage(relativeUrl);

        // Check that the page reflects the values in the <web-page> element in our plugin XML
        // Scraping the HTML like this is simpler and faster than using WebDriver with XPath expressions
        assertThat(html, containsString("<meta name=\"decorator\" content=\"atl.general\">"));
        assertThat(html, containsString("<title>My Translated Page Title</title>")); // from i18n.properties
        assertThat(html, containsString("const extensionKey = \"" + DEMO_PLUGIN_KEY + ":myExtensionKey\";"));
    }

    private static String getDemoWebPage(final String relativeUrl) {
        return get("/plugins/servlet" + relativeUrl, responseEntity -> {
            try {
                return IOUtils.toString(responseEntity.getContent(), UTF_8);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        });
    }

    /**
     * Gets the response body from the given URL, applying the given mapping function to it before the response is closed.
     *
     * @param relativeUrl the URL to get, relative to the product's base URL
     * @param entityMapper the mapper to apply to the response entity
     * @param <R> the return type of the mapper
     * @return the mapped value
     */
    private static <R> R get(final String relativeUrl, final Function<HttpEntity, R> entityMapper) {
        final HttpUriRequest httpRequest = new HttpGet(PRODUCT_BASE_URL + relativeUrl);
        try (final CloseableHttpClient httpClient = HttpClients.createDefault();
                final CloseableHttpResponse httpResponse = httpClient.execute(httpRequest)) {
            assertThat(httpResponse.getStatusLine().getStatusCode(), is(HTTP_OK));
            return entityMapper.apply(httpResponse.getEntity());
        } catch (final IOException e) {
            throw new AssertionError(e);
        }
    }
}
