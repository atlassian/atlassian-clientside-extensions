/* eslint-disable-next-line spaced-comment */
/// <reference types="cypress" />

describe('Refapp AsyncPanel Component', () => {
    beforeEach(() => {
        cy.visit('/');

        // Wait for the page to be loaded and extension to be initialized so we can navigate else-where
        cy.contains('Client-side Extensions');
    });

    it('should render async panels', () => {
        cy.contains('7. Async Panels with context').click();
        cy.get('[role="tablist"] [role="tab"]').should(($tabs) => {
            expect($tabs).to.have.length(2);

            expect($tabs.get(0)).to.contain('Async Tab 1 with Context');
            expect($tabs.get(1)).to.contain('Async Tab 2 with Context');
        });

        cy.get('[role="tabpanel"]').should('exist');

        // Wait for the initial number to be visible
        cy.get('[role="tabpanel"]')
            .find('[data-testid="counter"]')
            .should(($counter) => {
                expect($counter).to.text('0');
            });

        // Wait for the next number to be visible
        cy.clock();
        cy.tick(1000);

        cy.get('[role="tabpanel"]')
            .find('[data-testid="counter"]')
            .should(($counter) => {
                expect($counter).to.text('1');
            });

        // Wait for the next number to be visible
        cy.clock();
        cy.tick(1000);

        cy.get('[role="tabpanel"]')
            .find('[data-testid="counter"]')
            .should(($counter) => {
                expect($counter).to.text('2');
            });
    });
});
