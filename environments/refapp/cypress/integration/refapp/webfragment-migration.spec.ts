/* eslint-disable-next-line spaced-comment */

describe('Webfragment migration to CSE', () => {
    const expectedWebItems = ['Web item C', 'Web item A', 'Web item E', 'Web item D', 'Web item B'];

    beforeEach(() => {
        cy.visit('/plugins/servlet/web-items-as-cse');

        // Wait for the page to be loaded and extension to be initialized so we can navigate else-where
        cy.contains('Hello there from the Web-Fragments migration page!');
    });

    describe('Web Items', () => {
        it('Render Web-items onto page', () => {
            expectedWebItems.forEach((item) => {
                cy.contains(item).should('be.visible');
            });
        });

        it('Render Web-items into the dropdown', () => {
            cy.findByText('Dropdown trigger').click();
            cy.get('#dwarfers a').should('have.length', 5);
            cy.get('#dwarfers a').each((item, index) => {
                cy.wrap(item).contains(expectedWebItems[index]).should('be.visible');
            });
            cy.findByText('Dropdown trigger').click();
        });

        it('Add dynamically generated Clientside-Extensions to the web item location.', () => {
            cy.findByTestId('base-web-items').children().should('have.length', 5);
            cy.findByText('Add a "link"-type clientside extension').click();
            cy.findByTestId('base-web-items').children().should('have.length', 6);
        });

        it('Correctly set up onAction-handlers for clientside-extensions', () => {
            cy.findByText('Add a "link"-type clientside extension').click();
            cy.findByTestId('base-web-items').children().should('have.length', 6);
            cy.findByTestId('base-web-items').children().last().click();
            cy.findByTestId('base-web-items')
                .children()
                .last()
                .invoke('text')
                .then((text) => {
                    cy.contains(`You clicked: ${text}`);
                });
        });
    });

    describe('Web Panels', () => {
        it('Correctly renders Web Panels', () => {
            cy.findByText('I am a WebPanel with an inline script').should('be.visible');
            cy.findByText('I also am a WebPanel with an inline script').should('be.visible');
        });

        it('Correctly executes inline scripts of Web Panels', (done) => {
            cy.get('#webpanel-with-script-container').should('be.visible');
            cy.get('#webpanel-with-script-container').children().should('have.length', 1);

            cy.get('.random-number-container').should('be.visible');
            cy.get('.random-number-container')
                .invoke('text')
                .then((text) => {
                    // wait for time to update
                    // eslint-disable-next-line cypress/no-unnecessary-waiting
                    cy.wait(2000).then(() => {
                        cy.get('.random-number-container')
                            .invoke('text')
                            .then((text2) => {
                                expect(text).not.to.equal(text2);
                                done();
                            });
                    });
                });
        });

        it('Add dynamically generated Clientside-Extensions to the web panel location.', () => {
            cy.findByTestId('base-web-panel').children().should('have.length', 4);
            cy.findByText('Add a "panel"-type clientside extension').click();
            cy.findByTestId('base-web-panel').children().should('have.length', 5);
            cy.findByText('Add a "panel"-type clientside extension').click();
            cy.findByTestId('base-web-panel').children().should('have.length', 6);
        });

        it('Adding more panels will not reevaluate inline scripts of Web Panels again', () => {
            // we demonstrate this by showing that there is still only 1 red circle on the page.
            cy.get('#webpanel-with-script-container').should('be.visible');
            cy.get('#webpanel-with-script-container').children().should('have.length', 1);

            // add 3 new panels
            cy.findByText('Add a "panel"-type clientside extension').click();
            cy.findByText('Add a "panel"-type clientside extension').click();
            cy.findByText('Add a "panel"-type clientside extension').click();
            cy.findByTestId('base-web-panel').children().should('have.length', 7);

            // still the same amount available:
            cy.get('#webpanel-with-script-container').should('be.visible');
            cy.get('#webpanel-with-script-container').children().should('have.length', 1);
        });

        it('Executes scripts in the correct order', () => {
            // we demonstrate this by showing that there is still only 1 red circle on the page.
            cy.get('.square-container > div').should('be.visible');
            cy.get('.square-container > div').then((el) => {
                expect(el).to.have.css('background-color', 'rgb(0, 128, 0)');
                expect(el.text()).to.match(/^\d\d:\d\d:\d\d$/);
            });
        });
    });
});
