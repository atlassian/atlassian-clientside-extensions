/* eslint-disable-next-line spaced-comment */
/// <reference types="cypress" />

describe('Refapp PageExtension', () => {
    beforeEach(() => {
        cy.visit('/');

        // Wait for the page to be loaded and extension to be initialized so we can navigate else-where
        cy.contains('Client-side Extensions');
    });

    afterEach(() => {
        cy.visit('/plugins/servlet/logout');
    });

    it('should render a custom page link', () => {
        cy.contains('My Custom Page').should(($link) => {
            expect($link).to.have.attr('href', '/refapp/plugins/servlet/my-custom-page');
        });
    });

    it('should navigate to a custom page', () => {
        cy.visit('/plugins/servlet/my-custom-page');

        // Has a visible navigation link
        cy.contains('My Custom Page');

        // Renders a page content
        cy.contains('Custom React Page');
    });

    it('should receive a server-side data from the page data provider', () => {
        // navigate to the custom page
        cy.contains('My Custom Page').click();

        // Assert server-side data
        cy.contains('Server-side hello world!');
    });

    it('should render a custom admin page in the admin panel', () => {
        // TODO: Non-DRY approach since there are some issues with defining a custom Cypress commands using TypeScript
        cy.visit('/plugins/servlet/login');
        cy.get('#os_username').type('admin');
        cy.get('#os_password').type('admin');
        cy.get('#os_login').click();

        // Has a visible navigation link
        cy.contains('My Admin Feature').should(($link) => {
            expect($link).to.have.attr('href', '/refapp/plugins/servlet/my-custom-admin-page');
        });
    });

    it('should navigate to a custom admin page', () => {
        cy.visit('/plugins/servlet/login');
        cy.get('#os_username').type('admin');
        cy.get('#os_password').type('admin');
        cy.get('#os_login').click();

        cy.visit('/plugins/servlet/my-custom-admin-page');

        // Has a visible navigation link
        cy.contains('My Admin Feature');

        // Renders a page content
        cy.contains('Custom React Admin Panel');
    });

    it('should render complex server-side data', () => {
        cy.visit('/plugins/servlet/login');
        cy.get('#os_username').type('admin');
        cy.get('#os_password').type('admin');
        cy.get('#os_login').click();

        cy.contains('My Admin Feature').click();

        // Renders a page content
        cy.contains('Custom React Admin Panel');

        cy.contains('"name": "John"');
        cy.contains('"surname": "Doe"');
        cy.contains('"isAdmin": true');
    });

    it('should not render a navigation link when page is hidden by a condition', () => {
        // should display page
        cy.visit('/?__MAGIC_STRING_FOR_DISPLAYING_HIDDEN_PAGE__');
        cy.contains('Hidden Page');

        // should hide the page
        cy.visit('/', { timeout: 5 * 1000 });
        cy.contains('Hidden Page').should('not.exist');
    });

    it('should allow to navigate to a page hidden by a condition', () => {
        cy.visit('/plugins/servlet/my-hidden-page');

        cy.contains('A hidden page');
    });

    it('should load all the resources defined by web-page module descriptor', () => {
        cy.visit('/plugins/servlet/demo-web-page-with-multiple-web-resources');

        cy.contains('Page with multiple web-resources');
        cy.contains('Module B');
        cy.contains('Module C');
    });
});
