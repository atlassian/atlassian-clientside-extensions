/* eslint-disable-next-line spaced-comment */

describe('CSE-124', () => {
    beforeEach(() => {
        cy.visit('/#/8-kitchen-sink');

        // Wait for the page to be loaded and extension to be initialized so we can navigate else-where
        cy.contains('Client-side Extensions');
        cy.contains('Discovery');
        cy.get('#Discovery').check();
    });

    it('ExtensionInfo should not submit forms when clicked', () => {
        cy.contains('Just a form').scrollIntoView();
        cy.contains('Form has NOT been submitted').should('be.visible');
        cy.get('form.form-with-extension .discover-bubble button')
            .click()
            .then(() => {
                cy.contains('Extension point: discover.submit.test').should('be.visible');
                cy.contains('Form has NOT been submitted').should('be.visible');
                cy.get('button')
                    .contains('Close')
                    .click()
                    .then(() => {
                        cy.get('form.form-with-extension .submitter')
                            .click()
                            .then(() => {
                                cy.contains('Form has NOT been submitted').should('not.exist');
                                cy.contains('Form has been submitted').should('be.visible');
                            });
                    });
            });
    });
});
