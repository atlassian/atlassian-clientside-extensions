# Atlassian Client-side Extensions - Demo Refapp

-   Demonstrates how product developers and plugin developers can make use of the system's APIs.
-   Proves that the `atlassian-clientside-extensions-page-bootstrapper` plugin correctly provides the `web-page` plugin point.

## Running Demo

In monorepo root folder, run:

1. `mvn clean install` to build the whole project + demo.
2. `yarn demo/refapp` to start the demo.

## Consideration with this Demo

Since we're trying to create locations to render plugins and web-items using an Atlassian Plugin, and not directly modifying
the product (Reffapp in this case), we're using a regular WebPanel and a few JS resources to serve as the example of
how a product should implement the API.

`/resources/`

-   `atlassian-plugin.xml`: regular Altassian Plugin declaration. Here's the declaration of the WebPanel used for Demoing.
-   `web-panel.vm`: WebPanel's template, where the `#myPlugin` and `#myWebItems` divs are declared for demoing.

`/frontend``

-   `consumer`: demonstrate how Plugin Devs should define their plugin entrypoints and how to use the attributes API.
    This is used in combination with `atlassian-plugin.xml` where the web-items are defined.
-   `provider`: demonstrate how Products should provide plugin locations and how to use the default handlers

## Features

### Sample web page

This plugin has a `<web-page>` element in its `atlassian-plugin.xml` file; this causes the plugin system to:

-   create an instance of the `ExtensionPointServlet` class (which is provided by the bootstrapper plugin), and
-   map that servlet to the URL `http://localhost:5990/refapp/plugins/servlet/demo-web-page`.

### Backdoor

This plugin provides a REST endpoint that acts as a backdoor for inspecting the relevant parts of the plugin system:

-   `/backdoor/module-types` lists all the module types being provided by plugins, grouped by plugin. In this list, if the bootstrapper
    plugin is working properly, you should see that the `com.atlassian.plugins.atlassian-clientside-extensions-page-bootstrapper` plugin
    provides a module type called `web-page`.
-   `/backdoor/servlet-modules` lists the keys of all servlet modules registered with the plugin system, again grouped by plugin.
    If the demo plugin and the bootstrap plugin are working properly, you should see a servlet called `demoWebPage` listed
    under the `com.atlassian.plugins.atlassian-clientside-extensions-demo-plugin`.

## Running with no frontend build step

1. Make sure the project is freshly built by running `mvn clean install -DskipTests` from the root.
1. `cd environments/refapp`.
1. `mvn refapp:run`, or if you want to debug something, `mvn refapp:debug -Djvm.debug.suspend`.
1. Once refapp is up, you can:
    - hit one of the above backdoor endpoints to inspect the plugin system,
    - go to `http://localhost:5990/refapp/plugins/servlet/demo-web-page` to view the demo page (blank with a custom title), or
    - run the `WebPageModuleTest` from IDEA.
